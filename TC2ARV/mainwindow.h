// lambda open in TC: use sRegKey as arg, not an entry
// then it's ok to allow it during the run

// inside burialAtSea(), close the db

// the trouble with tribbles: make room for duplicates to be saved as sponges
// i.e. allow > 1 visit per day (in the same vein as for companions)

// tag inte med "Yttre orsaker" flavored diagnoser i ARV-filen

/*--------------------------------------------------------------------------*/
/* mainwindow.h                                                             */
/*                                                                          */
/* 2021-03-23 First version                                             HS  */
/* 2023-09-15 Support LOL companion ATG code (a single one for now)     HS  */
/*--------------------------------------------------------------------------*/

#include <QMainWindow>

// pull in the Tungware libraries
#include INCLUDETWUTILS
#include INCLUDETWDB
#include INCLUDETWTCSUPPORT

// need these for the readonly() lambda and the background image
#include "qtablewidget.h"
#include "qlabel.h"

// where we are in the app
enum class AppState { eConnecting, eConnectedOk, eProcessing, eRubbernecking };

// animation time when sliding up the calendars
const int nnSlideAnimDuration = 444;

// list of pairs of filenames and captions ("none", "Ingen" must be the first 2 chaps)
const QStringList slBackgroundImages = {
"none", "Ingen", ":/snowfall.jpg", "Snöfall", ":/notwinter.jpg", "Ej vinter", ":/berg.jpg", "Berg", ":/moln.jpg", "Moln" };

// table widget stuff -------------------------------
enum  TableViewStates { eShowAll, eShowWarnings, eShowErrors, viewStateCount };
const QColor acVisibilitySliderColors[viewStateCount] = { "white", "yellow", "#ff6060"};

enum  TableColumns    { colMapKey /* width 0 for this column (i.e. hidden) */,
                        colDayOfTheWeek, colVisitDateTime, colPersonNo,
                        colATGCode,  colARVRateCode, colVisitType,
                        colARVReceiptOrError, colWarningOrDiagnoses,
                        colJumpToTC, colCount };

// column widths                      MKey  DW   DVT   Pno  ATG Rate  Typ  RNoOrErr Warn JumpTC
const int anColumnWidths[colCount] = { 00,  50,  210,  200,  95,  95,  110,    125,  510,  35 };

// more table widget stuff
static auto setTableWidgetItemRO   = [] (QTableWidgetItem* tw) { tw->setFlags(tw->flags() ^ Qt::ItemIsEditable); };
const QTime tNotFoundInPAS(23,59,00);     // for sorting the rows so entries missing in PAS end up last on each days

// ARV file leadin and leadout lines EDIFACT flavored (see https://en.wikipedia.org/wiki/EDIFACT )
const QString ssARVClientID   = "PD3A11"; // seems to the standard
const QString ssARVFirstLines = "UNA:+,? 'UNB+UNOC:2+00072321000016:30:K%1+00072321000016:30:S7010+%2:%3+%4++SLLPVR'\nUNH\n%4SLLPVR1  2  ZZ";
const QString ssARVLastLines  = "UNT\n%1%2\nUNZ+1+%2'";

// ARV file fixed constants
const QString ssIDType        = "P"; // "P" (Personnummer) or " " for Smittskydd visits
const QString ssSmittskydd    = "1"; // set "1" for ARV ratecode = 07 (Smittskydd)
const QString ssNoSmittskydd  = "0"; // else "0"
const int     nnBSKLineLength = 73;  // (mismatch in line length --> guru meditation)

// TC open patient click twinklebox delays (in milliseconds)
const int     nnShortTCDelay  = 222;
const int     nnLongTCDelay   = 555;

// age of consultrefs before issuing an expiry warning
// (note: just a warning, written to the file anyway)
const int     nnConsultRefsOkAgeInYears = 5;

// no. of months back before issuing a "too old" warning in the date range selection
const int     nnMaxMonthsBackForARV     = 6;  // yes half a year

// don't support date ranges that begin before this date
const QDate   ddEarliestVisit(2021,04,22);

// default start date for ConsultRefs requirement
const QDate   ddConsultRefsBegin(2021,01,01);

// use this rate code for companion visits when patient fee > 0
// (so that only one patient fee is registered/uploaded for a single day and patient)
const QString ssCompanionRateCode = "10";

// running an old/outdated version of TC2ARV? yes if .exe filename starts with this
const QString ssOldVersionPrefix  = "Gammal";

// ----------------------------------------------------
// ARV entry struct
struct ARVEntryStruct
{
    bool    bOk;             // if bOk is false then expect some Swedish/localized text in sError
    QString sError;          // if nonempty: has an error and it will not be written to the ARV file
    QString sWarning;        // if nonempty: warning (this visit will be written anyway, i.e. bOk is true)
    PID_t   nPID;            // XTEA encrypted personno (used in Intelligence instead of the real thing)
    int     nDocumentID;     // used for looking up care provider ids (when locum tenens is active)
    bool    bSpongeNoARV4U;  // visits that are not to be uploaded/written (can be ok or have an error)
    QDate   dVisit;          // visit date: either PAS date or journal date (the same for ok entries)
    QString sVisitDate;      // same as above but as an ISO printable version (YYYY-MM-DD) for convenience
    QTime   tVisit;          // time from PAS or 23:59:00 if that's missing (so they're sorted *after* normal ones)
    QString sVisitDateTime;  // printable representation (YYYY-MM-DD HH:MM) of the above dVisit + tVisit

// PAS stuff
    QString sReceiptNo;      // receiptno from TC: billing counter (nnn-nn) + 4 digits (1000...9999) + check digit)
    QString sVisitType;      // one digit '0' -- '9' (TC has letters also but they're not used for ARV)
    bool    bFirstVisit;     // set to true if VisitType == '0' (new visit), else false
    bool    bEmergencyVisit; // (in Intelligence db: EmergencyID: Akut = 1, Ej akut = 2)
    QString sRateCode;       // TC rate code: "01" -- "99" (2 digits)
    int     nFeePaid;        // 0 kr -- full fee (currently 250 kr)
    int     nARVReceiptNo;   // 5 or 8 digits (companion ATGCode if any + last digit of billingcounter + 4 digits)
    QString sARVRateCode;    // code adjusted for ARV: "01" -- "99" (2 digits)

// locum tenens/"vikarie" stuff (used when locum tenens/"vikarie" is active)
    QString sCareProvider;   // 2 digits CareProviderID from PAS_Billing + PAS AppointmentWithUserName (if found)

// patient stuff
    QString sPersonNo;       // no hyphens or spaces but can have ???? question marks for an incomplete no.

// consultref stuff
    int     nAppointmentID;  // (from PAS) for looking up ReferralDocumentID in Appointments
    QString sRefKombika;
    QDate   dRefIssued;

// the ATG code for this visit (they're listed in tcterms.h)
    QString sATGCode;        // LOLs: can be "ATR", "AE", "N" or an Snnn code
    bool    bIsCompanion;    // some LOLs are allowed a 2nd ARV Snnn code for the same visit day
    QString sATGCodeToolTip; // LOFs: can be "ATRS", "NI", "NP", "AEI", "AEG" or "AS1" .. "AS7"

// diagnose chap(s) for this visit
    QStringList slDiagnosis; // ARV accepts 0 - 99 of these for a single visit

// the key for sorting/finding this entry in our QMap
    QString sKey; // visitday as YYYYMMDD concatenated with the PID and an optional "C" (for a companion entry)
                  // (duplicate/error entries have timeofday and a random number added as a suffix)

// key creations: visit date into 8 digits (YYYYMMDD) and concatenate with the PID and an optional "C" for companions
    static QString createKey(QDate d, PID_t nPID, bool bCompanion)
    {
        return TWUtils::toYMD(d) + TWTCSupport::PID2String(nPID) + (bCompanion ? "C" : "");
    }

// for an error entry
    static QString createKeyForErrorEntries(QDate d, PID_t nPID, bool bCompanion)
    {
    // append # of milliseconds elapsed since midnight and a random number to the normal key
    // (this is used to disambiguate duplicate error entries occurring for the same PAS and/or CaseNotes)
        QString sDisambiguationTail = QString::number(QTime::currentTime().msecsSinceStartOfDay()) + QString::number(rand());
        return createKey(d,nPID,bCompanion) + sDisambiguationTail;
    }
};

// ----------------------------------------------------
namespace Ui { class MainWindow; }

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void closeEvent(QCloseEvent* event);

// db and the Samba crawlers
    TWAsyncDB    db;
    SambaCrawler scInitial;
    SambaCrawler scMain;

// settings double flavors (one r/o and one r/w)
    TWAppSettings roSettings     {TWAppSettings::eEmbedded};
    TWAppSettings appDataSettings{TWAppSettings::eAppData};

// use either R/O or R/W flavor for the db settings
    TWAppSettings::IniLocation eIniFlavorForDB;
    QString sIntelligenceSectionName; // for finding our R/W section

// customer stuff -------------------------------------
    QString          sCustomerName;
    eTCCustomerTypes eCustomerType; // either eLOLCustomerType (doctors) or eLOFCustomerType

    int     nCustomerNo;            // number starting with 1
    bool    bConsultRefs;           // if true: need referral kombika and referral date for all first visits
    bool    bAutoStart;             // if true: don't wait for the Go button to be clicked
    bool    bBatchMode;             // true if two valid dates are on the command line
    bool    bDiagnoses;             // if true: look for diagnoses in Intelligence's vRegistryCodes view
    bool    bCheckRandomDOBs;       // if true: check DOBS for some but not all of them (random selection)
    bool    bCheckAllDOBs;          // if true: check DOBs for all (PersonNo vs Y,M,D in Intelligence's vPatInfo view)
    int     nCareUnitID;
    int     nCompanyID;
    int     nCompanyCode;
    QString sKombika;               // require this kombika for a valid entry in PAS
    QString sHSAID;                 // (only verified with the current value in Codes_CareUnits at start)

    QString sSambaShareName;        // for error checking in Codes_Companies_v2
    QString sLocalDirectory;        // if nonempty: use this instead of connecting remotely to the Samba server

    QString sTaxType;               // usually "F" but can be "A"

// locum tenens ("vikarie") stuff ---------------------
    bool        bLocumTenensActive; // true: we have a locum tenens active ("vikarie")
    bool        bIsLocumTenens;     // true: this is the locum tenens ("vikarie")
    QString     sLocumTenensCode;   // locum tenens: a 5-digit code, non-locum tenens: 5 spaces
    QStringList slPASProcess;       // these are processed when stepping through PAS
    QStringList slPASSkipOver;      // these are ignored when stepping through PAS
    QStringList slIgnorePASMissing; // PAS errors for these can be ignored (store as error sponges)

// options galore -------------------------------------
    QDate   dConsultRefsBegin;      // date when starting to be active (default 2021-01-01)
    bool    bObfuscatePersonNo;     // if true: use personno. from testpatients only and don't create an ARV-file
    bool    bShowErrorSponges;      // if true show sponges with error(s) but hide the other sponges
    bool    bShowSponges;           // if true show *all* sponges regardless of error/warnings
    QString sTCPathOverride;        // if nonempty: use this path as first choice for launching TC
    QString sARVFilePathOverride;   // if nonempty: use this path as the directory for the ARV-file
    QString sBackgroundImage;       // if nonempty: the filename for a background image

// resize/refresh stuff -------------------------------
    QSize                szOriginal;           // initial/original mainwindow size
    QMap<QWidget*,QRect> mOriginalGeometries;  // map of the original/designed widget geometries (used when resizing)

// keyword TermID when searching for ATGs ("N", "ATR" etc. S-codes) in the journals
    int     nKeywordTermID;         // KeywordTermID to look for (either the LOL or the LOF flavor)

// icons we use
    QIcon   iconTC;
    QIcon   iconUploadOk;

// fonts we use
    QFont  fCompanionReceiptNo; // use a smaller font (80%) for showing companion ARV receipt no. in table widget

// helper string lists (used by some lambdas)
    QStringList slErrors;
    QStringList slWarnings;

// -- our big wheels ----------------------------------
    AppState                     appState;
    TableViewStates              tableViewState;
    QDate                        dFrom,dTo;
    QMap<QString,ARVEntryStruct> mArvEntries;
    bool                         bWaitingForSamba;
    MapPID2PersonNo_t            mPID2PersonNo;

// startup: connect to Samba and Intelligence
    QString connectToSambaAndIntelligence();

// our own override of the virtual resizeEvent()
    void virtual resizeEvent(QResizeEvent* event);

// more UI functions
    void setupUI();
    void setGoButtonCaption();
    void switchAppState(AppState newAppState);
    void refreshVisibilitySlider(TableViewStates e);
    void refreshTableWidget();
    void showProgress(QString sText,int nBarValue = 0);
    void restockSingleTableWidget(TableViewStates eState);   // 3 flavors
    void restockAllTableWidgets();

// fetching and saving
    void refreshPersonNo();
    void dbError(QString sError);
    void fetchARVData();
    void burialAtSea();
    void saveARVFile();

// Qt slots
private slots:
    void on_calendarWidgetFrom_clicked(const QDate &date);
    void on_calendarWidgetTo_clicked(const QDate &date);
    void on_pushButtonExit_clicked();
    void on_visibilitySlider_valueChanged(int value);
    void on_pushButtonGo_clicked();
    void on_settingsButton_clicked();

private:
    Ui::MainWindow* ui;
    QWidget* pSliderGrooveFrame;  // for the visibility slider's groove
    QLabel*  pLabelBackground;    // for holding the background image (if any)
};
