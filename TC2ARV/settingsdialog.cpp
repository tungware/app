/*--------------------------------------------------------------------------*/
/* settingsdialog.cpp                                                       */
/*                                                                          */
/* 2023-10-16 First version                                             HS  */
/*--------------------------------------------------------------------------*/

#include "settingsdialog.h"

SettingsDialog::SettingsDialog(QWidget *parent, bool bIsLocumTenens) : QDialog(parent), ui(new Ui::SettingsDialog)
{
    ui->setupUi(this);

// do the usual preamble
    TWUtils::helloQt(this);
    setWindowTitle("Inställningar");

// if this is a locum tenens, change the icon
    if (bIsLocumTenens)
        ui->toolButtonAbout->setIcon(QIcon(":/TC2ARVLocumTenens.ico"));

// fill our combo boxes with some choices
    QStringList slDateRanges = { " Varje dag", " Varje vecka", " Varannan vecka", " Varje månad", " Varannan månad" };
    for (auto s : slDateRanges)
        ui->comboBoxDateInterval->addItem(s);

    QStringList slDBAndPrefixes = { " PRSINTDB01",
                                    " PRSINTDB02",
                                    " PRSINTDB01 " + TWUtils::rightArrow() + " PRSINTDB02",
                                    " PRSINTDB02 " + TWUtils::rightArrow() + " PRSINTDB01"
                                  };
    for (auto s : slDBAndPrefixes)
        ui->comboBoxDBSettings->addItem(s);

// set default indices
    nDefaultDateRangeIndex = nDBServerAndPrefixIndex = nBackgroundImageIndex = 0;

// add some info text
    ui->labelInfo->setText("(Ev. ändringar märks först när du avslutar TC2ARV\noch startar igen.)");
}

// such a tosser
SettingsDialog::~SettingsDialog()
{
    delete ui;
}

// use two setters for setting the combobox indeces
void SettingsDialog::setDefaultDateRangeIndex(int i)
{
    nDefaultDateRangeIndex  = i;
    ui->comboBoxDateInterval->setCurrentIndex(nDefaultDateRangeIndex);
}

void SettingsDialog::setDBServerAndPrefixIndex(int i)
{
    nDBServerAndPrefixIndex = i;
    ui->comboBoxDBSettings->setCurrentIndex(nDBServerAndPrefixIndex );
}

void SettingsDialog::setBackgroundImageIndex(QString sBackgroundImage, QStringList slBackgroundImages)
{
// calculate the index for the given BackgroundImage
// note: the stringlist contains pairs of filename, caption so we need to divide by 2 to get the index
    if (slBackgroundImages.contains(sBackgroundImage)) // slight cheating: searching in both captions and filenames (harmless)
        nBackgroundImageIndex = slBackgroundImages.indexOf(sBackgroundImage) / 2;

// fill up the combobox with the captions? only need to do this once
    if (ui->comboBoxBackgroundImage->count() < 1)
        for (int i = 0; (i < slBackgroundImages.count()); ++i)
            if (1 == (i % 2))   // expect the captions to be in the odd positions
                ui->comboBoxBackgroundImage->addItem(slBackgroundImages[i]);

// and set the index to the specified chap
    ui->comboBoxBackgroundImage->setCurrentIndex(nBackgroundImageIndex);
}

// user pressed Cancel, take the easy way out
void SettingsDialog::on_pushButtonCancel_clicked()
{
   QDialog::reject();
}

// user pressed ok, save the current combobox indeces and exit via QDialog::accept()
void SettingsDialog::on_pushButtonOk_clicked()
{
    nDefaultDateRangeIndex  = ui->comboBoxDateInterval->currentIndex();
    nDBServerAndPrefixIndex = ui->comboBoxDBSettings->currentIndex();
    nBackgroundImageIndex   = ui->comboBoxBackgroundImage->currentIndex();

    QDialog::accept();
}

// show a nice About box
void SettingsDialog::on_toolButtonAbout_clicked()
{
// retrieve Careunits.Name from NameAndDescription.pri (need to convert from Windows 1252)
    QString sBuiltFor = QString::fromLatin1(QT_STRINGIFY(BUILTFOR));
    QString sTosser   = " byggd ";
    sBuiltFor = sBuiltFor.mid(sTosser.length() + sBuiltFor.indexOf(sTosser));  // toss prefix "TC2XXX byggd "

// and show a nice info box
    TWUtils::infoBox(QString("<body>TC2ARV version %1.<br>"
                     "Byggd %2 %3. <p>" // use a trailing space for a nicer formatting
                     "TC2ARV är öppen källkod:<br>"
                     "<a href=\"https://gitlab.com/tungware\">gitlab.com/tungware</a><p>"
                     "Programmet är skrivet i C++ och<br>använder Qt utvecklingsverktyg: <a href=\"https://qt.io\">qt.io</a></body>").
                     arg(TWUtils::versionMajorMinor(),TWUtils::toISODate(TWUtils::buildDate()),sBuiltFor));
}
