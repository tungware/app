#----------------------------------------------------------------------------------#
# TC2ARV.pro                                                                       #
#                                                                                  #
# 2019-10-02 First version                                                     HS  #
# 2022-12-02 Say hello to Version 4 (aiming for a distribution during 2023)    HS  #
# 2023-10-17 Version 7: New server address and introduce a settings dialog box HS  #
# 2023-10-20 7.1: Better consultrefs error messages + nicer About box          HS  #
# 2023-12-12 7.1.1: Bugfix: CFUDOB is 8 digits (not a real date)               HS  #
# 2024-01-04 2024.1: Adjust for new patient fees                               HS  #
# 2024-03-28 2024.3: Stomp Go pushbutton double click guru meditation          HS  #
# 2024-04-09 2024.4: SLLINT02 as Server3                                       HS  #
# 2024-06-24 2024.6: R.I.P. SLLINT01 and SLLINT02                              HS  #
#----------------------------------------------------------------------------------#

QT      += core gui widgets sql network

TARGET   = TC2ARV
TEMPLATE = app

HEADERS += mainwindow.h tcterms.h settingsdialog.h
SOURCES += main.cpp mainwindow.cpp settingsdialog.cpp
FORMS   += mainwindow.ui settingsdialog.ui

# version and buildno
VERSION  = 2024.11.0.1

# customer name and description
include (NameAndDescription.pri)

# keep CareUnits.Name for a show in the About box (note: Windows-1252 not UTF8)
DEFINES += "BUILTFOR=\"$$QMAKE_TARGET_DESCRIPTION\""

# set ourselves as the company and copyright
QMAKE_TARGET_COMPANY=Tungware
QMAKE_TARGET_COPYRIGHT=Tungware

# resources stuff here
RESOURCES = TC2ARV.qrc

# include our common QtProjects helper
include (../../include/QtProjects.pri)

# say hello to the usual suspects
# note: smb2 is only needed for static builds (currently Windows only)
LIBS    += $$TWLibLine(TWUtils)
LIBS    += $$TWLibLine(TWDB)
LIBS    += $$TWLibLine(TWTCSupport)
win32: LIBS += $$TWLibLine(smb2)

# and the #defines for the #include statements
DEFINES += $$TWDefineInclude(TWUtils)
DEFINES += $$TWDefineInclude(TWDB)
DEFINES += $$TWDefineInclude(TWTCSupport)
