/*--------------------------------------------------------------------------*/
/* tcterms.h                                                                */
/*                                                                          */
/* 2021-03-23 First version                                             HS  */
/* 2021-05-07 Added S602, S611 and S612                                 HS  */
/* 2021-05-14 Added S604, S605 and S613 -- S616                         HS  */
/* 2022-05-20 Lots of S3XX terms                                        HS  */
/* 2022-10-21 Added 4 S96x terms                                        HS  */
/* 2022-11-12 Added 11 S3xx terms                                       HS  */
/* 2022-11-19 Say hello to a single S953                                HS  */
/*--------------------------------------------------------------------------*/

#include <QString>

// term flavors
enum class TermFlavors { eNormal, eCompanion };

// term attributes struct
struct TermAttributesStruct
{
    TermFlavors eTermFlavor;            // see the enums above

// this is nice to have but not yet implemented in code (as of 2024-10-28)
    // QString     slAllowedCompanions;    // a comma separated list of allowed companion S codes
};

// attribute structs for normal terms and companion terms
static const TermAttributesStruct stNormal    = { TermFlavors::eNormal    };
static const TermAttributesStruct stCompanion = { TermFlavors::eCompanion };

// holding struct for TC's termIDs for the ARV codes
struct TTStruct
{
    int                  nValueTermID;  // internal term #
    QString              sATGCode;      // official code
    TermAttributesStruct stAttr;        // attribute for this term
    QString              sDescription;  // optional descriptive text (mostly for tooltips)
};

// array of LOL TC terms
static const int nnLOLKeywordTermID = 17322;  // leadin/prefix for LOL terms

// LOL terms: ------------------------------
// 2024-10-23 Added S351 - S381 combo
static const TTStruct astLOL[] =
{
    { 17325, "ATR" , stNormal, "Telefonrådgivning" },
    { 17324, "AE"  , stNormal, "Enkel åtgärd"      },
    { 17323, "N"   , stNormal, "Normal åtgärd"     },

    { 16701, "S202", stNormal, "" },
    { 17612, "S203", stNormal, "" },
    { 16703, "S208", stNormal, "" },
    { 16705, "S214", stNormal, "" },
    { 16708, "S215", stNormal, "" },
    { 16710, "S217", stNormal, "" },
    { 16721, "S218", stNormal, "" },
    { 16722, "S219", stNormal, "" },
    { 16723, "S220", stNormal, "" },
    { 17488, "S221", stNormal, "" },
    { 16540, "S222", { TermFlavors::eNormal /*, "S225" */ }, "" },
    { 16541, "S223", { TermFlavors::eNormal /*, "S225" */ }, "" },
    { 16542, "S224", { TermFlavors::eNormal /*, "S225" */ }, "" },
    { 16543, "S225", stCompanion, "" }, // ok with S222, S223, S224
    { 17494, "S226", stNormal, "" },

    { 18586, "S301", stNormal, "" },
    { 18587, "S302", stNormal, "" },
    { 18588, "S303", stNormal, "" },
    { 18589, "S304", stNormal, "" },
    { 17576, "S305", stNormal, "" },
    { 17577, "S306", { TermFlavors::eNormal /*, "S360" */ }, "" },
    { 18590, "S307", stNormal, "" },
    { 18829, "S308", stNormal, "" },
    { 17578, "S309", stNormal, "" },
    { 17579, "S312", stNormal, "" },
    { 17580, "S314", { TermFlavors::eNormal /*, "S386" */ }, "" },
    { 18591, "S315", stNormal, "" },
    { 18592, "S318", stNormal, "" },
    { 18830, "S319", stNormal, "" },
    { 17581, "S320", stNormal, "" },
    { 17582, "S321", stNormal, "" },
    { 17583, "S322", stNormal, "" },
    { 17584, "S324", stNormal, "" },
    { 17585, "S325", stNormal, "" },
    { 17586, "S326", stNormal, "" },
    { 18593, "S327", { TermFlavors::eNormal /*, "S360" */ }, "" },
    { 17587, "S328", stNormal, "" },
    { 18594, "S329", { TermFlavors::eNormal /*, "S360" */ }, "" },
    { 17588, "S330", { TermFlavors::eNormal /*, "S380" */ }, "" },
    { 17589, "S332", { TermFlavors::eNormal /*, "S387" */ }, "" },
    { 18595, "S334", stNormal, "" },
    { 17590, "S335", stNormal, "" },
    { 18596, "S336", stNormal, "" },
    { 18831, "S337", { TermFlavors::eNormal /*, "S360" */ }, "" },
    { 17591, "S338", stNormal, "" },
    { 18832, "S339", { TermFlavors::eNormal /*, "S388" */ }, "" },
    { 18833, "S340", stNormal, "" },
    { 18834, "S346", stNormal, "" },
    { 18597, "S347", stNormal, "" },
    { 18598, "S348", stNormal, "" },
    { 18599, "S349", stNormal, "" },
    { 18600, "S350", stNormal, "" },
    { 17592, "S351", { TermFlavors::eNormal /*, "S381" */ }, "" },
    { 18601, "S352", stNormal, "" },
    { 18602, "S353", { TermFlavors::eNormal /*, "S361" */ }, "" },
    { 18603, "S354", { TermFlavors::eNormal /*, "S361" */ }, "" },
    { 18604, "S355", { TermFlavors::eNormal /*, "S361" */ }, "" },
    { 17593, "S356", { TermFlavors::eNormal /*, "S381" */ }, "" },
    { 17594, "S357", { TermFlavors::eNormal /*, "S382" */ }, "" },
    { 18835, "S358", stNormal, "" },
    { 17595, "S360", stCompanion, "" }, // ok with S306, S327, S329 and S337
    { 18605, "S361", stCompanion, "" }, // ok with S353, S354 and S355
    { 18836, "S364", stNormal, "" },
    { 17596, "S366", stNormal, "" },
    { 17597, "S367", { TermFlavors::eNormal /*, "S383" */ }, "" },
    { 17598, "S368", stNormal, "" },
    { 18606, "S369", stNormal, "" },
    { 18837, "S370", stNormal, "" },
    { 18838, "S371", { TermFlavors::eNormal /*, "S388" */ }, "" },
    { 17599, "S380", stCompanion, "" }, // ok with S356, S357, S367
    { 17600, "S381", stCompanion, "" }, // ok with S330, S351, S357, S367
    { 17601, "S382", stCompanion, "" }, // ok with S330, S356, S367
    { 17602, "S383", stCompanion, "" }, // ok with S330, S356, S537
    { 17603, "S386", stCompanion, "" }, // ok with S332
    { 17604, "S387", stCompanion, "" }, // ok with S314
    { 18839, "S388", stCompanion, "" }, // ok with S313, S339, S371
    { 18607, "S389", stNormal, "" }, // note: S313 does not yet exist in this table

    { 16545, "S601", stNormal, "" },
    { 17886, "S602", stNormal, "" },
    { 17916, "S604", stNormal, "" },
    { 17917, "S605", stNormal, "" },
    { 16546, "S606", stNormal, "" },
    { 16547, "S607", stNormal, "" },
    { 16548, "S608", stNormal, "" },
    { 16549, "S609", stNormal, "" },
    { 16550, "S610", stNormal, "" },
    { 17887, "S611", stNormal, "" },
    { 17888, "S612", stNormal, "" },
    { 17918, "S613", stNormal, "" },
    { 17919, "S614", stNormal, "" },
    { 17920, "S615", stNormal, "" },
    { 17921, "S616", stNormal, "" },

    { 18840, "S953", stNormal, "" },
    { 18775, "S963", stNormal, "" },
    { 18776, "S964", stNormal, "" },
    { 18777, "S965", stNormal, "" },
    { 18778, "S966", stNormal, "" },
    { 17326, "S971", stNormal, "" },
    { 17327, "S972", stNormal, "" },
    { 17328, "S973", stNormal, "" },
    { 17329, "S974", stNormal, "" },
    { 17330, "S975", stNormal, "" },
    { 17331, "S976", stNormal, "" },
    { 17332, "S977", stNormal, "" },
    { 17333, "S978", stNormal, "" },
    { 17334, "S979", stNormal, "" },
};

// LOF terms: ------------------------------
static const int nnLOFKeywordTermID = 17394;  // leadin/prefix for LOF terms

static const TTStruct astLOF[] =
{
    { 17325, "ATRS", stNormal, "Telefonrådgivning" },
    { 17395, "NI"  , stNormal, "Normal Individuell" },
    { 17396, "NP"  , stNormal, "Normal Parallell" },
    { 17397, "AEI" , stNormal, "Enkel Individuell" },
    { 17398, "AEG" , stNormal, "Enkel Grupp" },

    { 17399, "AS1" , stNormal, "" },
    { 17400, "AS2" , stNormal, "" },
    { 17401, "AS3" , stNormal, "" },
    { 17402, "AS4" , stNormal, "" },
    { 17403, "AS5" , stNormal, "" },
    { 17404, "AS6" , stNormal, "" },
    { 17405, "AS7" , stNormal, "" },
};
