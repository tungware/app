/*--------------------------------------------------------------------------*/
/* settingsdialog.h                                                         */
/*                                                                          */
/* 2023-10-16 First version                                             HS  */
/*--------------------------------------------------------------------------*/

#include <QDialog>
#include "ui_settingsdialog.h"

// pull in the Tungware libraries
#include INCLUDETWUTILS

// use a QDialog for our settings
class SettingsDialog : public QDialog
{
    Q_OBJECT

public:
    explicit SettingsDialog(QWidget *parent, bool bIsLocumTenens);
    ~SettingsDialog();

// use an integer index to get/set the date range and db stuff
    int  nDefaultDateRangeIndex;
    int  nDBServerAndPrefixIndex;
    int  nBackgroundImageIndex;

// use the setters, Luke
    void setDefaultDateRangeIndex(int i);
    void setDBServerAndPrefixIndex(int i);
    void setBackgroundImageIndex(QString sBackgroundImage, QStringList slBackgroundImages);

private slots:
    void on_pushButtonOk_clicked();
    void on_toolButtonAbout_clicked();

    void on_pushButtonCancel_clicked();

private:
    Ui::SettingsDialog *ui;
};
