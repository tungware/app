/*--------------------------------------------------------------------------*/
/* mainwindow.cpp                                                           */
/*                                                                          */
/* 2021-03-23 First version                                             HS  */
/*--------------------------------------------------------------------------*/

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "qcommandlineparser.h"
#include "qtimer.h"
#include "qevent.h"
#include "qpropertyanimation.h"
#include "qparallelanimationgroup.h"

// pull in the TCTerms
#include "tcterms.h"

// and the Settings dialog
#include "settingsdialog.h"

//----------------------------------------------------------------------------
// MainWindow ctor
MainWindow::MainWindow(QWidget* parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);

// usual preamble (will also change current directory to where the .exe file is)
    TWUtils::helloQt(this);
    setWindowTitle(QString("Välkommen till TC2ARV (version %1)").arg(TWUtils::versionMajorMinor()));

// check our .exe filename, is it an old/outdated version? if so warn the user
    if (TWUtils::getAppName().startsWith(ssOldVersionPrefix))
        TWUtils::warningBox("Detta är en gammal version av TC2ARV, inte säkert att den fungerar korrekt.");

// read in our RO settings, check major+minor version and customername with the one in the .pro file
    roSettings.setCurrentSection("Update");
#if defined(Q_OS_WIN)
// if we're on Windows, check the version nos.
    int nBuildVersionNo = TWUtils::versionMajor();
    QString sMinimumVersion = roSettings.readString("MinimumVersion");
    if (!sMinimumVersion.contains("."))
        sMinimumVersion = "1.0";
    int nMinimumVersionNo = sMinimumVersion.split(".")[0].toInt();
    if (nMinimumVersionNo > nBuildVersionNo)
        fubar(QString("Major version %1 is lower than the required minimum version %2.").arg(nBuildVersionNo).arg(nMinimumVersionNo));
#endif

// read in lots of stuff from the general/first section of ROSettings
    roSettings.setCurrentSection("General");
    sCustomerName     = roSettings.readString("CustomerName");
    QString sExpected = QT_STRINGIFY(CUSTOMERNAME);
    if (sExpected != sCustomerName)
        fubar(QString("expected customer name: '%1' but got '%2' from the .ini file").arg(sExpected,sCustomerName));

    eCustomerType = eUnknownCustomerType;
    if ("LOL" == roSettings.readString("LOLOrLOF"))
        eCustomerType = eLOLCustomerType;
    if ("LOF" == roSettings.readString("LOLOrLOF"))
        eCustomerType = eLOFCustomerType;
    if (eUnknownCustomerType == eCustomerType)
        fubar("LOLOrLOF setting must be either \"LOL\" or \"LOF\"");

    nCustomerNo      = roSettings.readInt("CustomerNo");
    bConsultRefs     = roSettings.readBool("ConsultRefs");  // TBD: check that LOFs do not have bConsultRefs = true
    bAutoStart       = roSettings.readBool("AutoStart");
    bBatchMode       = false;     // placeholder, we'll set it later in this function
    bDiagnoses       = roSettings.readBool("Diagnoses");
    bCheckRandomDOBs = roSettings.readBool("CheckRandomDOBs");
    bCheckAllDOBs    = roSettings.readBool("CheckAllDOBs");

    nCareUnitID      = roSettings.readInt("CareUnitID");
    nCompanyID       = roSettings.readInt("CompanyID");
    nCompanyCode     = roSettings.readInt("CompanyCode");
    sKombika         = roSettings.readString("Kombika");
    sHSAID           = roSettings.readString("HSAID");

// (read in [DefaultPeriod] later)
// check if we have some r/w settings in AppData
// the default date range is stored in [General] and is shared ok (if more than one TC2ARV user runs on the same desktop)
// the db server and table prefix is stored in [Intelligence%CustomerNo%] to keep possible multiple instances apart
    sIntelligenceSectionName = "Intelligence";                        // default: no R/W settings seen
    eIniFlavorForDB          = TWAppSettings::IniLocation::eEmbedded; //       -  "  -

// have something in the R/W ini file?
    QString sRWIntelligenceSectionName = sIntelligenceSectionName + QString::number(nCustomerNo);
    appDataSettings.setCurrentSection(sRWIntelligenceSectionName);
    if (appDataSettings.readIntWithDefault("UseServer",0) > 0)  // use this one as a canary (should not be 0)
    {
    // got something, so we will to use AppData settings for the db settings
        eIniFlavorForDB          = TWAppSettings::IniLocation::eAppData;
        sIntelligenceSectionName = sRWIntelligenceSectionName;  // and we will use the R/W section name
    }

// Samba chaps
    roSettings.setCurrentSection("Samba");
    sSambaShareName = roSettings.readString("ShareName");
    sLocalDirectory = "";
    if (roSettings.readBoolWithDefault("UseLocalDirectory",false))
    // used as a fallback when we cannot connect directly to the Samba server
        sLocalDirectory = roSettings.readStringOSSuffix("LocalDirectory");

// optional locum tenens section, is there an active one?
    roSettings.setCurrentSection("LocumTenens");
    bLocumTenensActive = roSettings.readBoolWithDefault("Active",false);
    bIsLocumTenens     = false;    // default to false
    sLocumTenensCode   = "     ";  // default to 5 spaces

    if (bLocumTenensActive)
    {
    // yes active, slurp in all the settings
        bIsLocumTenens   = roSettings.readBool("IsLocumTenens");
        sLocumTenensCode = roSettings.readString("LocumTenensCode");
        if ((5 != sLocumTenensCode.length()) || (sLocumTenensCode != TWUtils::tossAllButDigits(sLocumTenensCode)))
            if ("     " != sLocumTenensCode)
                fubar("Fel på LocumTenensCode i ROSettings: ange antingen 5 siffror eller 5 mellanslag");

        if (bIsLocumTenens == ("     " == sLocumTenensCode))
            fubar("if LocumTenens is true then LocumTenensCode must be nonblank (or the other way around)");

    // these have be non-empty (and the IgnorePASMissing comma list is expected to be encrypted but not enforced yet)
        slPASProcess  = roSettings.readStringList("PASProcess");
        slPASSkipOver = roSettings.readStringList("PASSkipOver");

        if (roSettings.readStringWithDefault("IgnorePASMissing","").isEmpty())
        // the legacy unencrypted is not present (or is empty), read the encrypted one
            slIgnorePASMissing = roSettings.readStringListEncrypted(sCustomerName,"EncryptedIgnorePASMissing");
        else
            slIgnorePASMissing = roSettings.readStringList("IgnorePASMissing");

    // check PASProcess and PASSkipOver lists, can contain either HSAIDs or PAS CareProviderIDs (01,02 ...)
        if (slPASProcess.isEmpty())
            fubar("PASProcess cannot be empty");
        if (slPASSkipOver.isEmpty())
            fubar("PASSkipOver cannot be empty");

        for (auto s : slPASProcess)
        {
            if (s.contains(" "))
                fubar("PASProcess chap: no spaces please");

            if (s.length() < 2)
                fubar("PASProcess chap must at least 2 characters/digits");
        }

        for (auto s : slPASSkipOver)
        {
            if (s.contains(" "))
                fubar("PASSkipOver chap: no spaces please");

            if (s.length() < 2)
                fubar("PASSkipOver chap must at least 2 characters/digits");
        }

        for (auto s : slPASProcess)
            if (slPASSkipOver.contains(s))
                fubar("Same ID found in both PASProcess and PASSkipOver lists");

    // also check slIgnorePasMissing list (expect it to contain valid personno(s))
        if (slIgnorePASMissing.isEmpty())
            fubar("IgnorePASMissing cannot be empty");

        for (auto &sPersonNo : slIgnorePASMissing)
        {
            if (sPersonNo.isEmpty())
                fubar("IgnorePasMissing list: didn't expect an empty personno");

            if ("" != TWUtils::checkPersonNo(sPersonNo))
                fubar(QString("IgnorePasMissing list: bad PersonNo '%1'").arg(sPersonNo));
        }
    }

// get taxation flavor (to be written to the ARV file)
    roSettings.setCurrentSection("ARVFile");
    sTaxType = roSettings.readString("FSkattEllerASkatt");
    if (!QStringList{"F","A"}.contains(sTaxType))
        fubar("FSkattEllerASkatt bad value");

// almost done, parse the [Options] section (yes they are all optional, hence the clever section name)
    roSettings.setCurrentSection("Options");
    appDataSettings.setCurrentSection("Options");   // support appdata override for the background image
    dConsultRefsBegin    = TWUtils::fromISODate(roSettings.readStringWithDefault("ConsultRefsBegin",TWUtils::toISODate(ddConsultRefsBegin)));
    bObfuscatePersonNo   = roSettings.readBoolWithDefault("ObfuscatePersonNo",false);
    bShowErrorSponges    = roSettings.readBoolWithDefault("ShowErrorSponges",false);
    bShowSponges         = roSettings.readBoolWithDefault("ShowSponges",false);
    sTCPathOverride      = roSettings.readStringWithDefault("TCPathOverride","");
    sARVFilePathOverride = roSettings.readStringWithDefault("ARVFilePathOverride","");
    if (!sARVFilePathOverride.isEmpty())
        fubar("The ARVFilePathOverride option is not yet supported");

// 2024-01-07: background image filename: check appdata settings first, then ROSettings
    if (slBackgroundImages.count() < 2)
        fubar("bad slBackgroundImages list (too short)");
    sBackgroundImage = appDataSettings.readStringWithDefault("BackgroundImage","");
    if (sBackgroundImage.isEmpty())
    // nothing in AppData R/W? try the ROSettings and if nothing's there, default to moln.jpg
        sBackgroundImage = roSettings.readStringWithDefault("BackgroundImage",":/moln.jpg");
    if (sBackgroundImage == slBackgroundImages[0])  // set to "none"? if so set filename empty
        sBackgroundImage = "";

// setup the default date period? yes unless a date range has been specified in the command line (for batch mode)
    bBatchMode = false;

    QCommandLineParser lp;
    lp.process(qApp->arguments());
    auto slArgs = lp.positionalArguments();
    if (slArgs.count() == 2)
    {
    // need two valid dates
        dFrom = TWUtils::fromISODate(slArgs[0]);
        dTo   = TWUtils::fromISODate(slArgs[1]);

        bool bOk = (dFrom.isValid() && dTo.isValid());
        if (bOk)
            bOk = (dFrom <= dTo);                 // same day ok
        if (bOk)
            bOk = (dTo < QDate::currentDate());   // yesterday or earlier ok

    // we have two valid dates, enable batch mode
    // (if we didn't get a valid range, dFrom and dTo will be assigned new values below)
        bBatchMode = bOk;
    }

    if (!bBatchMode)
    {
    // ok let the user select the date range
    // calc. the default date range from the R/O settings
        roSettings.setCurrentSection("DefaultPeriod");
        int nPeriodMonths = roSettings.readInt("Months");
        int nPeriodWeeks  = roSettings.readInt("Weeks");
        int nPeriodDays   = roSettings.readInt("Days");

    // if we have some R/W settings try to use them instead
        appDataSettings.setCurrentSection("DefaultPeriod"); // use a common setting (regardless of customer no.)
        nPeriodMonths = appDataSettings.readIntWithDefault("Months",nPeriodMonths);
        nPeriodWeeks  = appDataSettings.readIntWithDefault("Weeks", nPeriodWeeks);
        nPeriodDays   = appDataSettings.readIntWithDefault("Days",  nPeriodDays);

    // some sanity checks please
        if ((nPeriodMonths < 0) || (nPeriodWeeks < 0) || (nPeriodDays < 0))
            fubar("don't like negative [DefaultPeriod] values");

        if ((nPeriodMonths == 0) && (nPeriodWeeks == 0) && (nPeriodDays == 0))
            fubar("sorry but at least one of Months,Weeks or Days in [DefaultPeriod] in the .ini file has to be > 0");

    // default to a period of some time specified in the settings (days, weeks or months)
        dFrom = QDate::currentDate().addDays(-1);   // default range: [yesterday to today[
        dTo   = QDate::currentDate();               //
        if (nPeriodDays > 0)
        {
            dFrom = QDate::currentDate().addDays(0 - nPeriodDays);
            if (dFrom.dayOfWeek() > 5)      // Saturday or Sunday?
            {
                dFrom = dFrom.addDays(-1);
                if (dFrom.dayOfWeek() > 5)  // try again
                    dFrom = dFrom.addDays(-1);
            }
        }

        if (nPeriodWeeks > 0)
        {
        // set a period of weeks, Monday to Monday (note: we're assuming Monday is the first day of the week)
            dFrom = QDate::currentDate().addDays(0 - 7 * nPeriodWeeks); // step # of weeks back
            while (dFrom.dayOfWeek() > Qt::Monday)
                dFrom = dFrom.addDays(-1);

            dTo = dFrom.addDays(7 * nPeriodWeeks);
        }

        if (nPeriodMonths > 0)
        {
        // set period 1st to 1st of months
            dFrom = QDate::currentDate().addMonths(0 - nPeriodMonths); // step # of months back
            if (QDate::currentDate().day() > 6)    // but if we're later than 6 days into a month
                dFrom = dFrom.addMonths(1);        // step one month forward
            dFrom = QDate(dFrom.year(),dFrom.month(),1);
            dTo = dFrom.addMonths(nPeriodMonths);
        }

        if (dTo > QDate::currentDate())     // trying to step into the future?
            dTo = QDate::currentDate();     // today is as far as we can go

    // now we have [first, last[, i.e. last is exclusive
    // make an inclusive range i.e. [first, last] by subtracting 1 day
        dTo = dTo.addDays(-1);
    }

    if (dTo < dFrom)  // make sure we're coding correctly here
        guruMeditation("dTo < dFrom :-( something is seriously wrong with the date calculations");

// set 'em on our form as the default date selection
    ui->calendarWidgetFrom->setSelectedDate(dFrom);
    ui->calendarWidgetTo->setSelectedDate(dTo);

// set the maximum allowed date range, i.e. first and last days that we allow to be selected
    QDate dEarliest = ddEarliestVisit;                  // (because we changed receipt no.s style then)
    QDate dLatest   = QDate::currentDate().addDays(-1); // yesterday is the most recent day we'll allow

    ui->calendarWidgetFrom->setMinimumDate(dEarliest);
    ui->calendarWidgetFrom->setMaximumDate(dLatest);

    ui->calendarWidgetTo->setMinimumDate(dEarliest);
    ui->calendarWidgetTo->setMaximumDate(dLatest);

// use our clicked() methods to initially set the Go-button caption
    on_calendarWidgetFrom_clicked(dFrom);
    on_calendarWidgetTo_clicked  (dTo  );

// load up the icons we use
    int nIconWidthAndHeight = 24;
    iconTC       = QIcon(":/tc.ico");
    iconUploadOk = QIcon(":/uploadOk.ico");
    ui->tableWidgetAll->setIconSize(QSize(nIconWidthAndHeight,nIconWidthAndHeight));
    ui->tableWidgetWarnings->setIconSize(QSize(nIconWidthAndHeight,nIconWidthAndHeight));
    ui->tableWidgetErrors->setIconSize(QSize(nIconWidthAndHeight,nIconWidthAndHeight));

// set LOL/LOF specific stuff
    nKeywordTermID = 0;
    if (eLOLCustomerType == eCustomerType)
    // set for LOL flavor
        nKeywordTermID = nnLOLKeywordTermID;

    if (eLOFCustomerType == eCustomerType)
    // set for LOF flavor
        nKeywordTermID = nnLOFKeywordTermID;

    if (nKeywordTermID == 0)
        fubar("LOLOrLOF setting must be either \"LOL\" or \"LOF\"");

// ------------------------------------
// prepare the UI, start with the title
    QString sTitle = "TC2ARV för " + sKombika;
    if (bIsLocumTenens)
        sTitle += " vikarie: " + sLocumTenensCode;
    ui->labelTitle->setText(sTitle);

    if (bObfuscatePersonNo)
        ui->labelTitle->setText("TC2ARV med testpatienter");

    setupUI();  // do the rest here

// almost done, final step: connect Samba and Intelligence (via a timed lambda)
    switchAppState(AppState::eConnecting);
    QTimer::singleShot(nnShortDelay,this,[this]
    {
    // now is a good time to check that we can create a new file in the current directory
        if (!TWTextFileWriter::canCreateFile("Testfil.ARV"))
            if (!TWUtils::okCancelBox(QString("Kan inte skapa en ARV-fil i denna mapp ('%1').\nOk att fortsätta ändå?").arg(TWUtils::getCurrentDirectory())))
                return qApp->quit();

    // ok carry on trying to connect to Samba and Intelligence
        QString sError = connectToSambaAndIntelligence();
        if (!sError.isEmpty())
        {
        // better luck next time
            TWUtils::errorBox(sError);
            return qApp->quit();
        }

    // wait for app state to change (and the Go button to be pressed)
    });
}

//----------------------------------------------------------------------------
// MainWindow main tosser
//
// 2020-02-20 First version
// 2023-04-12 Nice addition: save the geometry
//----------------------------------------------------------------------------
MainWindow::~MainWindow()
{
// save our mainwindow geometry
    appDataSettings.setCurrentSection("General");
    appDataSettings.writeByteArray("Geometry",saveGeometry());
    appDataSettings.sync(); // in case we're doing an ignominious exit

// if we're processing, chances are that some other thread is running
    if ((AppState::eConnecting == appState) || (AppState::eProcessing == appState))
    // if so exit the hard way
        TWUtils::ignominiousExit();

// else the slow way
    delete ui;
}

//----------------------------------------------------------------------------
// closeEvent (MainWindow extra tosser when typing Alt-F4 or clicking 'x')
//
// 2023-11-06 First version
//----------------------------------------------------------------------------
void MainWindow::closeEvent(QCloseEvent* event)
{
    Q_UNUSED(event);

    TWUtils::ignominiousExit();
}

//----------------------------------------------------------------------------
// setupUI
//
// 2020-01-31 First version
// 2023-01-04 Setup the slider's groove here
// 2023-04-16 Introduce a geometry stash map
// 2023-04-26 3 tablewidgets (white, yellow and red flavors)
// 2023-09-17 Smaller font for companion ARV receipt nos.
//----------------------------------------------------------------------------
void MainWindow::setupUI()
{
// show the calendars but hide the Go button until we're connected nice and dandy
    ui->pushButtonGo->hide();

// prepare the 3 table widgets, first the columns
    auto setupColumns = [] (QTableWidget* tw)
    {
        tw->setColumnCount(colCount);
        for (int i = 0; (i < colCount); ++i)
            tw->setColumnWidth(i,anColumnWidths[i]);
    };
    setupColumns(ui->tableWidgetAll);
    setupColumns(ui->tableWidgetWarnings);
    setupColumns(ui->tableWidgetErrors);

// and hide 'em for now
    ui->tableWidgetAll->hide();
    ui->tableWidgetWarnings->hide();
    ui->tableWidgetErrors->hide();

// set the default table widget view state
    tableViewState = eShowAll;

// prepare the visibility slider and hide it until we've started processing
    ui->visibilitySlider->hide();
    ui->visibilitySlider->setValue(tableViewState); // start with the "ShowAll" leftmost position
    refreshVisibilitySlider(tableViewState);        //

// create a new groove fpr the slider (the original groove vanishes due to the stylesheet applied above)
// wire up a new QFrame to act as a surrogate groove with the same parent and geometry as the slider
    QFrame* pFrame = new QFrame(ui->visibilitySlider->parentWidget());
    pFrame->setGeometry(ui->visibilitySlider->geometry());
    pFrame->setObjectName(ui->visibilitySlider->objectName() + "Frame");
    pFrame->setFrameStyle(QFrame::HLine | QFrame::Sunken);
    pFrame->setLineWidth(2);  // (looks nicer than width 1)

// change the z-order so that our custom groove is drawn under the handle (just like the real groove)
    pFrame->stackUnder(ui->visibilitySlider);

// in order for hide()/show() and resize() to work remember this QFrame
    pSliderGrooveFrame = static_cast<QWidget*>(pFrame);

// check that we really have a slider groove QFrame alive and well
    if (nullptr == pSliderGrooveFrame)
        fubar("Couldn't create a slider groove frame");
    pSliderGrooveFrame->hide(); // and hide it for now

// reset and hide the progressbar until we start the processing
    ui->progressBar->setValue(0);
    ui->progressBar->hide();

// background image requested?
    pLabelBackground = nullptr;
    if (!sBackgroundImage.isEmpty())
    {
    // wire up a new QLabel, set the geometry to use the same size as the main window
        pLabelBackground = new QLabel(this);
        auto r = geometry();
        r.setTopLeft(QPoint(0,0));
        pLabelBackground->setGeometry(r);
        pLabelBackground->setScaledContents(true);
        pLabelBackground->setPixmap(sBackgroundImage);
        pLabelBackground->show();
        pLabelBackground->stackUnder(ui->centralWidget);
    }

// resizing support: remember original mainwindow size and set the minimum size
    szOriginal = size();
    setMinimumSize(szOriginal); // designed size ---> minimum size (for now)

// save all our widgets original/designed geometries
    auto sg = [this] (QWidget* w) { mOriginalGeometries.insert(w,w->geometry()); };
    sg(ui->labelTitle);
    sg(ui->pushButtonExit);
    sg(ui->visibilitySlider);
    sg(pSliderGrooveFrame);   // don't forget this one
    sg(ui->frameCalendars);   // (includes calendars and the Go button)
    sg(ui->tableWidgetAll);
    sg(ui->tableWidgetWarnings);
    sg(ui->tableWidgetErrors);
    sg(ui->progressBar);
    sg(ui->labelProgress);
    if (nullptr != pLabelBackground)
        sg(pLabelBackground);

// prepare a smaller font for companion receipt no. cell (use 80% of the original size)
    fCompanionReceiptNo = ui->tableWidgetAll->font();
    fCompanionReceiptNo.setPointSize((fCompanionReceiptNo.pointSize() * 80) / 100);

// finally, restore mainwindow geometry if we have one saved (unless a shift key is held down)
    appDataSettings.setCurrentSection("General");
    QByteArray baGeometry = appDataSettings.readByteArray("Geometry");
    if ((baGeometry.length() > 0) && (!TWUtils::isShiftKeyDown()))
        restoreGeometry(baGeometry);
}

//-----------------------------------------------------------------------------
// connectToSambaAndIntelligence
// returns "" when all is good, else returns an error message
//
// 2022-08-22 First version
// 2023-04-16 Use an initial samba crawler to detect connection problems
// 2023-10-16 Use a db prober to catch initial SQL errors
//-----------------------------------------------------------------------------
QString MainWindow::connectToSambaAndIntelligence()
{
// say hello to Samba and the Intelligence server
    showProgress(QString(" Öppnar Samba och vårdgivarkontot för %1...").arg(sCustomerName));

// use a db prober to check our Intelligence connection
// need to do this *before* launching initial Samba crawler (since it switches app state automatically)
    QString sDBProbeError = TWTCSupport::probeSQLServer(eIniFlavorForDB,sIntelligenceSectionName);
    if (!sDBProbeError.isEmpty())
    {
    // got a db error (showstopper), hide the calendars for a visible effect
        ui->frameCalendars->hide();
        showProgress("Öppna Intelligence/vårdgivarkontot misslyckades.");

        TWUtils::errorBox(sDBProbeError);
        return "";  // tell a lie (no errors) to stay in the app so that user can go to Settings if needed
    }

// use our initial Samba instance for checking the connection
    if (sLocalDirectory.isEmpty())
    // no local directory set for Samba files? on Windows we support a remote connection as well
        scInitial.setSambaCredentialsFromIniFile();
    else
        scInitial.setLocalDirectory(sLocalDirectory);

// wire up the crawling done signal
    connect(&scInitial,&SambaCrawler::crawlingDone,this,[this] (int nMapCount)
    {
        Q_UNUSED(nMapCount);

    // if there's an error kiss the user goodbye
        QString sError = scInitial.sErrorMsg;
        if (!sError.isEmpty())
        {
            TWUtils::errorBox(sError);
            TWUtils::ignominiousExit(); // exit here directly avoiding getting stuck waiting for the Samba worker thread
        }

    // no error(s), what about Intelligence, is it connected ok? i.e. we're the last man out?
        if (bWaitingForSamba)
        {
            showProgress(QString(" Intelligence/vårdgivarkontot och Samba öppnade för %1%2.").
                                 arg(sCustomerName).arg(bIsLocumTenens ? " (vikarie)" : ""));

        // yes the app was waiting for the initial Samba crawler, so switch the app state now
            switchAppState(AppState::eConnectedOk);
         }
    });

// set it to crawl only one day back (we're not interested in any hits, just verifying we can have a chat with the server)
    scInitial.crawl(nCompanyCode,1);
    bWaitingForSamba = false; // guessing this initial crawl will finish before we've checked Intelligence

// time to launch the Intelligence connection ---------------------------------
    db.setDBErrorCallback([this](QString sError) { dbError(sError); });
    TWTCSupport::setBobbyTablesPrefixFromIniFile(eIniFlavorForDB,sIntelligenceSectionName);
    db.setConnectionName("TC2ARV1");

    QString sError = TWTCSupport::openSQLServerFromIniFile(&db,eIniFlavorForDB,sIntelligenceSectionName);
    if (!sError.isEmpty())
        return sError;  // not good, we're out of here

// match current info with what we have from the .ini file for the careunit and company
    slErrors.clear();
    db.select("CareUnitID,CompanyID,Kombika,HSAID",stTCCorral.Codes_CareUnits,QString("CareUnitID=%1").arg(nCareUnitID),
    [this](DBRowStruct rs)
    {
        if (nCareUnitID != rs.intValue("CareUnitID"))
            slErrors += "CareUnitID är inte korrekt";

        if (nCompanyID  != rs.intValue("CompanyID"))
            slErrors += "CompanyID är inte korrekt";

        if (sKombika    != rs.stringValue("Kombika"))
            slErrors += "Kombikakoden är inte korrekt";

        if (sHSAID      != rs.stringValue("HSAID"))
            slErrors += "HSAID är inte korrekt";
    },eDBRowsExpectExactlyOne);

// got some error(s)? if so quit
    if (!slErrors.isEmpty())
        dbError("Kan inte fortsätta:\n" + TWUtils::stringList2CommaList(slErrors).replace(",","\n"));

// check some more stuff in Codes_Companies_v2 (mismatches here are not considered fatal)
    slWarnings.clear();
    db.select("CompanyCode,EconomicalCatalogue",stTCCorral.Codes_Companies_v2,QString("CompanyID=%1").arg(nCompanyID),
    [this](DBRowStruct rs)
    {
        if (nCompanyCode != rs.intValue("CompanyCode"))
            slWarnings += "Företagskoden är ändrad i Intelligence";

        auto sl = rs.stringValue("EconomicalCatalogue").split("/");
        if (sl.isEmpty())
            slWarnings += "Ekonomiska katalogen i Intelligence saknas (ej ifylld)";
        else
        {
            if (sl.last().isEmpty())
                sl.removeLast();

            if (!sl.isEmpty())
                if (sSambaShareName != sl.last())
                    slWarnings += "Ekonomiska katalogen i Intelligence är inte identisk med inställningarna för din Samba";
        }
    },eDBRowsExpectExactlyOne);

// done: show progress bar message saying we have a valid Intelligence connection
    QString sProgress = QString(" Intelligence/vårdgivarkontot öppnat för %1, väntar på Samba...").arg(sCustomerName);
    if (scInitial.bCrawlingDone)
        sProgress = QString(" Samba och Intelligence/vårdgivarkontot öppnade för %1%2.").
                            arg(sCustomerName).arg(bIsLocumTenens ? " (vikarie)" : "");
    showProgress(sProgress);

// got some warnings to show?
    if (!slWarnings.isEmpty())
        TWUtils::warningBox("Varning:\n" + TWUtils::stringList2CommaList(slWarnings).replace(",","\n"));

// we've opened Intelligence ok, what about Samba?
    if (scInitial.bCrawlingDone)
    // yes Samba is connected as well, switch app state and light up the Go button
        switchAppState(AppState::eConnectedOk);
    else
    // no set the boolean and go idle (the crawling done slot will switch app state for us)
        bWaitingForSamba = true;

// return with an empty string (meaning: no Intelligence or Samba errors)
    return "";
}

//----------------------------------------------------------------------------
// resizeEvent
//
// 2023-04-14 First version
// 2024-01-07 Support resize of the background image
//----------------------------------------------------------------------------
void MainWindow::resizeEvent(QResizeEvent* event)
{
// get the new size of our mainwindow
    QSize szNew = event->size();

// calc. the horizontal and vertical deltas
    int nDeltaX = szNew.width() - szOriginal.width();
    int nDeltaY = szNew.height() - szOriginal.height();

// move/resize our widgets (regardless if they're visible or not)
    auto adjustWidget = [this] (QWidget* w, int dx1, int dy1, int dx2, int dy2)
    {
        w->setGeometry(mOriginalGeometries[w].adjusted(dx1,dy1,dx2,dy2));
    };

    adjustWidget(ui->labelTitle,      nDeltaX / 2, 0, nDeltaX / 2, 0);    // recenter in x, fixed in y
    adjustWidget(ui->pushButtonExit,  nDeltaX,     0, nDeltaX,     0);    // follow in x, fixed in y
    adjustWidget(ui->visibilitySlider,nDeltaX / 2, 0, nDeltaX / 2, 0);    // recenter in x, fixed in y
    adjustWidget(pSliderGrooveFrame,  nDeltaX / 2, 0, nDeltaX / 2 ,0);    // recenter in x, fixed in y

    adjustWidget(ui->frameCalendars,  nDeltaX / 2, nDeltaY / 2, nDeltaX / 2, nDeltaY / 2);   // recenter in both x and y

    adjustWidget(ui->tableWidgetAll,      0,       0, nDeltaX, nDeltaY);  // fixed at the top/left, follow in bottom/right
    adjustWidget(ui->tableWidgetWarnings, 0,       0, nDeltaX, nDeltaY);  // fixed at the top/left, follow in bottom/right
    adjustWidget(ui->tableWidgetErrors,   0,       0, nDeltaX, nDeltaY);  // fixed at the top/left, follow in bottom/right

    adjustWidget(ui->progressBar,   0, nDeltaY, nDeltaX, nDeltaY);        // fixed at the left, follow to the right and in y
    adjustWidget(ui->labelProgress, 0, nDeltaY, nDeltaX, nDeltaY);        // fixed at the left, follow to the right and in y

    if (nullptr != pLabelBackground)
        adjustWidget(pLabelBackground, 0, 0, nDeltaX, nDeltaY);           // fixed at the top/left, follow in bottom/right
}

//----------------------------------------------------------------------------
// setGoButtonCaption
//
// 2019-11-08 First version
//----------------------------------------------------------------------------
void MainWindow::setGoButtonCaption()
{
// assume we have valid dFrom and dTo dates, select different caption if we have 2 dates range of just 1 date
    if (dFrom == dTo)
        ui->pushButtonGo->setText(QString("Hämta ARV-data för %1en %2")
                                  .arg(TWUtils::getSwedishWeekdayName(dFrom),TWUtils::toISODate(dFrom)));
    else
        ui->pushButtonGo->setText(QString("Hämta ARV-data fr.o.m. %1en %2 t.o.m. %3en %4")
                                  .arg(TWUtils::getSwedishWeekdayName(dFrom),TWUtils::toISODate(dFrom),
                                       TWUtils::getSwedishWeekdayName(dTo  ),TWUtils::toISODate(dTo  )));
}

//----------------------------------------------------------------------------
// on_calendarWidgetFrom_clicked
//
// 2019-11-01 First version
// 2023-10-05 Don't show an info box, just follow the other calendar
//----------------------------------------------------------------------------
void MainWindow::on_calendarWidgetFrom_clicked(const QDate &date)
{
// have a new "From" date
    dFrom = date;

// is it after the "To" date?
    if (dFrom > dTo)
    {
    // yes, set the "To" date to the same day as the "From" date
        dTo = dFrom;

    // refresh the other calendar (will call the other's slot but it's harmless)
        ui->calendarWidgetTo->setSelectedDate(dTo);
    }

// and refresh the Go button caption
    setGoButtonCaption();
}

//----------------------------------------------------------------------------
// on_calendarWidgetTo_clicked
//
// 2019-11-01 First version
// 2023-10-05 Don't show an info box, just follow the other calendar
//----------------------------------------------------------------------------
void MainWindow::on_calendarWidgetTo_clicked(const QDate &date)
{
// have a new "To" date
    dTo = date;

// is it before the "From" date?
    if (dTo < dFrom)
    {
    // yes, set the "From" date to the same day as the "To" date
        dFrom = dTo;

    // refresh the other calendar (will call the other's slot but it's harmless)
        ui->calendarWidgetFrom->setSelectedDate(dFrom);
    }

// and refresh the Go button caption
    setGoButtonCaption();
}

//----------------------------------------------------------------------------
// on_pushButtonExit_clicked
//
// 2019-10-02 First version
//----------------------------------------------------------------------------
void MainWindow::on_pushButtonExit_clicked()
{
// if we're processing, ask nicely first
    if ((AppState::eConnecting == appState) || (AppState::eProcessing == appState))
    {
        if (!TWUtils::yesNoBox("Vill du avsluta?", "Avsluta TC2ARV"))
            return;

    // since we're processing, jump ship immediately (avoid hanging threads etc.)
        TWUtils::ignominiousExit();
    }

// else do a more vanilla exit
    QApplication::quit();
}

//----------------------------------------------------------------------------
// switchAppState (change app state and probably also the UI)
//
// 2020-02-01 First version
// 2023-04-20 Split into 2 processing states (Intelligence and Samba)
// 2023-10-06 Stomp timing bug: single shot the showing of the Go button
//----------------------------------------------------------------------------
void MainWindow::switchAppState(AppState newAppState)
{
// hide/show different things depending on app state
    switch (newAppState)
    {
    case AppState::eConnecting    :
        break;

    case AppState::eConnectedOk   :
    // successfully connected, queue up for the Go button to be shown
        QTimer::singleShot(nnNormalDelay,this,[this] { ui->pushButtonGo->show(); });

    // autostart or batch mode enabled? if so, press the Go button automagically after a slightly (3 times) longer delay
        if ((bAutoStart) || (bBatchMode))
            QTimer::singleShot(nnNormalDelay * 3,this,[this] { on_pushButtonGo_clicked(); });

    // else wait for the user to do the honors (i.e. clicking the button)
        break;

    case AppState::eProcessing    :
    // processing: time to slide the calendars and the Go button up and show the table widget instead
        {
        // disable the Go pushbutton directly to avoid recursive double clicks
            ui->pushButtonGo->setEnabled(false);

        // fiddle with the geometries for these two widgets
        // (we don't need to animate the other two table widgets, since the ending geometry is the designed geometry)
            auto rCalendars   = ui->frameCalendars->geometry();
            auto rTableWidget = ui->tableWidgetAll->geometry();

            QPropertyAnimation* animFrame    = new QPropertyAnimation(ui->frameCalendars,"geometry");
            QPropertyAnimation* animTableAll = new QPropertyAnimation(ui->tableWidgetAll,"geometry");

            animFrame->setDuration(nnSlideAnimDuration);
            animTableAll->setDuration(nnSlideAnimDuration);

        // starting geometries for the calendars: use the current
        // for the table widget: current geometry slided down one full height
            animFrame->setStartValue(rCalendars);
            animTableAll->setStartValue(rTableWidget.translated(0,rTableWidget.height()));

        // ending geometries: for the calendars: squished at top (top y same but zero height)
        // for the table widget: the current/designed geometry
            rCalendars.setHeight(0);
            animFrame->setEndValue(rCalendars);
            animTableAll->setEndValue(rTableWidget);

        // prepare for the group animation
            QParallelAnimationGroup* animGroup = new QParallelAnimationGroup;
            animGroup->addAnimation(animFrame);
            animGroup->addAnimation(animTableAll);

        // say what to do at the end of the animation
            connect(animGroup,&QParallelAnimationGroup::finished,this,[this]
            {
                ui->frameCalendars->hide();
                ui->visibilitySlider->show();
                pSliderGrooveFrame->show();
                ui->progressBar->show();

            // do an initial restock to setup the "No warnings.." and "No errors.." rows
            // most likely we have some PAS entries by now since fetchARVData() is already running
                restockAllTableWidgets();
            });

        // make sure the calenders and showAll tablewidget are visible and launch the animation
            ui->frameCalendars->show();
            ui->tableWidgetAll->show();
            ui->tableWidgetWarnings->hide();
            ui->tableWidgetErrors->hide();
            animGroup->start(QAbstractAnimation::DeleteWhenStopped); // (when done also deletes the 2 anim props)
        }
        break;

    case AppState::eRubbernecking :
    // we don't need the progressbar any more, so hide it
        ui->progressBar->hide();

    // hand over the vertical screen real estate owned by the progress bar to the table widgets above it
        auto rTableWidgetAll = mOriginalGeometries[ui->tableWidgetAll]; // reuse this one for all 3
        auto rProgressBar    = mOriginalGeometries[ui->progressBar];

        rTableWidgetAll.setBottom(rProgressBar.bottom());
        rProgressBar.setHeight(0);   // (not really needed since it's hidden now, but can't hurt)

        mOriginalGeometries[ui->tableWidgetAll]      = rTableWidgetAll;
        mOriginalGeometries[ui->tableWidgetWarnings] = rTableWidgetAll;
        mOriginalGeometries[ui->tableWidgetErrors]   = rTableWidgetAll;
        mOriginalGeometries[ui->progressBar] = rProgressBar;

    // do a dummy resize to update the screen now
        auto orgSize = size();
        resize(orgSize.width() + 1,orgSize.height());
        resize(orgSize);
    }

// we've switched ok, set our chap
    appState = newAppState;
}

//----------------------------------------------------------------------------
// refreshVisibilitySlider
//
// 2023-04-17 First version
//----------------------------------------------------------------------------
void MainWindow::refreshVisibilitySlider(TableViewStates e)
{
    auto r = ui->visibilitySlider->geometry();
    int nHeightInPixels = r.height();
    int nWidthInPixels  = r.width() / 2;  // assuming 3 positions (left, middle and right)

// construct and set the style sheet (2 parts, one for the groove and one for the handle)
    ui->visibilitySlider->setStyleSheet(QString(
                          ".QSlider::groove { background: transparent; height: %1px; } "
                          ".QSlider::handle { background: %2; border-radius: 11px; border: 1px solid gray; width: %3px;}")
                          .arg(nHeightInPixels).arg(acVisibilitySliderColors[e].name()).arg(nWidthInPixels));
}

//----------------------------------------------------------------------------
// on_visibilitySlider_valueChanged
//
// 2023-04-21 First version
//----------------------------------------------------------------------------
void MainWindow::on_visibilitySlider_valueChanged(int value)
{
// set the new view state
    auto newTableViewState = static_cast<TableViewStates>(value);

// same as before (i.e. no need to switch)?
    if (tableViewState == newTableViewState)
        return;

// ok update the viewstate
    tableViewState = newTableViewState;

// first change to color of the slider
    refreshVisibilitySlider(tableViewState);

// play hide and show with the table widgets
    switch (tableViewState)
    {
    case eShowAll      :
        ui->tableWidgetAll->clearSelection();   // clear possible prev. selection
        ui->tableWidgetAll->show();

        ui->tableWidgetWarnings->hide();
        ui->tableWidgetErrors->hide();
        break;

    case eShowWarnings :
        ui->tableWidgetAll->hide();

        ui->tableWidgetWarnings->clearSelection();
        ui->tableWidgetWarnings->show();

        ui->tableWidgetErrors->hide();
        break;

    case eShowErrors   :
        ui->tableWidgetAll->hide();
        ui->tableWidgetWarnings->hide();

        ui->tableWidgetErrors->clearSelection();
        ui->tableWidgetErrors->show();
        break;

    default:
        fubar("bad tableViewState");
    }
}

//----------------------------------------------------------------------------
// showProgress updates labelProgress and optionally steps the ProgressBar
//
// 2020-02-18 First version
//----------------------------------------------------------------------------
void MainWindow::showProgress(QString sText, int nBarValue /* = 0 */)
{
    ui->labelProgress->setText(sText);
    if (nBarValue > 0)
        ui->progressBar->setValue(nBarValue);

    qApp->processEvents();
}

//----------------------------------------------------------------------------
// on_pushButtonGo_clicked
//
// 2019-12-10 First version
// 2022-08-26 Check for early morning users selecting yesterday's visits
// 2023-09-17 Warn if first date is more than half a year back
//----------------------------------------------------------------------------
void MainWindow::on_pushButtonGo_clicked()
{
// sanity check the date range (cannot be backwards)
    if (dFrom > dTo)
        guruMeditation("didn't expect dFrom > dTo");

// if yesterday is selected and time is before 7 AM, ask a question
    if (dTo == QDate::currentDate().addDays(-1))
        if (QTime::currentTime().hour() < 7)
            if (!TWUtils::yesNoBox("Innan kl. 07:00 på morgonen syns inte gårdagens pat. personnummer. Ok att fortsätta ändå?"))
                return;

// also check if the first date is too old for payments
    if (dFrom.addMonths(nnMaxMonthsBackForARV) < dTo)
        if (!TWUtils::yesNoBox("ARV-portalen godkänner inte besök äldre än ett halvår. Ok att fortsätta ändå?"))
            return;

// check that we've wired up ok to Intelligence and Samba
    if (AppState::eConnectedOk != appState)
        guruMeditation("appState not eConnectedOk");

// start processing, change title and app state
    if (dFrom == dTo) // same day adventure?
        ui->labelTitle->setText(QString("ARV %1 %2:").arg(TWUtils::getSwedishWeekdayName(dFrom),TWUtils::toISODate(dFrom)));
    else
        ui->labelTitle->setText(QString("ARV fr.o.m. %1 t.o.m. %2:").arg(TWUtils::toISODate(dFrom),TWUtils::toISODate(dTo)));

// here we go, disable this button, hide the calendars and show the table widget
    switchAppState(AppState::eProcessing);

// start the party via a timer
    QTimer::singleShot(nnNormalDelay,this,[this] { fetchARVData(); });
}

//----------------------------------------------------------------------------
// refreshPersonNo
//
// 2023-04-24 First version
// 2023-09-25 Support obfuscated personno (by using testpatients)
//----------------------------------------------------------------------------
void MainWindow::refreshPersonNo()
{
// crawler still crawling?
    if (!scMain.bCrawlingDone)
        return;  // yes see you again

// obfuscation requested?
    if (bObfuscatePersonNo)
    {
    // replace the real personno. in the map with a bunch of test patients
        mPID2PersonNo.clear();  // do a white wedding on the real ones

        static auto slTestPatients = TWTCSupport::getAllTestPatients();
        if (slTestPatients.count() < 1)
            guruMeditation("didn't expect an empty list of test patients");

        for (auto st : mArvEntries)
        // make sure all entries in the easy-peasy map are filled with test patients
            mPID2PersonNo[st.nPID] = slTestPatients[abs(st.nPID) % slTestPatients.count()];
    }

// step thru them all and update the personno.s
    for (auto &st : mArvEntries)    // (& for r/w-access)
    {
    // try the easy map first
        if (mPID2PersonNo.contains(st.nPID))
        {
        // told you it was easy
            st.sPersonNo = mPID2PersonNo[st.nPID];
            continue;
        }

    // try the harder way (all entries have to make it through here at least once)
        if (st.sReceiptNo.isEmpty())  // no dice: need a nonempty receipt no.
            continue;

    // have a TC receiptno. look it up in the crawler's map
        QString sPersonNo = scMain.mRP.value(st.sReceiptNo);
        if (sPersonNo.isEmpty())
            continue;  // not found (probably the TC counter wasn't closed)

    // got a personno. is it any good?
        if (!TWUtils::checkPersonNo(sPersonNo).isEmpty())
            continue;  // some or other error, forget about it

    // ok gotcha
        st.sPersonNo = sPersonNo;

    // store it also in the easy-peasy map (for the next refresh)
        mPID2PersonNo[st.nPID] = sPersonNo;
    }
}

//----------------------------------------------------------------------------
// restockSingleTableWidget
// clear and refill a single table widget
//
// 2023-04-22 First version
//----------------------------------------------------------------------------
void MainWindow::restockSingleTableWidget(TableViewStates eState)
{
// use a helper map to keep the row listed in visit date/time order
    QMap<QString,QString> mVisitDateTime2ARVEntry;

// support scrolling to a "high water mark" to a nonempty ATG code on a new visit day
    static QDate adPrevHighestDates[viewStateCount];

// establish which table widget we're restocking
    QTableWidget* tw = nullptr;
    switch (eState)
    {
    case eShowAll      : tw = ui->tableWidgetAll;      break;
    case eShowWarnings : tw = ui->tableWidgetWarnings; break;
    case eShowErrors   : tw = ui->tableWidgetErrors;   break;
    default :
        guruMeditation("bad eState");
    }

    int nRows = 0;
    QDate dHighestDate;
    int nHighestRow = 0;
    for (auto st : mArvEntries)
    {
    // check for sponges, show them or not?
        if (st.bSpongeNoARV4U)    // sponge?
        {
        // establish visibility for this sponge
            if (st.bOk)
            // for normal sponges only check this boolean
                if (!bShowSponges)
                    continue;

            if (!st.bOk)
            // visibility for error sponges depends on two booleans
                if (!bShowErrorSponges && !bShowSponges)
                    continue;
        }

    // determine entry flavor
        bool bWarning = !st.sWarning.isEmpty();
        bool bError   = !st.bOk;
        if (bError == st.sError.isEmpty())  // both should either be true or false
            guruMeditation("bError and sError mismatch");

    // error trumps warning (i.e. when both are true, don't show this entry as a warning also)
        if (bError)
            bWarning = false;

    // not the ShowAll widget?
        if (eShowWarnings == eState)
            if (!bWarning)  // only warnings please
                continue;

        if (eShowErrors == eState)
            if (!bError)    // only errors please
                continue;

    // ok visible it is, create the key for the helper map
        QString sKey = st.sVisitDateTime + QString::number(nRows);  // (use nRows to make a unique key)
        if (mVisitDateTime2ARVEntry.contains(sKey))                 // so this should never happen
            guruMeditation("Found an existing visit-datetime+rowno. key in refreshTabletWidget (not good)");

    // this is a visible row, is it a row we should scroll to (i.e. make sure is visible)?
        if (!st.sATGCode.isEmpty())
        {
        // yes if this row has a nonempty ATG code remenber the visit date and this row #
            dHighestDate = st.dVisit;
            nHighestRow  = nRows;
        }

    // stuff it with the key to our main map and count the #
        mVisitDateTime2ARVEntry.insert(sKey,st.sKey);   // (map data here is the key to the main map)
        ++nRows;
    }

// start from scratch with a new set of rows
    tw->clearContents();
    tw->setRowCount(nRows);

    if ((nRows > 0) && (eShowAll != eState))
    // we have visible rows, make sure we cancel a possible previous setSpan() for a helper text
        if (tw->columnSpan(0,colDayOfTheWeek) > 1)
            tw->setSpan(0,colDayOfTheWeek,1,1);

// step thru the ARV entries through the visitdatetime map
    int nRow = 0;
    QString sPrevVisitDate;  // helper for deciding if we're to show the day of the week for this row

    for (auto sVisitKey : mVisitDateTime2ARVEntry.keys())
    {
    // resuable chap for those columns that need it
        QString sToolTip;

    // get the ARV entry for this row (and do some sanity checks)
        if (!mVisitDateTime2ARVEntry.contains(sVisitKey))
            fubar(QString("Something is wrong with the visit date/time map ('%1')").arg(sVisitKey));
        QString sKey = mVisitDateTime2ARVEntry[sVisitKey];
        if (!mArvEntries.contains(sKey))
            fubar(QString("Something is wrong with the ARV entries map ('%1')").arg(sKey));

        auto st = mArvEntries[sKey];
        if (st.sKey != sKey)    // these should always match up
            guruMeditation("st.sKey != sKey (not good)");

    // determine error status for this entry
        bool bError   = !st.bOk;
        if (bError == st.sError.isEmpty())  // both should either be true or false
            guruMeditation("bError and sError mismatch");

    // do the columns: first one is the key to this ARV entry in the main map
    // (column has zero width, don't bother with with readonly)
        tw->setItem(nRow,colMapKey,new QTableWidgetItem(sKey));

    // day of the week: show only if this row is the first for a specific day
        bool bShowDOW = (sPrevVisitDate != st.sVisitDate);
        auto twDOW    = new QTableWidgetItem(bShowDOW ? TWUtils::getSwedishWeekdayNameU1(st.dVisit.dayOfWeek()).left(2) : "");
        setTableWidgetItemRO(twDOW);
        twDOW->setTextAlignment(Qt::AlignCenter);
        tw->setItem(nRow,colDayOfTheWeek,twDOW);
        sPrevVisitDate = st.sVisitDate; // remember for the next row :=)

    // visit date and time
        QString sVisitDateTime  = st.sVisitDateTime;         // default: show "YYYY-MM-DD HH:MM"
        if (tNotFoundInPAS == st.tVisit)                     // unless the visit time is missing
            sVisitDateTime = st.sVisitDate + QString(6,' '); // 6 spaces = about the same width as " HH:MM"
        auto twDateTime = new QTableWidgetItem(sVisitDateTime);
        setTableWidgetItemRO(twDateTime);
        twDateTime->setTextAlignment(Qt::AlignCenter);
        tw->setItem(nRow,colVisitDateTime,twDateTime);

    // personno.: if it's empty (i.e. we're still waiting for the main Samba crawler) show "..."
        QString sPersonNo = TWUtils::midLineEllipsis();
        if (!st.sPersonNo.isEmpty())
        // got one, pretty print it
            sPersonNo = TWTCSupport::personNoForTC(st.sPersonNo);

        auto twPersonNo = new QTableWidgetItem(sPersonNo);
        setTableWidgetItemRO(twPersonNo);
        twPersonNo->setTextAlignment(Qt::AlignCenter);
        tw->setItem(nRow,colPersonNo,twPersonNo);

    // ATG code
        auto twATGCode = new QTableWidgetItem(st.sATGCode);
        setTableWidgetItemRO(twATGCode);
        twATGCode->setTextAlignment(Qt::AlignCenter);
        twATGCode->setToolTip(st.sATGCodeToolTip);
        tw->setItem(nRow,colATGCode,twATGCode);

    // ARV rate code
        QString sRateCode = st.sRateCode;
        if (sRateCode != st.sARVRateCode)        // changed by digestVisit() or because this is a companion?
            sRateCode += " " + TWUtils::rightArrow() + " " + st.sARVRateCode; // if so, show both old and new

        auto twRateCode = new QTableWidgetItem(sRateCode);
        setTableWidgetItemRO(twRateCode);
        twRateCode->setTextAlignment(Qt::AlignCenter);
        sToolTip = QString("Pat. betalade %1 kr%2.").arg(st.nFeePaid).arg(st.bEmergencyVisit ? " (akut besök)" : "");
        twRateCode->setToolTip(sToolTip);
        tw->setItem(nRow,colARVRateCode,twRateCode);

    // visit type, is this first visit or a revisit? show empty if no receiptno. found (i.e. no PAS info yet)
        if (st.sReceiptNo.isEmpty())
        {
            auto twVisitType = new QTableWidgetItem("");
            setTableWidgetItemRO(twVisitType);
            twVisitType->setTextAlignment(Qt::AlignCenter);
            tw->setItem(nRow,colVisitType,twVisitType);
        }
        else
        {
            auto twVisitType = new QTableWidgetItem(st.bFirstVisit ? "Nybesök" : "Återb.");
            setTableWidgetItemRO(twVisitType);
            twVisitType->setTextAlignment(Qt::AlignCenter);
            sToolTip = QString("Besökstyp %1 registrerat i kassan").arg(st.sVisitType);

        // for those requiring consultrefs. for new visits: show the consultref. info in the tooltip
            if (bConsultRefs && st.bFirstVisit && (st.dVisit >= dConsultRefsBegin))
            {
                sToolTip = "Remiss saknas"; // sad default
                if ((!st.sRefKombika.isEmpty()) && (st.dRefIssued.isValid()))
                    sToolTip = QString("Remisskombika: %1\nRemissdatum: %2").arg(st.sRefKombika,TWUtils::toISODate(st.dRefIssued));
            }
            twVisitType->setToolTip(sToolTip);
            tw->setItem(nRow,colVisitType,twVisitType);
        }

    // upload ok icon + ARV receipt no.: + diagnoses (or a warning text) or an error text across both columns
    // in the ARV receipt column: upload ok icon + 5-digits or 8-digits ARV receipt no. (and TC's receiptno. as a tooltip)
    // in the warning column: show the warning text (if there is one) else show diagnoses
    // for error entries: show the error message spanning both columns
        if (st.bOk)
        {
        // need to reset the span from a previous warning or error row?
            if (tw->columnSpan(nRow,colARVReceiptOrError) > 1)
                tw->setSpan(nRow,colARVReceiptOrError,1,1);

        // no error, so show the ARV receipt no.
            if (st.nARVReceiptNo < 1000)    // sanity check
                fubar("bad ARV receipt no. (< 1000)");

            QString sARVReceiptNo = QString::number(st.nARVReceiptNo);
            auto twARVReceiptNo = new QTableWidgetItem(iconUploadOk,sARVReceiptNo);
            setTableWidgetItemRO(twARVReceiptNo);
            twARVReceiptNo->setTextAlignment(Qt::AlignCenter);
            twARVReceiptNo->setToolTip("Ok att ladda upp\nKvittonr. i ARV: " + sARVReceiptNo + "\nKvittonr. i TakeCare: " + st.sReceiptNo);
            if (st.bIsCompanion)
                twARVReceiptNo->setFont(fCompanionReceiptNo); // companions have 8 digits no. so use the smaller font flavor
            tw->setItem(nRow,colARVReceiptOrError,twARVReceiptNo);

        // show a warning text or diagnose(s)?
            QString s = st.sWarning;
            if (s.isEmpty())
            // no warning, so show the diagnose(s) instead
                s  = " " + st.slDiagnosis.join("  ");       // show all diagnoses on the same line separated by space
            auto twWarning = new QTableWidgetItem(" " + s); // prefix both warning(s) and diagnoses with a space (looks nicer)
            setTableWidgetItemRO(twWarning);
            twWarning->setTextAlignment(Qt::AlignLeft | Qt::AlignVCenter);
            twWarning->setToolTip(s);
            if (!st.sWarning.isEmpty())
            // color the background (usually yellow) if we have a warning
                twWarning->setBackground(QBrush(acVisibilitySliderColors[eShowWarnings]));
            tw->setItem(nRow,colWarningOrDiagnoses,twWarning);
        }
        else
        {
        // got an error to show, set it to span both columns (i.e. obscuring ARV receipt no. and warnings/diagnoses)
            tw->setSpan(nRow,colARVReceiptOrError,1,2);
            auto twError = new QTableWidgetItem(" " + st.sError); // prefix with a space, I think it looks better
            setTableWidgetItemRO(twError);
            twError->setTextAlignment(Qt::AlignLeft | Qt::AlignVCenter);
            twError->setBackground(QBrush(acVisibilitySliderColors[eShowErrors]));
            twError->setToolTip(st.sError); // can't hurt
            tw->setItem(nRow,colARVReceiptOrError,twError);
        }

    // if we're rubbernecking and have a valid personno, show the jump to TC icon
        if ((AppState::eRubbernecking == appState) && ("" == TWUtils::checkPersonNo(st.sPersonNo)))
        {
            auto twJumpToTC = new QToolButton();
            twJumpToTC->setIcon(iconTC);
            twJumpToTC->setToolTip(QString("Klicka här för att öppna pat. %1 i TakeCare").arg(TWTCSupport::personNoForTC(st.sPersonNo)));
            twJumpToTC->setEnabled(true);
            tw->setCellWidget(nRow,colJumpToTC,twJumpToTC);

        // click handler: try to jump/open TC
            connect(twJumpToTC,&QToolButton::clicked,this,[this,st]
            {
            // get the personno. (or reservno.) for this row, is it a good one?
                QString sPersonNo = st.sPersonNo;
                if (sPersonNo.isEmpty())  // no dice no good
                    return;

            // is a smartcard present? look for a username from the first card (if any) require it to be nonblank
                QString sTCUserName = TWTCSupport::getTCUserNameFromSmartcard(0);
                if (sTCUserName.isEmpty())
                    return TWUtils::warningBox("Hittar inget e-tjänstekort (behövs för att öppna pat. i TakeCare).");

            // try to establish the path to TC
                QString sTCPath = sTCPathOverride;      // if we have a preset path use that
                if (sTCPath.isEmpty())                  // else try to find out right now
                    sTCPath = TWTCSupport::getTCPath();
                if (sTCPath.isEmpty())
                // use this as the last ditch attempt
                    sTCPath = "C:\\TakeCare";

            // prepare the twinkle box time (if TC is not started, increase it)
            // check now before actually launching TC
                int nDelay = nnShortTCDelay;
                if (eTCAppNotStarted == TWTCSupport::getTCAppState())
                    nDelay = nnLongTCDelay;

            // bombs away
                QString sError = TWTCSupport::openTCPersonNo(sTCPath,sTCUserName,sPersonNo);
                if (!sError.isEmpty())
                // something didn't agree with the launching, show it as a warning
                    return TWUtils::warningBox(sError,"Öppna TakeCare");

            // ok TC should be launching now, issue the twinklebox as a comfort for the user
                TWUtils::twinkleBox(QString("Öppnar %1 i TakeCare...").arg(sPersonNo),"Start av TakeCare",nDelay);
            });
        }
        else
        {
            auto twEmpty = new QTableWidgetItem("");
            setTableWidgetItemRO(twEmpty);
            twEmpty->setToolTip("Personnummer saknas eller är felaktigt");
                tw->setItem(nRow,colJumpToTC,twEmpty);
        }

    // that's all for this row, next please
        ++nRow;
    }

// sanity check # of rows
    if (nRow != nRows)
        guruMeditation("nRow != nRows");

// no rows shown? if we're displaying the warning or error widget show a helper text on the first row
    if ((0 == nRows) && (eShowWarnings == eState))
    {
        tw->setRowCount(1);

        tw->setSpan(0,colDayOfTheWeek,1,colCount - colDayOfTheWeek);
        auto tw0 = new QTableWidgetItem("Inga varningar.");

        QFont fntItalic(tw->font());
        fntItalic.setItalic(true);
        tw0->setFont(fntItalic);

        setTableWidgetItemRO(tw0);
        tw->setItem(0,colDayOfTheWeek,tw0);
    }

    if ((0 == nRows) && (eShowErrors == eState))
    {
        tw->setRowCount(1);

        tw->setSpan(0,colDayOfTheWeek,1,colCount - colDayOfTheWeek);
        auto tw0 = new QTableWidgetItem("Inga fel.");

        QFont fntItalic(tw->font());
        fntItalic.setItalic(true);
        tw0->setFont(fntItalic);

        setTableWidgetItemRO(tw0);
        tw->setItem(0,colDayOfTheWeek,tw0);
    }

// finally, should we scroll to a new "high-water" mark i.e. row?
// yes if we stumbled on a new highest date
    if (dHighestDate > adPrevHighestDates[eState])
    {
    // specify the ATG code column for a nice view
        tw->scrollToItem(tw->item(nHighestRow,colATGCode),QAbstractItemView::PositionAtTop);

    // and remember this new "high-water" date
        adPrevHighestDates[eState] = dHighestDate;
    }

// that's all for this restocking, thank you
}

//----------------------------------------------------------------------------
// restockAllTableWidgets
//
// 2023-04-26 First version
// 2023-10-11 Only restock if the calendar widgers are gone/hidden
//----------------------------------------------------------------------------
void MainWindow::restockAllTableWidgets()
{
// calendars still visible (because we're busy scrolling them off the screen)?
    if (ui->frameCalendars->isVisible())
        return; // hold your horses until they're gone

// pretty simple, just call the 3 single chaps
    restockSingleTableWidget(eShowAll);
    restockSingleTableWidget(eShowWarnings);
    restockSingleTableWidget(eShowErrors);
}

//----------------------------------------------------------------------------
// dbError
//
// 2022-08-22 First version
//----------------------------------------------------------------------------
void MainWindow::dbError(QString sError)
{
// if the error is less than 8 chars (probably an error no.) try to prettyprint it
    if (sError.length() < 8)
        sError = TWTCSupport::prettyPrintErrors(sError);

// show an errorbox unless it's the "forcibly closed" error
    if ("db forcibly closed" != sError)
        TWUtils::errorBox(sError);

// say goodbye to a cruel world (all db errors are considered fatal)
// cannot use qApp->exit() because it will resume/continue our main (GUI) thread
    TWUtils::ignominiousExit();
}

//----------------------------------------------------------------------------
// fetchARVData (process PAS, journals, diagnoses and optionally consultrefs
//
// 2022-08-24 First version
//----------------------------------------------------------------------------
void MainWindow::fetchARVData()
{
// user has pressed the Go button
// launch the main Samba crawler since we know now how many days to go back
    auto nDaysBack = dFrom.daysTo(QDate::currentDate()) + 3; // overshoot a couple of days (to be on the safe side)

    if (sLocalDirectory.isEmpty())
    // no local directory set for Samba files? on Windows we support a remote connection as well
        scMain.setSambaCredentialsFromIniFile();
    else
        scMain.setLocalDirectory(sLocalDirectory);

// wire up the main Samba crawler done signal
    connect(&scMain,&SambaCrawler::crawlingDone,this,[this] (int nMapCount)
    {
        Q_UNUSED(nMapCount);

    // if there's an error I'm afraid we have to call it a day
        auto sError = scMain.sErrorMsg;
        if (!sError.isEmpty())
        {
            TWUtils::errorBox(sError);
            TWUtils::ignominiousExit();
        }

    // no error(s), is the app waiting for us?
        if (bWaitingForSamba)
        // yes, do the final step from here (where the ARV file also is written)
            burialAtSea();
    });

// reset our personno. map
    mPID2PersonNo.clear();

// and start the our main Samba crawler asynchronously
    scMain.crawl(nCompanyCode,nDaysBack);
    bWaitingForSamba = false;   // assume the Samba main crawler will finish before Intelligence (almost always true)


// -----------------------------------------------------------------------
// --- Intelligence time: start with PAS ---------------------------------
    showProgress("Hämtar besöken från kassan...",5);

    mArvEntries.clear();    // prepare ourselves for the PAS retrieval
    slErrors.clear();       // possible error(s) go here

// cook our own bobbytable: select from both PAS and PAS_Billing simultaneously
    TCBobbyTables stPASAndPASBilling("PAS as pa,PAS_Billing as pb");

// search in PAS and PAS_Billing (note: the search term CompanyID actually means CompanyCode)
// note: the convert(int,PatientFee) is for https://bugreports.qt.io/browse/QTBUG-108912
// also: don't bother to filter out PAS.RegistrationStatus > 1 or PAS.IsInpatient = true (don't see them anyway for ARV users)
    QString sTerms = "pa.PatientID,pa.DocumentID,StartDatetime,ReceiptNumber,VisitTypeID,EmergencyID,RateID,convert(int,PatientFee) as PatientFee";
    if (bConsultRefs)
    // note: using ReferringKombika from PAS for the consultrefs is of limited value since there's no Referring Date to be found in PAS
        sTerms += ",AppointmentID";           // useful when locating consultrefs (via the ConsultRefs table)
    if (bLocumTenensActive)
        sTerms += ",AppointmentWithUserName"; // for matching with receipts should be sponges or the real thing

    db.select(sTerms,stPASAndPASBilling,
              QString("IsRetail = 0 and IsCancelled = 0 and CompanyID = %1 and SavedAtCareUnitID = %2 and EconomicalInKombika = '%3' and "
                      "StartDatetime >= '%4' and StartDatetime < '%5' and pa.PatientID = pb.PatientID and pa.DocumentID = pb.DocumentID").
                       arg(nCompanyCode).arg(nCareUnitID).arg(sKombika,TWUtils::toISODate(dFrom),TWUtils::toISODate(dTo.addDays(1))),
    [this](DBRowStruct rs)
    {
    // remember this lambda is running on another thread (don't forget: no user interaction!)
        ARVEntryStruct st;

    // got a new chap for you
        st.bOk             = true;  // optimistic default
        st.sError          = "";
        st.sWarning        = "";
        st.nPID            = rs.longlongValue("PatientID");
        st.nDocumentID     = rs.intValue("DocumentID");
        st.bSpongeNoARV4U  = false;  //      - " -
        st.dVisit          = rs.dateValue("StartDatetime");
        st.sVisitDate      = TWUtils::toISODate(st.dVisit);
        st.tVisit          = rs.timeValue("StartDatetime");
        st.sVisitDateTime  = st.sVisitDate + " " + TWUtils::toHHMM(st.tVisit);

        st.sReceiptNo      = rs.stringValue("ReceiptNumber");
        st.sVisitType      = rs.stringValue("VisitTypeID");
        st.bFirstVisit     = ("0" == st.sVisitType);     // treat all other visit types as revisits
        st.bEmergencyVisit = ("1" == rs.stringValue("EmergencyID"));  // ("1" = akut, "2" = normal)
        st.sRateCode       = rs.stringValue("RateID");
        st.nFeePaid        = rs.intValue("PatientFee");
        st.nARVReceiptNo   = 0;
        st.sARVRateCode    = "";
        st.sCareProvider   = "";
        if (bLocumTenensActive)
            st.sCareProvider = rs.stringValue("AppointmentWithUserName");
        st.sPersonNo       = "";

        st.nAppointmentID  = 0;
        st.sRefKombika     = "";
        if (bConsultRefs)
            st.nAppointmentID = rs.intValue("AppointmentID");    // useful for finding consultrefs

        st.sATGCode        = "";
        st.bIsCompanion    = false;

    // evaluate this visit (good or not?)
        TCVisitStruct v;
        v.dVisit      = st.dVisit;
        v.sVisitType  = st.sVisitType;
        v.sRateCode   = st.sRateCode;
        v.nAgeInYears = -1;   // we don't know at this point
        v.nFeePaid    = st.nFeePaid;
        v.bLenient    = false;
        v = TWTCSupport::digestVisit(eCustomerType,v);

    // store the digest results in our ARV entry
        st.bOk            = v.bOk;
        st.bSpongeNoARV4U = !v.bARVMaterial;   // not for ARV? keep it around to absorb any matching journal entries (soaking up errors)
        st.sError         = v.sError;
        if (st.bSpongeNoARV4U)
            if (st.sWarning.isEmpty())
                st.sWarning = "Skall ej laddas upp till ARV";

        st.sARVRateCode   = v.sRateCodeForARV;  // (digestVisit() converts some rate codes for ARV)

    // is this a test (not a bona fide) patient?
        if (TWTCSupport::isTestPatient(st.nPID))
        {
            st.bOk    = false;
            st.sError = "Testpatient (laddas ej upp till ARV)";
        }

    // transmogrify the receipt no. from TC into a more compact int (max 8 digits) for our use in ARV
    // note: this ARV receipt no. is used for the non-companion ARV code, so do this again later for companions
        int nARVReceiptNo = TWTCSupport::receiptNo2ARVReceiptNo(st.sReceiptNo);
        if (0 == nARVReceiptNo)
        // there was something wrong with the receipt (shouldn't happen)
            slErrors += QString("Felaktigt kvittonr '%1'").arg(st.sReceiptNo);

        st.nARVReceiptNo = nARVReceiptNo;

    // unexpected fee? if so add a warning
        int nPaid       = st.nFeePaid;
        int nDefaultFee = TWTCSupport::getDefaultFee(st.dVisit);
        if (v.nExpectedFee != nPaid)
        {
        // show a warning for mismatching fees? yes unless the expected fee is -1 (means digestVisit() does not know)
            if (v.nExpectedFee >= 0)
                st.sWarning = QString("%1 kr betalt, borde vara %2 kr").arg(nPaid).arg(v.nExpectedFee);

        // for normal visits and for fees less than the default one instead suggest another ratecode (the "reduced ones")
            if ((eNormalVisitFlavor == v.evf) && (nPaid > 0) && (nPaid < nDefaultFee))
            {
            // calc. differently for visits before January 1st 2024
                int nSuggestedCode = 20 + nPaid / ((st.dVisit < QDate(2024,01,01)) ? 10 : 5);
                st.sWarning = QString("Taxa %1, pat. betalade %2 kr borde vara taxa %3").arg(st.sARVRateCode).arg(nPaid).arg(nSuggestedCode);
            }
        }

     // for visits that are distance-flavored that paid a fee, issue a warning
        if ((eDistanceContactVisitFlavor == v.evf) && (nPaid > 0))
            if (nPaid < nDefaultFee)    // skip the warning if full/default fee was paid (probably just a typing error)
                st.sWarning = QString("Avgift %1 kr är ej tillämplig för telefonrådgivning").arg(st.nFeePaid);

     // or perhaps an unexpected visit flavor? currently we're checking for visits of type 3 (that are not private visits)
        if (eOOBVisitFlavor == v.evf)   // digestVisit() thinks this is an "out-of-band" visit
            if ("3" == st.sVisitType)
            {
            // 2023-10-06 Always mark this as an error (toss "TossVisitType3"/bBypassVisitsType3 option in ROSettings.ini)
                st.bOk    = false;
                st.sError = "Besökstyp 3 (\"Ej vård\") laddas ej upp till ARV";
            }

    // time to save in our map, create the key for this visit
        st.sKey = st.createKey(st.dVisit,st.nPID,false);  // no ATG codes yet, so no companions yet

    // have a PAS entry for this visit date already?
        if (mArvEntries.contains(st.sKey))
        {
        // flag this visit as a duplicate receipt
            st.bOk    = false;
            st.sError = QString("Dublettkvitto i kassan nr. %1 (överhoppat)").arg(st.sReceiptNo);

        // and make a new (hopefully unique) key so we can store it as an error entry
            st.sKey = st.createKeyForErrorEntries(st.dVisit,st.nPID,false);
        }

    // make sure we have a unique key
        if (mArvEntries.contains(st.sKey))
        // this shouldn't happen :-)
            slErrors += QString("Kunde inte spara besöket med kvittonr %1 (för många dubletter)").arg(st.sReceiptNo);

    // ok, store this entry
        mArvEntries.insert(st.sKey,st);
    });

// --- done retrieving from PAS --------------------------------------------------------
    restockAllTableWidgets();   // show what we've got so far

// if locum tenens is active, step thru and turn those found in the skipover list into sponges
    if (bLocumTenensActive)
    {
        QString sProgress = bIsLocumTenens ? "Sorterar bort ej vikariebesök (%1%)..." : "Sorterar bort vikariebesök (%1%)...";

        int nNo    = 0;    // for showProgress()
        int nTotal = mArvEntries.count();

        for (auto &st : mArvEntries)    // need & for r/w access
        {
            ++nNo;

        // process error entries as well (but we can skip over visits that are already sponges)
            if (st.bSpongeNoARV4U)
                continue;

        // have a care provider already (from PAS AppointmentWithUserName)?
            QString sCareProvider = st.sCareProvider;
            if (sCareProvider.isEmpty())
            {
            // no so instead we'll use the CareProviderID from the PAS_Billing_Providers table as the CareProvider
            // show some progress (8% - 10%)
                showProgress(QString(sProgress).arg((nNo * 100) / nTotal),8 + ((nNo * 2) / nTotal)); // (nTotal > 0)

                db.select("CompanyID,CareProviderID",stTCCorral.PAS_Billing_Providers,
                         QString("Row = 1 and PatientID = %1 and DocumentID = %2").arg(st.nPID).arg(st.nDocumentID),  // look only in 1st row
                [this,&sCareProvider](DBRowStruct rs)
                {
                // duplicates shouldn't occur but if they do, they're harmless (just overwrite prev. entry)
                    if (nCompanyCode != rs.intValue("CompanyID"))  // sanity check (yes Intelligence CompanyID means CompanyCode)
                        slErrors += QString("Felaktig företagskod %1, skall vara %2)").arg(rs.intValue("CompanyID")).arg(nCompanyCode);

                    sCareProvider = rs.stringValueTrimmed("CareProviderID");
                });

            // got 2 digits? (other variations might occur but likely never for LOLs/LOFs)
                if ((sCareProvider.length() != 2) || (sCareProvider != TWUtils::tossAllButDigits(sCareProvider)))
                {
                    st.bOk    = false;
                    st.sError = "Yrkeskategori ej angiven (eller är felaktig)";
                    continue;
                }

            // don't bother to update the ARV entry with this CareProviderID (it's only used here anyway)
            }

        // is the CareProvider in our lists?
            if (slPASProcess.contains(sCareProvider))
            {
            // yes, proceed with this one as a normal/non locum tenens chap
                continue;
            }

            if (slPASSkipOver.contains(sCareProvider))
            {
            // mark this one as a sponge
                st.bSpongeNoARV4U = true;
                continue;
            }

        // if we get here then the careprovider was not in the lists, that's bad :-(
            st.bOk    = false;
            st.sError = QString("Okänd eller felaktig yrkeskategori/vårdgivare '%1'").arg(sCareProvider);
        }

    // refresh since we probably have a lesser # of entries after the locum tenens culling
        restockAllTableWidgets();
    }

// got any errors? they are considered fatal, just grab the first one and fubar() out
    if (slErrors.count() > 0)
        fubar("Fel vid hämtning av kassa: " + slErrors[0]);

// see how many ok visits we found in PAS
    int nVisits = 0;
    for (auto st : mArvEntries)
    {
    // skip over error entries
        if (!st.bOk)
            continue;

    // and skip over sponges if they're not visible
        if (st.bSpongeNoARV4U)
            if (!bShowSponges)
                continue;

    // count 'em
        ++nVisits;
    }

// step up the progressbar and show what we've got so far
    showProgress(QString("Hittade %1 besök i kassan, hämtar ARV åtgärdstyper från journalerna...").arg(nVisits),10);


// -----------------------------------------------------------------------
// --- next up: looking for journals having ARV codes --------------------
    QMap<QString,int> mPIDDocs;  // for locating the highest/current version for patientid, documentid pairs

// cast a wide net: retrieve all journals (patientids + documentids) within the specified date range
// this will most likely return too many hits/false positives (since highest version for a documentid can exist outside our date range)
    db.select("distinct PatientID,DocumentID",stTCCorral.CaseNotes,
              QString("CreatedAtCareUnitID = %1 and EventDate >= '%2' and EventDate < '%3'").
              arg(nCareUnitID).arg(TWUtils::toISODate(dFrom),TWUtils::toISODate(dTo.addDays(1))),
    [&mPIDDocs](DBRowStruct rs)
    {
    // duplicates? shouldn't occur, but if they do, they'll just overwrite each other in the map
        mPIDDocs.insert(TWUtils::stringList2CommaList({rs.stringValue("PatientID"),rs.stringValue("DocumentID")}), 0);  // 0 = placeholder version no.
    });
    showProgress(QString("Kontrollerar %1 journaler...").arg(mPIDDocs.count()),12);

// store valid journal entries in a map (sorted on the visit date + patientid + documentid)
    struct JournalEntryStruct
    {
        QDate dEvent;
        PID_t nPID;
        int   nDocumentID;
        int   nVersion;
    };
    QMap <QString,JournalEntryStruct> mJournalEntries;

// --- step thru and retrieve the highest version no. for each PatientID, DocumentID pair --------
    int nNo    = 0; // for showProgress()
    int nTotal = mPIDDocs.count();

    for (auto sKey : mPIDDocs.keys())
    {
    // decompose the key back into a PID and a documentID
        auto sl = TWUtils::commaList2StringList(sKey);
        if (sl.count() != 2)
            fubar("Something is seriously wrong with the PIDDocs map");
        PID_t nPID      = sl[0].toLongLong();
        int nDocumentID = sl[1].toInt();

        QDate dEvent;   // to be set by the lambda

    // look for the highest version no. (i.e. currently valid one) for this DocumentID
        db.select("top 1 Version,EventDate",stTCCorral.CaseNotes,
                  QString("CreatedAtCareUnitID = %1 and PatientID = %2 and DocumentID = %3").
                  arg(nCareUnitID).arg(nPID).arg(nDocumentID),"Version desc",
        [this,sKey,&dEvent,&mPIDDocs](DBRowStruct rs)
        {
        // is the highest version of this documentID within our date range?
            dEvent = rs.dateValue("EventDate");
            if ((dEvent >= dFrom) && (dEvent < dTo.addDays(1)))
            {
                int nVersion = rs.intValue("Version"); // yes, within our range so keep it
                if (nVersion < 1)
                    fubar("Got a bad version number (lower than 1) from Intelligence");

                mPIDDocs[sKey] = nVersion;  // refresh/update the version no.
            }
        },eDBRowsExpectExactlyOne);

    // is this version inside our date range?
        int nVersion = mPIDDocs[sKey];
        if (0 == nVersion)
            continue;   // no, still only the placeholder, next please

    // yes it's inside the date range, but check for test patients
        if (TWTCSupport::isTestPatient(nPID))
            continue;   // skip over test patients

    // ok we found a casenote we need to check out, save it in our journal map
        QString sJournalKey = TWUtils::toYMD(dEvent) + TWTCSupport::PID2String(nPID) + QString::number(nDocumentID);
        if (mJournalEntries.contains(sJournalKey))
        // this shouldn't happen
            guruMeditation(QString("got a duplicate journal entry, key = '%1'").arg(sJournalKey));

        mJournalEntries.insert(sJournalKey,{ dEvent, nPID, nDocumentID, nVersion});

    // show the user we're making some progress (start at total progress = 15%)
        ++nNo;
        showProgress(QString("Hämtar journaler (%1%)... ").arg((nNo * 100) / nTotal),15 + ((nNo * 15) / nTotal));   // (nTotal is always > 0)
    }

// --- got some journal entries, step thru them (in ascending date order) -------------------------------
    nNo    = 0; // for showProgress() (starting at total progress = 30%)
    nTotal = mJournalEntries.count();

    for (auto stJournal : mJournalEntries)
    {
        QString sCaption = bDiagnoses ? "Hämtar ARV åtgärder och diagnoser från %1..." : "Hämtar ARV åtgärder från %1...";
        int nProgress = bConsultRefs ? 50 : 65; // for consultrefs stop at 80 else 95
        showProgress(sCaption.arg(TWUtils::toISODate(stJournal.dEvent)),30 + ((nNo++ * nProgress) / nTotal));  // (nTotal is always > 0)

    // get all ATG codes in CaseNotes_ValueTerms for this journal entry
    // more than one journal can contain (perhaps identical) ARV codes for the same PID and date, so we have to deduplicate
        QList<TTStruct> slTT;  // collect 'em here

        db.select("ValueTermID",stTCCorral.CaseNotes_ValueTerms,
                  QString("PatientID = %1 and DocumentID = %2 and Version = %3 and KeywordTermID = %4").
                  arg(stJournal.nPID).arg(stJournal.nDocumentID).arg(stJournal.nVersion).arg(nKeywordTermID),
        [this,&slTT](DBRowStruct rs)
        {
        // get the termid
            int nValueTermID = rs.intValue("ValueTermID");

            TTStruct stNew;
            bool bFound = false;    // pessimistic default

        // step thru the correct table (either LOL or LOF flavor)
            if (eLOLCustomerType == eCustomerType)
                for (auto stTT : astLOL)
                    if (stTT.nValueTermID == nValueTermID)
                    {
                        stNew  = stTT;
                        bFound = true;
                        break;
                    }

            if (eLOFCustomerType == eCustomerType)
                for (auto stTT : astLOF)
                    if (stTT.nValueTermID == nValueTermID)
                    {
                        stNew  = stTT;
                        bFound = true;
                        break;
                    }

        // if termid wasn't found: panic (we *require* that all possible ARV values are present in our table)
            if (!bFound)
                fubar("Could not find ARV ValueTermID %d",nValueTermID);

        // got an ATG code, keep it unless we have the same code already (yes deduplicate, baby)
            bool bGotItAlready = false;
            for (auto stTT : slTT)
            // step thru the previous hits (if any)
                if (stTT.sATGCode == stNew.sATGCode)
                {
                    bGotItAlready = true;
                    if (stTT.nValueTermID != stNew.nValueTermID)
                    // do a sanity check, these should always match
                        guruMeditation("ARV ValueTermID mismatch (%d, %d)",stTT.nValueTermID,stNew.nValueTermID);
                    break;
                }

        // ok seems fine, save it? yes unless this is a duplicate
            if (!bGotItAlready)
                slTT.append(stNew);
        }); // can be several valueterms in one journal (so no row expectation can be set)

    // ------------------------------------------------------------------------------------------------------
    // step thru all ATG codes we found for the current journal (normally 1, can be 2 if there's a companion)
        for (auto stTT : slTT)
        {
        // if locum tenens is active, introduce a possibility to bypass an error, i.e not creating an error entry
            auto isLocumTenensIgnoramus = [this] (JournalEntryStruct stJournal)
            {
            // locum tenens active?
                if (!bLocumTenensActive)
                // no dice, need to flag this entry as an error
                    return false;

            // get the signed user id, or if that is empty, get the saved user id
                QString sUserID;
                db.select("SavedByUserID,SignedByUserID",stTCCorral.CaseNotes,
                          QString("PatientID = %1 and DocumentID = %2 and Version = %3").
                          arg(stJournal.nPID).arg(stJournal.nDocumentID).arg(stJournal.nVersion),
                [&sUserID](DBRowStruct rs)
                {
                    sUserID = rs.stringValueTrimmed("SignedByUserID");
                    if (sUserID.length() < 2)  // kinky comparison for detecting null entries disguised as "0"
                        sUserID = rs.stringValueTrimmed("SavedByUserID");
                },eDBRowsExpectExactlyOne);

                if (!sUserID.isEmpty()) // should never be empty, can't hurt to check anyway
                    if (slIgnorePASMissing.contains(sUserID))
                        return true;    // yes this is an ignoramus, just let it be (no error)

            // default: create an error for this entry
                return false;
            };

        // use this handy lambda for creating a "missing from PAS" error entry
            auto createMissingInPASEntry = [this] (JournalEntryStruct stJournal, TTStruct stTT)
            {
                ARVEntryStruct st;
                st.bOk             = false;
                st.sError          = "Saknar registrering i kassan";
                st.sWarning        = "";
                st.nPID            = stJournal.nPID;
                st.bSpongeNoARV4U  = false;
                st.dVisit          = stJournal.dEvent;
                st.sVisitDate      = TWUtils::toISODate(stJournal.dEvent);
                st.tVisit          = tNotFoundInPAS;    // "magic" time of the day 23:59
                st.sVisitDateTime  = st.sVisitDate + " " + TWUtils::toHHMM(st.tVisit);

                st.sReceiptNo      = "";
                st.sVisitType      = "";
                st.bFirstVisit     = false;
                st.bEmergencyVisit = false;
                st.sRateCode       = "";
                st.nFeePaid        = 0;
                st.nARVReceiptNo   = 0;
                st.sARVRateCode    = "";
                st.sPersonNo       = "";
                st.nAppointmentID  = 0;
                st.sRefKombika     = "";

                st.sATGCode        = stTT.sATGCode;
                st.bIsCompanion    = (TermFlavors::eCompanion == stTT.stAttr.eTermFlavor);
                st.sATGCodeToolTip = stTT.sDescription;

                st.sKey            = st.createKeyForErrorEntries(st.dVisit,st.nPID,st.bIsCompanion);

            // insert it and we're done
                mArvEntries.insert(st.sKey,st);
            };

        // so is this a normal ATG code or a companion? process companions first
            if (TermFlavors::eCompanion == stTT.stAttr.eTermFlavor)
            {
            // got a companion ATG code, is there an existing companion-flavored ARV entry?
                QString sCompanionKey = ARVEntryStruct::createKey(stJournal.dEvent,stJournal.nPID,true); // true --> companion key
                if (mArvEntries.contains(sCompanionKey))
                {
                // indeed, we have an existing ARV entry, does it have exactly the same companion ATG code?
                    if (stTT.sATGCode == mArvEntries[sCompanionKey].sATGCode)
                        continue;   // yes it's a duplicate, skip to the next ATG code

                // not a twin, so is it a sponge?
                    if (mArvEntries[sCompanionKey].bSpongeNoARV4U)
                        continue;   // yes, toss this one and skip to the next ATG code

                // ouch we're another ATG companion, create a new error entry (there can only be one companion entry per day and patient)
                    ARVEntryStruct stError  = mArvEntries[sCompanionKey];   // clone from the existing
                    if (stError.sATGCode.isEmpty())
                    // the case of an original "virgin" ARV entry fresh from PAS should not occur for companions, verify that
                        guruMeditation(QString("didn't expect an empty ATG code in an existing companion ARV entry (%1)").arg(sCompanionKey));

                    stError.bOk             = false;
                    stError.bSpongeNoARV4U  = false;
                    stError.sError          = "Dublett ARV-åtgärd (överhoppad)";
                    stError.sATGCode        = stTT.sATGCode;
                    stError.bIsCompanion    = true;
                    stError.sATGCodeToolTip = stTT.sDescription;
                    stError.sKey            = stError.createKeyForErrorEntries(stError.dVisit,stError.nPID,true); // true --> companion
                    mArvEntries.insert(stError.sKey,stError);

                // next ATG code please
                    continue;
                }

            // we're the first (companion for this patient and visit day), is there a matching non-companion entry created when reading PAS?
                QString sARVKey = ARVEntryStruct::createKey(stJournal.dEvent,stJournal.nPID,false); // set bIsCompanion false
                if (!mArvEntries.contains(sARVKey))
                {
                // no, it's missing in PAS, create an ARV error entry? yes unless this is a locum tenens ignoramus
                    if (isLocumTenensIgnoramus(stJournal))
                        continue;

                // no, nothing from PAS to make a clone from, create an ARV error entry ("missing in PAS")
                // (note: this error entry will be stored with bIsCompanion set, i.e. not touching any non-companion entry/entries)
                    createMissingInPASEntry(stJournal,stTT);

                // next ATG code please
                    continue;
                }

            // --- ok we have a kosher companion ARV entry, but don't mess with any non-companion ARV entries ---
            // instead: clone the original PAS entry, update and save it with this new companion ATG code
                ARVEntryStruct stNew  = mArvEntries[sARVKey];

                stNew.sATGCode        = stTT.sATGCode;
                stNew.bIsCompanion    = true;
                stNew.sATGCodeToolTip = stTT.sDescription;

                stNew.sKey            = sCompanionKey;  // don't forget to update the key!

            // some special treatment: if fee paid > 0, change the ARV rate code to a harmless code with fee = 0
                if (stNew.nFeePaid > 0)
                {
                    stNew.nFeePaid     = 0;  // note: don't change the original rate code so we can display both
                    stNew.sARVRateCode = ssCompanionRateCode;

                // if there is a warning for a bad fee (wrong rate code etc.) toss it since we switched to a free tier
                    if (stNew.sWarning.contains(" kr "))  // (pretty lame test for bad fees but these are only warning texts)
                        stNew.sWarning = "";
                }

            // also create a new ARV receipt no (8 digits, companion flavored)
                stNew.nARVReceiptNo = TWTCSupport::receiptNo2ARVReceiptNo(stNew.sReceiptNo,stNew.sATGCode);

            // sanity check (this entry should not exist before)
                if (mArvEntries.contains(stNew.sKey))
                    guruMeditation("cloning into a companion but it existed already");

            // bombs away
                mArvEntries.insert(stNew.sKey,stNew);

            // next ATG code please
                continue;
            }

        // ------- processing normal (non-companions) ---------
        // do we have an existing, matching ARV entry?
            QString sARVKey = ARVEntryStruct::createKey(stJournal.dEvent,stJournal.nPID,false);  // false for non-companion
            if (!mArvEntries.contains(sARVKey))
            {
            // no, it's missing in PAS, create an ARV error entry? yes unless this is a locum tenens ignoramus
                if (isLocumTenensIgnoramus(stJournal))
                    continue;

             // create an ARV error entry ("missing in PAS")
                createMissingInPASEntry(stJournal,stTT);

            // next ATG code please
                continue;
            }

        // ok we have an existing ARV entry, does it have exactly the same ATG code?
            if (stTT.sATGCode == mArvEntries[sARVKey].sATGCode)
                continue;   // yes it's a duplicate, skip to next ATG code

        // not a twin, is it a sponge?
            if (mArvEntries[sARVKey].bSpongeNoARV4U)
                continue;   // yes so just ignore this one and skip to next ATG code

        // not a twin, is it the "virgin" entry from PAS? i.e. no ATG code written yet?
            if (mArvEntries[sARVKey].sATGCode.isEmpty())
            {
            // we got the virgin, all good so store our non-companion ATG code here
                mArvEntries[sARVKey].sATGCode        = stTT.sATGCode;
                mArvEntries[sARVKey].bIsCompanion    = false;  // (not a companion code)
                mArvEntries[sARVKey].sATGCodeToolTip = stTT.sDescription;

            // we're done, next ATG code please
                continue;
            }

        // no we're not the first, this is an extra/duplicate ATG code
        // clone from the first one into a new error entry
            ARVEntryStruct stError  = mArvEntries[sARVKey];
            stError.bOk             = false;
            stError.bSpongeNoARV4U  = false;
            stError.sError          = "Dublett ARV-åtgärd (överhoppad)";
            stError.sATGCode        = stTT.sATGCode;
            stError.bIsCompanion    = false;
            stError.sATGCodeToolTip = stTT.sDescription;
            stError.sKey            = stError.createKeyForErrorEntries(stError.dVisit,stError.nPID,false); // false -> not a companion
            mArvEntries.insert(stError.sKey,stError);

        // next ATGCode for this journal, please
        }

    // -- done with the ATG code(s), search for diagnoses for this journal entry (do this only for non-companions)
        QString sARVKey = ARVEntryStruct::createKey(stJournal.dEvent,stJournal.nPID,false); // false for non-companion
        if ((bDiagnoses) && (mArvEntries.contains(sARVKey)))    // only do it if diagnoses are requested
            if (mArvEntries[sARVKey].bOk)                       // and this is an error-free entry
            {
                QStringList slDiagnoses;

            // get them all in one fell swoop
                db.select("Code",stTCCorral.CaseNotes_RegistryCodes,
                          QString("PatientID = %1 and DocumentID = %2 and Version = %3 and CodeTable is not null").
                          arg(stJournal.nPID).arg(stJournal.nDocumentID).arg(stJournal.nVersion),
                [&slDiagnoses](DBRowStruct rs)
                {
                    QString sD = rs.stringValue("Code").trimmed();
                    sD.truncate(5); // ARV does not like diagnose codes longer than 5 letters and digits

                // append it to the list
                    slDiagnoses += sD;

                }); // (can be several diagnoses for one journal entry)

            // save what we got (with deduplication)
                for (auto sD : slDiagnoses)
                    if (mArvEntries[sARVKey].slDiagnosis.count() < 99)       // max 99 (ARV limitation)
                        if (!mArvEntries[sARVKey].slDiagnosis.contains(sD))  // insert only new/unique ones
                            mArvEntries[sARVKey].slDiagnosis += sD;

            // do we have a companion ARV entry as well? if so, store the same diagnoses there
                QString sARVCompanionKey = ARVEntryStruct::createKey(stJournal.dEvent,stJournal.nPID,true);
                if (mArvEntries.contains(sARVCompanionKey))
                    mArvEntries[sARVCompanionKey].slDiagnosis = mArvEntries[sARVKey].slDiagnosis;
            }


    // have a new entry (or more than one) now is a good time to refresh the personno.
        refreshPersonNo();

    // have more stuff to show so update the table widgets
        restockAllTableWidgets();

    // and give me next journal please
    }

// -----------------------------------------------------------------------
// done with the journals, check for new visits that needs consultrefs?
// (progress is at 80% go to 95% after consultrefs is done)
    int nNoOfFirstVisits = 0;
    if (bConsultRefs)
    {
    // consultrefs enabled, so step thru and see how many first visits we have
        for (auto st : mArvEntries)
            if (st.bOk) // don't count error entries (they will not be uploaded anyway)
                if (st.bFirstVisit) // only first visits
                    if (st.dVisit >= dConsultRefsBegin) // and only for days when consultrefs is required
                        ++nNoOfFirstVisits;

        QString sCaption = "Hittade inga nybesök som behöver remisser";
        if (1 == nNoOfFirstVisits)
            sCaption = "Hämtar remissen för ett nybesök...";
        if (nNoOfFirstVisits > 1)
            sCaption = QString("Hämtar remisser för %1 st. nybesök...").arg(nNoOfFirstVisits);

        showProgress(sCaption,80);
    }

// prepare for fetching
    nNo    = 0;    // for showProgress
    nTotal = nNoOfFirstVisits;

// any consultref(s) to retrieve today?
    if (nTotal > 0)
    {
    // step thru and look for them
        for (auto &st : mArvEntries)
        {
            if (!st.bOk)         // only do this for healthy chaps
                continue;

            if (!st.bFirstVisit) // and for new visits
                continue;

            if (st.dVisit < dConsultRefsBegin)
                continue;        // and skip this if we're before the first day for consultrefs

        // show something's cooking
            showProgress(QString("Hämtar remisser för nybesök %1... ").arg(st.sVisitDate),80 + ((nNo++ * 15) / nTotal));   // (nTotal is always > 0)

        // we want to obtain a DocumentID for a lookup in the ConsultRefs table
            int nDocumentID = 0;

        // first try it the easy way (if have a nonzero appointmentid from PAS)
            if (st.nAppointmentID > 0)
                db.select("top 1 ReferralDocumentID",stTCCorral.Appointments,
                          QString("PatientID = %1 and AppointmentID = %2").arg(st.nPID).arg(st.nAppointmentID),
                          "Row desc",   // in case there are more than one row: get the last one
                [&nDocumentID](DBRowStruct rs)
                {
                    nDocumentID = rs.intValue("ReferralDocumentID");
                },eDBRowsExpectZeroOrOne);

        // or try the harder way?
            if (0 == nDocumentID)
            // the hard way: look for the most recent incoming consultref for this PatientID (actually the one with the highest DocumentID, should be the same)
            // it has to have a nonempty ReferringCareUnitKombika and the receiving careunit has to be our careunit
            // also skip over those consultrefs that are issued by the kombika for this careunit (i.e. bypass consultrefs to yourself)
            // and finally: the referral date cannot be later than the visit date
            // maybe later: add check that referral date is not before the patient is born :-)
                db.select("top 1 DocumentID",stTCCorral.ConsultRefs,
                          QString("PatientID = %1 and ReferringCareUnitKombika is not null and "
                                  "RecipientCareUnitID = %2 and ReferringCareUnitKombika <> '%3' and ReferralDate <= '%4'").
                          arg(st.nPID).arg(nCareUnitID).arg(sKombika,st.sVisitDate),"DocumentID desc",
                [&nDocumentID](DBRowStruct rs)
                {
                    nDocumentID = rs.intValue("DocumentID");
                },eDBRowsExpectZeroOrOne);

        // got something (the easy or the hard way)?
            if (0 == nDocumentID)
            {
            // no dice, bail
                st.bOk    = false;
                st.sError = "Nybesök saknar remiss";

                continue;   // next please
            }

        // ok we have a documentid, go fishing for the consultref
            QString sRefKombika;
            QDate dRefIssued;
            db.select("top 1 with ties RecipientCareUnitID,PatientID,DocumentID,ReferringCareUnitKombika,ReferralDate",stTCCorral.ConsultRefs,
                      QString("RecipientCareUnitID = %1 and PatientID = %2 and DocumentID = %3").arg(nCareUnitID).arg(st.nPID).arg(nDocumentID),
                      "row_number() over (partition by RecipientCareUnitID,PatientID,DocumentID order by Version desc)",
            [&sRefKombika,&dRefIssued](DBRowStruct rs)
            {
                sRefKombika = rs.stringValueTrimmed("ReferringCareUnitKombika");
                dRefIssued  = rs.dateValue("ReferralDate");
            },eDBRowsExpectZeroOrOne);

            if (!dRefIssued.isValid())
            // no good ref date? if so toss the kombika
                sRefKombika = "";

        // in case we retrieved the documentid the easy way above, check for consultrefs to yourself and late referral dates (after the visit day)
        // do this for better error msgs when a single consultref can be selected via an AppointmentID and ReferralDocumentID (i.e. the easy way)
            if (!sRefKombika.isEmpty()) // got a kombika to check?
            {
                if (sRefKombika == sKombika)
                {
                    st.bOk    = false;
                    st.sError = "Egenremiss ej tillåten";

                    continue;   // next please
                }

                if (dRefIssued > st.dVisit)
                {
                    st.bOk    = false;
                    st.sError = "Remissdatum får inte vara efter besöksdatum";

                    continue;   // next please
                }
            }

        // got something?
            if (sRefKombika.isEmpty())
            {
            // no dice again, sorry
                st.bOk    = false;
                st.sError = "Nybesök saknar remiss.";

                continue;   // better luck next time
            }

        // yes we have a lucky winner, save them
            st.sRefKombika = sRefKombika;
            st.dRefIssued  = dRefIssued;

        // finally also check for vintage consultrefs
            if (dRefIssued.addYears(nnConsultRefsOkAgeInYears) < QDate::currentDate())
                if (st.sWarning.isEmpty())
                    st.sWarning = QString("Remissdatum %1 ser antikt ut").arg(TWUtils::toISODate(dRefIssued));

        // consultrefs are only visible as tooltips, so don't bother with a table widget restocking here
        }
    }

// ----------------------------------------------------------------------------------
// --- winding down: step through all ARV entries and check for missing ATG codes ---
    for (auto &st : mArvEntries)
        if (st.sATGCode.isEmpty())
        {
        // sanity check: companion entries should not have an empty ATG code
            if (st.bIsCompanion)
                guruMeditation("Didn't expect an ARV companion to have an empty ATG code");

        // this PAS entry didn't get a matching ATG code, so flag it as an error
            st.bOk = false;
            if (st.sError.isEmpty())   // however don't override an existing error
                 st.sError = "Saknar ARV åtgärdstyp";
        }


// update the tablewidgets regardless if something changed or not
    restockAllTableWidgets();

// we're done with PAS, journals, diagnoses, consultrefs and final error checking
// if Samba is done, time to wrap it up
    if (scMain.bCrawlingDone)
    {
        burialAtSea();
    }
    else
    {
    // Samba is slow today (when it's finished it will call burialAtSea(), trust me)
        bWaitingForSamba = true;
        showProgress("Väntar på Samba...");
    }
}

//----------------------------------------------------------------------------
// burialAtSea (the final steps are taken here)
//
// 2023-04-23 First version
//----------------------------------------------------------------------------
void MainWindow::burialAtSea()
{
// when we arrive here the main Samba crawler should be done
    if (!scMain.bCrawlingDone)
        guruMeditation("the main Samba crawler was not done");

// make sure to update the ARV entries with all we have now from Samba
    refreshPersonNo();

// --- step thru and check all the personno. ---
    int nNo    = 0;    // for showProgress()
    int nTotal = mArvEntries.count();

    for (auto &st : mArvEntries)   // & for r/w access
    {
    // show some progress
        ++nNo;
        showProgress(QString("Strax klar, kontrollerar pat. personnummer (%1%)...").arg((nNo * 100) / nTotal),95 + ((nNo * 5) / nTotal)); // (nTotal > 0)

    // is this a bad personno. or a missing/empty one?
        QString sError;
        if (!st.sPersonNo.isEmpty())
            sError = TWUtils::checkPersonNo(st.sPersonNo);

        bool bBadApple = (!sError.isEmpty() || (st.sPersonNo.isEmpty()));
        if (bBadApple)
            goto checkDOB;

    // this personno. is ok but are we to anyway check all of them?
        if (bCheckAllDOBs)
            goto checkDOB;  // yes please check this one

    // or perform a random inspection? i.e. check a selected few?
        if (bCheckRandomDOBs)
            if (1 == (rand() % 11))
            // let's check on average one of every 11 chaps
                goto checkDOB;

    // we're not checking this one, assume a valid no. (not empty for sure)
        continue;   // next please

checkDOB:
    // checking time: try to get the DOB for this PID
        QDate dDOB;
        QDate dCFUDOB;

        db.select("DateOfBirth,CFUDateOfBirth",stTCCorral.PatInfo,QString("PatientID = %1").arg(st.nPID),
        [&dDOB,&dCFUDOB](DBRowStruct rs)
        {
            dDOB    = rs.dateValue("DateOfBirth");
            dCFUDOB = TWUtils::fromYMD(rs.stringValueTrimmed("CFUDateOfBirth"));  // expect YYYYMMDD from the db
        },eDBRowsExpectZeroOrOne);

    // have a bad or (most likely missing) personno? replace it
        if (bBadApple)
        {
        // arriving here: either sError is already set or we have an empty personno
        // missing from the Samba crawler's table? i.e. an empty personno?
            if (st.sPersonNo.isEmpty())
            {
                sError = "Kassa " + st.sReceiptNo.left(6) + " i TakeCare är inte avslutad";
                if (st.bOk) // if there is no prior error, set this error
                {
                    st.bOk    = false;
                    st.sError = sError;
                }
            }

        // create a new, default (empty) personno.
            st.sPersonNo = "???????? ????";

        // if we found a valid DOB, use it to construct a partial personno.
            if (dDOB.isValid())
                st.sPersonNo = TWUtils::toYMD(dDOB) + " ????";   // use "????" to show we're missing the last 4 digits

        // if DOB was no good, maybe this is a reservnr
            if (!dDOB.isValid() && dCFUDOB.isValid())
            // no birthday but have an ok CFU DOB? assume then it's a reservnummer (Region Stockholm flavored)
                st.sPersonNo = "99?????? ????";     // use even more "????" to show we're missing even more digits
        }
        else
        {
        // this has no error but do a comparison of the personno. vs. DOB
        // however if this is a reservnr (Region Stockholm flavored), skip this DOB check
            if (TWUtils::isReservNo(st.sPersonNo))
                continue;

            if (!dDOB.isValid())
            // didn't get a valid date for the DOB, so skip the check
                continue;

        // match first 8 digits with DOB
            if (st.sPersonNo.left(8) != TWUtils::toYMD(dDOB))    // 8 chars = YYYYMMDD
                if ((st.sWarning.isEmpty()) && (!TWTCSupport::isTestPatient(st.sPersonNo)))
                // not a testpatient: issue a warning (unless this entry has got a previous warning already)
                    if (!bObfuscatePersonNo)    // don't bother if we're in an obfuscation run
                        st.sWarning = "Födelsedatum och personnr. stämmer inte överens";
        }
    }

// update the tablewidgets regardless if we found any ARV errors or not
    restockAllTableWidgets();

// we're done, switch app state to rubbernecking and write the ARV-file
    showProgress("Skriver ARV-filen...",99);
    switchAppState(AppState::eRubbernecking);

    saveARVFile();

// do a final update (can't hurt)
    restockAllTableWidgets();
}

//----------------------------------------------------------------------------
// saveARVFile
//
// 2020-02-29 First version
// 2021-04-22 Switch to 5 (or 8) digits for the ARV receipt no.
//----------------------------------------------------------------------------
void MainWindow::saveARVFile()
{
// first some global, static checks
    if (sKombika.length()     != 11)
        fubar("sKombika must have a length of 11");
    if (sLocumTenensCode.length() !=  5)    // 5 blanks for normal chaps
        fubar("sLocumTenensCode must have a length of 5");

// prepare the counting
    int nVisits   = 0;
    int nErrors   = 0;
    int nWarnings = 0;

// use an helper map for stepping through the entries in date + ARV receiptno order (and bypass errors)
    QMap<QString,QString> mDateReceiptNo2ArvEntry;
    for (auto st : mArvEntries)
    {
    // skip over error entries
        if (!st.bOk)
        {
        // count this as an error unless this is a hidden sponge
            if (!st.bSpongeNoARV4U || bShowSponges || bShowErrorSponges)
                ++nErrors;

            continue;   // next entry please
        }

    // and skip over sponges (no questions asked)
        if (st.bSpongeNoARV4U)
            continue;

    // if we arrive here, this entry should be error free
        if (!st.sError.isEmpty())
            guruMeditation("got a nonempty error message when saving to the ARV file");

    // also make sure we have a nonempty and valid personno
        if (st.sPersonNo.isEmpty()) // empty personno? (shouldn't happen if bOk is true)
            guruMeditation("got an empty personno when writing the ARV file");

        if ("" != TWUtils::checkPersonNo(st.sPersonNo))
            guruMeditation("got a bad personno when writing the ARV file");

    // count # of warnings (a valid entry but with a warning)
        if (!st.sWarning.isEmpty())
            ++nWarnings;

    // have a valid candidate for the ARV file, save it in the map
        ++nVisits;
        QString sKey = st.sVisitDate + QString::number(st.nARVReceiptNo);
        mDateReceiptNo2ArvEntry.insert(sKey,st.sKey);
    }

// check map size (if there's a mismatch an entry was overwritten due to a duplicate ARV receipt no.)
// (we're supporting nnn-0n-nnnn-n and nnn-9n-nnnn-n TC receipt nos. but not nnn-1n-nnnn-n, nnn-2n-nnnn-n etc.)
    if (mDateReceiptNo2ArvEntry.count() != nVisits)
        fubar("mismatch helper map vs. nVisits (%d/%d)",mDateReceiptNo2ArvEntry.count(),nVisits);

// --- start writing the .ARV file --- (append _VIK for locum tenens chaps)
    QString sFileName = QString("%1_%2_%3%4.ARV").arg(TWUtils::toYMD(dFrom).right(6),TWUtils::toYMD(dTo).right(4))
                                                 .arg(nVisits).arg(bIsLocumTenens ? "_VIK" : "");
    TWTextFileWriter tfw(sFileName);
    if (!tfw.isOpen())
        return TWUtils::errorBox(QString("Sorry det gick inte att spara ARV-filen i:\n'%1'").arg(TWUtils::getCurrentDirectory()));

// prepare the msg ref stuff (used in the ARV file header and footer)
    QString sCurrentYYMMDD = TWUtils::toYMD(QDate::currentDate()).right(6);                    // toss the century digits
    QString sCurrentMMDD   = sCurrentYYMMDD.right(4);                                          // keep only month and day
    QString sCurrentHHMM   = TWUtils::tossAllButDigits(TWUtils::toHHMM(QTime::currentTime())); // and hours and minutes

    QString sMsgRef = QString("%1%2%3").arg(ssARVClientID,sCurrentMMDD,sCurrentHHMM);

// start with the 3 line header
    int nARVItems = 0;
    ++nARVItems;
    tfw.ts << QString(ssARVFirstLines).arg(sKombika,sCurrentYYMMDD,sCurrentHHMM,sMsgRef) << "\n";

// step thru our ARV entries in date, ARV receiptno. order
    for (auto sKey : mDateReceiptNo2ArvEntry)
    {
        auto st = mArvEntries[sKey];

    // create the ARV receipt 8 digit string, insert leading zeros (usually needed except for companions)
        QString sARVReceiptNo = ("0000000" + QString::number(st.nARVReceiptNo)).right(8);      // support 1..8 digits

        if (sARVReceiptNo.length() != 8)  // ARV wants 8 digits for receipt no.
            fubar("ARV receiptno. is not 8 digits");

    // ARV rate code
        QString sARVRateCode = st.sARVRateCode;

    // do we have ARV ratecode 07 (Smittskydd)?
        bool bSmittskydd = ("07" == sARVRateCode);

    // write BSK, ATG and optionally DIA items for each ATG code this visit has (usually it's just one)
        ++nARVItems;
        tfw.ts << "BSK" << "\n";

        QString sBSKLine = "1";                                        // new ARV entry BSK line: start with "1"
        sBSKLine += sKombika;
        sBSKLine += sLocumTenensCode;
        sBSKLine += sARVReceiptNo;
        sBSKLine += sTaxType;                                          // usually "F" but can be "A" (global setting)
        sBSKLine += ((bSmittskydd) ? QString(12,' ') : st.sPersonNo);  // 12 spaces or 12 digits
        sBSKLine += ((bSmittskydd) ? QString(" ") : ssIDType);         // either a space or a "P"
        sBSKLine += QString(6,' ');                                    // (DOB YYMMDD not in use)
        sBSKLine += TWUtils::toYMD(st.dVisit).right(6);                // no century digits, please
        sBSKLine += ((st.bFirstVisit) ? QString("0") : QString("1"));
        sBSKLine += ((st.bEmergencyVisit) ? QString("1") : QString("0"));

        sBSKLine += sARVRateCode;
        sBSKLine += ((bSmittskydd) ? ssSmittskydd : ssNoSmittskydd);

    // set default referral kombika and date for ref.
        QString sRefKombika = QString(11,' ');
        QString sRefDate    = QString( 6,' ');

        if (bConsultRefs && st.bFirstVisit && (st.dVisit >= dConsultRefsBegin)) // consultref. for this one?
            if (!st.sRefKombika.trimmed().isEmpty())                            // the ref. kombika needs to be set
            {
                sRefKombika = TWTCSupport::canonicalizeKombika(st.sRefKombika); // tidy it up
                sRefDate    = TWUtils::toYMD(st.dRefIssued).right(6);           // toss the century digits
            }

        sBSKLine += sRefKombika;
        sBSKLine += sRefDate;
        if (nnBSKLineLength != sBSKLine.length())
            fubar("Bad length of BSK line in ARV file (expected %d but got %d)",nnBSKLineLength,sBSKLine.length());
        tfw.ts << sBSKLine << "\n";

        ++nARVItems;
        tfw.ts << "ATG" << "\n";
        tfw.ts << st.sATGCode.leftJustified(4) << "\n";

    // what about DIA chaps? can be 0 - 99 of them
        for (auto sDiagnose : st.slDiagnosis)
        {
            ++nARVItems;
            tfw.ts << "DIA" << "\n";
            tfw.ts << sDiagnose.leftJustified(5) << "\n";
        }
    }

// wrap up with the 3 lines footer
    ++nARVItems;
    QString sFooter = QString(ssARVLastLines).arg(QString::number(nARVItems).leftJustified(6),sMsgRef);
    tfw.ts << sFooter;    // note: folklore implies that we should not append an '\n' here (contrary to the spec.)

// that's all, save the file
    tfw.close();

// show a final progress message and a message box ---------------------------------------
    QString sMsg = QString("ARV-fil %1 skapad med %2 besök.").arg(sFileName).arg(nVisits);

    if ((0 == nErrors) && (0 == nWarnings))
        sMsg += " Inga fel och inga varningar.";

    if ((0 == nErrors) && (1 == nWarnings))
        sMsg += QString(" Inga fel. 1 besök fick en varning.");

    if ((0 == nErrors) && (nWarnings > 1))
        sMsg += QString(" Inga fel. %1 besök fick varningar.").arg(nWarnings);

    if ((nErrors > 0) && (0 == nWarnings))
        sMsg += QString(" %1 besök hade fel och är inte med i ARV-filen. Inga varningar.").arg(nErrors);

    if ((nErrors > 0) && (1 == nWarnings))
        sMsg += QString(" %1 besök hade fel och är inte med i ARV-filen. 1 besök fick en varning.").arg(nErrors);

    if ((nErrors > 0) && (nWarnings > 1))
        sMsg += QString(" %1 besök hade fel och är inte med i ARV-filen. %2 besök fick varningar.").arg(nErrors).arg(nWarnings);

    showProgress(sMsg);
    sMsg.replace(". ",".\n");  // show same message in the info box but split it into multiple lines
    TWUtils::infoBox(sMsg);
}

//----------------------------------------------------------------------------
// on_settingsButton_clicked
//
// 2023-10-16 First version
// 2024-01-07 Support changing the background image
//----------------------------------------------------------------------------
void MainWindow::on_settingsButton_clicked()
{
// wire up a Settings dialog, pass along the locum tenens boolean
    SettingsDialog sd(this,bIsLocumTenens);
    sd.setModal(true);
    sd.show();

// before launching the dialog, get our current settings
    roSettings.setCurrentSection("DefaultPeriod");
    int nPeriodMonths = roSettings.readInt("Months");
    int nPeriodWeeks  = roSettings.readInt("Weeks");
    int nPeriodDays   = roSettings.readInt("Days");

// if we have some R/W settings, try to use them instead
    appDataSettings.setCurrentSection("DefaultPeriod");
    nPeriodMonths = appDataSettings.readIntWithDefault("Months",nPeriodMonths);
    nPeriodWeeks  = appDataSettings.readIntWithDefault("Weeks", nPeriodWeeks);
    nPeriodDays   = appDataSettings.readIntWithDefault("Days",  nPeriodDays);

// got the settings, calc. the index we'll use in the combo box
    int nDefaultDateRangeIndex = 0; // default: one day range
    if (1 == nPeriodWeeks)
        nDefaultDateRangeIndex = 1;
    if (2 == nPeriodWeeks)
        nDefaultDateRangeIndex = 2;
    if (1 == nPeriodMonths)
        nDefaultDateRangeIndex = 3;
    if (2 == nPeriodMonths)
        nDefaultDateRangeIndex = 4;

// get the db and table prefix settings
    roSettings.setCurrentSection("Intelligence");
    int nUseServer      = roSettings.readInt("UseServer");
    int nUseTablePrefix = roSettings.readInt("UseTablePrefix");

// if we already have some R/W settings for those, use them instead
    if (TWAppSettings::IniLocation::eAppData == eIniFlavorForDB)
    {
        appDataSettings.setCurrentSection(sIntelligenceSectionName);
        nUseServer      = appDataSettings.readInt("UseServer");
        nUseTablePrefix = appDataSettings.readInt("UseTablePrefix");
    }

// calc. the combox index
    int nDBServerAndPrefixIndex = 0; // default: PRSINTDB01
    if ((1 == nUseServer) && (1 == nUseTablePrefix))
        nDBServerAndPrefixIndex = 0;
    if ((2 == nUseServer) && (1 == nUseTablePrefix))
        nDBServerAndPrefixIndex = 1;
    if ((1 == nUseServer) && (2 == nUseTablePrefix))
        nDBServerAndPrefixIndex = 2;
    if ((2 == nUseServer) && (2 == nUseTablePrefix))
        nDBServerAndPrefixIndex = 3;

// set them now in the dialog class and launch the modal dialog
    sd.setDefaultDateRangeIndex(nDefaultDateRangeIndex);
    sd.setDBServerAndPrefixIndex(nDBServerAndPrefixIndex);

// also set the background image stuff
    sd.setBackgroundImageIndex(sBackgroundImage,slBackgroundImages);

// wait for user pressing Ok or Cancel
    if (QDialog::Accepted == sd.exec())  // user pressed Ok?
    {
    // yes, so any changes to the settings?
        if (nDefaultDateRangeIndex != sd.nDefaultDateRangeIndex)
        {
        // got a new date range, write it to the R/W ini file
            nDefaultDateRangeIndex = sd.nDefaultDateRangeIndex;

            int nPeriodMonths = 0;
            int nPeriodWeeks  = 0;
            int nPeriodDays   = 0;
            if (0 == nDefaultDateRangeIndex)
                nPeriodDays   = 1;
            if (1 == nDefaultDateRangeIndex)
                nPeriodWeeks  = 1;
            if (2 == nDefaultDateRangeIndex)
                nPeriodWeeks  = 2;
            if (3 == nDefaultDateRangeIndex)
                nPeriodMonths = 1;
            if (4 == nDefaultDateRangeIndex)
                nPeriodMonths = 2;

            appDataSettings.setCurrentSection("DefaultPeriod");
            appDataSettings.writeInt("Months",nPeriodMonths);
            appDataSettings.writeInt("Weeks" ,nPeriodWeeks );
            appDataSettings.writeInt("Days"  ,nPeriodDays  );
        }

        if (nDBServerAndPrefixIndex != sd.nDBServerAndPrefixIndex)
        {
        // got a new DB setting, write it to the R/W ini file
            nDBServerAndPrefixIndex = sd.nDBServerAndPrefixIndex;

        // default is 1,1
            int nUseServer      = 1;
            int nUseTablePrefix = 1;

            if (0 == nDBServerAndPrefixIndex)
            {
                nUseServer      = 1;
                nUseTablePrefix = 1;
            }
            if (1 == nDBServerAndPrefixIndex)
            {
                nUseServer      = 2;
                nUseTablePrefix = 1;
            }
            if (2 == nDBServerAndPrefixIndex)
            {
                nUseServer      = 1;
                nUseTablePrefix = 2;
            }
            if (3 == nDBServerAndPrefixIndex)
            {
                nUseServer      = 2;
                nUseTablePrefix = 2;
            }

        // we need to copy the whole R/O shebang/section (for the benefit of TWTCSupport)
            auto copySetting = [this] (QString sKeyName) { appDataSettings.writeString(sKeyName,roSettings.readString(sKeyName)); };

            roSettings.setCurrentSection("Intelligence");
            appDataSettings.setCurrentSection("Intelligence" + QString::number(nCustomerNo)); // don't use sIntelligenceSectionName

            copySetting("UserName");
            copySetting("EncryptedPassword");
            copySetting("Database");
            copySetting("PortNo");
            appDataSettings.writeInt("UseServer"     ,nUseServer     );
            appDataSettings.writeInt("UseTablePrefix",nUseTablePrefix);
            copySetting("Server1");
            copySetting("Server2");
            copySetting("Server1TablePrefix1");
            copySetting("Server1TablePrefix2");
            copySetting("Server2TablePrefix1");
            copySetting("Server2TablePrefix2");

        // written lots of stuff, so make a flush here
            appDataSettings.sync();
        }

    // user changed the background image? if so write the new filename to our R/W appdata settings
        QString sNewBackgroundImage = slBackgroundImages[sd.nBackgroundImageIndex * 2]; // use * 2 to skip over the captions
        if (sNewBackgroundImage != sBackgroundImage)
        {
            appDataSettings.setCurrentSection("Options");
            appDataSettings.writeString("BackgroundImage",sNewBackgroundImage);
        }
    }

// that's for the Setting dialog
}
