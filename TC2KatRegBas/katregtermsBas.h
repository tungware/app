/*--------------------------------------------------------------------------*/
/* katregtermsBas.h                                                         */
// KatReg BAS TC terms stuff                                                */
/*                                                                          */
/* 2024-02-11 First version                                             HS  */
/*--------------------------------------------------------------------------*/

#pragma once
// pull in TWUtils since we have some fubar() calls in here
#include INCLUDETWUTILS

// BasReg form consists of 17 fields
// (clinicno. and personno. are not managed here, so that leaves 15 for us)
enum class KatRegFields
{
    eRightOrLeftEye,
    eVisusRight,
    eVisusLeft,
    eNIKE,
    eDateWaitingList,
    eDateOperation,
    eEyeStates,
    eOtherIndications,
    eTypeOfOperation,
    eLensMaterial,
    eSpecificProps,
    ePeroperativeActions,
    eAntibiotics,
    ePeroperativeDamages,
    ePostoperativeIP,
    eSurgeon
};

// store those enum in a list for easy traversal (poor man's reflection before C++26)
// obviously any changes above ---> change in the list here as well
const static QList<enum KatRegFields> lKatRegFields
{
    KatRegFields::eRightOrLeftEye,
    KatRegFields::eVisusRight,
    KatRegFields::eVisusLeft,
    KatRegFields::eNIKE,
    KatRegFields::eDateWaitingList,
    KatRegFields::eDateOperation,
    KatRegFields::eEyeStates,
    KatRegFields::eOtherIndications,
    KatRegFields::eTypeOfOperation,
    KatRegFields::eLensMaterial,
    KatRegFields::eSpecificProps,
    KatRegFields::ePeroperativeActions,
    KatRegFields::eAntibiotics,
    KatRegFields::ePeroperativeDamages,
    KatRegFields::ePostoperativeIP,
    KatRegFields::eSurgeon
};

// * means this is a mandatory field
// #1: klinik nr. *           (skipped in here)
// #2: pat. personno. *       (     - " -     )

// #3: öga som skall opereras (       - " -     )
const int nnRightOrLeftEyeKeywordTermID    =  7597;  // CaseNotes_ValueTerms
const int nnRightTermID                    =  1905;
const int nnLeftTermID                     =  2050;
const QChar ccRightEye                     =   'H';
const QChar ccLeftEye                      =   'V';
const QChar ccUnspecifiedEye               =   ' ';

// the rest of these terms are stored in a QMap
// #4: visus höger och vänster (yes, two fields) *
const int nnVisusRightKeywordTermID        = 17720;   // CaseNotes_Measurements
const int nnVisusLeftKeywordTermID         = 17721;
const int nnFR4mTermID                     = 17715;   // CaseNotes_ValueTerms
const int nnFR3mTermID                     = 17716;
const int nnFR2mTermID                     = 17717;
const int nnFR1mTermID                     = 17718;
const int nnHRPLPTermID                    = 17719;
const int nnAmaurosisTermID                = 12713;

// #5: indikationsgrupp *
const int nnNIKEKeywordTermID              =  5705;   // CaseNotes_Measurements

// #6: date when registered in the waiting list *
const int nnDateWaitingListKeywordTermID   =  2917;   // CaseNotes_Notes

// #7: date of operation *
const int nnDateOperationKeywordTermID     =  7023;   // CaseNotes_DateTimes

// #8: tillstånd i operationsögat
const int nnStatesRightEyeKeywordTermID    = 17670;   // CaseNotes_ValueTerms
const int nnStatesLeftEyeKeywordTermID     = 17779;   // two keyword flavors (right and left eye)
const int nnGlaukomTermID                  = 17671;
const int nnMakulasjukdomTermID            = 18290;   // (before 2022-01-24: 17672)
const int nnDiabetesretinopatiTermID       = 17673;
const int nnCorneaGuttataTermID            = 17674;
const int nnPseudoexfoliationerTermID      = 17675;
const int nnAnnatSynhotandeTermID          = 17678;
const int nnUveitTermID                    = 12424;
const int nnTidigareVitrektomiTermID       = 17676;
const int nnTidigareRefraktivKirurgiTermID = 17677;

// #9: annan operationsindikation (note: reuses the same KeywordTerm IDs as #8)
const int nnAnisometropiTermID             = 18285;
const int nnAndraSubjektivaSynBesvarTermID = 18286;
const int nnForhojtOgontryckTermID         = 18287;
const int nnAnnanMedicinskIndikationTermID = 18288;

// keep TermIDs for field #8 and #9 separated via this list (they're using the same KeywordTermID)
const static QList<int> lTermIDsForOtherIndications
{
    nnAnisometropiTermID,
    nnAndraSubjektivaSynBesvarTermID,
    nnForhojtOgontryckTermID,
    nnAnnanMedicinskIndikationTermID
};

// #10: operationstyp *
const int nnTypeOfOperationKeywordTermID   = 17669;   // CaseNotes_Measurements
const int nnFakoBKLValue                   =     0;   // to XML as:  1
const int nnFakoBKLAnnanSamtidaOpValue     =     2;   //             8
const int nnAnnanOpValue                   =     4;   //             6

// #11: linsmaterial
//      note: nnIngenLins is an artificial value inserted by the cooking when TC value is absent
const int nnLinsmaterialKeywordTermID      = 17679;   // CaseNotes_Measurements
const int nnAcrylHydrofobValue             =     0;   // to XML as: 10
const int nnAcrylHydrofilValue             =     2;   //            11
const int nnAcrylAnnatValue                =     4;   //             5
const int nnIngenLinsValue                 =     6;   //             6

// #12: särskilda linsegenskaper
const int nnLinsegenskaperKeywordTermID    = 17681;   // CaseNotes_ValueTerms
const int nnGulLinsTermID                  = 17682;
const int nnMultifokalLinsTermID           = 17684;
const int nnToriskLinsTermID               = 17685;
//const int nnEDOFLinsTermID                 = 17687;   // EDOF is renamed ...
const int nnLinsMedForlangtFokusTermID     = 17687;     // ... to this 2024-01-01

// #13: peroperativa åtgärder
const int nnPeroperativaAtgKeywordTermID   = 18284;   // CaseNotes_ValueTerms
const int nnMekanisktVidgadPupillTermID    = 17693;
const int nnKapselFargningTermID           = 17694;
const int nnHakarIRexiskantenTermID        = 17695;
const int nnKapselringInlagdTermID         = 17696;
const int nnGenerellAnestesiTermID         =  6316;

// #14: antibiotika intrakameralt
const int nnAntibiotikaKeywordTermID       = 17698;   // CaseNotes_ValueTerms
const int nnCefuroximTermID                = 17699;
const int nnDoktacillinTermID              = 17700;
const int nnVigamoxTermID                  = 17701;
const int nnAnnanAntibiotikaTermID         = 14737;
const int nnNejIngenAntibiotikaTermID      =   366;

// #15: peroperativ skada *
const int nnPeroperativSkadaKeywordTermID  = 17697;   // CaseNotes_Measurements
const int nnNoValue                        =     0;
const int nnYesValue                       =     1;

// #16: postoperativt inflammationsprofylax
const int nnPostoperativtIPKeywordTermID   = 17702;   // CaseNotes_ValueTerms
const int nnTopikalaNSAIDTermID            = 17703;
const int nnTopikalaStTermID               = 17704;
const int nnSubkonjunktivalaStTermID       = 17705;
const int nnAnnatProfylaxTermID            = 14941;
const int nnIngenProfylaxTermID            = 14023;

// #17: kirurg *
const int nnSurgeonKeywordTermID           =  1960;   // CaseNotes_Users


// ---------------------------------------------------------------------------------
// all keyword terms are applicable for both eye flavors, except these:
const static QList<int> nlKeywordsForRightEyeOnly = { nnStatesRightEyeKeywordTermID };
const static QList<int> nlKeywordsForLeftEyeOnly  = { nnStatesLeftEyeKeywordTermID  };

// a map for retrieving the KatRegField enum from a TC keyword term id
const static QMap<int,enum KatRegFields> mKeyword2KatRegField =
{ // TC keyword term id --> field enum
    { nnRightOrLeftEyeKeywordTermID,   KatRegFields::eRightOrLeftEye      },
    { nnVisusRightKeywordTermID,       KatRegFields::eVisusRight          },
    { nnVisusLeftKeywordTermID,        KatRegFields::eVisusLeft           },
    { nnNIKEKeywordTermID,             KatRegFields::eNIKE                },
    { nnDateWaitingListKeywordTermID,  KatRegFields::eDateWaitingList     },
    { nnDateOperationKeywordTermID,    KatRegFields::eDateOperation       },
    { nnStatesRightEyeKeywordTermID,   KatRegFields::eEyeStates           },  // note: right and left
    { nnStatesLeftEyeKeywordTermID,    KatRegFields::eEyeStates           },  // reusing same field
    // note: no keyword term for KatRegFields::eOtherIndications in TC        // (reuses the same keyword term in TC)
    { nnTypeOfOperationKeywordTermID,  KatRegFields::eTypeOfOperation     },
    { nnLinsmaterialKeywordTermID,     KatRegFields::eLensMaterial        },
    { nnLinsegenskaperKeywordTermID,   KatRegFields::eSpecificProps       },
    { nnPeroperativaAtgKeywordTermID,  KatRegFields::ePeroperativeActions },
    { nnAntibiotikaKeywordTermID,      KatRegFields::eAntibiotics         },
    { nnPeroperativSkadaKeywordTermID, KatRegFields::ePeroperativeDamages },
    { nnPostoperativtIPKeywordTermID,  KatRegFields::ePostoperativeIP     },
    { nnSurgeonKeywordTermID,          KatRegFields::eSurgeon             }
};


// ---------------------------------------------------------------------------------------------------
// specify which katreg fields are term sets or value strings and how many values can/should they hold
using  MinMaxPair = std::pair<int,int>; // std::pair for spec of # of min. params and # of max. params
struct TermSetOrValuePairsStruct
{
    MinMaxPair pTermSetSpec; // min/max # of terms expected in the QSet in mTermSets
    MinMaxPair pValueSpec;   // min/max # of values expected in the QStringList in mValues
};

// prepare some popular pairs:
const static MinMaxPair pNone      (0,0);
const static MinMaxPair pZeroOrOne (0,1);
const static MinMaxPair pExactlyOne(1,1);

const static QMap<enum KatRegFields,TermSetOrValuePairsStruct> mKatRegFieldParamSpecs =
{
// for the katreg fields, specify two pairs of min. and max. no. of params
// first one is the spec. for term sets, second pair is the spec. for values
    { KatRegFields::eRightOrLeftEye,      {  pExactlyOne, pNone       }  }, //  3 * 'H' or 'V'
    { KatRegFields::eVisusRight,          {  pZeroOrOne,  pZeroOrOne  }  }, //  4 * either a value or a term
    { KatRegFields::eVisusLeft,           {  pZeroOrOne,  pZeroOrOne  }  }, //  4 * (right and left)
    { KatRegFields::eNIKE,                {  pNone,       pExactlyOne }  }, //  5 * single value
    { KatRegFields::eDateWaitingList,     {  pNone,       pExactlyOne }  }, //  6 * single date
    { KatRegFields::eDateOperation,       {  pNone,       pExactlyOne }  }, //  7 * single date
    { KatRegFields::eEyeStates,           {  { 0 ,9 },    pNone       }  }, //  8   multiple optional checkboxes
    { KatRegFields::eOtherIndications,    {  { 0, 4 },    pNone       }  }, //  9   multiple optional checkboxes
    { KatRegFields::eTypeOfOperation,     {  pNone,       pExactlyOne }  }, // 10 * single checkbox
    { KatRegFields::eLensMaterial,        {  pNone,       pZeroOrOne  }  }, // 11   single checkbox (absent --> "None")
    { KatRegFields::eSpecificProps,       {  { 0, 4 },    pNone       }  }, // 12   multiple optional checkboxes
    { KatRegFields::ePeroperativeActions, {  { 0, 5 },    pNone       }  }, // 13   multiple optional checkboxes
    { KatRegFields::eAntibiotics,         {  { 0, 5 },    pNone       }  }, // 14   multiple optional checkboxes
    { KatRegFields::ePeroperativeDamages, {  pNone,       pExactlyOne }  }, // 15 * single yes or no
    { KatRegFields::ePostoperativeIP,     {  { 0, 5 },    pNone       }  }, // 16   multiple optional checkboxes
    { KatRegFields::eSurgeon,             {  pNone,       pExactlyOne }  }  // 17 * single no. (name of the surgeon)
};

// --- also some translation tables ------------------------------------------------------------------------------
// field prompts/titles
const static QMap<enum KatRegFields,QString> mKatRegFieldCaptions =
{
    { KatRegFields::eRightOrLeftEye,      "Öga som ska opereras"                            }, //  3 * 'H' or 'V'
    { KatRegFields::eVisusRight,          "Preoperativ synskärpa höger öga",                }, //  4 * two fields
    { KatRegFields::eVisusLeft,           "Preoperativ synskärpa vänster öga"               }, //  4 * (right and left)
    { KatRegFields::eNIKE,                "Indikationsgrupp"                                }, //  5 * single value
    { KatRegFields::eDateWaitingList,     "Uppsättning på väntelistan"                      }, //  6 * single date
    { KatRegFields::eDateOperation,       "Operationsdatum"                                 }, //  7 * single date
    { KatRegFields::eEyeStates,           "Tillstånd i operationsögat"                      }, //  8   multiple optional checkboxes
    { KatRegFields::eOtherIndications,    "Annan operationsindikation än synnedsättning"    }, //  9   multiple optional checkboxes
    { KatRegFields::eTypeOfOperation,     "Operationstyp"                                   }, // 10 * single checkbox (1 of 3)
    { KatRegFields::eLensMaterial,        "Linsmaterial"                                    }, // 11 * single checkbox (1 of 4)
    { KatRegFields::eSpecificProps,       "Särskilda linsegenskaper"                        }, // 12   multiple optional checkboxes
    { KatRegFields::ePeroperativeActions, "Peroperativa åtgärder"                           }, // 13   multiple optional checkboxes
    { KatRegFields::eAntibiotics,         "Antibiotika intrakameralt"                       }, // 14   multiple optional checkboxes
    { KatRegFields::ePeroperativeDamages, "Peroperativ skada på bakre kapsel eller zonulae" }, // 15 * single yes or no
    { KatRegFields::ePostoperativeIP,     "Postoperativt inflammationsprofylax"             }, // 16   multiple optional checkboxes
    { KatRegFields::eSurgeon,             "Kirurg"                                          }  // 17 * single no. (name of surgeon)
};

// captions for the terms that belongs in a set
// (note: don't use '+' inside these caption since it's used as a separator)
const static QMap<int,QString> mTermSetCaptions =
{
    { nnGlaukomTermID,                  "Glaukom"                    },
    { nnMakulasjukdomTermID,            "Makulasjukdom"              },
    { nnDiabetesretinopatiTermID,       "Diabetesretinopati"         },
    { nnCorneaGuttataTermID,            "Cornea Guttata"             },
    { nnPseudoexfoliationerTermID,      "Pseudoexfoliationer"        },
    { nnAnnatSynhotandeTermID,          "Annat synhotande"           },
    { nnUveitTermID,                    "Uveit"                      },
    { nnTidigareVitrektomiTermID,       "Tidigare vitrektomi"        },
    { nnTidigareRefraktivKirurgiTermID, "Tidigare refraktiv kirurgi" },
    { nnAnisometropiTermID,             "Anisometropi"               },
    { nnAndraSubjektivaSynBesvarTermID, "Andra subjektiva synbesvär" },
    { nnForhojtOgontryckTermID        , "Förhöjt ögontryck"          },
    { nnAnnanMedicinskIndikationTermID, "Annan medicinsk indikation" },
    { nnGulLinsTermID,                  "Gulfärgad lins"             },
    { nnMultifokalLinsTermID,           "Multifokal lins"            },
    { nnToriskLinsTermID,               "Torisk lins"                },
    // { nnEDOFLinsTermID,              "EDOF lins" },  // obsolete 2024-01-01
    { nnLinsMedForlangtFokusTermID,     "Lins med förlängt fokus"    },
    { nnMekanisktVidgadPupillTermID,    "Mekaniskt vidgad pupill"    },
    { nnKapselFargningTermID,           "Kapselfärgning"             },
    { nnHakarIRexiskantenTermID,        "Hakar i Rexiskanten"        },
    { nnKapselringInlagdTermID,         "Kapselring inlagd"          },
    { nnGenerellAnestesiTermID,         "Generell anestesi"          },
    { nnCefuroximTermID,                "Cefuroxim"                  },
    { nnDoktacillinTermID,              "Doktacillin"                },
    { nnVigamoxTermID,                  "Vigamox"                    },
    { nnAnnanAntibiotikaTermID,         "Annat"                      },
    { nnNejIngenAntibiotikaTermID,      "Nej"                        },
    { nnTopikalaNSAIDTermID,            "Topikala NSAID"             },
    { nnTopikalaStTermID,               "Topikala steroider"         },
    { nnSubkonjunktivalaStTermID,       "Subkonjunktivala steroider" },
    { nnAnnatProfylaxTermID,            "Annat"                      },
    { nnIngenProfylaxTermID,            "Ingen"                      }
};

// XML stuff ----------------------------------------------------------------
// map of KatReg terms ---> XML element names
const static QMap<int,QString> mKatRegTermXMLNames
{
    { nnDateWaitingListKeywordTermID,   "UppsattPåVäntelista"       },
    { nnRightOrLeftEyeKeywordTermID,    "Öga"                       },
    // the 2 visus use not right/left, but "VisusOperationsÖga" or "VisusAndraÖgat"
    { nnNIKEKeywordTermID,              "Indikationsgrupp"          },

    { nnPeroperativaAtgKeywordTermID,   "PeroperativÅtgärd"         },
    { nnMekanisktVidgadPupillTermID,    "MekanisktVidgadPupill"     },
    { nnKapselFargningTermID,           "Kapselfärgning"            },
    { nnHakarIRexiskantenTermID,        "HakarIRhexiskanten"        },
    { nnKapselringInlagdTermID,         "KaplselringInlagd"         }, // yes it's misspeled
    { nnPeroperativSkadaKeywordTermID,  "PeroperativSkada"          }, // note: keyword term embedded here
    { nnGenerellAnestesiTermID,         "GenerellAnestesi"          },

    { nnStatesRightEyeKeywordTermID,    "TillståndOpÖga"            }, // two keywords --> same XML element
    { nnStatesLeftEyeKeywordTermID,     "TillståndOpÖga"            }, //
    { nnGlaukomTermID,                  "Glaukom"                   },
    { nnMakulasjukdomTermID,            "Makulasjukdom"             },
    { nnDiabetesretinopatiTermID,       "Diabetesretinopati"        },
    { nnCorneaGuttataTermID,            "CorneaGuttata"             },
    { nnPseudoexfoliationerTermID,      "Pseudoexfoliation"         },
    { nnUveitTermID,                    "Uveit"                     },
    { nnAnnatSynhotandeTermID,          "AnnatSynhot"               },
    { nnTidigareRefraktivKirurgiTermID, "TidigareRefraktivKirurgi"  },
    { nnTidigareVitrektomiTermID,       "TidigareVitrektomi"        },

    { nnAnisometropiTermID,             "Anisometropi"              }, // stored in KatRegFields::eOtherIndications
    { nnForhojtOgontryckTermID        , "FörhöjtÖgontryck"          },
    { nnAndraSubjektivaSynBesvarTermID, "AndraSubjektivaSynbesvär"  },
    { nnAnnanMedicinskIndikationTermID, "AnnanMedicinskIndikation"  },

    { nnDateOperationKeywordTermID,     "Operationsdatum"           },
    { nnTypeOfOperationKeywordTermID,   "Operationstyp"             },
    { nnLinsmaterialKeywordTermID,      "LinsMaterial"              },

    { nnLinsegenskaperKeywordTermID,    "LinsEgenskaper"            },
    { nnGulLinsTermID,                  "GulfärgadLins"             },
    { nnMultifokalLinsTermID,           "MultifokalLins"            },
    { nnToriskLinsTermID,               "ToriskLins"                },
    { nnLinsMedForlangtFokusTermID,     "EDOFLins"                  }, // 2024-01-01: they're keeping the old name

    { nnAntibiotikaKeywordTermID,       "AntibiotikaIntrakameralt"  },
    { nnCefuroximTermID,                "Cefuroxim"                 },
    { nnDoktacillinTermID,              "Doktacillin"               },
    { nnVigamoxTermID,                  "Vigamox"                   },
    { nnAnnanAntibiotikaTermID,         "AnnatAntibiotika"          },
    // nnNejIngenAntibiotikaTermID not used in the XML

    { nnSurgeonKeywordTermID,           "Kirurgkod"                 }, // use the table in ROSettings.ini

    { nnPostoperativtIPKeywordTermID,   "Inflammationsprofylax"     },
    { nnTopikalaNSAIDTermID,            "TopikalaNSAID"             },
    { nnTopikalaStTermID,               "TopikalaSteroider"         },
    { nnSubkonjunktivalaStTermID,       "SubkonjunktivalaSteroider" },
    { nnAnnatProfylaxTermID,            "AnnanSteroider"            }
    // nnIngenProfylaxTermID not used in the XML
};

// --- that's it for BasReg
