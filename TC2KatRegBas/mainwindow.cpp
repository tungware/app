/*--------------------------------------------------------------------------*/
/* mainwindow.cpp                                                           */
/*                                                                          */
/* 2023-12-01 First version                                             HS  */
/* 2024-03-16 Require OperationsTyp to be present in the journal        HS  */
/* 2024-06-02 Toss kombika filtering in PAS lookup                      HS  */
/*--------------------------------------------------------------------------*/

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "qcommandlineparser.h"
#include "qtimer.h"
#include "qevent.h"
#include "qpropertyanimation.h"
#include "qparallelanimationgroup.h"

// ShowBox and the Settings dialog
#include "showbox.h"
#include "settingsdialog.h"

// for the file saving
#include "qfile.h"
#include "qxmlstream.h"

#include "qdebug.h"

//----------------------------------------------------------------------------
// MainWindow ctor
MainWindow::MainWindow(QWidget* parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);

// usual preamble (will also change current directory to where the .exe file is)
    TWUtils::helloQt(this);
    setWindowTitle(QString("Välkommen till TC2KatRegBas (version %1)").arg(TWUtils::versionMajorMinor()));

// read in our RO settings, check major+minor version and customername with the one in the .pro file
    roSettings.setCurrentSection("Update");
#if defined(Q_OS_WIN)
// if we're on Windows, check major+minor version
    QString sBuildVersion   = TWUtils::versionMajorMinor();
    QString sMinimumVersion = roSettings.readString("MinimumVersion");
    if (sMinimumVersion > sBuildVersion)
        fubar(QString("Build %1 is lower than the required minimum %2.").arg(sBuildVersion,sMinimumVersion));
#endif

// read in lots of stuff from the general/first section of ROSettings
    roSettings.setCurrentSection("General");
    sCustomerName     = roSettings.readString("CustomerName");
    QString sExpected = QT_STRINGIFY(CUSTOMERNAME);
    if (sExpected != sCustomerName)
        fubar(QString("expected customer name: '%1' but got '%2' from the .ini file").arg(sExpected,sCustomerName));

    nCustomerNo      = roSettings.readInt("CustomerNo");
    bAutoStart       = roSettings.readBool("AutoStart");
    bBatchMode       = false;     // placeholder, we'll set it later in this function
    // don't need bCheckAll and bCheckRandom (we're checking all regardless)

    nCompanyID       = roSettings.readInt("CompanyID");
    nCompanyCode     = roSettings.readInt("CompanyCode");
    nCareUnitID      = roSettings.readInt("CareUnitID");
    sHSAID           = roSettings.readString("HSAID");

// (read in [DefaultPeriod] later)
// check if we have some r/w settings in AppData
// the default date range is stored in [General] and is shared ok (if more than one TC2KatReg user runs on the same desktop)
// the db server and table prefix is stored in [Intelligence%CustomerNo%] to keep possible multiple instances apart
    sIntelligenceSectionName = "Intelligence";                        // default: no R/W settings seen
    eIniFlavorForDB          = TWAppSettings::IniLocation::eEmbedded; //       -  "  -

// have something in the R/W ini file?
    QString sRWIntelligenceSectionName = sIntelligenceSectionName + QString::number(nCustomerNo);
    appDataSettings.setCurrentSection(sRWIntelligenceSectionName);
    if (appDataSettings.readIntWithDefault("UseServer",0) > 0)  // use this one as a canary (should not be 0)
    {
    // got something, so we will to use AppData settings for the db settings
        eIniFlavorForDB          = TWAppSettings::IniLocation::eAppData;
        sIntelligenceSectionName = sRWIntelligenceSectionName;  // and we will use the R/W section name
    }

// Samba time
    roSettings.setCurrentSection("Samba");
    sSambaShareName = roSettings.readString("ShareName");
    sLocalDirectory = "";
    if (roSettings.readBoolWithDefault("UseLocalDirectory",false))
    // used as a fallback when we cannot connect directly to the Samba server
        sLocalDirectory = roSettings.readStringOSSuffix("LocalDirectory");

// KatReg static stuff
    roSettings.setCurrentSection("KatReg");
    sClinicNo           = roSettings.readString("ClinicNo");
    sClinicName         = roSettings.readString("ClinicName");
    slSurgeons          = roSettings.readStringList("Surgeons");
    if (slSurgeons.isEmpty())
        fubar("The list of surgeons cannot be empty");
    slSurgeonNos        = roSettings.readStringList("SurgeonNos");
    if (slSurgeonNos.count() != slSurgeons.count())
        fubar("Lists of surgeons and their code nos. are not the same size");
    sFallbackSurgeonNo = roSettings.readString("FallbackSurgeonNo");

// almost done, parse the [Options] section (yes they are all optional, hence the clever section name)
    roSettings.setCurrentSection("Options");
    bDebugDump              = roSettings.readBoolWithDefault("DebugDump",false);
    bObfuscatePersonNo      = roSettings.readBoolWithDefault("ObfuscatePersonNo",false);
    bShowErrorSponges       = roSettings.readBoolWithDefault("ShowErrorSponges",false);
    bShowSponges            = roSettings.readBoolWithDefault("ShowSponges",false);
    sTCPathOverride         = roSettings.readStringWithDefault("TCPathOverride","");
    sKatRegFilePathOverride = roSettings.readStringWithDefault("KatRegFilePathOverride","");

    if (!sKatRegFilePathOverride.isEmpty())
        fubar("The KatRegFilePathOverride option is not yet supported");

// if we're not on Windows, support a debug dump of each visit
#if !defined(Q_OS_WIN)
    #if !defined(QDEBUG_H)
        if (bDebugDump)
            fubar("DebugDump is requested but qdebug.h is not #included");
    #endif
#endif

// setup the default date period? yes unless a date range has been specified in the command line (for batch mode)
    bBatchMode = false;

    QCommandLineParser lp;
    lp.process(qApp->arguments());
    auto slArgs = lp.positionalArguments();
    if (slArgs.count() == 2)
    {
    // need two valid dates
        dFrom = TWUtils::fromISODate(slArgs[0]);
        dTo   = TWUtils::fromISODate(slArgs[1]);

        bool bOk = (dFrom.isValid() && dTo.isValid());
        if (bOk)
            bOk = (dFrom <= dTo);                 // same day ok
        if (bOk)
            bOk = (dTo < QDate::currentDate());   // yesterday or earlier ok

    // we have two valid dates, enable batch mode
    // (if we didn't get a valid range, dFrom and dTo will be assigned new values below)
        bBatchMode = bOk;
    }

    if (!bBatchMode)
    {
    // ok let the user select the date range
    // calc. the default date range from the R/O settings
        roSettings.setCurrentSection("DefaultPeriod");
        int nPeriodMonths = roSettings.readInt("Months");
        int nPeriodWeeks  = roSettings.readInt("Weeks");
        int nPeriodDays   = roSettings.readInt("Days");

    // if we have some R/W settings try to use them instead
        appDataSettings.setCurrentSection("DefaultPeriod"); // use a common setting (regardless of customer no.)
        nPeriodMonths = appDataSettings.readIntWithDefault("Months",nPeriodMonths);
        nPeriodWeeks  = appDataSettings.readIntWithDefault("Weeks", nPeriodWeeks);
        nPeriodDays   = appDataSettings.readIntWithDefault("Days",  nPeriodDays);

    // some sanity checks please
        if ((nPeriodMonths < 0) || (nPeriodWeeks < 0) || (nPeriodDays < 0))
            fubar("don't like negative [DefaultPeriod] values");

        if ((nPeriodMonths == 0) && (nPeriodWeeks == 0) && (nPeriodDays == 0))
            fubar("sorry but at least one of Months,Weeks or Days in [DefaultPeriod] in the .ini file has to be > 0");

    // default to a period of some time specified in the settings (days, weeks or months)
        dFrom = QDate::currentDate().addDays(-1);   // default range: [yesterday to today[
        dTo   = QDate::currentDate();               //
        if (nPeriodDays > 0)
        {
            dFrom = QDate::currentDate().addDays(0 - nPeriodDays);
            if (dFrom.dayOfWeek() > 5)      // Saturday or Sunday?
            {
                dFrom = dFrom.addDays(-1);
                if (dFrom.dayOfWeek() > 5)  // try again
                    dFrom = dFrom.addDays(-1);
            }
        }

        if (nPeriodWeeks > 0)
        {
        // set a period of weeks, Monday to Monday (note: we're assuming Monday is the first day of the week)
            dFrom = QDate::currentDate().addDays(0 - 7 * nPeriodWeeks); // step # of weeks back
            while (dFrom.dayOfWeek() > Qt::Monday)
                dFrom = dFrom.addDays(-1);

            dTo = dFrom.addDays(7 * nPeriodWeeks);
        }

        if (nPeriodMonths > 0)
        {
        // set period 1st to 1st of months
            dFrom = QDate::currentDate().addMonths(0 - nPeriodMonths); // step # of months back
            if (QDate::currentDate().day() > 6)    // but if we're later than 6 days into a month
                dFrom = dFrom.addMonths(1);        // step one month forward
            dFrom = QDate(dFrom.year(),dFrom.month(),1);
            dTo = dFrom.addMonths(nPeriodMonths);
        }

        if (dTo > QDate::currentDate())     // trying to step into the future?
            dTo = QDate::currentDate();     // today is as far as we can go

    // now we have [first, last[, i.e. last is exclusive
    // make an inclusive range i.e. [first, last] by subtracting 1 day
        dTo = dTo.addDays(-1);
    }

    if (dTo < dFrom)  // make sure we're coding correctly here
        guruMeditation("dTo < dFrom :-( something is seriously wrong with the date calculations");

// set 'em on our form as the default date selection
    ui->calendarWidgetFrom->setSelectedDate(dFrom);
    ui->calendarWidgetTo->setSelectedDate(dTo);

// set the maximum allowed date range, i.e. first and last days that we allow to be selected
    QDate dEarliest = ddEarliestVisit;                  // (because we changed receipt no.s style then)
    QDate dLatest   = QDate::currentDate().addDays(-1); // yesterday is the most recent day we'll allow

    ui->calendarWidgetFrom->setMinimumDate(dEarliest);
    ui->calendarWidgetFrom->setMaximumDate(dLatest);

    ui->calendarWidgetTo->setMinimumDate(dEarliest);
    ui->calendarWidgetTo->setMaximumDate(dLatest);

// use our clicked() methods to initially set the Go-button caption
    on_calendarWidgetFrom_clicked(dFrom);
    on_calendarWidgetTo_clicked  (dTo  );

// load up the icons we use
    iconShowBox             = QIcon(":/showbox.ico");
    iconTC                  = QIcon(":/tc.ico");
    int nIconHeightAndWidth = 30;
    ui->tableWidgetAll->setIconSize(QSize(nIconHeightAndWidth,nIconHeightAndWidth));
    ui->tableWidgetWarnings->setIconSize(QSize(nIconHeightAndWidth,nIconHeightAndWidth));
    ui->tableWidgetErrors->setIconSize(QSize(nIconHeightAndWidth,nIconHeightAndWidth));

// ------------------------------------
// prepare the UI, start with the title
    QString sTitle = QString("KatReg Bas för %1 (nr. %2)").arg(sClinicName,sClinicNo);
    ui->labelTitle->setText(sTitle);

    if (bObfuscatePersonNo)
        ui->labelTitle->setText("TC2KatRegBas med testpatienter");

    setupUI();  // do the rest here

// almost done, final step: connect Samba and Intelligence (via a timed lambda)
    switchAppState(AppState::eConnecting);
    QTimer::singleShot(nnShortDelay,this,[this]
    {
    // now is a good time to check that we can create a mew file in the current directory
        if (!TWTextFileWriter::canCreateFile("TestfilKatReg.xml"))
            if (!TWUtils::okCancelBox(QString("Kan inte skapa en KatReg-xmlfil i denna mapp ('%1').\nOk att fortsätta ändå?").arg(TWUtils::getCurrentDirectory())))
                return qApp->quit();

    // ok carry on trying to connect to Samba and Intelligence
        QString sError = connectToSambaAndIntelligence();
        if (!sError.isEmpty())
        {
        // better luck next time
            TWUtils::errorBox(sError);
            return qApp->quit();
        }

    // wait for app state to change (and the Go button to be pressed)
    });
}

//----------------------------------------------------------------------------
// MainWindow main tosser
//
// 2023-12-01 First version
//----------------------------------------------------------------------------
MainWindow::~MainWindow()
{
// save our mainwindow geometry
    appDataSettings.setCurrentSection("General");
    appDataSettings.writeByteArray("Geometry",saveGeometry());
    appDataSettings.sync(); // in case we're doing an ignominious exit

// if we're processing, chances are that some other thread is running
    if ((AppState::eConnecting == appState) || (AppState::eProcessing == appState))
    // if so exit the hard way
        TWUtils::ignominiousExit();

// else the slow way
    delete ui;
}

//----------------------------------------------------------------------------
// closeEvent (MainWindow extra tosser when typing Alt-F4 or clicking 'x')
//
// 2023-12-01 First version
//----------------------------------------------------------------------------
void MainWindow::closeEvent(QCloseEvent* event)
{
    Q_UNUSED(event);

#if defined(Q_OS_WIN)
// on Windows always exit the hard way regardless of app state
    TWUtils::ignominiousExit();
#endif
// on other machines only go south if we're connecting or processing
    if ((AppState::eConnecting == appState) || (AppState::eProcessing == appState))
        TWUtils::ignominiousExit();
}

//----------------------------------------------------------------------------
// setupUI
//
// 2023-12-01 First version
//----------------------------------------------------------------------------
void MainWindow::setupUI()
{
// show the calendars but hide the Go button until we're connected nice and dandy
    ui->pushButtonGo->hide();

// prepare the 3 table widgets, first the columns
    auto setupColumns = [] (QTableWidget* tw)
    {
        tw->setColumnCount(colCount);
        for (int i = 0; (i < colCount); ++i)
            tw->setColumnWidth(i,anColumnWidths[i]);
    };
    setupColumns(ui->tableWidgetAll);
    setupColumns(ui->tableWidgetWarnings);
    setupColumns(ui->tableWidgetErrors);

// and hide 'em for now
    ui->tableWidgetAll->hide();
    ui->tableWidgetWarnings->hide();
    ui->tableWidgetErrors->hide();

// set the default table widget view state
    tableViewState = eShowAll;

// prepare the visibility slider and hide it until we've started processing
    ui->visibilitySlider->hide();
    ui->visibilitySlider->setValue(tableViewState); // start with the "ShowAll" leftmost position
    refreshVisibilitySlider(tableViewState);        //

// create a new groove for the slider (the original groove vanishes due to the stylesheet applied above)
// wire up a new QFrame to act as a surrogate groove with the same parent and geometry as the slider
    QFrame* pFrame = new QFrame(ui->visibilitySlider->parentWidget());
    pFrame->setGeometry(ui->visibilitySlider->geometry());
    pFrame->setObjectName(ui->visibilitySlider->objectName() + "Frame");
    pFrame->setFrameStyle(QFrame::HLine | QFrame::Sunken);
    pFrame->setLineWidth(2);  // (looks nicer than width 1)

// change the z-order so that our custom groove is drawn under the handle (just like the real groove)
    pFrame->stackUnder(ui->visibilitySlider);

// in order for hide()/show() and resize() to work remember this QFrame
    pSliderGrooveFrame = static_cast<QWidget*>(pFrame);

// check that we really have a slider groove QFrame alive and well
    if (nullptr == pSliderGrooveFrame)
        fubar("Couldn't create a slider groove frame");
    pSliderGrooveFrame->hide(); // and hide it for now

// reset and hide the progressbar until we start the processing
    ui->progressBar->setValue(0);
    ui->progressBar->hide();

// resizing support: remember original mainwindow size and set the minimum size
    szOriginal = size();
    setMinimumSize(szOriginal); // designed size ---> minimum size (for now)

// save all our widgets original/designed geometries
    auto sg = [this] (QWidget* w) { mOriginalGeometries.insert(w,w->geometry()); };
    sg(ui->labelTitle);
    sg(ui->pushButtonExit);
    sg(ui->visibilitySlider);
    sg(pSliderGrooveFrame);   // don't forget this one
    sg(ui->frameCalendars);   // (includes calendars and the Go button)
    sg(ui->tableWidgetAll);
    sg(ui->tableWidgetWarnings);
    sg(ui->tableWidgetErrors);
    sg(ui->progressBar);
    sg(ui->labelProgress);

// finally, restore mainwindow geometry if we have one saved (unless a shift key is held down)
    appDataSettings.setCurrentSection("General");
    QByteArray baGeometry = appDataSettings.readByteArray("Geometry");
    if ((baGeometry.length() > 0) && (!TWUtils::isShiftKeyDown()))
        restoreGeometry(baGeometry);
}

//-----------------------------------------------------------------------------
// connectToSambaAndIntelligence
// returns "" when all is good, else returns an error message
//
// 2023-12-01 First version
//-----------------------------------------------------------------------------
QString MainWindow::connectToSambaAndIntelligence()
{
// say hello to Samba and the Intelligence server
    showProgress(QString(" Öppnar Samba och vårdgivarkontot för %1...").arg(sCustomerName));

// use a db prober to check our Intelligence connection
// need to do this *before* launching initial Samba crawler (since it switches app state automatically)
    QString sDBProbeError = TWTCSupport::probeSQLServer(eIniFlavorForDB,sIntelligenceSectionName);
    if (!sDBProbeError.isEmpty())
    {
    // got a db error (showstopper), hide the calendars for a visible effect
        ui->frameCalendars->hide();
        showProgress("Öppna Intelligence/vårdgivarkontot misslyckades.");

        TWUtils::errorBox(sDBProbeError);
        return "";  // tell a lie (no errors) to stay in the app so that user can go to Settings if needed
    }

// use our initial Samba instance for checking the connection
    if (sLocalDirectory.isEmpty())
    // no local directory set for Samba files? on Windows we support a remote connection as well
        scInitial.setSambaCredentialsFromIniFile();
    else
        scInitial.setLocalDirectory(sLocalDirectory);

// wire up the crawling done signal
    connect(&scInitial,&SambaCrawler::crawlingDone,this,[this] (int nMapCount)
    {
        Q_UNUSED(nMapCount);

    // if there's an error kiss the user goodbye
        QString sError = scInitial.sErrorMsg;
        if (!sError.isEmpty())
        {
            TWUtils::errorBox(sError);
            TWUtils::ignominiousExit(); // exit here directly avoiding getting stuck waiting for the Samba worker thread
        }

    // no error(s), what about Intelligence, is it connected ok? i.e. we're the last man out?
        if (bWaitingForSamba)
        {
            showProgress(QString(" Intelligence/vårdgivarkontot och Samba öppnade för %1.").arg(sCustomerName));

        // yes the app was waiting for the initial Samba crawler, so switch the app state now
            switchAppState(AppState::eConnectedOk);
         }
    });

// set it to crawl only one day back (we're not interested in any hits, just verifying we can have a chat with the server)
    scInitial.crawl(nCompanyCode,1);
    bWaitingForSamba = false; // guessing this initial crawl will finish before we've checked Intelligence

// time to launch the Intelligence connection ---------------------------------
    db.setDBErrorCallback([this](QString sError) { dbError(sError); });
    TWTCSupport::setBobbyTablesPrefixFromIniFile(eIniFlavorForDB,sIntelligenceSectionName);
    db.setConnectionName("TC2KatRegBas1");

    QString sError = TWTCSupport::openSQLServerFromIniFile(&db,eIniFlavorForDB,sIntelligenceSectionName);
    if (!sError.isEmpty())
        return sError;  // not good, we're out of here

// match current info with what we have from the .ini file for the careunit and company
    slErrors.clear();
    db.select("CompanyID,CareUnitID,HSAID",stTCCorral.Codes_CareUnits,QString("CareUnitID=%1").arg(nCareUnitID),
    [this](DBRowStruct rs)
    {
        if (nCompanyID  != rs.intValue("CompanyID"))
            slErrors += "CompanyID är inte korrekt";

        if (nCareUnitID != rs.intValue("CareUnitID"))
            slErrors += "CareUnitID är inte korrekt";

        if (sHSAID      != rs.stringValue("HSAID"))
            slErrors += "HSAID är inte korrekt";
    },eDBRowsExpectExactlyOne);

// got some error(s)? if so quit
    if (!slErrors.isEmpty())
        dbError("Kan inte fortsätta:\n" + TWUtils::stringList2CommaList(slErrors).replace(",","\n"));

// check some more stuff in Codes_Companies_v2 (mismatches here are not considered fatal)
    slWarnings.clear();
    db.select("CompanyCode,EconomicalCatalogue",stTCCorral.Codes_Companies_v2,QString("CompanyID=%1").arg(nCompanyID),
    [this](DBRowStruct rs)
    {
        if (nCompanyCode != rs.intValue("CompanyCode"))
            slWarnings += "Företagskoden är ändrad i Intelligence";

        auto sl = rs.stringValue("EconomicalCatalogue").split("/");
        if (sl.isEmpty())
            slWarnings += "Ekonomiska katalogen i Intelligence saknas (ej ifylld)";
        else
        {
            if (sl.last().isEmpty())
                sl.removeLast();

            if (!sl.isEmpty())
                if (sSambaShareName != sl.last())
                    slWarnings += "Ekonomiska katalogen i Intelligence är inte identisk med inställningarna för din Samba";
        }
    },eDBRowsExpectExactlyOne);

// done: show progress bar message saying we have a valid Intelligence connection
    QString sProgress = QString(" Intelligence/vårdgivarkontot öppnat för %1, väntar på Samba...").arg(sCustomerName);
    if (scInitial.bCrawlingDone)
        sProgress = QString(" Samba och Intelligence/vårdgivarkontot öppnade för %1.").arg(sCustomerName);
    showProgress(sProgress);

// got some warnings to show?
    if (!slWarnings.isEmpty())
        TWUtils::warningBox("Varning:\n" + TWUtils::stringList2CommaList(slWarnings).replace(",","\n"));

// we've opened Intelligence ok, what about Samba?
    if (scInitial.bCrawlingDone)
    // yes Samba is connected as well, switch app state and light up the Go button
        switchAppState(AppState::eConnectedOk);
    else
    // no set the boolean and go idle (the crawling done slot will switch app state for us)
        bWaitingForSamba = true;

// return with an empty string (meaning: no Intelligence or Samba errors)
    return "";
}

//----------------------------------------------------------------------------
// resizeEvent
//
// 2023-12-01 First version
//----------------------------------------------------------------------------
void MainWindow::resizeEvent(QResizeEvent* event)
{
// get the new size of our mainwindow
    QSize szNew = event->size();

// calc. the horizontal and vertical deltas
    int nDeltaX = szNew.width() - szOriginal.width();
    int nDeltaY = szNew.height() - szOriginal.height();

// move/resize our widgets (regardless if they're visible or not)
    auto adjustWidget = [this] (QWidget* w, int dx1, int dy1, int dx2, int dy2) { w->setGeometry(mOriginalGeometries[w].adjusted(dx1,dy1,dx2,dy2)); };

    adjustWidget(ui->labelTitle,      nDeltaX / 2, 0, nDeltaX / 2, 0);    // recenter in x, fixed in y
    adjustWidget(ui->pushButtonExit,  nDeltaX,     0, nDeltaX,     0);    // follow in x, fixed in y
    adjustWidget(ui->visibilitySlider,nDeltaX / 2, 0, nDeltaX / 2, 0);    // recenter in x, fixed in y
    adjustWidget(pSliderGrooveFrame,  nDeltaX / 2, 0, nDeltaX / 2 ,0);    // recenter in x, fixed in y

    adjustWidget(ui->frameCalendars,  nDeltaX / 2, nDeltaY / 2, nDeltaX / 2, nDeltaY / 2);   // recenter in both x and y

    adjustWidget(ui->tableWidgetAll,      0,       0, nDeltaX, nDeltaY);  // fixed at the top/left, follow in bottom/right
    adjustWidget(ui->tableWidgetWarnings, 0,       0, nDeltaX, nDeltaY);  // fixed at the top/left, follow in bottom/right
    adjustWidget(ui->tableWidgetErrors,   0,       0, nDeltaX, nDeltaY);  // fixed at the top/left, follow in bottom/right

    adjustWidget(ui->progressBar,   0, nDeltaY, nDeltaX, nDeltaY);        // fixed at the left, follow to the right and in y
    adjustWidget(ui->labelProgress, 0, nDeltaY, nDeltaX, nDeltaY);        // fixed at the left, follow to the right and in y
}

//----------------------------------------------------------------------------
// setGoButtonCaption
//
// 2023-12-01 First version
//----------------------------------------------------------------------------
void MainWindow::setGoButtonCaption()
{
// assume we have valid dFrom and dTo dates, select different caption if we have 2 dates range of just 1 date
    if (dFrom == dTo)
        ui->pushButtonGo->setText(QString("Hämta KatReg-data för %1en %2")
                                  .arg(TWUtils::getSwedishWeekdayName(dFrom),TWUtils::toISODate(dFrom)));
    else
        ui->pushButtonGo->setText(QString("Hämta KatReg-data fr.o.m. %1en %2 t.o.m. %3en %4")
                                  .arg(TWUtils::getSwedishWeekdayName(dFrom),TWUtils::toISODate(dFrom),
                                       TWUtils::getSwedishWeekdayName(dTo  ),TWUtils::toISODate(dTo  )));
}

//----------------------------------------------------------------------------
// on_calendarWidgetFrom_clicked
//
// 2023-12-01 First version
//----------------------------------------------------------------------------
void MainWindow::on_calendarWidgetFrom_clicked(const QDate &date)
{
// have a new "From" date
    dFrom = date;

// is it after the "To" date?
    if (dFrom > dTo)
    {
    // yes, set the "To" date to the same day as the "From" date
        dTo = dFrom;

    // refresh the other calendar (will call the other's slot but it's harmless)
        ui->calendarWidgetTo->setSelectedDate(dTo);
    }

// and refresh the Go button caption
    setGoButtonCaption();
}

//----------------------------------------------------------------------------
// on_calendarWidgetTo_clicked
//
// 2023-12-01 First version
//----------------------------------------------------------------------------
void MainWindow::on_calendarWidgetTo_clicked(const QDate &date)
{
// have a new "To" date
    dTo = date;

// is it before the "From" date?
    if (dTo < dFrom)
    {
    // yes, set the "From" date to the same day as the "To" date
        dFrom = dTo;

    // refresh the other calendar (will call the other's slot but it's harmless)
        ui->calendarWidgetFrom->setSelectedDate(dFrom);
    }

// and refresh the Go button caption
    setGoButtonCaption();
}

//----------------------------------------------------------------------------
// on_pushButtonExit_clicked
//
// 2023-12-01 First version
//----------------------------------------------------------------------------
void MainWindow::on_pushButtonExit_clicked()
{
// if we're processing (and on Windows), ask nicely first
    if ((AppState::eConnecting == appState) || (AppState::eProcessing == appState))
    {
    #if defined(Q_OS_WIN)
        if (!TWUtils::yesNoBox("Vill du avsluta?", "Avsluta TC2KatRegBas"))
            return;
    #endif

    // since we're processing, jump ship immediately (avoid hanging threads etc.)
        TWUtils::ignominiousExit();
    }

// else do a more vanilla exit
    QApplication::quit();
}

//----------------------------------------------------------------------------
// switchAppState (change app state and probably also the UI)
//
// 2023-12-01 First version
//----------------------------------------------------------------------------
void MainWindow::switchAppState(AppState newAppState)
{
// hide/show different things depending on app state
    switch (newAppState)
    {
    case AppState::eConnecting    :
        break;

    case AppState::eConnectedOk   :
    // successfully connected, queue up for the Go button to be shown
        QTimer::singleShot(nnNormalDelay,this,[this] { ui->pushButtonGo->show(); });

    // autostart or batch mode enabled? if so, press the Go button automagically after a slightly (3 times) longer delay
        if ((bAutoStart) || (bBatchMode))
            QTimer::singleShot(nnNormalDelay * 3,this,[this] { on_pushButtonGo_clicked(); });

    // else wait for the user to do the honors (i.e. clicking the button)
        break;

    case AppState::eProcessing    :
    // processing: time to slide the calendars and the Go button up and show the table widget instead
        {
        // disable the Go pushbutton directly to avoid recursive double clicks
            ui->pushButtonGo->setEnabled(false);

        // fiddle with the geometries for these two widgets
        // (we don't need to animate the other two table widgets, since the ending geometry is the designed geometry)
            auto rCalendars   = ui->frameCalendars->geometry();
            auto rTableWidget = ui->tableWidgetAll->geometry();

            QPropertyAnimation* animFrame    = new QPropertyAnimation(ui->frameCalendars,"geometry");
            QPropertyAnimation* animTableAll = new QPropertyAnimation(ui->tableWidgetAll,"geometry");

            animFrame->setDuration(nnSlideAnimDuration);
            animTableAll->setDuration(nnSlideAnimDuration);

        // starting geometries for the calendars: use the current
        // for the table widget: current geometry slided down one full height
            animFrame->setStartValue(rCalendars);
            animTableAll->setStartValue(rTableWidget.translated(0,rTableWidget.height()));

        // ending geometries: for the calendars: squished at top (top y same but zero height)
        // for the table widget: the current/designed geometry
            rCalendars.setHeight(0);
            animFrame->setEndValue(rCalendars);
            animTableAll->setEndValue(rTableWidget);

        // prepare for the group animation
            QParallelAnimationGroup* animGroup = new QParallelAnimationGroup;
            animGroup->addAnimation(animFrame);
            animGroup->addAnimation(animTableAll);

        // say what to do at the end of the animation
            connect(animGroup,&QParallelAnimationGroup::finished,this,[this]
            {
                ui->frameCalendars->hide();
                ui->visibilitySlider->show();
                pSliderGrooveFrame->show();
                ui->progressBar->show();

            // do an initial restock to setup the "No warnings.." and "No errors.." rows
            // most likely we have some PAS entries by now since fetchKatRegData() is already running
                restockAllTableWidgets();
            });

        // make sure the calenders and showAll tablewidget are visible and launch the animation
            ui->frameCalendars->show();
            ui->tableWidgetAll->show();
            ui->tableWidgetWarnings->hide();
            ui->tableWidgetErrors->hide();
            animGroup->start(QAbstractAnimation::DeleteWhenStopped); // (when done also deletes the 2 anim props)
        }
        break;

    case AppState::eRubbernecking :
    // we don't need the progressbar any more, so hide it
        ui->progressBar->hide();

    // hand over the vertical screen real estate owned by the progress bar to the table widgets above it
        auto rTableWidgetAll = mOriginalGeometries[ui->tableWidgetAll]; // reuse this one for all 3
        auto rProgressBar    = mOriginalGeometries[ui->progressBar];

        rTableWidgetAll.setBottom(rProgressBar.bottom());
        rProgressBar.setHeight(0);   // (not really needed since it's hidden now, but can't hurt)

        mOriginalGeometries[ui->tableWidgetAll]      = rTableWidgetAll;
        mOriginalGeometries[ui->tableWidgetWarnings] = rTableWidgetAll;
        mOriginalGeometries[ui->tableWidgetErrors]   = rTableWidgetAll;

        mOriginalGeometries[ui->progressBar] = rProgressBar;

    // do a dummy resize to update the screen now
        auto orgSize = size();
        resize(orgSize.width() + 1,orgSize.height());
        resize(orgSize);
    }

// we've switched ok, set our chap
    appState = newAppState;
}

//----------------------------------------------------------------------------
// refreshVisibilitySlider
//
// 2023-12-01 First version
//----------------------------------------------------------------------------
void MainWindow::refreshVisibilitySlider(TableViewStates e)
{
    auto r = ui->visibilitySlider->geometry();
    int nHeightInPixels = r.height();
    int nWidthInPixels  = r.width() / 2;  // assuming 3 positions (left, middle and right)

// construct and set the style sheet (2 parts, one for the groove and one for the handle)
    ui->visibilitySlider->setStyleSheet(QString(
                          ".QSlider::groove { background: transparent; height: %1px; } "
                          ".QSlider::handle { background: %2; border-radius: 11px; border: 1px solid gray; width: %3px;}")
                          .arg(nHeightInPixels).arg(acVisibilitySliderColors[e].name()).arg(nWidthInPixels));
}

//----------------------------------------------------------------------------
// on_visibilitySlider_valueChanged
//
// 2023-12-01 First version
//----------------------------------------------------------------------------
void MainWindow::on_visibilitySlider_valueChanged(int value)
{
// set the new view state
    auto newTableViewState = static_cast<TableViewStates>(value);

// same as before (i.e. no need to switch)?
    if (tableViewState == newTableViewState)
        return;

// ok update the viewstate
    tableViewState = newTableViewState;

// first change to color of the slider
    refreshVisibilitySlider(tableViewState);

// play hide and show with the table widgets
    switch (tableViewState)
    {
    case eShowAll      :
        ui->tableWidgetAll->clearSelection();   // clear possible prev. selection
        ui->tableWidgetAll->show();

        ui->tableWidgetWarnings->hide();
        ui->tableWidgetErrors->hide();
        break;

    case eShowWarnings :
        ui->tableWidgetAll->hide();

        ui->tableWidgetWarnings->clearSelection();
        ui->tableWidgetWarnings->show();

        ui->tableWidgetErrors->hide();
        break;

    case eShowErrors   :
        ui->tableWidgetAll->hide();
        ui->tableWidgetWarnings->hide();

        ui->tableWidgetErrors->clearSelection();
        ui->tableWidgetErrors->show();
        break;

    default:
        fubar("bad tableViewState");
    }
}

//----------------------------------------------------------------------------
// showProgress updates labelProgress and optionally steps the ProgressBar
//
// 2023-12-01 First version
//----------------------------------------------------------------------------
void MainWindow::showProgress(QString sText, int nBarValue /* = 0 */)
{
    ui->labelProgress->setText(sText);
    if (nBarValue > 0)
        ui->progressBar->setValue(nBarValue);

    qApp->processEvents();
}

//----------------------------------------------------------------------------
// on_pushButtonGo_clicked
//
// 2023-12-01 First version
//----------------------------------------------------------------------------
void MainWindow::on_pushButtonGo_clicked()
{
// sanity check the date range (cannot be backwards)
    if (dFrom > dTo)
        guruMeditation("didn't expect dFrom > dTo");

// if yesterday is selected and time is before 7 AM, ask a question
    if (dTo == QDate::currentDate().addDays(-1))
        if (QTime::currentTime().hour() < 7)
            if (!TWUtils::yesNoBox("Innan kl. 07:00 på morgonen syns inte gårdagens pat. personnummer. Ok att fortsätta ändå?"))
                return;

// check that we've wired up ok to Intelligence and Samba
    if (AppState::eConnectedOk != appState)
        guruMeditation("appState not eConnectedOk");

// start processing, change title and app state
    if (dFrom == dTo) // same day adventure?
        ui->labelTitle->setText(QString("KatReg Bas %1 %2").arg(TWUtils::getSwedishWeekdayName(dFrom),TWUtils::toISODate(dFrom)));
    else
        ui->labelTitle->setText(QString("KatReg Bas fr.o.m. %1 t.o.m. %2").arg(TWUtils::toISODate(dFrom),TWUtils::toISODate(dTo)));

// here we go, hide the calendars and show the table widget
    switchAppState(AppState::eProcessing);

// start the party via a timer
    QTimer::singleShot(nnNormalDelay,this,[this] { fetchKatRegData(); });
}

//----------------------------------------------------------------------------
// refreshPersonNo
//
// 2023-12-01 First version
//----------------------------------------------------------------------------
void MainWindow::refreshPersonNo()
{
// crawler still crawling?
    if (!scMain.bCrawlingDone)
        return;  // yes see you again

// obfuscation requested?
    if (bObfuscatePersonNo)
    {
    // replace the real personno. in the map with a bunch of test patients
        mPID2PersonNo.clear();  // do a white wedding on the real ones

        static auto slTestPatients = TWTCSupport::getAllTestPatients();
        if (slTestPatients.count() < 1)
            guruMeditation("didn't expect an empty list of test patients");

        for (auto st : mKatRegEntries)
        // make sure all entries in the easy-peasy map are filled with test patients
            mPID2PersonNo[st.nPID] = slTestPatients[abs(st.nPID) % slTestPatients.count()];
    }

// step thru them all and update the personno.s
    for (auto &st : mKatRegEntries)    // (& for r/w-access)
    {
    // try the easy map first
        if (mPID2PersonNo.contains(st.nPID))
        {
        // told you it was easy
            st.sPersonNo = mPID2PersonNo[st.nPID];
            continue;
        }

    // try the harder way (all entries have to make it through here at least once)
        if (st.sReceiptNo.isEmpty())  // no dice: need a nonempty receipt no.
            continue;

    // have a TC receiptno. look it up in the crawler's map
        QString sPersonNo = scMain.mRP.value(st.sReceiptNo);
        if (sPersonNo.isEmpty())
            continue;  // not found (probably the TC counter wasn't closed)

    // got a personno. is it any good?
        if (!TWUtils::checkPersonNo(sPersonNo).isEmpty())
            continue;  // some or other error, forget about it

    // ok gotcha
        st.sPersonNo = sPersonNo;

    // store it also in the easy-peasy map (for the next refresh)
        mPID2PersonNo[st.nPID] = sPersonNo;
    }
}

//----------------------------------------------------------------------------
// restockSingleTableWidget
// clear and refill a single table widget
//
// 2023-12-01 First version
//----------------------------------------------------------------------------
void MainWindow::restockSingleTableWidget(TableViewStates eState)
{
// use a helper map to keep the row listed in visit date/time order
    QMap<QString,QString> mVisitDateTime2KatRegEntry;

// support scroll to a "high water mark" (make the array static to persist across runs)
    static QDate adPrevHighest[viewStateCount];

// establish which table widget we're restocking
    QTableWidget* tw = nullptr;
    switch (eState)
    {
    case eShowAll      : tw = ui->tableWidgetAll;      break;
    case eShowWarnings : tw = ui->tableWidgetWarnings; break;
    case eShowErrors   : tw = ui->tableWidgetErrors;   break;
    default :
        guruMeditation("bad eState");
    }

    int   nRows       = 0;
    int   nHighestRow = 0;
    QDate dHighest;

    for (auto st : mKatRegEntries)
    {
    // check for sponges, show them or not?
        if (st.bSpongeDOA)    // sponge?
        {
        // establish visibility for this sponge
            if (st.bOk)
            // for normal sponges only check this boolean
                if (!bShowSponges)
                    continue;

            if (!st.bOk)
            // visibility for error sponges depends on two booleans
                if (!bShowErrorSponges && !bShowSponges)
                    continue;
        }

    // determine entry flavor
        bool bWarning = !st.sWarning.isEmpty();
        bool bError   = !st.bOk;
        if (bError == st.sError.isEmpty())  // both should either be true or false
            guruMeditation("bError and sError mismatch");

    // an error wins over a warning (i.e. when both are true, don't show this entry as a warning also)
        if (bError)
            bWarning = false;

    // not the ShowAll widget?
        if (eShowWarnings == eState)
            if (!bWarning)  // only warnings please
                continue;

        if (eShowErrors == eState)
            if (!bError)    // only errors please
                continue;

    // ok visible it is, create the key for the helper map
        QString sKey = st.sVisitDateTime + QString::number(nRows);  // (use nRows to make a unique key)
        if (mVisitDateTime2KatRegEntry.contains(sKey))              // so this should never happen
            guruMeditation("Found an existing visit-datetime+rowno. key in refreshTabletWidget (not good)");

    // this is a visible row, is it a row we should scroll to (i.e. make sure is visible)?
        if ((st.cEye == ccRightEye) || (st.cEye == ccLeftEye))
        {
        // yes if this row has the eye flavor set
            dHighest    = st.dVisit;
            nHighestRow = nRows;
        }

    // stuff it with the key to our main map and count the #
        mVisitDateTime2KatRegEntry.insert(sKey,st.sKey);   // (map data here is the key to the main map)
        ++nRows;
    }

// start from scratch with a new set of rows
    tw->clearContents();
    tw->setRowCount(nRows);

// any warnings or errors? if they just appeared we need to reset the column span for the first row
    if ((nRows > 0) && (eShowAll != eState))
    // we have visible rows now, make sure we cancel a previous setSpan() for the "No warnings/errors" caption
        if (tw->columnSpan(0,colDayOfTheWeek) > 1)
            tw->setSpan(0,colDayOfTheWeek,1,1);

// step thru the KatReg entries through the visitdatetime map
    int nRow = 0;
    QString sPrevVisitDate;  // helper for deciding if we're to show the day of the week for this row

    for (auto sVisitKey : mVisitDateTime2KatRegEntry.keys())
    {
    // get the KatReg entry for this row (and do some sanity checks)
        if (!mVisitDateTime2KatRegEntry.contains(sVisitKey))
            fubar(QString("Something is wrong with the visit date/time map ('%1')").arg(sVisitKey));
        QString sKey = mVisitDateTime2KatRegEntry[sVisitKey];
        if (!mKatRegEntries.contains(sKey))
            fubar(QString("Something is wrong with the KatReg entries map ('%1')").arg(sKey));

        auto st = mKatRegEntries[sKey];
        if (st.sKey != sKey)  // these should always match up
            guruMeditation("st.sKey != sKey (not good)");

    // determine error status for this entry
        bool bError = !st.bOk;
        if (bError == st.sError.isEmpty())  // both should either be true or false
            guruMeditation("bError and sError mismatch");

    // do the columns: first one is the key to this KatReg entry in the main map
    // (column has zero width, don't bother with readonly etc.)
        tw->setItem(nRow,colMapKey,new QTableWidgetItem(sKey));

    // --- use lambdas for composing the table widget items ---
        auto createAlignedTWI = [] (QString s, QString sToolTip, Qt::Alignment a = Qt::AlignHCenter)
        {
            auto wi = new QTableWidgetItem(s);
            setTableWidgetItemRO(wi); // set this item readonly
            wi->setTextAlignment(a);
            wi->setToolTip(sToolTip);
            return wi;
        };

        auto createCenterAlignedVWE = [createAlignedTWI] (VWEStruct vwe)
        {
        // have VWE chap, use the value and the tooltip (ignore warnings or errors)
            QString sValue = vwe.sValue;
            sValue.replace("\n"," + ");  // for multiple terms: avoid \n in the table view, use " + "
            return createAlignedTWI(sValue, vwe.sToolTip);
        };

    // day of the week (abbrev.): show only if this row is the first row for this day
        QString sValue = "";
        if (sPrevVisitDate != st.sVisitDate)
            sValue = TWUtils::getSwedishWeekdayNameU1(st.dVisit.dayOfWeek()).left(2);
        tw->setItem(nRow,colDayOfTheWeek,createAlignedTWI(sValue,"Besöksdag"));
        sPrevVisitDate = st.sVisitDate; // remember for the next row :=)

    // visit date and time
        sValue = st.sVisitDateTime;                  // default: show "YYYY-MM-DD HH:MM"
        if (tNotFoundInPAS == st.tVisit)             // unless the visit time is missing:
            sValue = st.sVisitDate + QString(6,' '); // then show 6 spaces = about the same width as " HH:MM"
        tw->setItem(nRow,colVisitDateTime,createAlignedTWI(sValue,"Besöksdatum och tid"));

    // field #2 personno.: if it's empty (because we're waiting for the Samba crawler) instead show "..."
        sValue = TWUtils::midLineEllipsis();         // pessimistic default (no personno. present)
        if (!st.sPersonNo.isEmpty())
        // got one, pretty print it
            sValue = TWTCSupport::personNoForTC(st.sPersonNo);
        tw->setItem(nRow,colPersonNo,createAlignedTWI(sValue,"Pat. personnummer"));

    // #3: right or left eye
        tw->setItem(nRow,colEye,createCenterAlignedVWE(st.mAllCooked[KatRegFields::eRightOrLeftEye]));

    // #4: visus right eye (any value terms are expected to be merged with the measurement value)
        tw->setItem(nRow,colVisusRight,createCenterAlignedVWE(st.mAllCooked[KatRegFields::eVisusRight]));

    // #4: visus left eye (any value terms are expected to be merged into the measurement value)
        tw->setItem(nRow,colVisusLeft,createCenterAlignedVWE(st.mAllCooked[KatRegFields::eVisusLeft]));

    // #5: NIKE (ind. group)
        tw->setItem(nRow,colNIKE,createCenterAlignedVWE(st.mAllCooked[KatRegFields::eNIKE]));

    // #6: date for entering waiting list (set the date in italics for a nice effect)
        auto wiWaiting = createCenterAlignedVWE(st.mAllCooked[KatRegFields::eDateWaitingList]);
        auto f = wiWaiting->font();
        f.setItalic(true);
        wiWaiting->setFont(f);
        tw->setItem(nRow,colDateWaitingList,wiWaiting);

    // don't show #7: date for operation here (should be same as the visit date anyway)
    // also skip over #8: and #9: eye states

    // #10: op. typ (a single value)
        tw->setItem(nRow,colOpType,createCenterAlignedVWE(st.mAllCooked[KatRegFields::eTypeOfOperation]));

    // #11: linsmaterial (should be a single value)
        tw->setItem(nRow,colLinsmaterial,createCenterAlignedVWE(st.mAllCooked[KatRegFields::eLensMaterial]));

    // skip over #12 and #13

    // #14: antibiotika (can be multiple values)
        tw->setItem(nRow,colAntibiotika,createCenterAlignedVWE(st.mAllCooked[KatRegFields::eAntibiotics]));

    // skip over #15

    // #16: profylax (can be 0 or more values)
        tw->setItem(nRow,colPostOperativt,createCenterAlignedVWE(st.mAllCooked[KatRegFields::ePostoperativeIP]));

    // #17: name of the surgeon (or an error or a warning)
        if (!st.bOk)
        {
        // an error (or more) unfortunately
            if (st.sError.isEmpty())
                guruMeditation("didn't expect an empty sError");

        // collect all error texts
            QStringList slErrors(st.sError); // first this one
            for (auto e : lKatRegFields)     // then all the katreg flavored ones
                if (!st.mAllCooked[e].sError.isEmpty())
                    slErrors += st.mAllCooked[e].sError;

            slErrors.removeDuplicates();     // (in case st.sError was a copy from the cooked errors)

        // set the error and tooltip strings
            QString sError = slErrors[0];           // for the red error text, keep only the top/first error
            if (slErrors.count() > 1)               // prepend with # of errrors? yes if there's more than one
                sError = QString("%1 fel: %2").arg(slErrors.count()).arg(sError);

            QString sToolTip = slErrors.join("\n"); // for the tooltip show each error on separate lines

            auto wiError = createAlignedTWI(" " + sError,sToolTip,Qt::AlignLeft);  // prefix with " " for better visibility
            wiError->setBackground(QBrush(acVisibilitySliderColors[eShowErrors]));
            tw->setItem(nRow,colSurgeonOrWOrE,wiError);
        }
        else if (!st.sWarning.isEmpty())
        {
        // a warning
        // there are currently very few warning flavors in TC2KatReg so no need for looping in mAllCooked
            auto wiWarning = createAlignedTWI(" " + st.sWarning,st.sWarning,Qt::AlignLeft);
            wiWarning->setBackground(QBrush(acVisibilitySliderColors[eShowWarnings]));
            tw->setItem(nRow,colSurgeonOrWOrE,wiWarning);
        }
        else
        {
        // no error(s) or warning(s), so there's room to show the surgeon's name
            QString sText    = st.mAllCooked[KatRegFields::eSurgeon].sValue;
            QString sToolTip = st.mAllCooked[KatRegFields::eSurgeon].sToolTip;
            auto wiSurgeon = createAlignedTWI(" " + sText,sToolTip,Qt::AlignLeft);
            tw->setItem(nRow,colSurgeonOrWOrE,wiSurgeon);
        }

    // light up the "ShowBox" icon for the full Monty of this row? yes, if we have an eye flavor and a visit date set
        if ((st.cEye != ccUnspecifiedEye) && (st.dVisit.isValid()))
        {
            auto wiShowBox = new QToolButton();
            wiShowBox->setIcon(iconShowBox);
            wiShowBox->setToolTip("Klicka här för att visa alla KatReg-data för detta besök");
            wiShowBox->setEnabled(true);
            tw->setCellWidget(nRow,colShowBox,wiShowBox);

        // prepare a nice click handler lambda
            connect(wiShowBox,&QToolButton::clicked,this,[this,st]
            {
            // wire up a ShowBox dialog and modally wait for it
                ShowBox sh(this,st);
                sh.setModal(true);
                sh.exec();
            });
        }

    // and if we're have a valid personno, show the jump to TC icon
        if ("" == TWUtils::checkPersonNo(st.sPersonNo))
        {
            auto wiJumpToTC = new QToolButton();
            wiJumpToTC->setIcon(iconTC);
            wiJumpToTC->setToolTip(QString("Klicka här för att öppna pat. %1 i TakeCare").arg(TWTCSupport::personNoForTC(st.sPersonNo)));
            wiJumpToTC->setEnabled(true);
            tw->setCellWidget(nRow,colJumpToTC,wiJumpToTC);

        // establish a click handler for this row: try to jump/open TC
            connect(wiJumpToTC,&QToolButton::clicked,this,[this,st]
            {
            // get the personno. (or reservno.) for this row, is it a good one?
                QString sPersonNo = st.sPersonNo;
                if (sPersonNo.isEmpty())  // no dice no good
                    return;

            // is a smartcard present? look for a username from the first card (if any) require it to be nonblank
                QString sTCUserName = TWTCSupport::getTCUserNameFromSmartcard(0);
                if (sTCUserName.isEmpty())
                    return TWUtils::warningBox("Hittar inget e-tjänstekort (behövs för att öppna pat. i TakeCare).");

            // try to establish the path to TC
                QString sTCPath = sTCPathOverride;      // if we have a preset path use that
                if (sTCPath.isEmpty())                  // else try to find it right now
                    sTCPath = TWTCSupport::getTCPath();
                if (sTCPath.isEmpty())
                // use this path as a last-ditch attempt
                    sTCPath = "C:\\TakeCare";

            // prepare the twinkle box time (if TC is not started, increase it)
            // check now before actually launching TC
                int nDelay = nnShortTCDelay;
                if (eTCAppNotStarted == TWTCSupport::getTCAppState())
                    nDelay = nnLongTCDelay;

            // bombs away
                QString sError = TWTCSupport::openTCPersonNo(sTCPath,sTCUserName,sPersonNo);
                if (!sError.isEmpty())
                // something didn't agree with the launching, show it as a warning
                    return TWUtils::warningBox(sError,"Öppna TakeCare");

            // ok TC should be launching now, issue the twinklebox as a comfort for the user
                TWUtils::twinkleBox(QString("Öppnar %1 i TakeCare...").arg(sPersonNo),"Start av TakeCare",nDelay);
            });
        }
        else
        {
            auto wiEmpty = new QTableWidgetItem("");
            setTableWidgetItemRO(wiEmpty);
            wiEmpty->setToolTip("Personnummer saknas eller är felaktigt");
            tw->setItem(nRow,colJumpToTC,wiEmpty);
        }

    // that's all for this row, next please
        ++nRow;
    }

// sanity check # of rows
    if (nRow != nRows)
        guruMeditation("nRow != nRows");

// no rows shown? if we're displaying the warning or error widget show a helper text on the first row
    if ((0 == nRows) && (eShowWarnings == eState))
    {
        tw->setRowCount(1);

        tw->setSpan(0,colDayOfTheWeek,1,colCount - colDayOfTheWeek);
        auto tw0 = new QTableWidgetItem("Inga varningar.");

        QFont fntItalic(tw->font());
        fntItalic.setItalic(true);
        tw0->setFont(fntItalic);

        setTableWidgetItemRO(tw0);
        tw->setItem(0,colDayOfTheWeek,tw0);
    }

    if ((0 == nRows) && (eShowErrors == eState))
    {
        tw->setRowCount(1);

        tw->setSpan(0,colDayOfTheWeek,1,colCount - colDayOfTheWeek);
        auto tw0 = new QTableWidgetItem("Inga fel.");

        QFont fntItalic(tw->font());
        fntItalic.setItalic(true);
        tw0->setFont(fntItalic);

        setTableWidgetItemRO(tw0);
        tw->setItem(0,colDayOfTheWeek,tw0);
    }

// finally, should we scroll to a new "high-water" mark i.e. row?
// yes if we have a new highest date
    if (dHighest > adPrevHighest[eState])
    {
    // specify the eye flavor column in case of horizontal scrolling frenzy
        tw->scrollToItem(tw->item(nHighestRow,colEye),QAbstractItemView::PositionAtTop);

    // and remember this new "high-water" row
        adPrevHighest[eState] = dHighest;
    }

// that's all for this restocking, thank you
}

//----------------------------------------------------------------------------
// restockAllTableWidgets
//
// 2023-12-01 First version
//----------------------------------------------------------------------------
void MainWindow::restockAllTableWidgets()
{
// calendars still visible (because we're busy scrolling them off the screen)?
    if (ui->frameCalendars->isVisible())
        return; // hold your horses until they're gone for good

// pretty simple, just call the 3 single chaps
    restockSingleTableWidget(eShowAll);
    restockSingleTableWidget(eShowWarnings);
    restockSingleTableWidget(eShowErrors);
}

//----------------------------------------------------------------------------
// dbError
//
// 2023-12-01 First version
//----------------------------------------------------------------------------
void MainWindow::dbError(QString sError)
{
// if the error is less than 8 chars (probably an error no.) try to prettyprint it
    if (sError.length() < 8)
        sError = TWTCSupport::prettyPrintErrors(sError);

// show an errorbox unless it's the "forcibly closed" error
    if ("db forcibly closed" != sError)
        TWUtils::errorBox(sError);

// say goodbye to a cruel world (all db errors are considered fatal)
// cannot use qApp->exit() because it will resume/continue our main (GUI) thread
    TWUtils::ignominiousExit();
}

//----------------------------------------------------------------------------
// fetchKatRegData
//
// 2023-12-01 First version
// 2024-02-11 New valid op check: type of operation is present
//----------------------------------------------------------------------------
void MainWindow::fetchKatRegData()
{
// user has pressed the Go button
// launch the main Samba crawler since we know now how many days to go back
    auto nDaysBack = dFrom.daysTo(QDate::currentDate()) + 3; // overshoot a couple of days (to be on the safe side)

    if (sLocalDirectory.isEmpty())
    // no local directory set for Samba files? on Windows we support a remote connection as well
        scMain.setSambaCredentialsFromIniFile();
    else
        scMain.setLocalDirectory(sLocalDirectory);

// wire up the main Samba crawler done signal
    connect(&scMain,&SambaCrawler::crawlingDone,this,[this] (int nMapCount)
    {
        Q_UNUSED(nMapCount);

    // if there's an error I'm afraid we have to call it a day
        auto sError = scMain.sErrorMsg;
        if (!sError.isEmpty())
        {
            TWUtils::errorBox(sError);
            TWUtils::ignominiousExit();
        }

    // no error(s), is the app waiting for us?
        if (bWaitingForSamba)
        // yes, do the final step from here (where the KatReg file also is written)
            burialAtSea();
    });

// reset our personno. map
    mPID2PersonNo.clear();

// and start the our main Samba crawler asynchronously
    scMain.crawl(nCompanyCode,nDaysBack);
    bWaitingForSamba = false;   // assume the Samba main crawler will finish before Intelligence (almost always true)


// -----------------------------------------------------------------------
// -------------- Intelligence time: start with PAS ----------------------
    showProgress("Hämtar besöken från kassan...",5);

    mKatRegEntries.clear(); // prepare ourselves for the PAS retrieval
    slErrors.clear();       // possible error(s) go here

// cook our own bobbytable: select from both PAS and PAS_Billing simultaneously
    TCBobbyTables stPASAndPASBilling("PAS as pa,PAS_Billing as pb");

    db.select("pa.PatientID,StartDatetime,ReceiptNumber,VisitTypeID",stPASAndPASBilling,
              QString("IsRetail = 0 and IsCancelled = 0 and CompanyID = %1 and SavedAtCareUnitID = %2 and "
                      "StartDatetime >= '%3' and StartDatetime < '%4' and pa.PatientID = pb.PatientID and pa.DocumentID = pb.DocumentID").
                      arg(nCompanyCode).arg(nCareUnitID).arg(TWUtils::toISODate(dFrom),TWUtils::toISODate(dTo.addDays(1))),
    [this](DBRowStruct rs)
    {
    // don't forget this lambda is running on another thread (so for example user interaction is *verboten*)
        KatRegEntryStruct st;

    // TC2KatReg: create two entries (right eye and left eye) for a single PAS receipt
    // but set them all to sponges/dormant state (PAS entries without matching journals are considered harmless here)
    // duplicate PAS entries (same patient + same visit day) are not flagged as errors/duplicates (consider harmless)
        st.bOk            = true;  // optimistic default
        st.sError         = "";
        st.sWarning       = "";
        st.nPID           = rs.longlongValue("PatientID");

        st.bSpongeDOA     = true;  // will be unsponged when a matching eye journal is found
        st.dVisit         = rs.dateValue("StartDatetime");
        st.sVisitDate     = TWUtils::toISODate(st.dVisit);
        st.tVisit         = rs.timeValue("StartDatetime");
        st.sVisitDateTime = TWUtils::toISODateTimeNoSeconds(st.dVisit,st.tVisit);

        st.sReceiptNo     = rs.stringValue("ReceiptNumber");
        st.sVisitType     = rs.stringValue("VisitTypeID");

        st.sPersonNo      = "";
        st.cEye           = ccRightEye;  // no we're not forgetting the left eye (done in the clone further down)

    // is this a test (not a real) patient?
        if (TWTCSupport::isTestPatient(st.nPID))
        {
            st.bOk    = false;
            st.sError = "Testpatient (laddas ej upp till KatReg)";
        }

    // for KatReg we need a physical visit (visit type 0 or 1) so skip over distance contacts and absentees
        if (("0" != st.sVisitType) && ("1" != st.sVisitType))
            return;

    // save this entry in our map, create the key for this visit
        st.sKey = st.createKey(st.dVisit,st.nPID,st.cEye);

    // have a PAS entry for the right eye for this visit date already? if so skip over this one
    // we support only one right- and left-eye op. per day and patient anyway so skipping here is harmless
        if (mKatRegEntries.contains(st.sKey))
            return;

    // save this entry (for the right eye)
        mKatRegEntries.insert(st.sKey,st);

    // and clone into a 2nd entry (for the left eye)
        auto stLeft = st;           // clone everything from the right eye
        stLeft.cEye = ccLeftEye;    // except the eye flavor
        stLeft.sKey = stLeft.createKey(stLeft.dVisit,stLeft.nPID,stLeft.cEye);

    // the left eye should *not* be present in the map already
        if (mKatRegEntries.contains(stLeft.sKey))
            guruMeditation("Left eye PAS was already in the map (not good)");

        mKatRegEntries.insert(stLeft.sKey,stLeft);
    });

// --- done retrieving from PAS --------------------------------------------------------
    restockAllTableWidgets();   // show what we've got so far

// got any errors? they are considered fatal, grab the first one and fubar() out
    if (slErrors.count() > 0)
        fubar("Fel vid hämtning av kassa: " + slErrors[0]);

// no need for counting # of visits yet (there are all sponges for now)

// just step up the progressbar
    showProgress("Letar efter journaler med operationstyp...",10);


// ------------------------------------------------------------------------------------------------------------
// -------------------------------------- next up: looking for journals ---------------------------------------
// store patientids + documentids found for the specified date range in this map (with the version as the data)
    QMap<QString,int> mPIDDocs;

// use this struct for storing all the journals for a single day and patient
    struct JournalEntryStruct
    {
        QDate dEvent;
        PID_t nPID;
        int   nDocumentID;
        int   nVersion;
    };

// create a "whitelist" set of all the patientIDs that have a TypeOfOperation termid for the given date range
    QSet<PID_t> setWhitelistPIDs;

// also create a "whitelist" map of the dates+PIDs that have a TypeOfOperation termid present
// this first one is a preliminary map which might contain "false positives"
// key is date+PID and data is the jouurnals found for that date+patient with a matching termid
    QMap<QString,QList<JournalEntryStruct>> mPreliminaryWhitelistDatesAndPIDs;

// retrieve all journals within the specified date range
// left join on the type of operation termid in measurements
// (for every visit day and patient found, we'll require at least one TypeOfOperation termid)
    QString sCustomSQL = "select c.EventDate, c.PatientID, c.DocumentID, c.Version, m.KeywordTermID ";
    sCustomSQL += "from " + stTCCorral.CaseNotes.cook() + " as c ";
    sCustomSQL += "left join " + stTCCorral.CaseNotes_Measurements.cook() + " as m ";
    sCustomSQL += "on ";
    sCustomSQL += "c.PatientID = m.PatientID and c.DocumentID = m.DocumentID and c.Version = m.Version and ";
    sCustomSQL += QString("KeywordTermID = %1 ").arg(nnTypeOfOperationKeywordTermID);
    sCustomSQL += "where ";
    sCustomSQL += QString("CreatedAtCareUnitID = %1 and EventDate >= '%2' and EventDate < '%3'").
                          arg(nCareUnitID).arg(TWUtils::toISODate(dFrom),TWUtils::toISODate(dTo.addDays(1)));

    db.sqlcmd(sCustomSQL,[&mPIDDocs,&setWhitelistPIDs,&mPreliminaryWhitelistDatesAndPIDs](DBRowStruct rs)
    {
    // retrieve the patientid + documentid pairs we should verify the highest version for
    // use 0 as the placeholder version no (also: don't worry about duplicates, they'll just overwrite each other)
        mPIDDocs.insert(TWUtils::stringList2CommaList({rs.stringValue("PatientID"),rs.stringValue("DocumentID")}), 0);

    // if this entry has a nonnull termid, create an entry in the whitelist set and map
        int nKeywordTermID = rs.intValue("KeywordTermID");
        if (0 == nKeywordTermID)
        // returned NULL so no termid for you, next journal please
            return;

    // a consistency check can't hurt
        if (nnTypeOfOperationKeywordTermID != nKeywordTermID)
            guruMeditation("Got a bad termid %d",nKeywordTermID);

    // get the stuff for the whitelists
        auto nPID         = rs.longlongValue("PatientID");
        setWhitelistPIDs += nPID;

        auto dEvent       = rs.dateValue("EventDate");
        auto nDocumentID  = rs.intValue("DocumentID");
        auto nVersion     = rs.intValue("Version");
        QString sDPKey    = TWUtils::toYMD(dEvent) + TWTCSupport::PID2String(nPID);

        QList<JournalEntryStruct> lJournals;    // assume we're the first journal for this day+patient in the whitelist
        if (mPreliminaryWhitelistDatesAndPIDs.contains(sDPKey))
            lJournals = mPreliminaryWhitelistDatesAndPIDs[sDPKey]; // if we're not first, append to the existing list

        lJournals.append({dEvent, nPID, nDocumentID, nVersion});
        mPreliminaryWhitelistDatesAndPIDs[sDPKey] = lJournals;
    });

// introduce map mDatesAndPIDs a temp storage of all the journals for a given date and patient
// key is (visit date + PID) amd the data value is the list of JournalEntrystructs for that PID and that date
// (note: in contrast to TC2ARV/TC2PatReg: where the key is (date + PID + documentID), value is a single JournalEntryStruct)
// this is a bigger version of the map, after checking the whitelist we'll create another one further down
    QMap<QString,QList<JournalEntryStruct>> mDatesAndPIDsBeforeScreening;

// ------------- step thru and retrieve the highest version no. for each PatientID, DocumentID pair -------------
// yes the visit dates and PIDs are duplicated (both in the key and the struct) makes the coding easier below :-)
    int nNo    = 0; // for showProgress()
    int nTotal = mPIDDocs.count();

// step thru the PIDDocs map and get their version nos.
    showProgress(QString("Kontrollerar %1 journaler...").arg(mPIDDocs.count()),12);
    for (auto sKey : mPIDDocs.keys())
    {
    // count 'em
        ++nNo;

    // decompose the mPIDDOCs key back into a PID and a documentID
        auto sl = TWUtils::commaList2StringList(sKey);
        if (sl.count() != 2)
            fubar("Something is seriously wrong with the PIDDocs map");
        PID_t nPID      = sl[0].toLongLong();
        int nDocumentID = sl[1].toInt();

    // is this PID a kosher one? i.e. included in the whitelist set?
        if (!setWhitelistPIDs.contains(nPID))
            continue;   // no so skip this chap

    // ok, so look for the highest version no. (i.e. the currently valid one) for this DocumentID and patient
        QDate dEvent;   // to be set by the lambda
        db.select("top 1 Version,EventDate",stTCCorral.CaseNotes,
                  QString("CreatedAtCareUnitID = %1 and PatientID = %2 and DocumentID = %3").
                  arg(nCareUnitID).arg(nPID).arg(nDocumentID),"Version desc",
        [this,sKey,&dEvent,&mPIDDocs](DBRowStruct rs)
        {
        // is the highest version of this documentID within our date range?
            dEvent = rs.dateValue("EventDate");
            if ((dEvent >= dFrom) && (dEvent < dTo.addDays(1)))
            {
                int nVersion = rs.intValue("Version"); // yes, within our range so stash it
                if (nVersion < 1)
                    fubar("Got a bad version number (lower than 1) from Intelligence");

                mPIDDocs[sKey] = nVersion;  // refresh/update the version no.
            }
        },eDBRowsExpectExactlyOne);

    // so was this document no. inside our date range?
        int nVersion = mPIDDocs[sKey];
        if (0 == nVersion)
            continue;   // no, still only the placeholder, ignore this journal

    // yes it's inside the date range, but check for test patients
        if (TWTCSupport::isTestPatient(nPID))
            continue;   // step over test patients

    // store this journal in our "before screening" map (sorted on visit date + patientid)
        QString sDPKey = TWUtils::toYMD(dEvent) + TWTCSupport::PID2String(nPID);

        QList<JournalEntryStruct> lJournals;                  // assume we're the first journal for this visit day and patient/PID
        if (mDatesAndPIDsBeforeScreening.contains(sDPKey))
            lJournals = mDatesAndPIDsBeforeScreening[sDPKey]; // if we're not first, append to the existing list

        lJournals.append({dEvent, nPID, nDocumentID, nVersion});
        mDatesAndPIDsBeforeScreening[sDPKey] = lJournals;

    // show the user that we're indeed making some progress (start at total progress = 15% and end at 30%)
        showProgress(QString("Hämtar journaler (%1%)... ").arg((nNo * 100) / nTotal),15 + ((nNo * 15) / nTotal));   // (nTotal is always > 0)
    }

// --- step thru and copy from the preliminary whitelist map to the definitive one (skipping those that were "false positive" hits)
    QMap<QString,QList<JournalEntryStruct>> mWhitelistDatesAndPIDs;
    for (auto sDPKey : mPreliminaryWhitelistDatesAndPIDs.keys())
    {
    // do this date + pid exist in the mDatesAndPIDsBeforeScreening map?
        if (!mDatesAndPIDsBeforeScreening.contains(sDPKey))
        // none found (so all whitelist preliminary entries were "false positives" for that day+patient)
            continue;   // skip to next date+patient

    // get the list for this date+patient
        auto lPreliminary = mPreliminaryWhitelistDatesAndPIDs[sDPKey];

    // and the list of journals for this day+patient from the "beforescreening" map
        auto lJournalsBefore = mDatesAndPIDsBeforeScreening[sDPKey];

    // prepare a list for the "real/definitive" whitelist map
        QList<JournalEntryStruct> l;

    // step thru our preliminary whitelist and see if each entry exist in the "before screening" list
        for (auto stPrel : lPreliminary)
        {
            bool bExists = false;   // pessimistic default

        // step thru and check for the same document id and version no. in the "before screening" list
        // note: this is indeed terrible perfomance but usually each list contains just a couple of entries
            for (auto stBefore : lJournalsBefore)
            {
            // integrity checks
                if (stPrel.dEvent != stBefore.dEvent)
                    guruMeditation("dEvent was not the same (this is bad)");

                if (stPrel.nPID   != stBefore.nPID  )
                    guruMeditation("nPID was not the same (this is bad)");

            // do we have a match?
                bExists = ((stPrel.nDocumentID == stBefore.nDocumentID) && (stPrel.nVersion == stBefore.nVersion));
                if (bExists)
                    break;
            }

        // got a match, fill up the real/definitive list
            if (bExists)
                l.append(stPrel);
        }

    // got at least one entry in the "definitive" list for this date+patient?
        if (l.isEmpty())
        // no, skip please
            continue;

    // ok save this to the map
        if (mWhitelistDatesAndPIDs.contains(sDPKey))
            guruMeditation(QString("the whitelist map already had the key '%1'").arg(sDPKey));

        mWhitelistDatesAndPIDs[sDPKey] = l;
    }

#if !defined(Q_OS_WIN) && defined(QDEBUG_H)
// see if there was any difference/shrinking between the two whitelist maps
    int nPrelims     = mPreliminaryWhitelistDatesAndPIDs.count();
    int nDefinitives = mWhitelistDatesAndPIDs.count();
    qDebug() << "Preliminary whitelist map has" << nPrelims << "entries";
    qDebug() << "Definitive whitelist map has" << nDefinitives << "entries";
#endif

// create another mDatesAndPID maps, this is where we store the journals after being screened by the whitelist map
// key is (visit date + PID) amd the data value is the list of JournalEntrystructs for that PID and that date
    QMap<QString,QList<JournalEntryStruct>> mDatesAndPIDs;

// start the screening process: only those journals that have a matching key in thw whitelist map survives
    for (auto sDPKey : mDatesAndPIDsBeforeScreening.keys())
    {
    // do this date + pid exist in the whitelist map?
        if (!mWhitelistDatesAndPIDs.contains(sDPKey))
        // no so sorry
            continue;   // skip to next date+patient

    // this entry has a matching TypeOfOperation, copy it to the new map
        if (mDatesAndPIDs.contains(sDPKey))
            guruMeditation(QString("the DatesAndPIDs map already had the key '%1'").arg(sDPKey));

        mDatesAndPIDs[sDPKey] = mDatesAndPIDsBeforeScreening[sDPKey];
    }

// check the sizes here as well
#if !defined(Q_OS_WIN) && defined(QDEBUG_H)
// see if there was any difference/shrinking between the two whitelist maps
    int nBefore  = mDatesAndPIDsBeforeScreening.count();
    int nAfter   = mDatesAndPIDs.count();
    qDebug() << "DatesAndPIDs map before screeening has" << nBefore << "entries";
    qDebug() << "DatesAndPIDs map after screeening has" << nAfter << "entries";
#endif


// ----------------------------------------------------------------------------------------------------
// ------------ have all the journals in mDatesAndPIDs, step thru in ascending date order --------------
    nNo    = 0;   // for showProgress() (starting at total progress = 30%)
    nTotal = mDatesAndPIDs.count();

    for (auto lJournals : mDatesAndPIDs)
    {
    // check that the list for this visit day and patient is not empty (we require at least one JournalEntryStruct)
        if (lJournals.isEmpty())
            guruMeditation("Nice try but the list of journals is empty");

    // show some progress (get the current date from the first journal in the list)
        QString sCaption  = "Hämtar KatReg data från %1...";
        int nProgressStep = 60;
        showProgress(sCaption.arg(TWUtils::toISODate(lJournals[0].dEvent)),30 + ((nNo++ * nProgressStep) / nTotal));  // (nTotal is always > 0)

    // inside here we'll use journalentries with added eye booleans to keep track of those right/left flavors
        struct EyeJournalStruct : JournalEntryStruct
        {
            bool bRightEye;
            bool bLeftEye;
        };
        QList<EyeJournalStruct> lEyeJournals;

    // retrieve the value terms for this visit by stepping thru the journals for it
    // count # of right- or left eye value terms we see (if none found, we'll ignore this visit)
        int nRightEyeTermsFound = 0;
        int nLeftEyeTermsFound  = 0;

    // start with the value terms to determine if this visit is one we're interested in (i.e. has any right/left eye keyword terms)
        KatRegTermSetMap mTermSetsRightEye;  // save the value terms here (in a hash/set)
        KatRegTermSetMap mTermSetsLeftEye;   // or here for left-eyed terms

    // --- 1st loop: retrieve all the keyword terms for this visit day and patient
        for (auto stJournal : lJournals)
        {
        // copy the vanilla journal stuff into a new EyeJournal flavor (for keeping track of which stuff goes to which eye)
            EyeJournalStruct stEye;
            stEye.dEvent      = stJournal.dEvent;
            stEye.nPID        = stJournal.nPID;
            stEye.nDocumentID = stJournal.nDocumentID;
            stEye.nVersion    = stJournal.nVersion;
            stEye.bRightEye   = false;
            stEye.bLeftEye    = false;

        // create a map for all the value terms found for this journal/document (regardless of eye flavor)
        // (i.e. we need to use the keyword id as key because right/left eye flavors are not yet established)
            QMap<int,QSet<int>> mSingleJournalKeywordTerms;  // use a QSet for deduplication

            db.select("KeywordTermID,ValueTermID",stTCCorral.CaseNotes_ValueTerms,
                      QString("PatientID = %1 and DocumentID = %2 and Version = %3").
                      arg(stEye.nPID).arg(stEye.nDocumentID).arg(stEye.nVersion),
            [&mSingleJournalKeywordTerms](DBRowStruct rs)
            {
            // get the next pair of term IDs for this journal
                int nKeywordTermID = rs.intValue("KeywordTermID");
                int nValueTermID   = rs.intValue("ValueTermID");

            // store 'em in the map
                mSingleJournalKeywordTerms[nKeywordTermID].insert(nValueTermID);
            });

        // any right or left eye keyword terms found in this journal?
            if (mSingleJournalKeywordTerms.contains(nnRightOrLeftEyeKeywordTermID))
            {
            // yes, so get the value (QSet) for this keyword term
                auto s = mSingleJournalKeywordTerms[nnRightOrLeftEyeKeywordTermID];

            // set the eye flavor booleans directly (single pass, courtesy of QMap/QSet's automagic deduplication)
                stEye.bRightEye = (s.contains(nnRightTermID));
                stEye.bLeftEye  = (s.contains(nnLeftTermID ));

            // count 'em (# of right/left eye keywords found in all journals for this patient and visit day)
                if (stEye.bRightEye)
                    ++nRightEyeTermsFound;

                if (stEye.bLeftEye)
                    ++nLeftEyeTermsFound;
            }

        // any right/left eye terms in this journal?
            if ((!stEye.bRightEye) && (!stEye.bLeftEye))
            // no, so treat this journal as an eye-"neutral" one, i.e. equally applicable to both eyes
                stEye.bLeftEye = stEye.bRightEye = true;

        // -----------------------------------------------------------------------------------------------
        // we've established eye flavor(s), store/split the value set terms according to those flavor(s)
            for (auto nKeywordTermID : mSingleJournalKeywordTerms.keys())
            {
            // is there a KatRegFields enum for this keyword? otherwise this keyword is uninteresting to us
                if (!mKeyword2KatRegField.contains(nKeywordTermID))
                    continue;   // next keyword please

            // get the enum and QSet of TermIDs for this keyword term
                auto e = mKeyword2KatRegField      [nKeywordTermID];
                auto s = mSingleJournalKeywordTerms[nKeywordTermID];

            // merge with "master" right eye set? // yes, unless this keyword term is exclusively meant for the other eye flavor
                if (stEye.bRightEye)
                    if (!nlKeywordsForLeftEyeOnly.contains(nKeywordTermID))
                        mTermSetsRightEye[e] += s;

            // and/or the left set? same here, check for keyword terms only meant for the other eye
                if (stEye.bLeftEye)
                    if (!nlKeywordsForRightEyeOnly.contains(nKeywordTermID))
                        mTermSetsLeftEye[e] += s;
            }

        // append this eye journal to the list
            lEyeJournals += stEye;
        }

    // --- we've retrieved all the value terms for this visit, got any right- or left eye keyword terms?
        bool bHasRightEye = (nRightEyeTermsFound > 0);
        bool bHasLeftEye  = (nLeftEyeTermsFound  > 0);

    // any right- or left-eye terms found for this patient and visit date?
        if ((!bHasRightEye) && (!bHasLeftEye))
            continue;   // nope, so forget about this visit, next please


    // for the right/left sets we retrieved: fields #8 and #9 need some extra caring and love
        if (mTermSetsRightEye.contains(KatRegFields::eEyeStates))
        {
        // 4 of the terms in eEyeStates should really be stored in/moved to the eOtherIndications field
        // (this misplacement occurs because TC stores all terms for #9 in #8, i.e. there's no keyword term for #9 terms)
            QSet<int> setMoveIt;

        // any term(s) to move today?
            for (auto t : mTermSetsRightEye[KatRegFields::eEyeStates])
                if (lTermIDsForOtherIndications.contains(t))
                    setMoveIt += t;

            if (setMoveIt.count() > 0)
            {
                mTermSetsRightEye[KatRegFields::eEyeStates       ] -= setMoveIt;
                mTermSetsRightEye[KatRegFields::eOtherIndications] += setMoveIt;
            }
        }

    // do the same fiddling for the left eye's terms
        if (mTermSetsLeftEye.contains(KatRegFields::eEyeStates))
        {
            QSet<int> setMoveIt;

        // any term(s) to move today?
            for (auto t : mTermSetsLeftEye[KatRegFields::eEyeStates])
                if (lTermIDsForOtherIndications.contains(t))
                    setMoveIt += t;

            if (setMoveIt.count() > 0)
            {
                mTermSetsLeftEye[KatRegFields::eEyeStates       ] -= setMoveIt;
                mTermSetsLeftEye[KatRegFields::eOtherIndications] += setMoveIt;
            }
        }

    // -------------------------------------------------------------------------------------------------------------
    // 2nd loop: retrieve measurements, waiting list date from notes, op. date from datetimes and surgeon from users
        KatRegQListMap mValuesRightEye;  // store all the right-eye values here (use a stringlist in case of multiple values for the same field)
        KatRegQListMap mValuesLeftEye;   // or here for left-eyed values

        for (auto stEye : lEyeJournals)
        {
        // note: the convert(varchar,Value) is for circumventing https://bugreports.qt.io/browse/QTBUG-108912
            db.select("KeywordTermID,convert(varchar,Value) as Value",stTCCorral.CaseNotes_Measurements,
                      QString("PatientID = %1 and DocumentID = %2 and Version = %3").
                      arg(stEye.nPID).arg(stEye.nDocumentID).arg(stEye.nVersion),
            [stEye, &mValuesRightEye, &mValuesLeftEye](DBRowStruct rs)
            {
                int nKeywordTermID = rs.intValue("KeywordTermID");

            // is this one of our keyword terms?
                if (!mKeyword2KatRegField.contains(nKeywordTermID))
                // no, next please
                    return;

            // yes, it's one of ours, get the term value and the KatReg field enum
                QString sValue    = rs.stringValueTrimmed("Value"); // use ...Trimmed() to toss possible white space junk
                auto eKatRegField = mKeyword2KatRegField[nKeywordTermID];

            // destined for the right eye?
            // note: measurements keyword terms are never exclusive for a single eye flavor  (unlike some of the value terms above)
                if (stEye.bRightEye)
                    mValuesRightEye[eKatRegField].append(sValue);

            // or for the left?
                if (stEye.bLeftEye)
                    mValuesLeftEye[eKatRegField].append(sValue);
            });

        // next: get the date for entering the waiting list for operation
            int nKeywordTermID = nnDateWaitingListKeywordTermID;     // only look for this keyword term in notes
            db.select("Note",stTCCorral.CaseNotes_Notes,
                      QString("PatientID = %1 and DocumentID = %2 and Version = %3 and KeywordTermID = %4").
                      arg(stEye.nPID).arg(stEye.nDocumentID).arg(stEye.nVersion).arg(nKeywordTermID),
            [nKeywordTermID, stEye, &mValuesRightEye, &mValuesLeftEye](DBRowStruct rs)
            {
            // is this one of our keyword terms?
                if (!mKeyword2KatRegField.contains(nKeywordTermID))
                // fubar out because this keyword term is cherrypicked, i.e. should be found
                    guruMeditation("couldn't find KatRegField enum for dateWaitingList");

            // get the date as a string (because it's stored as a text note in TC :-(
                QString sValue    = rs.stringValueTrimmed("Note");
                auto eKatRegField = mKeyword2KatRegField[nKeywordTermID];

            // destined for the right eye?
                if (stEye.bRightEye)
                    mValuesRightEye[eKatRegField].append(sValue);

            // or for the left?
                if (stEye.bLeftEye)
                    mValuesLeftEye[eKatRegField].append(sValue);
            });

        // next: get the date for the operation
            nKeywordTermID = nnDateOperationKeywordTermID;         // we're only interested in this keyword
            db.select("Date",stTCCorral.CaseNotes_DateTimes,
                      QString("PatientID = %1 and DocumentID = %2 and Version = %3 and KeywordTermID = %4").
                      arg(stEye.nPID).arg(stEye.nDocumentID).arg(stEye.nVersion).arg(nKeywordTermID),
            [nKeywordTermID, stEye, &mValuesRightEye, &mValuesLeftEye](DBRowStruct rs)
            {
            // is this one of our keyword terms?
                if (!mKeyword2KatRegField.contains(nKeywordTermID))
                // fubar out because this keyword term is cherrypicked, i.e. should exist
                    guruMeditation("couldn't find KatRegField enum for dateOperation");

            // get the date and convert it to a string (to fit in our map)
                QString sValue    = TWUtils::toISODate(rs.dateValue("Date"));
                auto eKatRegField = mKeyword2KatRegField[nKeywordTermID];

            // destined for the right eye?
                if (stEye.bRightEye)
                    mValuesRightEye[eKatRegField].append(sValue);

            // or for the left?
                if (stEye.bLeftEye)
                    mValuesLeftEye[eKatRegField].append(sValue);
            });

       // and finally the name of the surgeon (in two steps)
            nKeywordTermID = nnSurgeonKeywordTermID;                 // only interested in this keyword
            QString sUserID;  // result from first step (surgeon persnno.)
            db.select("UserID",stTCCorral.CaseNotes_Users,
                      QString("PatientID = %1 and DocumentID = %2 and Version = %3 and KeywordTermID = %4").
                      arg(stEye.nPID).arg(stEye.nDocumentID).arg(stEye.nVersion).arg(nKeywordTermID),
            [&sUserID,nKeywordTermID](DBRowStruct rs)
            {
            // is this one of our keyword terms?
                if (!mKeyword2KatRegField.contains(nKeywordTermID))
                // fubar out because this keyword term is cherrypicked, i.e. should exist
                    guruMeditation("couldn't find KatRegField enum for surgeon");

            // save the userid (personno) in a temp chap
                sUserID = rs.stringValue("UserID");
            },eDBRowsExpectZeroOrOne);

        // if we found the surgeon's userid, try to get the name from the Users table
            if (!sUserID.isEmpty())
            {
                db.select("top 1 Name",stTCCorral.Users,  // get the last/most recent (if there are several names)
                          QString("UserID = '%1'").arg(sUserID),"Version desc",
                [nKeywordTermID, stEye, &mValuesRightEye, &mValuesLeftEye](DBRowStruct rs)
                {
                // get the name
                    QString sValue    = rs.stringValueTrimmed("Name");
                    auto eKatRegField = mKeyword2KatRegField[nKeywordTermID];

                // destined for the right eye?
                    if (stEye.bRightEye)
                        mValuesRightEye[eKatRegField].append(sValue);

                // or for the left?
                    if (stEye.bLeftEye)
                        mValuesLeftEye[eKatRegField].append(sValue);
                });
            }
        }

    // --- the 2nd loop done ----------------------------------------------------------------------------
    // a final check: check if we retrieved a TypeOfOperation measurement value: if not, skip this visit
    // normally the whitelist screening at the start should stop any unnecessary visit browsing
    // but if both eyes are present and only one has a TypeOfOperation this can happen
        if (bHasRightEye)
        // just check for the presence of the term, not the actual contents/measurement value
            bHasRightEye = mValuesRightEye.contains(KatRegFields::eTypeOfOperation);

        if (bHasLeftEye)
            bHasLeftEye  = mValuesLeftEye.contains(KatRegFields::eTypeOfOperation);

    // so is this visit a tosser? i.e. no eyes left?
        if ((!bHasRightEye) && (!bHasLeftEye))
            continue;   // issue no error(s) just skip over to the next visit


    // ===============================================================================
    // done with the data retrieval, cooking/processing time
    // before we go ahead, let's make sure we're still sane in the brain
        if (lJournals.count() < 1)
            guruMeditation("got an empty list of journals (shouldn't happen)");

    // use the visit date and PID from the list's first journal (all chaps in the list have the same date and PID)
        QDate dEvent = lJournals[0].dEvent;
        PID_t nPID   = lJournals[0].nPID;

    // use this lambda for both eyes
        auto cookAndSave = [this] (QDate d, PID_t nPID, QChar cEye, KatRegTermSetMap mTermSets, KatRegQListMap mValues)
        {
        // wite up a key for a (possible) matching PAS entry
            QString sKatRegKey = KatRegEntryStruct::createKey(d,nPID,cEye);

        // have a matching PAS entry?
            if (!mKatRegEntries.contains(sKatRegKey))
            {
            // no, not our day, instead create a new error entry
                KatRegEntryStruct st;
                st.bOk            = false;
                st.sError         = "Saknar registrering i kassan";
                st.sWarning       = "";
                st.nPID           = nPID;
                st.bSpongeDOA     = false;
                st.dVisit         = d;
                st.sVisitDate     = TWUtils::toISODate(st.dVisit);
                st.tVisit         = tNotFoundInPAS; // "magic" time (for errors :~) 23:59
                st.sVisitDateTime = TWUtils::toISODateTimeNoSeconds(st.dVisit,st.tVisit);
                st.sReceiptNo     = "";
                st.sVisitType     = "";
                st.sPersonNo      = "";
                st.cEye           = cEye;

                st.sKey           = st.createKeyForErrorEntries(st.dVisit,st.nPID,st.cEye);

            // file it as an error entry and we're done
                mKatRegEntries.insert(st.sKey,st);
                return;
            }

        // check that the PAS entry is still a sponge (single pass so that should always be true)
            if (!mKatRegEntries[sKatRegKey].bSpongeDOA)
                fubar(QString("PAS entry '%1' is not a sponge").arg(sKatRegKey));

        // *** deduplication time: mTermSets are good (we get automagic deduplication courtesy QSet)
        // but mValues can use some right now
            for (auto eKey : mValues.keys())
                mValues[eKey].removeDuplicates();

        // if we're not on Windows, debug dump this visit?
        #if !defined(Q_OS_WIN) && defined(QDEBUG_H)
            if (bDebugDump)
                qDebug().noquote() << "### Cooking" << mKatRegEntries[sKatRegKey].sVisitDateTime << mKatRegEntries[sKatRegKey].nPID << mKatRegEntries[sKatRegKey].sPersonNo << cEye;
        #endif

        // unsponge, cook and save
            mKatRegEntries[sKatRegKey].bSpongeDOA = false;

            mKatRegEntries[sKatRegKey].cEye       = cEye;
            mKatRegEntries[sKatRegKey].mTermSets  = mTermSets;
            mKatRegEntries[sKatRegKey].mValues    = mValues;

            mKatRegEntries[sKatRegKey].mAllCooked = cookEntry(mKatRegEntries[sKatRegKey]);

        // got any warnings in the cooking process? if so copy the 1st warning into "master" so that it's visible in the table widget
            for (auto e : lKatRegFields)
                if (!mKatRegEntries[sKatRegKey].mAllCooked[e].sWarning.isEmpty())
                {
                // is there a warning text in the master already? (don't clobber existing)
                    if (mKatRegEntries[sKatRegKey].sWarning.isEmpty())
                        mKatRegEntries[sKatRegKey].sWarning = mKatRegEntries[sKatRegKey].mAllCooked[e].sWarning;
                }

        // and any cooking errors? if so copy the 1st error text into the "master" text for better visibility (if it's currently empty)
            for (auto e : lKatRegFields)
                if (!mKatRegEntries[sKatRegKey].mAllCooked[e].sError.isEmpty())
                {
                // turn it off (this visit is not ok)
                    mKatRegEntries[sKatRegKey].bOk = false;

                // is there any error text(s) in the master string? If not, migrate this error into it
                // (i.e. if there are several cooking errors, only copy the first found into the master)
                    if (mKatRegEntries[sKatRegKey].sError.isEmpty())
                        mKatRegEntries[sKatRegKey].sError = mKatRegEntries[sKatRegKey].mAllCooked[e].sError;
                }

        // that's all for this visit
        };

    // got a right eye to save for this visit?
        if (bHasRightEye)
            cookAndSave(dEvent,nPID,ccRightEye,mTermSetsRightEye,mValuesRightEye);

    // or perhaps a left one?
        if (bHasLeftEye)
            cookAndSave(dEvent,nPID,ccLeftEye,mTermSetsLeftEye,mValuesLeftEye);

    // have at least one new entry (or two), so now is a good time to refresh the personno.
        refreshPersonNo();

    // have more stuff to show so update the table widgets
        restockAllTableWidgets();

    // next patient and visit date, please
    }

// -----------------------------------------------------------------------------------
// ------ all journals done, Samba is done, time to wrap it up
    if (scMain.bCrawlingDone)
    {
    // Samba is done, proceed
        QTimer::singleShot(nnShortTCDelay,this,[this] { burialAtSea(); } );
    }
    else
    {
    // Samba is slow today (when it's finished it will call burialAtSea(), trust me)
        bWaitingForSamba = true;
        showProgress("Väntar på Samba...");
    }
}

//----------------------------------------------------------------------------
// cookEntry
// step thru and parse/cook/check the KatReg terms and values for a visit
//
// 2024-01-25 First version
//----------------------------------------------------------------------------
KatRegVWEMap MainWindow::cookEntry(KatRegEntryStruct st)
{
// return this vwe when're done (hopefully with values, no warnings or errors)
    KatRegVWEMap vweCooked;

// step thru all the katreg fields
    for (auto e : lKatRegFields)
    {
        QString sFieldCaption = mKatRegFieldCaptions[e]; // this may serve you well (for errors and tooltips)

    // get # of terms and values specified for this field/enum
        int nNoOfTerms  = 0;
        int nNoOfValues = 0;

        if (st.mTermSets.contains(e))
            nNoOfTerms = st.mTermSets[e].count();

        if (st.mValues.contains(e))
            nNoOfValues = st.mValues[e].count();

    // and get the specs. for this katreg field (+ sanity check them)
        auto pTermSetSpec = mKatRegFieldParamSpecs[e].pTermSetSpec;
        auto pValueSpec   = mKatRegFieldParamSpecs[e].pValueSpec;

        int nNoOfTermsMin  = pTermSetSpec.first;
        int nNoOfTermsMax  = pTermSetSpec.second;
        if (nNoOfTermsMin > nNoOfTermsMax)
            guruMeditation("nNoOfTermsMin > nNofOfTermsMax, not good");
        int nNoOfValuesMin = pValueSpec.first;
        int nNoOfValuesMax = pValueSpec.second;
        if (nNoOfValuesMin > nNoOfValuesMax)
            guruMeditation("nNoOfValuesMin > nNofOfValuesMax, not good");

    // also check that max # value params are 1 (we don't support multiple values here)
        if (nNoOfValuesMax > 1)
            guruMeditation("nNoOfValuesMax > 1 that's not supported");

    // checking this field matches the spec. (note: deduplication has been done)
        bool bMissing = ((nNoOfTermsMin > nNoOfTerms) || (nNoOfValuesMin > nNoOfValues));
        bool bTooMany = ((nNoOfTermsMax < nNoOfTerms) || (nNoOfValuesMax < nNoOfValues));

    // is this field spec:ed to have both a term set and values? if so check missing param(s) again
        if ((pNone != pTermSetSpec) && (pNone != pValueSpec))
        // fpr this field, require both to be missing to issue an error
            bMissing = ((nNoOfTermsMin > nNoOfTerms) && (nNoOfValuesMin > nNoOfValues));
        // note: for the "too many" error, no need to recheck

    // have a value or a termset?
        if (bMissing)
        {
        // no, set a missing error and skip to next field
            vweCooked[e].sError = QString("%1 saknas").arg(sFieldCaption);
            continue;
        }

    // having too many values or terms?
        if (bTooMany)
        {
        // yes, so set an error and skip to next field
            vweCooked[e].sError = QString("%1 har för många olika värden").arg(sFieldCaption);
            continue;
        }

    // getting a single value (in a generic way) from the mValues map (if present)
        QString sValue = "";
        int     nValue = 0;     // courtesy for those that prefer numeric values
        if (st.mValues.contains(e))
        {
            if (st.mValues.count(e) > 1)
                guruMeditation("got too many values for enum #%d (shouldn't happen)",static_cast<int>(e));

        // now we know there's only one value
            sValue = st.mValues[e].first();
            nValue = sValue.toInt();  // don't care if this fails
        }

    // else if we have terms, get their captions concatenated with '+'
        if (st.mTermSets.contains(e))
        {
            QString sTerms = "";

            for (auto nTermID : st.mTermSets[e])
            {
                QString sCaption = QString("Okänd TC Term (ID = %1)").arg(nTermID); // pessimistic default
                if (mTermSetCaptions.contains(nTermID))
                    sCaption = mTermSetCaptions[nTermID];

            // is this the first one?
                if (!sTerms.isEmpty())
                // no, so prefix with a '\n' (as a line separator)
                    sCaption = "\n" + sCaption;

            // add 'em
                sTerms += sCaption;
            }

        // use the term(s) as the field value? yes, if this field only has set terms (i.e. nothing in mValues)
            if (pNone == pValueSpec)
                sValue = sTerms;
        }

    // --------------------------------------------------------------------
    // --- general cooking done, time for KatReg field specific cooking ---
    // #3: eye flavor
        if (KatRegFields::eRightOrLeftEye == e)
        {
        // check the eye terms match (they should)
            if (ccRightEye == st.cEye)
                if (!st.mTermSets[e].contains(nnRightTermID))
                    guruMeditation("right eye but no right eye term seen");

            if (ccLeftEye == st.cEye)
                if (!st.mTermSets[e].contains(nnLeftTermID))
                    guruMeditation("left eye but no left eye term seen");

            vweCooked[e].sValue   = (ccRightEye == st.cEye) ? ccRightEye : ccLeftEye;
            vweCooked[e].sToolTip = (ccRightEye == st.cEye) ? "Höger öga" : "Vänster öga";
        }

    // #4: visus is a bit tricky (can have both terms and a value)
        if ((KatRegFields::eVisusRight == e) || (KatRegFields::eVisusLeft == e))
        {
        // if we have a value, make sure it uses Swedish style decimal point (",")
            sValue.replace(".",",");

        // check for a term #
            QString sValueFromTerm = "";
            if (!st.mTermSets[e].isEmpty())
            {
            // use Swedish decimal point for cooking, for the XML later we'll change to US style points
            // (should only be one term in the set so just do simple if statements)
                auto s = st.mTermSets[e];
                if (s.contains(nnFR4mTermID))      sValueFromTerm = "0,08";
                if (s.contains(nnFR3mTermID))      sValueFromTerm = "0,06";
                if (s.contains(nnFR2mTermID))      sValueFromTerm = "0,04";
                if (s.contains(nnFR1mTermID))      sValueFromTerm = "0,02";
                if (s.contains(nnHRPLPTermID))     sValueFromTerm = "0,01";
                if (s.contains(nnAmaurosisTermID)) sValueFromTerm = "0,00";
            }

        // have both? then they better be the same number
            if ((!sValueFromTerm.isEmpty()) && (!sValue.isEmpty()))
                if (sValueFromTerm != sValue)
                {
                // sorry they're different, error out and skip to next field
                    vweCooked[e].sError = QString("%1 har för många olika värden: '%2 och %3'").arg(sFieldCaption,sValueFromTerm,sValue);
                    continue;
                }

        // have a term? use it by copying it into the value (which should be empty or contain the same number)
            if (!sValueFromTerm.isEmpty())
                sValue = sValueFromTerm;

        // got anything? cook it slightly first (tossing all white space)
            sValue = TWUtils::tossAllWhiteSpace(sValue);
            if (TWUtils::tossAllButDigits(sValue).isEmpty())
            {
            // no digits? fugget this visus
                vweCooked[e].sError = QString("%1 felaktigt värde (saknar siffror)").arg(sFieldCaption);
                continue;
            }

        // have at least 1 digit, split it into an integer and a fractional part with 2 decimal places
            QStringList sl = sValue.split(",");
            if ((sl.count() != 1) && (sl.count() != 2))
            {
            // too much junk (non-digits)? fugget this visus
                vweCooked[e].sError = QString("%1 felaktigt värde").arg(sFieldCaption);
                continue;
            }

            bool bBadParts = false; // optimistic default
            for (auto s : sl)       // verify that we have nothing but digits
                if (TWUtils::tossAllButDigits(s) != s)
                    bBadParts = true;

            if (bBadParts)
            {
            // junk part(s) fugget this visus
                vweCooked[e].sError = QString("%1 felaktigt värde").arg(sFieldCaption);
                continue;
            }

            int nIntegerPart = sl[0].toInt();
            if ((nIntegerPart < 0) || (nIntegerPart > 2))
            {
            // integer part is out of bounds
                vweCooked[e].sError = QString("%1 felaktigt heltal (ej 0, 1 eller 2)").arg(sFieldCaption);
                continue;
            }

        // splitting into an integer and a fraction part
            QString sFractionalPart;
            if (sl.count() > 1)
                sFractionalPart = sl[1];
            sFractionalPart += "00";
            sFractionalPart = sFractionalPart.left(2);         // keep only 2 decimal places (no rounding)

        // reconstruct and save the whole thing (and with Swedish style decimal point)
            vweCooked[e].sValue = QString::number(nIntegerPart) + "," + sFractionalPart;
        }

    // #5: NIKE
        if (KatRegFields::eNIKE == e)
        // this one is easier to cook: value from DB is [0 .. 3] but we want [1..4]
            vweCooked[e].sValue = QString::number(nValue + 1); // > 3 (4 cooked) cannot occur thanks to TC watching

    // #6: date when registered in the waiting list
        if (KatRegFields::eDateWaitingList == e)
        {
        // this date is stored as text note, i.e. we have to parse it first
            sValue = TWUtils::tossAllButDigits(sValue); // toss any "-", "/" or "."
            if (sValue.length() == 6)
                sValue = "20" + sValue;  // try to make it into a valid YYYYMMDD in the 21st century

            auto d = TWUtils::fromYMD(sValue);
            if (d.isValid())             // it it's a valid date, reformat it to iso and save it
                vweCooked[e].sValue = TWUtils::toISODate(d);
            else
                vweCooked[e].sError = "Ogiltigt datum för väntelistan";
        }

    // #7: op. date (should be the same as the visit date)
        if (KatRegFields::eDateOperation == e)
        {
        // assume sValue has a correct date
            auto d = TWUtils::fromISODate(sValue);

            if (d.isValid())  // a kosher date?
                vweCooked[e].sValue = TWUtils::toISODate(d);
            else
                vweCooked[e].sError = "Ogiltigt operationsdatum";

        // also require op. date to be the same as the visit date
            if ((d.isValid()) && (d != st.dVisit))
                vweCooked[e].sError = "Operationsdatum är inte samma som besöksdatum";
        }

    // #8: #9: #12: #13: #14: and #16 are optional term sets (they can be empty and it's ok)
    // just save the generic sValue we have (the term's captions concatenated together)
        if ((KatRegFields::eEyeStates           == e) ||
            (KatRegFields::eOtherIndications    == e) ||
            (KatRegFields::eSpecificProps       == e) ||
            (KatRegFields::ePeroperativeActions == e) ||
            (KatRegFields::eAntibiotics         == e) ||
            (KatRegFields::ePostoperativeIP     == e))
            vweCooked[e].sValue = sValue;

    // do the remaining fields
    // #10: op. type
        if (KatRegFields::eTypeOfOperation == e)
        {
        // cook the value from a number to a text
            QString sText = "";

            if (nnFakoBKLValue               == nValue)
                sText = "Fako+BKL";
            if (nnFakoBKLAnnanSamtidaOpValue == nValue)
                sText = "Fako+BKL+annan samtidig operation";
            if (nnAnnanOpValue               == nValue)
                sText = "Annan";

        // got something? thanks to TC checking params we should have
            if (sText.isEmpty())
                vweCooked[e].sError = QString("%1 felaktigt eller okänt värde").arg(sFieldCaption);
            else
                vweCooked[e].sValue = sText;
        }

    // #11: lens material
        if (KatRegFields::eLensMaterial == e)
        {
        // cook the value from a number to a text
            QString sText = "";

        // for this field: if nothing is entered, that's a value too
            if ("" == sValue)
            // replace with our own artificial value term
                nValue = nnIngenLinsValue;

            if (nnAcrylHydrofobValue == nValue)
                sText = "Acryl hydrofob";
            if (nnAcrylHydrofilValue == nValue)
                sText = "Acryl hydrofil";
            if (nnAcrylAnnatValue    == nValue)
                sText = "Annan";
            if (nnIngenLinsValue     == nValue)
                sText = "Ingen lins";

        // got something? thanks to TC checking params we should have
            if (sText.isEmpty())
                vweCooked[e].sError = QString("%1 felaktigt eller okänt värde").arg(sFieldCaption);
            else
                vweCooked[e].sValue = sText;
        }

    // #15: peroperative damages (a yes or no question)
        if (KatRegFields::ePeroperativeDamages == e)
        {
        // cook the value from a number to a text
            QString sText = "";

            if (nnNoValue  == nValue)
                sText = "Nej";
            if (nnYesValue == nValue)
                sText = "Ja";

        // got something?
            if (sText.isEmpty())
                vweCooked[e].sError = QString("%1 felaktigt eller okänt värde").arg(sFieldCaption);
            else
                vweCooked[e].sValue = sText;
        }

    // #20: surgeon's name
        if (KatRegFields::eSurgeon == e)
        {
        // check if this name is in the list (expected in ROSettings.ini)
            if (slSurgeons.indexOf(sValue) < 0)
                vweCooked[e].sWarning = QString("Kirurg '%1' saknas i listan på läkarkoder").arg(sValue);

        // store the value anyway (save to XML will use a fallback no. in case of missing chaps)
            vweCooked[e].sValue = sValue;
        }

    // --- tooltips for all: do a generic tooltip stuffing if it's currently empty ---
        if (vweCooked[e].sToolTip.isEmpty())
        {
        // construct a nice default tooltip
            QString sToolTip = sFieldCaption + ": " + vweCooked[e].sValue;
            sToolTip.replace("\n"," + ");   // don't want those line feeds mess up the tooltip

        // however if we have an error text, use that instead
            if (!vweCooked[e].sError.isEmpty())
                sToolTip = vweCooked[e].sError;

        // store it
            vweCooked[e].sToolTip = sToolTip;
        }

    // that's enough cooking for now
    }

// some extra postprocessing cooking/checking: check waiting list date <= operation date
    QDate dWaitingList = TWUtils::fromISODate(vweCooked[KatRegFields::eDateWaitingList].sValue);
    QDate dOperation   = TWUtils::fromISODate(vweCooked[KatRegFields::eDateOperation].sValue);
    if (dWaitingList.isValid() && dOperation.isValid())
        if (dWaitingList > dOperation)
            vweCooked[KatRegFields::eDateWaitingList].sError = "Väntelistans datum är efter operationen";

// debug dumping time (unless we're on Windows)?
#if !defined(Q_OS_WIN) && defined(QDEBUG_H)
    if (bDebugDump)
        for (auto e : lKatRegFields)
        {
            QString sLine = mKatRegFieldCaptions[e] + ": ";

        // term ID:s in a set
            QSet<int> s;
            if (st.mTermSets.contains(e))
                s = st.mTermSets[e];

            for (auto t : s)
                sLine += mTermSetCaptions[t] + " ";

        // then values
            QStringList sl;
            if (st.mValues.contains(e))
                sl = st.mValues[e];

            for (auto s : sl)
                sLine += s + " ";

        // then the cooked ones
            VWEStruct stVWE;
            if (vweCooked.contains(e))
                stVWE = vweCooked[e];

            QString sFS = "/";
            sLine += sFS + stVWE.sValue + sFS + stVWE.sWarning + sFS + stVWE.sError + sFS;

            qDebug().noquote() << sLine;
        }
#endif

// ok we're done
    return vweCooked;
}

//----------------------------------------------------------------------------
// burialAtSea (the final steps are taken here)
//
// 2023-04-23 First version
// 2024-01-25 TC2KatReg: need DOB and county # for pat. with reservnr.
//----------------------------------------------------------------------------
void MainWindow::burialAtSea()
{
// when we arrive here the main Samba crawler should be done
    if (!scMain.bCrawlingDone)
        guruMeditation("the main Samba crawler was not done");

// make sure to update the KatReg entries with all we got from Samba
    refreshPersonNo();

// --- step thru and check all the personno. ---
    int nNo    = 0;    // for showProgress()
    int nTotal = mKatRegEntries.count();

    for (auto &st : mKatRegEntries)   // & for r/w access
    {
    // show some progress
        ++nNo;
        showProgress(QString("Strax klar, kontrollerar pat. personnummer (%1%)...").arg((nNo * 100) / nTotal),95 + ((nNo * 5) / nTotal)); // (nTotal > 0)

    // bypass sponges (not written to the XML file, no need to check 'em)
        if (st.bSpongeDOA)
            continue;

    // is this a bad personno. or a missing/empty one?
        QString sError;
        if (!st.sPersonNo.isEmpty())
            sError = TWUtils::checkPersonNo(st.sPersonNo);

        bool bBadApple = (!sError.isEmpty() || (st.sPersonNo.isEmpty()));
        if (bBadApple)
            goto checkDOB;

    // this personno. is ok but are we to anyway check all of them?
        if (bCheckAllDOBs)
            goto checkDOB;  // yes please check this one

    // or perform a random inspection? i.e. check a selected few?
        if (bCheckRandomDOBs)
            if (1 == (rand() % 11))
            // let's check on average one of every 11 chaps
                goto checkDOB;

    // for TC2KatReg: if pat. has a reservno. we need his/her DOB for the XML file
        if (TWUtils::isReservNo(st.sPersonNo))
            goto checkDOB;

    // we're not checking this one, assume a valid no. (not empty for sure)
        continue;   // next please

checkDOB:
    // checking time: try to get the DOB for this PID
        QDate   dDOB;
        QString sSex;
        QDate   dCFUDOB;
        int     nCounty = 0;

        db.select("DateOfBirth,Sex,CFUDateOfBirth,CFUCounty",stTCCorral.PatInfo,
        QString("PatientID = %1").arg(st.nPID),
        [&dDOB,&sSex,&dCFUDOB,&nCounty](DBRowStruct rs)
        {
            dDOB    = rs.dateValue("DateOfBirth");
            sSex    = rs.stringValueTrimmed("Sex");
            dCFUDOB = TWUtils::fromYMD(rs.stringValueTrimmed("CFUDateOfBirth"));  // expect YYYYMMDD from the db
            nCounty = rs.intValue("CFUCounty");
        },eDBRowsExpectZeroOrOne);

    // have a bad or (most likely missing) personno? replace it
        if (bBadApple)
        {
        // arriving here: either sError is already set or we have an empty personno
        // missing from the Samba crawler's table? i.e. an empty personno?
            if (st.sPersonNo.isEmpty())
            {
                sError = "Kassa " + st.sReceiptNo.left(6) + " i TakeCare är inte avslutad";
                if (st.bOk) // if there is no current error, set this error (because it's less important)
                {
                    st.bOk    = false;
                    st.sError = sError;
                }
            }

        // create a new, default (empty) personno.
            st.sPersonNo = "???????? ????";

        // if we found a valid DOB, use it to construct a partial personno.
            if (dDOB.isValid())
                st.sPersonNo = TWUtils::toYMD(dDOB) + " ????";   // use "????" to show we're missing the last 4 digits

        // if DOB was no good, maybe this is a reservnr
            if (!dDOB.isValid() && dCFUDOB.isValid())
            // no birthday but a CFU date? assume then it's a reservnummer (Region Stockholm flavored)
                st.sPersonNo = "99?????? ????";     // use even more "????" to show we're missing even more digits

        // bOk should always be false at this point
            if (st.bOk)
                guruMeditation("bOk is still true");
        }
        else
        {
        // this pat. has no error but check the DOB anyway
            if (!dDOB.isValid())
                // maybe this a reservnr chap. try the CFUDOB
                dDOB = dCFUDOB;

        // and try again
            if (!dDOB.isValid())
            {
            // didn't get a valid date for the DOB, for TC2KatReg this will be an error
                st.bOk    = false;
                st.sError = "Saknar giltigt födelsedatum";

                continue;   // next chap pls
            }

        // as an optional extra: match first 8 digits with DOB (unless this is a pat. with reservno or a testpatient)
            if (!TWUtils::isReservNo(st.sPersonNo))
                if (st.sPersonNo.left(8) != TWUtils::toYMD(dDOB))    // 8 chars = YYYYMMDD
                    if ((st.sWarning.isEmpty()) && (!TWTCSupport::isTestPatient(st.sPersonNo)))
                        // not a testpatient: issue a warning (unless this entry has got a previous warning already)
                        if (!bObfuscatePersonNo)    // don't bother if we're in an obfuscation run
                            st.sWarning = "Födelsedatum och personnr. stämmer inte överens";
        }

    // save DOB, sex and County no
        st.dDOB    = dDOB;                         // we know DOB is valid (checked above)
        st.sSex    = sSex.isEmpty() ? "O" : sSex;  // default to "O" for unknown gender
        st.nCounty = nCounty;                      // don't bother error check (default is set to 0 above)

    // next pat. please
    }

// close the db and update the tablewidgets
    db.close();
    restockAllTableWidgets();

// we're done, switch app state to rubbernecking and write the KatReg XML file
    showProgress("Skriver KatReg-filen...",99);
    switchAppState(AppState::eRubbernecking);

    saveKatRegFile();

// do a final update (cannot hurt)
    restockAllTableWidgets();
}

//----------------------------------------------------------------------------
// saveKatRegFile
//
// 2023-12-01 First version
// 2024-01-25 Include eye flavor in the helper map (to support bilaterals)
//----------------------------------------------------------------------------
void MainWindow::saveKatRegFile()
{
// prepare the counting
    int nVisits   = 0;
    int nErrors   = 0;
    int nWarnings = 0;

// use a helper map for stepping through the entries in date + TC receiptno order (and bypass errors)
    QMap<QString,QString> mDateReceiptNo2KatRegEntry;
    for (auto st : mKatRegEntries)
    {
    // skip over error entries
        if (!st.bOk)
        {
        // and count this as an error unless this is a hidden sponge (with an error)
            if (!st.bSpongeDOA || bShowSponges || bShowErrorSponges)
                ++nErrors;

            continue;   // next entry please
        }

    // and skip over sponges (that's the whole point of being a sponge)
        if (st.bSpongeDOA)
            continue;

    // if we arrive here, this entry should be error free
        if (!st.sError.isEmpty())
            guruMeditation("got a nonempty error message when expecting an error-free entry");

    // also make sure we have a nonempty and valid personno
        if (st.sPersonNo.isEmpty()) // empty personno? (shouldn't happen if bOk is true)
            guruMeditation("got an empty personno when writing the KatReg file");

        if ("" != TWUtils::checkPersonNo(st.sPersonNo))
            guruMeditation("got a bad personno when writing the KatReg file");

    // count # of warnings (this is an ok entry but with a warning)
        if (!st.sWarning.isEmpty())
            ++nWarnings;

    // have a valid candidate for the KatReg file, save it in the map
    // append eye flavor for disambiguation for bilaterals (when both right and left eye ops occur on the same day)
        ++nVisits;
        QString sKey = st.sVisitDate + st.sReceiptNo + st.cEye;
        mDateReceiptNo2KatRegEntry.insert(sKey,st.sKey);
    }

// check map size (if there's a mismatch, it could be an entry was overwritten due to a duplicate KatReg receipt no.)
// (we're supporting nnn-0n-nnnn-n and nnn-9n-nnnn-n TC receipt nos. but not nnn-1n-nnnn-n, nnn-2n-nnnn-n etc.)
    if (mDateReceiptNo2KatRegEntry.count() != nVisits)
        fubar("mismatch helper map vs. nVisits (%d/%d)",mDateReceiptNo2KatRegEntry.count(),nVisits);

// XML time --------------------------------------------------
// compose a nice filename and start with some XML boilerplate
    QString sFileName = QString("BAS%1_%2_%3_%4.xml").arg(sClinicNo,TWUtils::toYMD(dFrom).right(6),TWUtils::toYMD(dTo).right(4)).arg(nVisits);

    auto f = new QFile(sFileName);
    if (!f->open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Truncate))
        return TWUtils::errorBox(QString("Sorry det gick inte att spara KatReg-filen i:\n'%1'").arg(TWUtils::getCurrentDirectory()));

    QXmlStreamWriter wx(f);
    wx.setAutoFormatting(true); // start off with some boring boilerplates
    wx.writeStartDocument();
    wx.writeStartElement("KataraktOperationer");
    wx.writeAttribute("xmlns:xsi","http://www.w3.org/2001/XMLSchema-instance");
    wx.writeAttribute("xmlns:xsd","http://www.w3.org/2001/XMLSchema");

// step thru our KatReg entries in ascending date and TC receiptno. order
    for (auto sKey : mDateReceiptNo2KatRegEntry)
    {
   // get our chap for this run
        auto st = mKatRegEntries[sKey];

    // --- helper lambdas for XML writing of booleans, integers, dates and strings
        auto writeBoolElement   = [&wx] (QString sName, bool    b) { wx.writeTextElement(sName,(b) ? "1" : "0");       };
        auto writeIntElement    = [&wx] (QString sName, int     i) { wx.writeTextElement(sName,QString::number(i));    };
        auto writeDateElement   = [&wx] (QString sName, QDate   d) { wx.writeTextElement(sName,TWUtils::toISODate(d)); };
        auto writeStringElement = [&wx] (QString sName, QString s) { wx.writeTextElement(sName,s);                     };

    // --- more fancy
        auto writeTermElement   = [&wx] (int nTermID, QString s)
        {
        // look up the termID in our XML QMap (has to exist)
            if (!mKatRegTermXMLNames.contains(nTermID))
                fubar("TermID %d not found in XML names map",nTermID);

            wx.writeTextElement(mKatRegTermXMLNames[nTermID],s);
        };

        auto writeStartElement  = [&wx] (int nKeywordTermID)
        {
        // look up the keyword termid in our XML QMap (has to exist)
            if (!mKatRegTermXMLNames.contains(nKeywordTermID))
                fubar("Keyword TermID %d not found in XML names map",nKeywordTermID);

            wx.writeStartElement(mKatRegTermXMLNames[nKeywordTermID]);
        };

    // --- even fancier
        auto writeCookedValueElement = [&wx,st] (int nKeywordTermID)
        {
        // writes the cooked .sValue for a given keyword termid
        // look up the keyword termID in our XML QMap (has to exist)
            if (!mKatRegTermXMLNames.contains(nKeywordTermID))
                fubar("Keyword termid %d not found in XML Names",nKeywordTermID);

        // check the enum for this keyword term id (has to exist in the map as well)
            if (!mKeyword2KatRegField.contains(nKeywordTermID))
                fubar("Keyword termid %d not found in keyword table",nKeywordTermID);

        // write the value
            wx.writeTextElement(mKatRegTermXMLNames[nKeywordTermID],st.mAllCooked[mKeyword2KatRegField[nKeywordTermID]].sValue);
        };

        auto writeBoolTermElement = [&wx,st] (int nKeywordTermID, int nTermID)
        {
        // check the enum for this keyword term id (has to exist in the map)
            if (!mKeyword2KatRegField.contains(nKeywordTermID))
                fubar("Keyword termid %d not found in keyword table",nKeywordTermID);

        // writes a bool (1) if the term is present in the QSet for the keyword termid, else false (0)
        // check that the termID is in our XML QMap
            if (!mKatRegTermXMLNames.contains(nTermID))
                fubar("Termid %d not found in XML Names",nTermID);

        // does this term exist in the set?
            bool bExists = st.mTermSets[mKeyword2KatRegField[nKeywordTermID]].contains(nTermID);

        // write it as "0" or "1" to XML
            wx.writeTextElement(mKatRegTermXMLNames[nTermID],(bExists) ? "1" : "0");
        };

    // ------ begin a new XML entry with <Operation> ---
        wx.writeStartElement("Operation");

    // every <Operation> has 5 mandatory parts/elements:
    // <Enhet>, <Person>, <Preoperativ>, <Interoperativ> and <Postoperativ>

    //  #1: this one is good for XML beginners
        writeStringElement("Enhet",sClinicNo);

    // --- hello <Person>
        wx.writeStartElement("Person");

    //  #2: personno.
        {
        // a kosher one or a reservnummer?
            QString sPersonIdentity = st.sPersonNo;
            bool bTemporary         = TWUtils::isReservNo(sPersonIdentity);
            if (bTemporary)
            {
            // make sure we have a valid DOB
                if (!st.dDOB.isValid())
                    fubar(QString("Found a bad DOB for %1").arg(st.sPersonNo));

            // and use that as a temp. id
                sPersonIdentity = TWUtils::toYMD(st.dDOB);  // toss the reservno, store DOB as YYYYMMDD
            }

            writeStringElement("PersonIdentity",sPersonIdentity);
            writeBoolElement("Temporary",bTemporary);

        // for temporary chaps, add Gender/DOB/Länskod to the XML
            if (bTemporary)
            {
                writeStringElement("Gender",st.sSex);       // same as in TC: "M", "K" and "O"
                writeDateElement("DOB",st.dDOB);
                writeIntElement("Länskod",st.nCounty);
            }
        }
        wx.writeEndElement();
    // --- end of <Person>

    // --- <Preoperativ> here we go
        wx.writeStartElement("Preoperativ");

    //  #6: date when entered waiting list
        writeCookedValueElement(nnDateWaitingListKeywordTermID);  // the cooked value should be YYYY-MM-DD

    //  #3: right or left eye (check that we in fact have an "H" or "V" stored)
        if ((ccRightEye != st.cEye) && (ccLeftEye != st.cEye))
            fubar("\"Öga\" är inte H eller V ");
        writeTermElement(nnRightOrLeftEyeKeywordTermID,st.cEye);

    //  #4: visus right and left eyes
        {
        // as an unpleasant surprise, element names are not <Right> and <Left> (that would have been too easy)
            QString sVisusRight = st.mAllCooked[KatRegFields::eVisusRight].sValue;
            QString sVisusLeft  = st.mAllCooked[KatRegFields::eVisusLeft].sValue;

            // make sure we have exactly one (Swedish style) decimal point
            if (1 != sVisusRight.count(","))
                fubar("Visus höger saknar decimalkomma");
            if (1 != sVisusLeft.count(","))
                fubar("Visus vänster saknar decimalkomma");

            // for the XML trip, replace the commas with US style decimal points
            sVisusRight.replace(",",".");
            sVisusLeft.replace(",",".");

            if (ccRightEye == st.cEye)
            {
            // for this visit, the right eye was the eye that had the operation
                writeStringElement("VisusOperationsÖga",sVisusRight);
                writeStringElement("VisusAndraÖgat",sVisusLeft);
            }
            else
            {
            // for this visit, the left eye was the eye that had the operation
                writeStringElement("VisusOperationsÖga",sVisusLeft);
                writeStringElement("VisusAndraÖgat",sVisusRight);
            }
        }

    //  #5: indikationsgrupp (NIKE)
        writeCookedValueElement(nnNIKEKeywordTermID);

    // #13: peroperativa åtgärd(er)
        {
            int nKeywordTermID = nnPeroperativaAtgKeywordTermID;

            writeStartElement(nKeywordTermID);
            writeBoolTermElement(nKeywordTermID,nnMekanisktVidgadPupillTermID  );
            writeBoolTermElement(nKeywordTermID,nnKapselFargningTermID         );
            writeBoolTermElement(nKeywordTermID,nnHakarIRexiskantenTermID      );
            writeBoolTermElement(nKeywordTermID,nnKapselringInlagdTermID       );
            writeBoolTermElement(nKeywordTermID,nnPeroperativSkadaKeywordTermID);
            writeBoolTermElement(nKeywordTermID,nnGenerellAnestesiTermID       );
            wx.writeEndElement();
        }

    //  #8: tillstånd i operationsögat
        {
        // note: we're using different keyword termids for different eye flavors
            int nKeywordTermID = (ccRightEye == st.cEye) ? nnStatesRightEyeKeywordTermID : nnStatesLeftEyeKeywordTermID;

            writeStartElement(nKeywordTermID);
            writeBoolTermElement(nKeywordTermID,nnGlaukomTermID                 );
            writeBoolTermElement(nKeywordTermID,nnMakulasjukdomTermID           );
            writeBoolTermElement(nKeywordTermID,nnDiabetesretinopatiTermID      );
            writeBoolTermElement(nKeywordTermID,nnCorneaGuttataTermID           );
            writeBoolTermElement(nKeywordTermID,nnPseudoexfoliationerTermID     );
            writeBoolTermElement(nKeywordTermID,nnUveitTermID                   );
            writeBoolTermElement(nKeywordTermID,nnAnnatSynhotandeTermID         );
            writeBoolTermElement(nKeywordTermID,nnTidigareRefraktivKirurgiTermID);
            writeBoolTermElement(nKeywordTermID,nnTidigareVitrektomiTermID      );

            // the following terms are stored in TC and in XML in the same place as nnStates above
            // but we're storing them ourselves in another KatRegField, so another lambda to the rescue
            auto writeBoolTermFromOtherIndications = [&wx,st] (int nTermID)
            {
            // check that the termID is in our XML QMap
                if (!mKatRegTermXMLNames.contains(nTermID))
                    fubar("Termid %d not found in XML Names",nTermID);

            // term exists or not? (hardwired for the eOtherIndications field)
                bool bExists = st.mTermSets[KatRegFields::eOtherIndications].contains(nTermID);

            // write it as "0" or "1"
                wx.writeTextElement(mKatRegTermXMLNames[nTermID],(bExists) ? "1" : "0");
            };

            writeBoolTermFromOtherIndications(nnAnisometropiTermID            );
            writeBoolTermFromOtherIndications(nnForhojtOgontryckTermID        );
            writeBoolTermFromOtherIndications(nnAndraSubjektivaSynBesvarTermID);
            writeBoolTermFromOtherIndications(nnAnnanMedicinskIndikationTermID);
            wx.writeEndElement();   // end of <TillståndOpÖga>
        }

    // --- and that's the end of <Preoperativ>
        wx.writeEndElement();

    // --- <Interoperativ> time
        wx.writeStartElement("Interoperativ");

    //  #7: operationsdatum
        writeCookedValueElement(nnDateOperationKeywordTermID);

    // #10: operationstyp, need the uncooked value for transmogrification (nValue ---> sXML)
        {
            int nValue  = 0;
            auto e      = KatRegFields::eTypeOfOperation;
            if (st.mValues.count(e) > 1)
                guruMeditation("got too many values for enum #%d",static_cast<int>(e));
            if (st.mValues.count(e) > 0)
                nValue = st.mValues[e].first().toInt();

            QString sXML;
            switch (nValue)
            {
            case nnFakoBKLValue               : sXML = "1"; break;
            case nnFakoBKLAnnanSamtidaOpValue : sXML = "8"; break;
            case nnAnnanOpValue               : sXML = "6"; break;
            default :
                guruMeditation("unknown uncooked optype value %d",nValue);
            }

            writeTermElement(nnTypeOfOperationKeywordTermID,sXML);
        }

    // #11: linsmaterial, same here, we need the uncooked value for transmogrification (nValue ---> sXML)
        {
            int nValue  = 0;
            auto e      = KatRegFields::eLensMaterial;
            if (st.mValues.count(e) > 1)
                guruMeditation("got too many values for enum #%d",static_cast<int>(e));
            if (st.mValues.count(e) > 0)
                nValue = st.mValues[e].first().toInt();

            QString sXML;
            switch (nValue)
            {
            case nnAcrylHydrofobValue : sXML = "10"; break;
            case nnAcrylHydrofilValue : sXML = "11"; break;
            case nnAcrylAnnatValue    : sXML =  "5"; break;
            case nnIngenLinsValue     : sXML =  "6"; break;
            default :
                guruMeditation("unknown uncooked optype value %d",nValue);
            }

            writeTermElement(nnLinsmaterialKeywordTermID,sXML);
        }

     // #12: linsegenskaper
        {
            int nKeywordTermID = nnLinsegenskaperKeywordTermID;

            writeStartElement(nKeywordTermID);
            writeBoolTermElement(nKeywordTermID,nnGulLinsTermID             );
            writeBoolTermElement(nKeywordTermID,nnMultifokalLinsTermID      );
            writeBoolTermElement(nKeywordTermID,nnToriskLinsTermID          );
            writeBoolTermElement(nKeywordTermID,nnLinsMedForlangtFokusTermID);
            wx.writeEndElement();
        }

     // #14: antibiotika
        {
            int nKeywordTermID = nnAntibiotikaKeywordTermID;

            writeStartElement(nKeywordTermID);
            writeBoolTermElement(nKeywordTermID,nnCefuroximTermID       );
            writeBoolTermElement(nKeywordTermID,nnDoktacillinTermID     );
            writeBoolTermElement(nKeywordTermID,nnVigamoxTermID         );
            writeBoolTermElement(nKeywordTermID,nnAnnanAntibiotikaTermID);
            wx.writeEndElement();

        // note: no storing of nnNejIngenAntibiotikaTermID in XML (all 4 terms == false means the same)
        }

     // #17 kirurg
        {
        // instead of the name we need a code (expected in ROSettings.ini)
            QString sXML;
            int nIndex = slSurgeons.indexOf(st.mAllCooked[KatRegFields::eSurgeon].sValue);
            if (nIndex >= 0)
                sXML = slSurgeonNos[nIndex];
            else
            // use this fallback no. if this surgeon's name was not in the list
                sXML = sFallbackSurgeonNo;

            writeTermElement(nnSurgeonKeywordTermID,sXML);
        }

    // --- end of <Interoperativ>
        wx.writeEndElement();

    // --- <Postoperativ> (the last one)
        wx.writeStartElement("Postoperativ");

    // #16: postoperativt inflammationsprofylax
        {
            int nKeywordTermID = nnPostoperativtIPKeywordTermID;

            writeStartElement(nKeywordTermID);
            writeBoolTermElement(nKeywordTermID,nnTopikalaNSAIDTermID);
            writeBoolTermElement(nKeywordTermID,nnTopikalaStTermID);
            writeBoolTermElement(nKeywordTermID,nnSubkonjunktivalaStTermID);
            writeBoolTermElement(nKeywordTermID,nnAnnatProfylaxTermID);
            wx.writeEndElement();

        // note: no storing of nnIngenProfylaxTermID in XML (all 4 terms == false means the same)
        }

    // --- end of <Postoperativ>
        wx.writeEndElement();

    // ------ that's all for this <Operation>
        wx.writeEndElement();
    }

    wx.writeEndElement();   // end of <KataraktOperationer>
    wx.writeEndDocument();  // end of document
    f->close();

// show a final progress message and a message box ---------------------------------------
    QString sMsg = QString("KatReg-fil %1 skapad med %2 besök.").arg(sFileName).arg(nVisits);

    if ((0 == nErrors) && (0 == nWarnings))
        sMsg += " Inga fel och inga varningar.";

    if ((0 == nErrors) && (1 == nWarnings))
        sMsg += QString(" Inga fel. 1 besök fick en varning.");

    if ((0 == nErrors) && (nWarnings > 1))
        sMsg += QString(" Inga fel. %1 besök fick varningar.").arg(nWarnings);

    if ((nErrors > 0) && (0 == nWarnings))
        sMsg += QString(" %1 besök hade fel och är inte med i KatReg-filen. Inga varningar.").arg(nErrors);

    if ((nErrors > 0) && (1 == nWarnings))
        sMsg += QString(" %1 besök hade fel och är inte med i KatReg-filen. 1 besök fick en varning.").arg(nErrors);

    if ((nErrors > 0) && (nWarnings > 1))
        sMsg += QString(" %1 besök hade fel och är inte med i KatReg-filen. %2 besök fick varningar.").arg(nErrors).arg(nWarnings);

    showProgress(sMsg);

#if defined(Q_OS_WIN)
    sMsg.replace(". ",".\n");  // on Windows, show same message in the info box but split it into multiple lines
    TWUtils::infoBox(sMsg);
#endif
}

//----------------------------------------------------------------------------
// on_settingsButton_clicked
//
// 2023-10-16 First version
// 2024-03-10 Drop SLLINT01 and support SLLINT02
//----------------------------------------------------------------------------
void MainWindow::on_settingsButton_clicked()
{
// wire up a Settings dialog
    SettingsDialog sd(this);
    sd.setModal(true);
    sd.show();

// before launching the dialog, get our current settings
    roSettings.setCurrentSection("DefaultPeriod");
    int nPeriodMonths = roSettings.readInt("Months");
    int nPeriodWeeks  = roSettings.readInt("Weeks");
    int nPeriodDays   = roSettings.readInt("Days");

// if we have some R/W settings, try to use them instead
    appDataSettings.setCurrentSection("DefaultPeriod");
    nPeriodMonths = appDataSettings.readIntWithDefault("Months",nPeriodMonths);
    nPeriodWeeks  = appDataSettings.readIntWithDefault("Weeks", nPeriodWeeks);
    nPeriodDays   = appDataSettings.readIntWithDefault("Days",  nPeriodDays);

// got the settings, calc. the index we'll use in the combo box
    int nDefaultDateRangeIndex = 0; // default: one day range
    if (1 == nPeriodWeeks)
        nDefaultDateRangeIndex = 1;
    if (2 == nPeriodWeeks)
        nDefaultDateRangeIndex = 2;
    if (1 == nPeriodMonths)
        nDefaultDateRangeIndex = 3;
    if (2 == nPeriodMonths)
        nDefaultDateRangeIndex = 4;

// get the db and table prefix settings
    roSettings.setCurrentSection("Intelligence");
    int nUseServer      = roSettings.readInt("UseServer");
    int nUseTablePrefix = roSettings.readInt("UseTablePrefix");

// if we already have some R/W settings for those, use them instead
    if (TWAppSettings::IniLocation::eAppData == eIniFlavorForDB)
    {
        appDataSettings.setCurrentSection(sIntelligenceSectionName);
        nUseServer      = appDataSettings.readInt("UseServer");
        nUseTablePrefix = appDataSettings.readInt("UseTablePrefix");
    }

// calc. the combox index
    int nDBServerAndPrefixIndex = 0; // default: PRSINTDB01
    if ((1 == nUseServer) && (1 == nUseTablePrefix))
        nDBServerAndPrefixIndex = 0;
    if ((2 == nUseServer) && (1 == nUseTablePrefix))
        nDBServerAndPrefixIndex = 1;
    if ((1 == nUseServer) && (2 == nUseTablePrefix))
        nDBServerAndPrefixIndex = 2;
    if ((2 == nUseServer) && (2 == nUseTablePrefix))
        nDBServerAndPrefixIndex = 3;

// set it them now in the dialog class and launch the modal dialog
    sd.setDefaultDateRangeIndex(nDefaultDateRangeIndex);
    sd.setDBServerAndPrefixIndex(nDBServerAndPrefixIndex);

    if (QDialog::Accepted == sd.exec())  // wait for user to click Ok or Cancel
    {
    // user pressed Ok, any changes to the settings?
        if (nDefaultDateRangeIndex != sd.nDefaultDateRangeIndex)
        {
        // got a new date range, write it to the R/W ini file
            nDefaultDateRangeIndex = sd.nDefaultDateRangeIndex;

            int nPeriodMonths = 0;
            int nPeriodWeeks  = 0;
            int nPeriodDays   = 0;
            if (0 == nDefaultDateRangeIndex)
                nPeriodDays   = 1;
            if (1 == nDefaultDateRangeIndex)
                nPeriodWeeks  = 1;
            if (2 == nDefaultDateRangeIndex)
                nPeriodWeeks  = 2;
            if (3 == nDefaultDateRangeIndex)
                nPeriodMonths = 1;
            if (4 == nDefaultDateRangeIndex)
                nPeriodMonths = 2;

            appDataSettings.setCurrentSection("DefaultPeriod");
            appDataSettings.writeInt("Months",nPeriodMonths);
            appDataSettings.writeInt("Weeks" ,nPeriodWeeks );
            appDataSettings.writeInt("Days"  ,nPeriodDays  );
        }

        if (nDBServerAndPrefixIndex != sd.nDBServerAndPrefixIndex)
        {
        // got a new DB setting, write it to the R/W ini file
            nDBServerAndPrefixIndex = sd.nDBServerAndPrefixIndex;

        // default is 1,1
            int nUseServer      = 1;
            int nUseTablePrefix = 1;

            if (0 == nDBServerAndPrefixIndex)
            {
                nUseServer      = 1;
                nUseTablePrefix = 1;
            }
            if (1 == nDBServerAndPrefixIndex)
            {
                nUseServer      = 2;
                nUseTablePrefix = 1;
            }
            if (2 == nDBServerAndPrefixIndex)
            {
                nUseServer      = 1;
                nUseTablePrefix = 2;
            }
            if (3 == nDBServerAndPrefixIndex)
            {
                nUseServer      = 2;
                nUseTablePrefix = 2;
            }

        // we need to copy the whole R/O shebang/section (for the benefit of TWTCSupport)
            auto copySetting = [this] (QString sKeyName) { appDataSettings.writeString(sKeyName,roSettings.readString(sKeyName)); };

            roSettings.setCurrentSection("Intelligence");
            appDataSettings.setCurrentSection("Intelligence" + QString::number(nCustomerNo)); // don't use sIntelligenceSectionName

            copySetting("UserName");
            copySetting("EncryptedPassword");
            copySetting("Database");
            copySetting("PortNo");
            appDataSettings.writeInt("UseServer"     ,nUseServer     );
            appDataSettings.writeInt("UseTablePrefix",nUseTablePrefix);
            copySetting("Server1");
            copySetting("Server2");
            copySetting("Server1TablePrefix1");
            copySetting("Server1TablePrefix2");
            copySetting("Server2TablePrefix1");
            copySetting("Server2TablePrefix2");

        // written lots of stuff, so make a flush here
            appDataSettings.sync();
        }
    }

// that's for the Setting dialog
}
