#----------------------------------------------------------------------------------#
# TC2KatRegBas.pro                                                                 #
#                                                                                  #
# 2024-02-11 First version                                                     HS  #
#----------------------------------------------------------------------------------#

QT      += core gui widgets sql network

TARGET   = TC2KatRegBas
TEMPLATE = app

HEADERS += katregtermsBas.h mainwindow.h showbox.h settingsdialog.h
SOURCES += main.cpp mainwindow.cpp showbox.cpp settingsdialog.cpp
FORMS   += mainwindow.ui showbox.ui settingsdialog.ui

# version and buildno
VERSION  = 3.0.0.1

# customer name and description
include (NameAndDescription.pri)

# keep CareUnits.Name for nice retrieval in the About box (note: Windows-1252 not UTF8)
DEFINES += "BUILTFOR=\"$$QMAKE_TARGET_DESCRIPTION\""

# set ourselves as the company and copyright
QMAKE_TARGET_COMPANY=Tungware
QMAKE_TARGET_COPYRIGHT=Tungware

# resources stuff
RESOURCES = TC2KatRegBas.qrc

# include our common QtProjects helper
include (../../include/QtProjects.pri)

# say hello to the usual suspects
# note: smb2 is only needed for static builds (Windows only)
LIBS    += $$TWLibLine(TWUtils)
LIBS    += $$TWLibLine(TWDB)
LIBS    += $$TWLibLine(TWTCSupport)
win32: LIBS += $$TWLibLine(smb2)

# and the #defines for the #include statements
DEFINES  += $$TWDefineInclude(TWUtils)
DEFINES  += $$TWDefineInclude(TWDB)
DEFINES  += $$TWDefineInclude(TWTCSupport)
