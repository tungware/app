/*--------------------------------------------------------------------------*/
/* showbox.h                                                                */
/*                                                                          */
/* 2024-01-02 First version                                             HS  */
/*--------------------------------------------------------------------------*/

#include <QDialog>
#include "ui_showbox.h"
#include "mainwindow.h"

// pull in the Tungware libraries
#include INCLUDETWUTILS

// use a QDialog as a baseclass for the ShowBox
class ShowBox : public QDialog
{
    Q_OBJECT

public:
    explicit ShowBox(QWidget *parent, KatRegEntryStruct st);

private slots:
    void on_pushButtonOk_clicked();

private:
    Ui::ShowBox* ui;
};
