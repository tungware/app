/*--------------------------------------------------------------------------*/
/* settingsdialog.h                                                         */
/*                                                                          */
/* 2023-12-01 First version                                             HS  */
/*--------------------------------------------------------------------------*/

#include <QDialog>
#include "ui_settingsdialog.h"

// pull in the Tungware libraries
#include INCLUDETWUTILS

// use a QDialog for our settings
class SettingsDialog : public QDialog
{
    Q_OBJECT

public:
    explicit SettingsDialog(QWidget *parent);
    ~SettingsDialog();

// use an integer index to get/set the date range and db stuff
    int nDefaultDateRangeIndex;
    int nDBServerAndPrefixIndex;

// use the setters
    void setDefaultDateRangeIndex(int i);
    void setDBServerAndPrefixIndex(int i);

private slots:
    void on_pushButtonOk_clicked();
    void on_toolButtonAbout_clicked();

    void on_pushButtonCancel_clicked();

private:
    Ui::SettingsDialog* ui;
};
