/*--------------------------------------------------------------------------*/
/* showbox.cpp                                                              */
/*                                                                          */
/* 2024-01-02 First version                                             HS  */
/*--------------------------------------------------------------------------*/

#include "showbox.h"

// use a ctor where you pass along the specific KatRegEntry
ShowBox::ShowBox(QWidget *parent, KatRegEntryStruct st) : QDialog(parent), ui(new Ui::ShowBox)
{
    ui->setupUi(this);

// preamble you know the drill
    TWUtils::helloQt(this);

// require the eye flavor and the visit date to be set (personno. is optional)
    if (st.cEye == ccUnspecifiedEye)
        guruMeditation("expected either a right or a left eye");
    if (!st.dVisit.isValid())
        guruMeditation("expected a valid visit date");

// have something for the window title?
    QString sTitle = "BasReg ";
    if (!st.sPersonNo.isEmpty())
        sTitle += st.sPersonNo + " ";
    if (st.cEye == 'H')
         sTitle += "höger";
    else sTitle += "vänster";
    sTitle += "  " + st.sVisitDateTime;  // assume this is valid if dVisit is valid
    setWindowTitle(sTitle);

// also set the title label
    ui->labelTitle->setText(sTitle);

// calculate the row height we'll use for creating QTextEdits from the one that exists already
    int nRowHeight = ui->textEditEye->height();

// prepare for the show with some handy lambdas
    auto setTextOrError = [st] (QTextEdit* pte, KatRegFields e, QString sPrefix = "")
    {
        if (st.mAllCooked[e].sError.isEmpty())
        {
        // no error text, so show the normal cooked value(s) with an optional prefix
            QString sText = sPrefix + st.mAllCooked[e].sValue;
            pte->setText(sText);
            pte->setToolTip(sText);  // why not?
        }
        else
        {
        // show the error message in red
            auto palette = pte->palette();
            palette.setColor(QPalette::Text,"red"); // normal "red" as a nice foreground color
            pte->setPalette(palette);

        // get the error text and elide it if necessary (to fit inside a single line for the QTextEdit)
            QString sError = st.mAllCooked[e].sError;

            int nAvailableWidth = pte->geometry().width();
            int nExtraMargin = 24;   // give some headroom the the right (since we're always left aligned)
            if (nAvailableWidth > (nExtraMargin * 2)) // unless this is a very *narrow* textedit?
                nAvailableWidth -= nExtraMargin;

            pte->setText(QFontMetrics(pte->font()).elidedText(sError,Qt::ElideMiddle,nAvailableWidth));
            pte->setToolTip(sError); // set the full error text as a tooltip (can be helpful)
        }

    // for both normal texts and errors, align them to the left
        pte->setAlignment(Qt::AlignLeft);

    // currently we don't care about warnings (only have one: "surgeon not found" as of spring 2024)
    };

    auto newTextEdit = [this,nRowHeight] (QLabel* l)
    {
    // wire up a new QTextEdit from thin air
        auto pte = new QTextEdit(this);

    // give this QTextEdit an object name similiar to the label's
        auto sObjectName = l->objectName();
        sObjectName.replace("label","textEdit");
        pte->setObjectName(sObjectName);

    // require the name have a #Row(s) suffix in the name
    // currently we support "..1Row" up to "..9Rows"
        if (!sObjectName.endsWith("1Row") && !sObjectName.endsWith("Rows"))
            fubar("Bad object name for label");

        bool bGotDigit = false;
        int nRows = TWUtils::tossAllButDigits(sObjectName).toInt(&bGotDigit);
        if (!bGotDigit)
            fubar("Label object name didn't contain # of rows for the TextEdit");
        if ((nRows < 1) || (nRows > 9))
            fubar("# of rows must be a number 1 .. 9");

    // do the geometry fiddling
        auto r = l->geometry();
        r.adjust(0,nRowHeight,0,nRowHeight * nRows);
        pte->setGeometry(r);

    // and font fiddling (reuse the same font as the label but set it to Bold)
        auto f = l->font();
        f.setBold(true);
        pte->setFont(f);

    // and some custom (boilerplate) stuff
        pte->setFrameShape(QFrame::NoFrame);
        pte->setFrameShadow(QFrame::Plain);
        pte->setMidLineWidth(0);
        pte->setReadOnly(true);
        pte->setPlaceholderText(QString());

    // ok, light it up and return it
        pte->show();
        return pte;
    };

// --- show time -----------------------------------------------------------------
// note: for #3 and #4 we should already have QTextEdits created from the .ui file
//  #3: eye flavor
    ui->textEditEye->setText((st.cEye == 'H') ? "Höger öga" : "Vänster öga");

// #4: visus right and left
    setTextOrError(ui->textEditVisusRight,KatRegFields::eVisusRight,"Höger     ");
    setTextOrError(ui->textEditVisusLeft ,KatRegFields::eVisusLeft ,"Vänster  " );

// for the rest we'll create new QTextEdits
    setTextOrError(newTextEdit(ui->labelIndikationsgrupp1Row    ),KatRegFields::eNIKE               );
    setTextOrError(newTextEdit(ui->labelWaitingList1Row         ),KatRegFields::eDateWaitingList    );
    setTextOrError(newTextEdit(ui->labelDateOperation1Row       ),KatRegFields::eDateOperation      );
    setTextOrError(newTextEdit(ui->labelTillstand4Rows          ),KatRegFields::eEyeStates          );
    setTextOrError(newTextEdit(ui->labelAnnanIndikation3Rows    ),KatRegFields::eOtherIndications   );
    setTextOrError(newTextEdit(ui->labelOpType1Row              ),KatRegFields::eTypeOfOperation    );
    setTextOrError(newTextEdit(ui->labelLinsmaterial1Row        ),KatRegFields::eLensMaterial       );
    setTextOrError(newTextEdit(ui->labelSarskildaEgenskaper3Rows),KatRegFields::eSpecificProps      );
    setTextOrError(newTextEdit(ui->labelPeroperativAtgard4Rows  ),KatRegFields::ePeroperativeActions);
    setTextOrError(newTextEdit(ui->labelAntibiotika3Rows        ),KatRegFields::eAntibiotics        );
    setTextOrError(newTextEdit(ui->labelPeroperativSkada1Row    ),KatRegFields::ePeroperativeDamages);
    setTextOrError(newTextEdit(ui->labelProfylax2Rows           ),KatRegFields::ePostoperativeIP    );
    setTextOrError(newTextEdit(ui->labelKirurg1Row              ),KatRegFields::eSurgeon            );
}

//----------------------------------------------------------------------------
// on_pushButtonOk_clicked
//
// 2024-02-04 First version
//----------------------------------------------------------------------------
void ShowBox::on_pushButtonOk_clicked()
{
// simply goodbye
    QDialog::accept();
}
