/*--------------------------------------------------------------------------*/
/* main.cpp                                                                 */
/*                                                                          */
/* 2019-05-11 First version                                             HS  */
/*--------------------------------------------------------------------------*/

#include "mainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    return a.exec();
}
