/*--------------------------------------------------------------------------*/
/* mainwindow.h                                                             */
/*                                                                          */
/* 2019-05-11 First version                                             HS  */
/*--------------------------------------------------------------------------*/
#pragma once

#include "qmainwindow.h"

// pull in windows.h for the Win32 low-level stuff :-(
#include "windows.h"

namespace Ui { class MainWindow; }

// maximum length of typed text
static const int nnMaxTypedLength = 8;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget* parent = nullptr);
    ~MainWindow();

// log text functions
    void logText(QString sText);

// for key injection (work in progress)
    void SendKey(WORD wVk);

public slots:
    void on_exitButton_clicked();
    void keyFromHook(QChar qc);

private:
    Ui::MainWindow* ui;
};

// need a separate class for the hooking stuff
class KeyboardHooker : public QObject
{
    Q_OBJECT

public:
// MainWindow instance ptr (for the signal to work)
    MainWindow* pMainWindow = 0;

// sets up and tears down the keyboard hook
    void hookOn();
    void hookOff();

// hook handle (keep it for the unhooking at program exit)
    HHOOK hhk;

// static chap that will do the callback duty
    static LRESULT CALLBACK KeyboardProc(int Code, WPARAM wParam, LPARAM lParam);

// signal to fire when we have a key
signals:
    void gotKey(QChar qc);
};
