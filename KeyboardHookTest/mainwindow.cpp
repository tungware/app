/*--------------------------------------------------------------------------*/
/* mainwindow.cpp                                                           */
/*                                                                          */
/* 2019-05-11 First version                                             HS  */
/*--------------------------------------------------------------------------*/

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "qdatetime.h"
#include "qdebug.h"

// setup a static instance of our hook class
KeyboardHooker kh;

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);

// make our main window non-resizable for now
    setMinimumSize(size());
    setMaximumSize(size());

// set window title
    setWindowTitle("Keyboard Hook Test");

// fire up our keyboard hook class
    kh.pMainWindow = this;
    kh.hookOn();

// bombs away
    logText("Hook set - go ahead and type something");
    logText("  (showing # of milliseconds and 'key'");
}

MainWindow::~MainWindow()
{
// don't forget to unhook
    kh.hookOff();

// sayonara
    delete ui;
}

void MainWindow::logText(QString sText)
{
    ui->listWidget->addItem(sText);
    ui->listWidget->scrollToBottom();
}

void MainWindow::on_exitButton_clicked()
{
    qApp->quit();
}

// just a simple logger for now
void MainWindow::keyFromHook(QChar qc)
{
    QString sc(qc);
    logText(QString::number(QTime::currentTime().msecsSinceStartOfDay() % 100000) + ": '" + sc + "'");
}

// KeyboardHooker methods
void KeyboardHooker::hookOn()
{
// try to set our hook
   hhk = ::SetWindowsHookEx(WH_KEYBOARD_LL, KeyboardProc, ::GetModuleHandle(NULL), 0);

// hooked ok?
    if (NULL == hhk)
    // no, sayonara
        qFatal("SetWindowsHookEx() failed");

// connect our signal to slot in MainWindows
    connect(this,&KeyboardHooker::gotKey,pMainWindow,&MainWindow::keyFromHook);

// ready to run
}

// call this at program exit
void KeyboardHooker::hookOff()
{
    ::UnhookWindowsHookEx(hhk);
}

// --------------------------------------------
// Windows calls here for every keyboard event
LRESULT CALLBACK KeyboardHooker::KeyboardProc(int code, WPARAM wParam, LPARAM lParam)
{
// only listen to WM_KEYUP (makes it much easier)
    if (WM_KEYUP != wParam)
    // we're not interested, but someone else might be, forward it
        return ::CallNextHookEx(NULL, code, wParam, lParam);

// decode the keyboard message
    KBDLLHOOKSTRUCT* kbdStruct = (KBDLLHOOKSTRUCT*) lParam;
    DWORD wVirtKey             = kbdStruct->vkCode;
    DWORD wScanCode            = kbdStruct->scanCode;

// handle control, shift, alt and capslock keys
    static BYTE lpKeyState[256] = {0};

    lpKeyState[VK_CONTROL] = (::GetKeyState(VK_LCONTROL) < 0) || (::GetKeyState(VK_RCONTROL) < 0) ? 0x80 : 0;
    lpKeyState[VK_SHIFT  ] = (::GetKeyState(VK_LSHIFT  ) < 0) || (::GetKeyState(VK_RSHIFT)   < 0) ? 0x80 : 0;
    lpKeyState[VK_MENU   ] = (::GetKeyState(VK_LMENU   ) < 0) || (::GetKeyState(VK_RMENU)    < 0) ? 0x80 : 0;

    lpKeyState[VK_CAPITAL] = ::GetKeyState(VK_CAPITAL) & 1;

// keystate array ready, try translate into Unicode char
    const int nnMaxChars = 8;
    WCHAR aw[nnMaxChars] = {0};

    int r = 0;
    r = ::ToUnicode(wVirtKey, wScanCode, lpKeyState, aw, nnMaxChars, 0);

// if c does not return 1 (1 wchar) ignore this (this means we're tossing single ALT, CTRL etc. keypresses)
    if (1 == r)
    {
    // convert the wchar to a QChar
        QChar qc(aw[0]);

    // we got something, fire the signal
        emit kh.gotKey(qc);
    }

// that's all, return back to the chain hook event handlers
    return ::CallNextHookEx(NULL, code, wParam, lParam);
}
