#-----------------------------------------------------------------------------
# KeyboardHookTest.pro
#
# 2019-05-11 First version
#-----------------------------------------------------------------------------

QT      += core gui widgets

TEMPLATE = app
TARGET   = KeyboardHookTest

SOURCES += main.cpp mainwindow.cpp
HEADERS += mainwindow.h
FORMS   += mainwindow.ui

# need to link with user32.lib for the hook stuff
LIBS += -luser32
