/*--------------------------------------------------------------------------*/
/* caesar.h                                                                 */
/*                                                                          */
/* 2025-01-01 First version                                             HS  */
/*--------------------------------------------------------------------------*/

// pull in these chaps
#include INCLUDETWUTILS
#include INCLUDETWDB
#include INCLUDETWTCSUPPORT

// need Windows.h for the Win32 APIs
#include "Windows.h"

// TC window navigation
#define TC_MAINCAPTION "Take Care"
#define TC_FORM_PREFIX "DyalogForm"
#define TC_EDIT_PREFIX "DyalogEdit"
#define TC_GRID_PREFIX "DyalogGrid"

// title for the TC message box (pat. is deceased, or moved outside of Sweden etc.)
static QString ssTCMessageCaption = "TakeCare-meddelande";

// title prefix for the patient properties dialog
static QString ssPatPropsPrefix   = "Patientuppgifter - ";

// our output files
static QString ssCSVFilename = "PatientImport.csv";
static QString ssLogFilename = "Caesar.log";


// patient struct for import into Webdoc
// note: when writing the CSV file, don't forget to throw away all ' "" and ;
struct PatStruct
{
// first some booleans
    bool    bFailed;        // if true, pat. lookup failed, we try again (one time)
    bool    bMessageBox;    // if true, skip over this patient (most likely deceased)
    bool    bRedacted;      // if true, skip over this patient (name address not visible)
    bool    bCityNotFound;  // if true, skip over TC city not found in our LLKK map

// then the patient data
    QString sPersonNo;      // YYYYMMDD-NNNC
                            // currently no support for reservno. i.e. "Nej" always
    QDate   dDOB;           // YYYY-MM-DD
    QString sFirstNames;    // max 36 chars
    QString sLastName;      // max 50 chars
    QString sSex;           // "M" or "K"
    QString sAddress;       // max 41 chars
    QString sZip;           // 5 digits
    QString sCity;          // max 50 chars
    QString sLLKK;          // county and municipality concatenated (2 digits each)
                            // country: fixed value "Sverige"
    QString sCareOf;        // nax 255 chars (currently always empty)
                            // email not yet supported, leave empty
    int     nNoOfMobileNos; // # of mobile phone numbers found (for this patient)
    QString sMobileNo;      // digits only please
    QString sHomeNo;        //     -  "  -
    QString sWorkNo;        //     -  "  -
                            // remarks: none, leave empty
};

// ---------------------------
class Caesar : public QObject
{
    Q_OBJECT

public:
    Caesar();

// big wheel list and map
    QStringList           slPersonNos;         // personnos. we want to process
    QMap<QString,QString> mCities2LLKK;        // key: city data: LLKK (4 digits)
    QMap<QString,QString> mShortCities2Cities; // helper map for TakeCare quirk

// helper functions
    void        SendKey(WORD wVk);
    void        SendControlShiftKey(WORD wVk);
    QString     getCurrentTCPersonNoAndActivate();
    HWND        getTCMessageBox();
    HWND        getPatPropertiesBox();
    QStringList getPropChildrenTexts(HWND hWndPatPropertiesBox);
    QString     getGridPhoneNo      (HWND hWndPatPropertiesBox);

// main processing chaps
    void        doit(int nIndex);

    PatStruct   getPat(QString sPersonNo);
    void        writePat(QTextStream* tsCSV,PatStruct st);
    void        writeLog(QTextStream* tsLog,QString sMsg);
};
