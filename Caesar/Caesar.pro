#-----------------------------------------------------------------------------#
# Caesar.pro                                                                  #
#                                                                             #
# 2025-01-01 First version                                                HS  #
#-----------------------------------------------------------------------------#

QT      += core gui widgets sql network
CONFIG  += console

HEADERS += caesar.h
SOURCES += main.cpp caesar.cpp

# include our common QtProjects helper
include(../../include/QtProjects.pri)

# note: smb2 is only needed for static builds (currently Windows only)
LIBS    += $$TWLibLine(TWUtils)
LIBS    += $$TWLibLine(TWDB)
LIBS    += $$TWLibLine(TWTCSupport)
win32: LIBS += $$TWLibLine(smb2)

# and the #defines for the #include statements
DEFINES += $$TWDefineInclude(TWUtils)
DEFINES += $$TWDefineInclude(TWDB)
DEFINES += $$TWDefineInclude(TWTCSupport)
