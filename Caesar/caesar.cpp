/*--------------------------------------------------------------------------*/
/* caesar.cpp                                                               */
/* exports patient data from TakeCare to a Webdoc flavored .csv file        */
/*                                                                          */
/* 2025-01-01 First version                                             HS  */
/*--------------------------------------------------------------------------*/

#include "caesar.h"
#include "qtimer.h"
#include "qcommandlineparser.h"

// --------------------------------------------
// Caesar starts here
// --------------------------------------------
Caesar::Caesar()
{
// say hello
    qputs(QString("Caesar version 1.3 built %1").arg(TWUtils::toISODate(TWUtils::buildDate())));


// get the personnos we're processing
    TWTextFileReader twPersonNos("Personno.txt");
    while (!twPersonNos.ts.atEnd())
    {
        auto sPersonNo = twPersonNos.ts.readLine();

    // check check and check
        if (sPersonNo.length() != 14)   // expect lines to be "YY YYMMDD-NNNC"
            fubar("bad personno. length (not 14)");

        if (sPersonNo[2] != ' ')
            fubar("3d position not a space character");

        if (sPersonNo[9] != '-')
            fubar("10th position not a hyphen");

        auto sError = TWUtils::checkPersonNo(sPersonNo);
        if (!sError.isEmpty())
            fubar(sError + ": " + sPersonNo);

    // good to go, store it the way Webdoc wants (same way as Fortnox, fortunate for us)
        slPersonNos += TWTCSupport::personNoForFortnox(TWUtils::tossAllButDigits(sPersonNo));
    }
    twPersonNos.close();
    qputs(QString("Read %1 personnos.").arg(slPersonNos.count()));


// read the list of cities --> LLKK numbers
    TWTextFileReader twCities("Postort-Kommun-Lan.csv");
    twCities.ts.readLine(); // toss first line (expected to be a header row)
    while (!twCities.ts.atEnd())
    {
        auto sLine = twCities.ts.readLine();
        auto sl = sLine.split(",");
        if (8 != sl.count())
            fubar("bad split count");

    // TC has a quirk: Cities are stored with max width of 13 characters
    // e.g. "Saltsjö-Duvnäs" is stored as "Saltsjö-Duvnä"
    //
    // so we create two maps: one with shortform/13 chars cities ---> cities
    //                        and one with cities ---> LLKKs
        QString sCity   = sl[0];
        QString sCity13 = sCity.left(13);

        QString sLLKK   = sl[3];

    // check that the city names are unique keys in the maps
        if (mCities2LLKK.contains(sCity))
            fubar("city not unique");

        if (mShortCities2Cities.contains(sCity13))
            fubar("shortcity not unique");

    // ok map stuffing time
        mCities2LLKK.insert(sCity,sLLKK);
        mShortCities2Cities.insert(sCity13,sCity);  // yes the data is the non-truncated city
    }
    twCities.close();
    qputs(QString("Got %1 lines of cities --> LLKK").arg(mCities2LLKK.count()));
    qputs("");


// start with the first patient in the list or?
    int nIndex = 0;  // assume a fresh start
    QCommandLineParser lp;
    lp.process(qApp->arguments());
    auto slArgs = lp.positionalArguments();
    if (slArgs.count() == 1)
    {
    // assume this is a personno. from a previous run
        QString sPrevPersonNo = slArgs[0];

        nIndex = slPersonNos.indexOf(sPrevPersonNo);
        if (nIndex < 0)
            fubar("Couldn't find the personno");
    }

// start the party
    QTimer::singleShot(nnNormalDelay,this,[this,nIndex] { doit(nIndex); });
}


//----------------------------------------------------------------------------
// SendKey
// send a single key using the SendInput() API
//
// 2025-01-02 First version
//----------------------------------------------------------------------------
void Caesar::SendKey(WORD wVk)
{
// prepare for the Win32 SendInput() call
    INPUT ip;
    ip.type           = INPUT_KEYBOARD; // set the invariant chaps here
    ip.ki.wScan       = 0;
    ip.ki.time        = 0;
    ip.ki.dwExtraInfo = static_cast<ULONG>(GetMessageExtraInfo());

    INPUT ipArray[2]; // need 2 structs concatenated

    ip.ki.wVk     = wVk;
    ip.ki.dwFlags = 0;
    ipArray[0]    = ip;

    ip.ki.dwFlags = KEYEVENTF_KEYUP;
    ipArray[1]    = ip;

    SendInput(2,ipArray,sizeof(ip));
}

//----------------------------------------------------------------------------
// SendControlShiftKey
// send a single control+shift key
//
// 2025-01-02 First version
//----------------------------------------------------------------------------
void Caesar::SendControlShiftKey(WORD wVk)
{
// prepare for the Win32 SendInput() call
    INPUT ip;
    ip.type           = INPUT_KEYBOARD; // set the invariant chaps here
    ip.ki.wScan       = 0;
    ip.ki.time        = 0;
    ip.ki.dwExtraInfo = static_cast<ULONG>(GetMessageExtraInfo());

    INPUT ipArray[6]; // for a control and shift key, we need 6 structs concatenated

    ip.ki.wVk     = VK_CONTROL;
    ip.ki.dwFlags = 0;
    ipArray[0]    = ip;

    ip.ki.wVk     = VK_SHIFT;
    ip.ki.dwFlags = 0;
    ipArray[1]    = ip;

    ip.ki.wVk     = wVk;
    ipArray[2]    = ip;

    ip.ki.dwFlags = KEYEVENTF_KEYUP;
    ipArray[3]    = ip;

    ip.ki.wVk     = VK_SHIFT;
    ipArray[4]    = ip;

    ip.ki.wVk     = VK_CONTROL;
    ipArray[5]    = ip;

    SendInput(6,ipArray,sizeof(ip));
}


//----------------------------------------------------------------------------
// getCurrentTCPersonNoAndActivate
//
// 2025-01-02 First version
//----------------------------------------------------------------------------
QString Caesar::getCurrentTCPersonNoAndActivate()
{
// need static for the lambda below to work good
    static HWND hWndParent;
    hWndParent = NULL; // pessimistic default

// look for the TC parent window
    EnumWindows([](HWND hWnd,LPARAM lParam)
    {
        Q_UNUSED(lParam)    // currently unused

    // get the current window's classname
        wchar_t szWin32HobbyHorse[MAX_PATH * 2];
        int nClassNameLen  = GetClassNameW(hWnd,szWin32HobbyHorse,MAX_PATH);
        QString sClassName = QString::fromWCharArray(szWin32HobbyHorse);

    // look for our TC form classname prefix
        if ((0 == nClassNameLen) || !sClassName.startsWith(TC_FORM_PREFIX))
            return TRUE;    // not one of ours, next please

    // we got a classname match: check the caption of this window
        memset(szWin32HobbyHorse,0,sizeof(szWin32HobbyHorse));   // clear before use
        GetWindowTextW(hWnd,szWin32HobbyHorse,MAX_PATH - 1);     // ignore any errors
        QString sCaption = QString::fromWCharArray(szWin32HobbyHorse);

    // is it TakeCare?
        if (TC_MAINCAPTION != sCaption)
            return TRUE;    // no next window, please

    // almost there: does this window own any child windows?
        wchar_t awClassName[MAX_PATH * 2] = {0};
        sClassName.toWCharArray(awClassName);
        if (nullptr == FindWindowEx(hWnd,nullptr,awClassName,nullptr))
        // no children: forget about this window, next please
            return TRUE;

    // got what we looked for, save the hWnd for this window
        hWndParent = hWnd;
        return FALSE;   // don't need any more enumerations, thanks

    }, 0);  // 0 = currently unused param

// found a TC window?
    if (NULL == hWndParent)
        return "";  // no sorry


// step #2: look for a child window containing the personno (or reservno)
    static QString sPersonNo;   // static so it's visible from within the lambda
    sPersonNo = "";
    EnumChildWindows(hWndParent,[](HWND hWnd,LPARAM lParam)
    {
        Q_UNUSED(lParam)    // currently unused

    // get the classname (you know the drill by now)
        wchar_t szWin32HobbyHorse[MAX_PATH * 2];
        int nClassNameLen  = GetClassNameW(hWnd,szWin32HobbyHorse,MAX_PATH);
        QString sClassName = QString::fromWCharArray(szWin32HobbyHorse);

    // look for DyalogEdit prefixes
        if ((0 == nClassNameLen) || !sClassName.startsWith(TC_EDIT_PREFIX))
            return TRUE;   // no dice next please

    // we got a classname match: check the caption for this window
    // use SendMessage() because GetWindowsText() fails across processes for control texts
        ZeroMemory(szWin32HobbyHorse,sizeof(szWin32HobbyHorse));
        SendMessage(hWnd,WM_GETTEXT,static_cast<WPARAM>(sizeof(szWin32HobbyHorse)),reinterpret_cast<LPARAM>(szWin32HobbyHorse));
        QString s = QString::fromWCharArray(szWin32HobbyHorse);

    // check for a string that looks like this "nn  nnnnnn-nnnn" or "nn  nnnnnn+nnnn"
        if ((11 != s.length()) && (15 != s.length()))
        // no good, expected a length of 11 (for reservnr) or 15 (personno)
            return TRUE;    // next please

    // prepend "99" if it looks like a reservnr
        if (11 == s.length())
            s = "99" + s;

    // ask TWUtil to verify the number
        s = TWUtils::tossAllButDigits(s);
        if ("" != TWUtils::checkPersonNo(s))
        // no good, most likely not a valid personno or reservnr
            return TRUE;    // next window please

    // gotcha stop, the enumeration
        sPersonNo = s;
        return FALSE;

    }, 0);  // 0 = currently unused param


// if we have a nonempty personno, activate the hWndParent (so it will have the keyboard focus)
    if (!sPersonNo.isEmpty())
        TWUtils::makeTopWindow(hWndParent);

// and we're done
    return sPersonNo;
}

//----------------------------------------------------------------------------
// getTCMessageBox
// if a dialog box is visible return the HWND for it, else return NULL
//
// 2025-01-06 First version
//----------------------------------------------------------------------------
HWND Caesar::getTCMessageBox()
{
// need static for the lambda below to work good
    static HWND hWndBox;
    hWndBox = NULL;

// look for the dialog box class
    EnumWindows([](HWND hWnd,LPARAM lParam)
    {
        Q_UNUSED(lParam)    // currently unused

    // never mind the classname, just get the caption
        wchar_t szWin32HobbyHorse[MAX_PATH * 2];
        memset(szWin32HobbyHorse,0,sizeof(szWin32HobbyHorse));   // clear before use
        GetWindowTextW(hWnd,szWin32HobbyHorse,MAX_PATH - 1);     // ignore any errors
        QString sCaption = QString::fromWCharArray(szWin32HobbyHorse);

        if (ssTCMessageCaption != sCaption)
            return TRUE;    // not ours, next please

    // ok we have a visible box, set the hWnd
        hWndBox = hWnd;
        return FALSE;   // don't need any more enumerations thank you

    }, 0);  // 0 = currently unused param

// we're done, return the HWND found (or NULL)
    return hWndBox;
}

//----------------------------------------------------------------------------
// getPatPropertiesBox
// if the pat. properties box is visible, return the HWND for it, else NULL
//
// 2025-01-06 First version
//----------------------------------------------------------------------------
HWND Caesar::getPatPropertiesBox()
{
// need static for the lambda below to work good
    static HWND hWndBox;
    hWndBox = NULL; // pessimistic default

// look for a patient prop bo
    EnumWindows([](HWND hWnd,LPARAM lParam)
    {
        Q_UNUSED(lParam)    // currently unused

    // get the current window's classname
        wchar_t szWin32HobbyHorse[MAX_PATH * 2];
        int nClassNameLen  = GetClassNameW(hWnd,szWin32HobbyHorse,MAX_PATH);
        QString sClassName = QString::fromWCharArray(szWin32HobbyHorse);

    // look for our TC form classname
        if ((0 == nClassNameLen) || !sClassName.startsWith(TC_FORM_PREFIX))
            return TRUE;    // not one of ours, next please

    // we got a classname match: check the caption of this window
        memset(szWin32HobbyHorse,0,sizeof(szWin32HobbyHorse));  // clear before use
        GetWindowTextW(hWnd,szWin32HobbyHorse,MAX_PATH - 1);    // ignore any errors
        QString sCaption = QString::fromWCharArray(szWin32HobbyHorse);

        if (!sCaption.startsWith(ssPatPropsPrefix))
            return TRUE;    // not the window we're looking for. next please

    // almost there: does this window own any child form windows?
        wchar_t awClassName[MAX_PATH * 2] = {0};
        sClassName.toWCharArray(awClassName);
        if (nullptr == FindWindowEx(hWnd,nullptr,awClassName,nullptr))
        // no dyalog form childs found: forget about this window, next please
            return TRUE;

    // got what we looked for, save the hWnd for this window
        hWndBox = hWnd;
        return FALSE;   // don't need any more enumerations, thanks
    }, 0);  // 0 = currently unused param

// we're done, return the HWND found (or NULL)
    return hWndBox;
}

//----------------------------------------------------------------------------
// getPropChildrenTexts
//
// 2025-01-02 First version
//----------------------------------------------------------------------------
QStringList Caesar::getPropChildrenTexts(HWND hWndPatPropertiesBox)
{
    static QStringList slTexts;
    slTexts.clear();

// step thru the child windows of the pat prop. box
    EnumChildWindows(hWndPatPropertiesBox,[](HWND hWnd,LPARAM lParam)
    {
        Q_UNUSED(lParam)    // currently unused

    // get the classname (you should know the drill by now)
        wchar_t szWin32HobbyHorse[MAX_PATH * 2];
        int nClassNameLen  = GetClassNameW(hWnd,szWin32HobbyHorse,MAX_PATH);
        QString sClassName = QString::fromWCharArray(szWin32HobbyHorse);

    // look for DyalogEdit prefixes
        if ((0 == nClassNameLen) || !sClassName.startsWith(TC_EDIT_PREFIX))
            return TRUE;    // next please

    // we got a classname match: get the caption for this window
        ZeroMemory(szWin32HobbyHorse,sizeof(szWin32HobbyHorse));
        SendMessage(hWnd,WM_GETTEXT,static_cast<WPARAM>(sizeof(szWin32HobbyHorse)),reinterpret_cast<LPARAM>(szWin32HobbyHorse));

    // add this to the stringlist regardless if it's empty or not
        slTexts += QString::fromWCharArray(szWin32HobbyHorse);

    // always continue the enumeration (until we run out of child windows)
        return TRUE;
    }, 0);  // 0 = currently unused param


// return what we got
    return slTexts;
}

//----------------------------------------------------------------------------
// getGridPhoneNo
// gets the text from the first Dyalog Edit found in the grid
//
// 2025-01-06 First version
//----------------------------------------------------------------------------
QString Caesar::getGridPhoneNo(HWND hWndPatPropertiesBox)
{
    static QString sPhoneNo;  // with a bit of luck this will be a phone no.
    sPhoneNo.clear();

    static HWND hWndEdit;
    hWndEdit = NULL;

    static int nNoOfGridChaps;
    nNoOfGridChaps = 0;

// step thru and look for a DyalogGrid control
    EnumChildWindows(hWndPatPropertiesBox,[](HWND hWnd,LPARAM lParam)
    {
        Q_UNUSED(lParam)    // currently unused

    // get the classname
        wchar_t szWin32HobbyHorse[MAX_PATH * 2];
        int nClassNameLen      = GetClassNameW(hWnd,szWin32HobbyHorse,MAX_PATH);
        QString sGridClassName = QString::fromWCharArray(szWin32HobbyHorse);

    // look for Dyalog Grid prefixes
        if ((0 == nClassNameLen) || !sGridClassName.startsWith(TC_GRID_PREFIX))
            return TRUE;    // next please

    // found a DyalogGrid control, however we want the 3rd (and last) one
        ++nNoOfGridChaps;
        if (nNoOfGridChaps < 3)
            return TRUE;    // next please

    // ok looks promising, what about child windows of this grid control?
    // need a DyalogEdit, create the classname for it from the grid class name we just got
        auto sEditClassName = sGridClassName;
        sEditClassName.replace(TC_GRID_PREFIX,TC_EDIT_PREFIX);

        wchar_t awClassName[MAX_PATH * 2] = {0};
        sEditClassName.toWCharArray(awClassName);
        hWndEdit = FindWindowEx(hWnd,nullptr,awClassName,nullptr);
        if (!hWndEdit)
        // no dyalogedit kid: forget about this window, next please
            return TRUE;

    // ok we got a classname match: get the caption/text for the DyalogEdit
        ZeroMemory(szWin32HobbyHorse,sizeof(szWin32HobbyHorse));
        SendMessage(hWndEdit,WM_GETTEXT,static_cast<WPARAM>(sizeof(szWin32HobbyHorse)),reinterpret_cast<LPARAM>(szWin32HobbyHorse));

    // get the text but toss everything expect digits
        sPhoneNo = TWUtils::tossAllButDigits(QString::fromWCharArray(szWin32HobbyHorse));

    // gotcha, stop the enumeration
        return FALSE;

    }, 0);  // 0 = currently unused param


// return what we got
    return sPhoneNo;
}


//----------------------------------------------------------------------------
// doit
//
// 2025-01-02 First version
// 2025-01-08 Skip over patients with redacted names/addresses
// 2025-01-10 Skip over patients with cities we cannot find in our LLKK map
//----------------------------------------------------------------------------
void Caesar::doit(int nIndex)   // (expecting index to be 0-based)
{
// open the output files for append
    TWTextFileWriter tfwCSV;
    tfwCSV.openForAppend(ssCSVFilename);
    if (!tfwCSV.isOpen())
        fubar("Could not open the CSV file in append mode");

    TWTextFileWriter tfwLog;
    tfwLog.openForAppend(ssLogFilename);
    if (!tfwLog.isOpen())
        fubar("Could not open the log file in append mode");

// start the party
    writeLog(&tfwLog.ts,QString("%1 starting with patient no. %2 (%3)").
                     arg(TWUtils::toISODateTimeNoSeconds(QDateTime::currentDateTime())).arg(nIndex).arg(slPersonNos[nIndex]));

    for (int i = nIndex; (i < slPersonNos.count()); ++i)
    {
    // get the personno we're processing
        QString sPersonNo = slPersonNos[i];

    // have a chat with TakeCare
        auto stPat = getPat(sPersonNo);

    // did it fail?
        if (stPat.bFailed)
        {
        // sleep for 1 second and try again
            TWUtils::sleepForABit(1000);

            stPat = getPat(sPersonNo);
            if (stPat.bFailed)
            // failed again? throw in the towel
                fubar(QString("Patient lookup failed for %1").arg(sPersonNo));
        }

    // got a message box?
        if (stPat.bMessageBox)
        {
            writeLog(&tfwLog.ts,QString("%1: skipped (got a messagebox)").arg(sPersonNo));
            continue;   // next please
        }

    // got a redacted patient?
        if (stPat.bRedacted)
        {
            writeLog(&tfwLog.ts,QString("%1: skipped (name and address redacted)").arg(sPersonNo));
            continue;   // next please
        }

    // got a missing city?
        if (stPat.bCityNotFound)
        {
            writeLog(&tfwLog.ts,QString("%1: skipped (city '%2' not found)").arg(sPersonNo,stPat.sCity));
            continue;   // next please
        }

    // --- got a patient, check the personno. matches with what we wanted
        if (sPersonNo != stPat.sPersonNo)
            fubar(QString("patientno mismatch ('%1','%2')").arg(sPersonNo,stPat.sPersonNo));

    // save to the CSV file
        writePat(&tfwCSV.ts,stPat);

    // show oersonno. # of mobile phone. nos and name
        writeLog(&tfwLog.ts,QString("%1: %2 %3 %4").
                            arg(sPersonNo).arg(stPat.nNoOfMobileNos).arg(stPat.sFirstNames,stPat.sLastName));

    // next please (sleep a bit for debouncing)
        TWUtils::sleepForABit();
    }

// that's all for today
    writeLog(&tfwLog.ts,QString("That's all folks, closing at %1.").
                        arg(TWUtils::toISODateTimeNoSeconds(QDateTime::currentDateTime())));
    qApp->exit();
}

//----------------------------------------------------------------------------
// getPat (returns a struct for a patient found in TakeCare)
//
// 2025-01-02 First version
//----------------------------------------------------------------------------
PatStruct Caesar::getPat(QString sPersonNo)
{
    PatStruct st;  // stuff the data here

    st.bFailed       = false; // set an optimistic default
    st.bMessageBox   = false; // default to normal (no message boxes for deceased pat., or similar)
    st.bRedacted     = false; // true for hidden pat.names+addresses
    st.bCityNotFound = false; // hope for the best
    QString sError;

// verify we have a smartcard (otherwise TakeCare will not start)
    QString sTCUserName = TWTCSupport::getTCUserNameFromSmartcard(0);
    if (sTCUserName.isEmpty())
        fubar("Hittar inget e-tjänstekort (behövs för att öppna pat. i TakeCare).");

// try to establish the path to TC
    QString sTCPath = TWTCSupport::getTCPath();
    if (sTCPath.isEmpty())
    // use this as the default path
        sTCPath = "C:\\TakeCare";

// geronimo ...
    sError = TWTCSupport::openTCPersonNo(sTCPath,sTCUserName,sPersonNo);
    if (!sError.isEmpty())
    // something didn't agree with the launching, go south
        fubar("Open TakeCare failed: " + sError);

// prepare for the waiting game (mostly by sleeping)
    QString sPersonNoInTC;
    TWUtils::sleepForABit();
    TWUtils::sleepForABit();

// go into a waiting loop
    for (int i = 0; (i < 77); ++i)
    {
    // TC has loaded the patient?
        sPersonNoInTC = getCurrentTCPersonNoAndActivate();
        if (!sPersonNoInTC.isEmpty())
            break;  // yes, got something

    // if not, maybe a "deceased" (or similar) dialog box is shown?
        auto hWndMessageBox = getTCMessageBox();
        if (hWndMessageBox)
        {
        // set focus to this box so we can send an <Enter> and dismiss it
            TWUtils::makeTopWindow(hWndMessageBox);

        // sleep a bit two times to debounce
            TWUtils::sleepForABit();
            TWUtils::sleepForABit();
            SendKey(VK_RETURN);
            TWUtils::sleepForABit();

        // set the boolean and we're done with this patient
            st.bMessageBox = true;
            return st;
        }

    // else wait a bit more
        TWUtils::sleepForABit();
    }

// check we have a kosher personno/patient, else venture south
    if (sPersonNoInTC.isEmpty())
        fubar("TC didn't return a personno (timeout)");
    sError = TWUtils::checkPersonNo(sPersonNoInTC);
    if (!sError.isEmpty())
        fubar("Bad personno: " + sError);

// got the same personno we wanted?
    if (TWUtils::tossAllButDigits(sPersonNo) != sPersonNoInTC)
        fubar(QString("not the same personno. we searched for ('%1','%2')").arg(sPersonNo,sPersonNoInTC));


// ok we're good, have the patient visible
// type the shortcut ctrl+shift+U to open the pat. properties dialog for him/her
    TWUtils::sleepForABit();
    SendControlShiftKey('U');
    TWUtils::sleepForABit();

// spin a waiting loop
    HWND hWndBox = NULL;
    QStringList slTexts; // stuff the child texts here

    for (int i = 0; (i < 33); ++i)
    {
    // got the box up and running?
        hWndBox = getPatPropertiesBox();
        if (hWndBox)
        {
        // got some texts?
            slTexts = getPropChildrenTexts(hWndBox);

            if (slTexts.count() >= 20)
                break;  // got at least 20 texts, we're good I think
        }

    // else try again with a bit of sleep
        TWUtils::sleepForABit();
    }

// get the texts again to be sure (but first sleep to debounce)
    TWUtils::sleepForABit();
    slTexts    = getPropChildrenTexts(hWndBox);
    st.bFailed = (slTexts.count() < 20);
    if (st.bFailed)
    // fail, return to try again
        return st;

// cargo cult check of one presumably static text (should always be there)
    st.bFailed = ("Aktuell identitet" != slTexts[32]);
    if (st.bFailed)
    // fail, return for another try
        return st;


// with a bit of luck we have the pat properties up and running now
// tab down into the phone nos. Dyalog grid, start with 7 tab keys
    for (int i = 0; (i < 7); ++i)
    {
        SendKey(VK_TAB);
        TWUtils::sleepForABit(44);  // 44 ms should suffice
    }

// try to get the phonenos and tab between them (6 of them in TakeCare)
    QStringList slPhoneNos;
    for (int i = 0; (i < 6); ++i)
    {
        slPhoneNos += getGridPhoneNo(hWndBox);
        SendKey(VK_TAB);
        TWUtils::sleepForABit(44);  // 44 ms should suffice
    }

// as a nice gesture, type an Esc to close the prop box
    SendKey(VK_ESCAPE);


// ---------------------------------------------------------------------
// ok step thru and fill up the struct (hardcode the indeces in slTexts)
    st.sPersonNo = sPersonNo;
    st.dDOB      = TWUtils::fromISODate(slTexts[23]);
    if (!st.dDOB.isValid())
        fubar(QString("Got a bad birthdate '%1'").arg(slTexts[23]));

    st.sFirstNames = slTexts[28];
    st.sLastName   = slTexts[27];
    st.sSex        = ((st.sPersonNo.mid(11,1).toInt() % 2) > 0) ? "M" : "K";
    st.sAddress    = slTexts[26];
    st.sZip        = TWUtils::tossAllButDigits(slTexts[25]);
    st.sCity       = slTexts[24];

// ------ patients we skip over:
// when the name is: "Skyddad Personuppgift" (cannot retrieve the real name or address)
    if (("Skyddad" == st.sFirstNames) && ("Personuppgift" == st.sLastName))
    {
    // set the boolean and we're done with this patient
        st.bRedacted = true;
        return st;
    }

// or the city is: "*"
    if ("*" == st.sCity)
    {
    // set the boolean and we're done with this patient
        st.bRedacted = true;
        return st;
    }

// guessing we have a short city name? replace it with the good/untruncated one from our map
    if (st.sCity.length() <= 13)
    {
    // (note: in 99% or more the short city will be identical to the normal city)
        if (!mShortCities2Cities.contains(st.sCity))
        {
        // city not found, set the boolean and we're done with this patient
            st.bCityNotFound = true;
            return st;
        }

    // replace with the good one (so the imported data into Webdoc will be more correct)
        st.sCity = mShortCities2Cities[st.sCity];
    }

// tru to get the county and municipality codes from our lookup table
    if (!mCities2LLKK.contains(st.sCity))
    {
    // city not found, set the boolean and we're done with this patient
        st.bCityNotFound = true;
        return st;
    }

// get the LLKK data
    st.sLLKK = TWUtils::tossAllButDigits(mCities2LLKK[st.sCity]);
    if (st.sLLKK.length() != 4)
        fubar(QString("LLKK entry not 4 digits").arg(st.sLLKK));


    st.sCareOf = "";    // not yet supported (leave empty)

// phone time, check/edit and split the ones we got from TC into mobile and non-mobile flavors
// (all nondigits are already removed) note: we're sort of only supporting Swedish phone no. right now
    QStringList slMobileNos;
    QStringList slNonMobileNos;
    for (auto s : slPhoneNos)
    {
        if (s.isEmpty())
        // skip over empty ones
            continue;

        if (s.length() < 6)
        // skip over nos. too short
            continue;

        if (s.startsWith("467"))    // got a Swedish country prefix? replace with "07..."
            s = "0" + s.mid(2);

        if (s.length() > 10)
        // toss all digits beyond length 10
            s.truncate(10);

    // assume it's a mobile if it begins with "07.." and is 10 digits
        if (s.startsWith("07") && (10 == s.length()))
             slMobileNos    += s;
        else slNonMobileNos += s;
    }

// deduplication time
    slMobileNos.removeDuplicates();
    slNonMobileNos.removeDuplicates();

// any takers for mobile phone(s)?
    st.nNoOfMobileNos = slMobileNos.count();
    if (slMobileNos.count() > 0)
        st.sMobileNo = slMobileNos[0];  // have at least one number
    if (slMobileNos.count() > 1)
        st.sMobileNo = slMobileNos[1];  // more than one? use the 2nd one
    if (slMobileNos.count() > 2)
        st.sMobileNo = slMobileNos[2];  // more than two? use the 3rd one

// non-mobile phones?
    if (slNonMobileNos.count() > 0)
        st.sHomeNo = slNonMobileNos[0]; // at least one number, use it as the home phone
    if (slNonMobileNos.count() > 1)
        st.sWorkNo = slNonMobileNos[1]; // more than one? use the 2nd one as the work phone

// ok we're done here
    return st;
}

//----------------------------------------------------------------------------
// writePat
//
// 2025-01-05 First version
//----------------------------------------------------------------------------
void Caesar::writePat(QTextStream* tsCSV, PatStruct st)
{
//------------------------
/* from the official docs:
1.*Personnummer:
o Svenskt: ÅÅÅÅMMDD-XXXX**
o Reservnummer: 1-13 tecken (inkl. ev. bindestreck)
2. *Är personnumret ett reservnummer? Ja / Nej
3. *Födelsedatum* - ÅÅÅÅ-MM-DD
4. Förnamn – Max 36 tecken
5. Efternamn - Max 50 tecken
6. *Kön – ”K”/”M”/”X”
7. *Avliden – Ja / Nej
8. Adress - Max 41 tecken
9. Postnummer: XXXXX (siffror)
10. Postort – Max 50 tecken
11. Län - XX (siffror)
12. Kommun - XX (siffror)
13. (Land) – Här kommer vi sätta ’Sverige’ på alla poster som importeras.
14. c/o-adress – Max 255 tecken
15. E-post – Max 50 tecken
16. Mobilnummer – Endast siffror
17. Telefon hem – Endast siffror
18. Telefon arbete – Endast siffror
19. Anteckningar
*/

// lambda for writing one term to the CSV file
    auto writeTerm = [tsCSV] (QString s, int nMaxCount = 255)
    {
    // toss possible occurrences of the 3 bad chars: ; ' " that can wreak havoc on CSV files
        QString sCleaned = s;

        sCleaned.remove(QChar(';'));
        sCleaned.remove(QChar('\''));
        sCleaned.remove(QChar('"'));

    // too long for Webdoc?
        if (sCleaned.length() > nMaxCount)
            sCleaned = TWUtils::truncateWithTrailingEllipses(sCleaned,nMaxCount);

    // ok fire away
        (*tsCSV) << sCleaned << ";";
    };

// one patient: one line with 19 terms
    writeTerm(st.sPersonNo);
    writeTerm("Nej");     // currently we do not support Reservnummer
    writeTerm(TWUtils::toISODate(st.dDOB));
    writeTerm(st.sFirstNames,36);
    writeTerm(st.sLastName,  50);
    writeTerm(st.sSex);
    writeTerm("Nej");     // currently no support for deceased pat.
    writeTerm(st.sAddress,   41);
    writeTerm(st.sZip,        5);
    writeTerm(st.sCity,      50);
    writeTerm(st.sLLKK.mid(0,2)); // LL goes here
    writeTerm(st.sLLKK.mid(2,2)); // KK goes here
    writeTerm("Sverige"); // only country supported right now

    writeTerm("");  // c/o
    writeTerm("");  // email

    writeTerm(st.sMobileNo);    // already checked above for length==10
    writeTerm(st.sHomeNo,20);   // prepare for future/more digits :-)
    writeTerm(st.sWorkNo,20);   //

// notes: skip over, we already have a semicolon from the workphone above
// (this means we're using semicolon as a separator, not a terminator. i.e. 18 of them, not 19)

// that's all for this patient
    (*tsCSV) << "\n";

// a flush cannot hurt
    (*tsCSV).flush();
}

//----------------------------------------------------------------------------
// writeLog
//
// 2025-01-05 First version
//----------------------------------------------------------------------------
void Caesar::writeLog(QTextStream* tsLog, QString sMsg)
{
// write to the log file
    (*tsLog) << sMsg << "\n";

// always flush
    (*tsLog).flush();

// and show the line on the console
    qputs(sMsg);
}
