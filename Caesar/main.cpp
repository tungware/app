/*--------------------------------------------------------------------------*/
/* main.cpp                                                                 */
/*                                                                          */
/* 2025-01-02 First version                                             HS  */
/*--------------------------------------------------------------------------*/

#include "qapplication.h"
#include "caesar.h"

// start here
int main(int argc, char** argv)
{
    QApplication a(argc, argv);

// launch Caesar
    Caesar c;

// spin up an event loop
    return a.exec();
}
