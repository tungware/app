/*--------------------------------------------------------------------------*/
/* katregtermsUppf.h                                                        */
// KatReg Uppf TC terms                                                     */
/*                                                                          */
/* 2024-04-01 First version                                             HS  */
/* 2024-07-29 Add operation date as a katreg field                      HS  */
/*--------------------------------------------------------------------------*/

#pragma once
// pull in TWUtils since we have some fubar() calls in here
#include INCLUDETWUTILS

// because we're processing 2B before 2A, list 2B chaps before 2A
enum class KatRegFields
{ // 2B fields
    eRightOrLeftEye,
    ePostOpChecks,
    eVisusRight,
    eRefractionRightSphere,
    eRefractionRightCylinder,
    eRefractionRightDegrees,
    eVisusLeft,
    eRefractionLeftSphere,
    eRefractionLeftCylinder,
    eRefractionLeftDegrees,
// 2A fields
    eK1Diopters,
    eK1Degrees,
    eK2Diopters,
    eK2Degrees,
    ePlannedRefraction,
    eAxisLength,
    ePreopRefractionRightSphere,
    ePreopRefractionRightCylinder,
    ePreopRefractionRightDegrees,
    ePreopRefractionLeftSphere,
    ePreopRefractionLeftCylinder,
    ePreopRefractionLeftDegrees,
    eLensFormula,
// extra chaps
    eDateOperation
};

// store those enum in a lists for easy traversal (poor man's reflection before C++26)
// obviously any changes above ---> change in these lists as well
const static QList<enum KatRegFields> lKatRegFields
{ // 2B
    KatRegFields::eRightOrLeftEye,
    KatRegFields::ePostOpChecks,
    KatRegFields::eVisusRight,
    KatRegFields::eRefractionRightSphere,
    KatRegFields::eRefractionRightCylinder,
    KatRegFields::eRefractionRightDegrees,
    KatRegFields::eVisusLeft,
    KatRegFields::eRefractionLeftSphere,
    KatRegFields::eRefractionLeftCylinder,
    KatRegFields::eRefractionLeftDegrees,
 // 2A
    KatRegFields::eK1Diopters,
    KatRegFields::eK1Degrees,
    KatRegFields::eK2Diopters,
    KatRegFields::eK2Degrees,
    KatRegFields::ePlannedRefraction,
    KatRegFields::eAxisLength,
    KatRegFields::ePreopRefractionRightSphere,
    KatRegFields::ePreopRefractionRightCylinder,
    KatRegFields::ePreopRefractionRightDegrees,
    KatRegFields::ePreopRefractionLeftSphere,
    KatRegFields::ePreopRefractionLeftCylinder,
    KatRegFields::ePreopRefractionLeftDegrees,
    KatRegFields::eLensFormula,
// extras
     KatRegFields::eDateOperation
};


// Form 2B terms -------------------------------------
// right or left eye
const int nnRightOrLeftEyeKeywordTermID        =  7597;  // CaseNotes_ValueTerms
const int nnRightTermID                        =  1905;
const int nnLeftTermID                         =  2050;
const QChar ccRightEye                         =   'H';
const QChar ccLeftEye                          =   'V';
const QChar ccUnspecifiedEye                   =   ' ';

// post op checks
const int nnPostOpChecksKeywordTermID          = 17680;  // CaseNotes_Measurements
const int nnEfterkontrollPagarValue            =     0;  // to XML as a boolean
const int nnPatientmedverkanOtillrackligValue  =     1;  //       - " -
// "Patienten avliden" not yet implemented as a value, so currently we're not reporting this via XML

// visus (right and left)
const int nnVisusRightKeywordTermID            = 17720;  // CaseNotes_Measurements
const int nnVisusLeftKeywordTermID             = 17721;
const int nnFR4mTermID                         = 17715;  // CaseNotes_ValueTerms
const int nnFR3mTermID                         = 17716;
const int nnFR2mTermID                         = 17717;
const int nnFR1mTermID                         = 17718;
const int nnHRPLPTermID                        = 17719;
const int nnAmaurosisTermID                    = 12713;

// refraction values (right and left)
const int nnRefractionRightContainerTermID     = 17714;  // (named KeywordTermIds in CaseNotes_ContainerNotes and _ContainerMeasurements)
const int nnRefractionLeftContainerTermID      = 17713;  //
const int nnRefractionSphericalKeywordTermID   =  6946;  // CaseNotes_ContainerNotes
const int nnRefractionCylindricalKeywordTermID =  6947;  //        - " -
const int nnRefractionDegreesKeywordTermID     =  6948;  // CaseNotes_ContainerMeasurements

// Form 2A terms -------------------------------------
// right or left eye (reuse the same termids as above)

// preop K1 and K2 chaps (right and left as separate container terms, note: only one is used per katreg entry)
const int nnKeratometryRightContainerTermID    = 17780;  // (named KeywordTermIds in CaseNotes_ContainerMeasurements
const int nnKeratometryLeftContainerTermID     = 17781;  //
const int nnK1DioptersKeywordTermID            = 17707;  // CaseNotes_ContainerMeasurements
const int nnK1DegreesKeywordTermID             = 17706;  //           - " -
const int nnK2DioptersKeywordTermID            = 17709;  //           - " -
const int nnK2DegreesKeywordTermID             = 17708;  //           - " -

// planned refraction
const int nnPlannedRefractionKeywordTermID     = 17712;  // CaseNotes_Measurements

// length of axis (right and left as separate keyword terms, use only one per katreg entry)
const int nnAxisLengthRightKeywordTermID       = 17711;  // CaseNotes_Measurements
const int nnAxisLengthLeftKeywordTermID        = 17777;  //        - " -

// preop refractions (right and left) reuse same terms as above

// lens formula
const int nnLensFormulaKeywordTermID           = 17683;  // CaseNotes_ValueTerms
const int nnSKRTTermID                         = 17691;
const int nnHaigisTermID                       = 17688;
const int nnHofferQTermID                      = 17689;
const int nnHolladyTermID                      = 17690;
const int nnBarrettTermID                      = 17686;
const int nnOtherFormulaTermID                 = 14737;  // reusing same term as "Antibiotika intrakameralt" Keyword 17698

// date of operation
const int nnDateOperationKeywordTermID         =  7023;  // CaseNotes_DateTimes


// -----------------------------------------------------------------------------------------------------------------------------------------------
// a map for retrieving the KatRegField enum based on journal flavor 2A/2B + which eye we're collecting data for + keyword and container term ids
// some keyword termids are right/left-eye specific and thus be used without any right/left eye flavors (yes, those with ccUnspecifiedEye lines)
const int nnNoContainer = 0;
#define MAPENTRY(form2AOr2B, eyeRightOrLeft, keywordTermID, containerTermID) QString("%1%2%3%4").arg(form2AOr2B).arg(eyeRightOrLeft).arg(keywordTermID).arg(containerTermID)

const static QMap<QString,enum KatRegFields> mKeyword2KatRegField =
{
// 2B fields:
    { MAPENTRY("2B", ccUnspecifiedEye ,nnRightOrLeftEyeKeywordTermID        ,nnNoContainer),                     KatRegFields::eRightOrLeftEye               },
    { MAPENTRY("2B", ccRightEye       ,nnRightOrLeftEyeKeywordTermID        ,nnNoContainer),                     KatRegFields::eRightOrLeftEye               },
    { MAPENTRY("2B", ccLeftEye        ,nnRightOrLeftEyeKeywordTermID        ,nnNoContainer),                     KatRegFields::eRightOrLeftEye               },

    // this one requires a specified eye flavor/term (in the same journal)
    { MAPENTRY("2B" ,ccRightEye       ,nnPostOpChecksKeywordTermID          ,nnNoContainer),                     KatRegFields::ePostOpChecks                 },
    { MAPENTRY("2B" ,ccLeftEye        ,nnPostOpChecksKeywordTermID          ,nnNoContainer),                     KatRegFields::ePostOpChecks                 },

    { MAPENTRY("2B" ,ccUnspecifiedEye ,nnVisusRightKeywordTermID            ,nnNoContainer),                     KatRegFields::eVisusRight                   },
    { MAPENTRY("2B" ,ccRightEye       ,nnVisusRightKeywordTermID            ,nnNoContainer),                     KatRegFields::eVisusRight                   },
    { MAPENTRY("2B" ,ccLeftEye        ,nnVisusRightKeywordTermID            ,nnNoContainer),                     KatRegFields::eVisusRight                   },

    { MAPENTRY("2B" ,ccUnspecifiedEye ,nnVisusLeftKeywordTermID             ,nnNoContainer),                     KatRegFields::eVisusLeft                    },
    { MAPENTRY("2B" ,ccRightEye       ,nnVisusLeftKeywordTermID             ,nnNoContainer),                     KatRegFields::eVisusLeft                    },
    { MAPENTRY("2B" ,ccLeftEye        ,nnVisusLeftKeywordTermID             ,nnNoContainer),                     KatRegFields::eVisusLeft                    },

    { MAPENTRY("2B" ,ccUnspecifiedEye ,nnRefractionSphericalKeywordTermID   ,nnRefractionRightContainerTermID),  KatRegFields::eRefractionRightSphere        },
    { MAPENTRY("2B" ,ccRightEye       ,nnRefractionSphericalKeywordTermID   ,nnRefractionRightContainerTermID),  KatRegFields::eRefractionRightSphere        },
    { MAPENTRY("2B" ,ccLeftEye        ,nnRefractionSphericalKeywordTermID   ,nnRefractionRightContainerTermID),  KatRegFields::eRefractionRightSphere        },

    { MAPENTRY("2B" ,ccUnspecifiedEye ,nnRefractionCylindricalKeywordTermID ,nnRefractionRightContainerTermID),  KatRegFields::eRefractionRightCylinder      },
    { MAPENTRY("2B" ,ccRightEye       ,nnRefractionCylindricalKeywordTermID ,nnRefractionRightContainerTermID),  KatRegFields::eRefractionRightCylinder      },
    { MAPENTRY("2B" ,ccLeftEye        ,nnRefractionCylindricalKeywordTermID ,nnRefractionRightContainerTermID),  KatRegFields::eRefractionRightCylinder      },

    { MAPENTRY("2B" ,ccUnspecifiedEye ,nnRefractionDegreesKeywordTermID     ,nnRefractionRightContainerTermID),  KatRegFields::eRefractionRightDegrees       },
    { MAPENTRY("2B" ,ccRightEye       ,nnRefractionDegreesKeywordTermID     ,nnRefractionRightContainerTermID),  KatRegFields::eRefractionRightDegrees       },
    { MAPENTRY("2B" ,ccLeftEye        ,nnRefractionDegreesKeywordTermID     ,nnRefractionRightContainerTermID),  KatRegFields::eRefractionRightDegrees       },

    { MAPENTRY("2B" ,ccUnspecifiedEye ,nnRefractionSphericalKeywordTermID   ,nnRefractionLeftContainerTermID),   KatRegFields::eRefractionLeftSphere         },
    { MAPENTRY("2B" ,ccRightEye       ,nnRefractionSphericalKeywordTermID   ,nnRefractionLeftContainerTermID),   KatRegFields::eRefractionLeftSphere         },
    { MAPENTRY("2B" ,ccLeftEye        ,nnRefractionSphericalKeywordTermID   ,nnRefractionLeftContainerTermID),   KatRegFields::eRefractionLeftSphere         },

    { MAPENTRY("2B" ,ccUnspecifiedEye ,nnRefractionCylindricalKeywordTermID ,nnRefractionLeftContainerTermID),   KatRegFields::eRefractionLeftCylinder       },
    { MAPENTRY("2B" ,ccRightEye       ,nnRefractionCylindricalKeywordTermID ,nnRefractionLeftContainerTermID),   KatRegFields::eRefractionLeftCylinder       },
    { MAPENTRY("2B" ,ccLeftEye        ,nnRefractionCylindricalKeywordTermID ,nnRefractionLeftContainerTermID),   KatRegFields::eRefractionLeftCylinder       },

    { MAPENTRY("2B" ,ccUnspecifiedEye ,nnRefractionDegreesKeywordTermID     ,nnRefractionLeftContainerTermID),   KatRegFields::eRefractionLeftDegrees        },
    { MAPENTRY("2B" ,ccRightEye       ,nnRefractionDegreesKeywordTermID     ,nnRefractionLeftContainerTermID),   KatRegFields::eRefractionLeftDegrees        },
    { MAPENTRY("2B" ,ccLeftEye        ,nnRefractionDegreesKeywordTermID     ,nnRefractionLeftContainerTermID),   KatRegFields::eRefractionLeftDegrees        },

// 2A fields:
    // eye flavor required
    { MAPENTRY("2A" ,ccRightEye       ,nnK1DioptersKeywordTermID            ,nnKeratometryRightContainerTermID), KatRegFields::eK1Diopters                   },
    { MAPENTRY("2A" ,ccLeftEye        ,nnK1DioptersKeywordTermID            ,nnKeratometryLeftContainerTermID),  KatRegFields::eK1Diopters                   },

    // eye flavor required
    { MAPENTRY("2A" ,ccRightEye       ,nnK1DegreesKeywordTermID             ,nnKeratometryRightContainerTermID), KatRegFields::eK1Degrees                    },
    { MAPENTRY("2A" ,ccLeftEye        ,nnK1DegreesKeywordTermID             ,nnKeratometryLeftContainerTermID),  KatRegFields::eK1Degrees                    },

    // eye flavor required
    { MAPENTRY("2A" ,ccRightEye       ,nnK2DioptersKeywordTermID            ,nnKeratometryRightContainerTermID), KatRegFields::eK2Diopters                   },
    { MAPENTRY("2A" ,ccLeftEye        ,nnK2DioptersKeywordTermID            ,nnKeratometryLeftContainerTermID),  KatRegFields::eK2Diopters                   },

    // eye flavor required
    { MAPENTRY("2A" ,ccRightEye       ,nnK2DegreesKeywordTermID             ,nnKeratometryRightContainerTermID), KatRegFields::eK2Degrees                    },
    { MAPENTRY("2A" ,ccLeftEye        ,nnK2DegreesKeywordTermID             ,nnKeratometryLeftContainerTermID),  KatRegFields::eK2Degrees                    },

    // eye flavor required
    { MAPENTRY("2A" ,ccRightEye       ,nnPlannedRefractionKeywordTermID     ,nnNoContainer),                     KatRegFields::ePlannedRefraction            },
    { MAPENTRY("2A" ,ccLeftEye        ,nnPlannedRefractionKeywordTermID     ,nnNoContainer),                     KatRegFields::ePlannedRefraction            },

    // eye flavor required (have right/left term id but eye flavor is needed anyway, since there's only one axis length field in the 2B form)
    { MAPENTRY("2A" ,ccRightEye       ,nnAxisLengthRightKeywordTermID       ,nnNoContainer),                     KatRegFields::eAxisLength                   },
    { MAPENTRY("2A" ,ccLeftEye        ,nnAxisLengthLeftKeywordTermID        ,nnNoContainer),                     KatRegFields::eAxisLength                   },

    // these are ok without eye flavor
    { MAPENTRY("2A" ,ccUnspecifiedEye ,nnRefractionSphericalKeywordTermID   ,nnRefractionRightContainerTermID),  KatRegFields::ePreopRefractionRightSphere   },
    { MAPENTRY("2A" ,ccRightEye       ,nnRefractionSphericalKeywordTermID   ,nnRefractionRightContainerTermID),  KatRegFields::ePreopRefractionRightSphere   },
    { MAPENTRY("2A" ,ccLeftEye        ,nnRefractionSphericalKeywordTermID   ,nnRefractionRightContainerTermID),  KatRegFields::ePreopRefractionRightSphere   },

    { MAPENTRY("2A" ,ccUnspecifiedEye ,nnRefractionCylindricalKeywordTermID ,nnRefractionRightContainerTermID),  KatRegFields::ePreopRefractionRightCylinder },
    { MAPENTRY("2A" ,ccRightEye       ,nnRefractionCylindricalKeywordTermID ,nnRefractionRightContainerTermID),  KatRegFields::ePreopRefractionRightCylinder },
    { MAPENTRY("2A" ,ccLeftEye        ,nnRefractionCylindricalKeywordTermID ,nnRefractionRightContainerTermID),  KatRegFields::ePreopRefractionRightCylinder },

    { MAPENTRY("2A" ,ccUnspecifiedEye ,nnRefractionDegreesKeywordTermID     ,nnRefractionRightContainerTermID),  KatRegFields::ePreopRefractionRightDegrees  },
    { MAPENTRY("2A" ,ccRightEye       ,nnRefractionDegreesKeywordTermID     ,nnRefractionRightContainerTermID),  KatRegFields::ePreopRefractionRightDegrees  },
    { MAPENTRY("2A" ,ccLeftEye        ,nnRefractionDegreesKeywordTermID     ,nnRefractionRightContainerTermID),  KatRegFields::ePreopRefractionRightDegrees  },

    { MAPENTRY("2A" ,ccUnspecifiedEye ,nnRefractionSphericalKeywordTermID   ,nnRefractionLeftContainerTermID),   KatRegFields::ePreopRefractionLeftSphere    },
    { MAPENTRY("2A" ,ccRightEye       ,nnRefractionSphericalKeywordTermID   ,nnRefractionLeftContainerTermID),   KatRegFields::ePreopRefractionLeftSphere    },
    { MAPENTRY("2A" ,ccLeftEye        ,nnRefractionSphericalKeywordTermID   ,nnRefractionLeftContainerTermID),   KatRegFields::ePreopRefractionLeftSphere    },

    { MAPENTRY("2A" ,ccUnspecifiedEye ,nnRefractionCylindricalKeywordTermID ,nnRefractionLeftContainerTermID),   KatRegFields::ePreopRefractionLeftCylinder  },
    { MAPENTRY("2A" ,ccRightEye       ,nnRefractionCylindricalKeywordTermID ,nnRefractionLeftContainerTermID),   KatRegFields::ePreopRefractionLeftCylinder  },
    { MAPENTRY("2A" ,ccLeftEye        ,nnRefractionCylindricalKeywordTermID ,nnRefractionLeftContainerTermID),   KatRegFields::ePreopRefractionLeftCylinder  },

    { MAPENTRY("2A" ,ccUnspecifiedEye ,nnRefractionDegreesKeywordTermID     ,nnRefractionLeftContainerTermID),   KatRegFields::ePreopRefractionLeftDegrees   },
    { MAPENTRY("2A" ,ccRightEye       ,nnRefractionDegreesKeywordTermID     ,nnRefractionLeftContainerTermID),   KatRegFields::ePreopRefractionLeftDegrees   },
    { MAPENTRY("2A" ,ccLeftEye        ,nnRefractionDegreesKeywordTermID     ,nnRefractionLeftContainerTermID),   KatRegFields::ePreopRefractionLeftDegrees   },

    // eye flavor required
    { MAPENTRY("2A" ,ccRightEye       ,nnLensFormulaKeywordTermID           ,nnNoContainer),                     KatRegFields::eLensFormula                  },
    { MAPENTRY("2A" ,ccLeftEye        ,nnLensFormulaKeywordTermID           ,nnNoContainer),                     KatRegFields::eLensFormula                  },

    // eye flavor required
    // note: this assumes date of operation is seen omly in 2A-flavored journals, i.e. never in 2B ones
    { MAPENTRY("2A" ,ccRightEye       ,nnDateOperationKeywordTermID         ,nnNoContainer),                     KatRegFields::eDateOperation                },
    { MAPENTRY("2A" ,ccLeftEye        ,nnDateOperationKeywordTermID         ,nnNoContainer),                     KatRegFields::eDateOperation                }
};

// === use these handy functions to look up and retrieve katreg field enums =====================================================================================
static bool hasKatRegField(bool bIs2B, QChar cEye, QString sTermIDs) // sTermIds: keywordTermID and ContainerTermID (or nnNoContainter) concatenated (as strings)
{
// build key string and try to find it
    return mKeyword2KatRegField.contains(QString("%1%2%3").arg(bIs2B ? "2B" : "2A").arg(cEye).arg(sTermIDs));
}

// eye flavor-less lookup
static bool hasKatRegField(bool bIs2B, QString sTermIDs)
{
    return hasKatRegField(bIs2B, ccUnspecifiedEye, sTermIDs);
}

static KatRegFields getKatRegField(bool bIs2B, QChar cEye, QString sTermIDs) // sTermIds: keywordTermID and ContainerTermID (or nnNoContainter) concatenated
{
// build key string
    QString sKey = QString("%1%2%3").arg(bIs2B ? "2B" : "2A").arg(cEye).arg(sTermIDs);

    if (!mKeyword2KatRegField.contains(sKey))
        fubar(QString("Nice try but '%1' was not found in the map").arg(sKey));

// ok return it
    return mKeyword2KatRegField[sKey];
}

// and the flavorless version
static KatRegFields getKatRegField(bool bIs2B, QString sTermIDs)
{
    return getKatRegField(bIs2B, ccUnspecifiedEye, sTermIDs);
}


// returns true for those KatReg fields that are only applicable to the right eye
static bool isForRightEyeOnly(KatRegFields f)
{
    return ((KatRegFields::eVisusRight              == f) ||
            (KatRegFields::eRefractionRightSphere   == f) ||
            (KatRegFields::eRefractionRightCylinder == f) ||
            (KatRegFields::eRefractionRightDegrees  == f) ||
            (KatRegFields::ePreopRefractionRightSphere   == f) ||
            (KatRegFields::ePreopRefractionRightCylinder == f) ||
            (KatRegFields::ePreopRefractionRightDegrees  == f));
}

// returns true for those KatReg fields that are only applicable to the left eyea
static bool isForLeftEyeOnly(KatRegFields f)
{
    return ((KatRegFields::eVisusLeft              == f) ||
            (KatRegFields::eRefractionLeftSphere   == f) ||
            (KatRegFields::eRefractionLeftCylinder == f) ||
            (KatRegFields::eRefractionLeftDegrees  == f) ||
            (KatRegFields::ePreopRefractionLeftSphere   == f) ||
            (KatRegFields::ePreopRefractionLeftCylinder == f) ||
            (KatRegFields::ePreopRefractionLeftDegrees  == f));
}


// =============================================================================================================
// min/max: specify which katreg fields are term sets or value strings and how many values can/should they hold
using  MinMaxPair = std::pair<int,int>; // std::pair for spec of # of min. params and # of max. params
struct TermSetOrValuePairsStruct
{
    MinMaxPair pTermSetSpec; // min/max # of terms expected in the QSet in mTermSets
    MinMaxPair pValueSpec;   // min/max # of values expected in the QStringList in mValues
};

// serve up some popular pairs:
const static MinMaxPair pNone      (0,0);
const static MinMaxPair pZeroOrOne (0,1);
const static MinMaxPair pExactlyOne(1,1);

const static QMap<enum KatRegFields,TermSetOrValuePairsStruct> mKatRegFieldParamSpecs =
{
// for the katreg fields, specify two pairs of min. and max. no. of params
// first one is the spec. for term sets, second pair is the spec. for values
// 2B:
    { KatRegFields::eRightOrLeftEye,              {  { 1, 2 },    pNone       }  }, // 'H' or 'V' or both (allowed in a single journal)
    { KatRegFields::ePostOpChecks,                {  pNone,       pZeroOrOne  }  }, // an optional value (only one supported anyway)
    { KatRegFields::eVisusRight,                  {  pZeroOrOne,  pZeroOrOne  }  }, // either a value or a term (or neither)
    { KatRegFields::eRefractionRightSphere,       {  pNone,       pZeroOrOne  }  }, // refraction and visus chaps are optional
    { KatRegFields::eRefractionRightCylinder,     {  pNone,       pZeroOrOne  }  }, //
    { KatRegFields::eRefractionRightDegrees,      {  pNone,       pZeroOrOne  }  }, //
    { KatRegFields::eVisusLeft,                   {  pZeroOrOne,  pZeroOrOne  }  }, // either a value or a term (or neither)
    { KatRegFields::eRefractionLeftSphere,        {  pNone,       pZeroOrOne  }  }, //
    { KatRegFields::eRefractionLeftCylinder,      {  pNone,       pZeroOrOne  }  }, //
    { KatRegFields::eRefractionLeftDegrees,       {  pNone,       pZeroOrOne  }  }, //
// 2A:
    { KatRegFields::eK1Diopters,                  {  pNone,       pExactlyOne }  }, //
    { KatRegFields::eK1Degrees,                   {  pNone,       pExactlyOne }  }, //
    { KatRegFields::eK2Diopters,                  {  pNone,       pExactlyOne }  }, //
    { KatRegFields::eK2Degrees,                   {  pNone,       pExactlyOne }  }, //
    { KatRegFields::ePlannedRefraction,           {  pNone,       pExactlyOne }  }, //
    { KatRegFields::eAxisLength,                  {  pNone,       pExactlyOne }  }, //
    { KatRegFields::ePreopRefractionRightSphere,  {  pNone,       pZeroOrOne  }  }, //
    { KatRegFields::ePreopRefractionRightCylinder,{  pNone,       pZeroOrOne  }  }, //
    { KatRegFields::ePreopRefractionRightDegrees, {  pNone,       pZeroOrOne  }  }, //
    { KatRegFields::ePreopRefractionLeftSphere,   {  pNone,       pZeroOrOne  }  }, //
    { KatRegFields::ePreopRefractionLeftCylinder, {  pNone,       pZeroOrOne  }  }, //
    { KatRegFields::ePreopRefractionLeftDegrees,  {  pNone,       pZeroOrOne  }  }, //
    { KatRegFields::eLensFormula,                 {  { 1, 6 },    pNone       }  }, // one or more checkboxes (at least one)

    { KatRegFields::eDateOperation,               {  pNone,       pExactlyOne }  }  //
};


// --- UI texts ---------------------------------------------------------------------
// field prompts/titles
const static QMap<enum KatRegFields,QString> mKatRegFieldCaptions =
{
// 2B:
    { KatRegFields::eRightOrLeftEye,               "Öga"                                      },
    { KatRegFields::ePostOpChecks,                 "Efterkontroller etc."                     },
    { KatRegFields::eVisusRight,                   "Höger öga Visus"                          },
    { KatRegFields::eRefractionRightSphere,        "Höger öga med refraktion Sfäriskt"        },
    { KatRegFields::eRefractionRightCylinder,      "Höger öga med refraktion Cylinder"        },
    { KatRegFields::eRefractionRightDegrees,       "Höger öga med refraktion Grader"          },
    { KatRegFields::eVisusLeft,                    "Vänster öga Visus"                        },
    { KatRegFields::eRefractionLeftSphere,         "Vänster öga med refraktion Sfäriskt"      },
    { KatRegFields::eRefractionLeftCylinder,       "Vänster öga med refraktion Cylinder"      },
    { KatRegFields::eRefractionLeftDegrees,        "Vänster öga med refraktion Grader"        },
// 2A:
    { KatRegFields::eK1Diopters,                   "Preoperativa K1 dioptrier"                },
    { KatRegFields::eK1Degrees,                    "Preoperativa K1 grader"                   },
    { KatRegFields::eK2Diopters,                   "Preoperativa K2 dioptrier"                },
    { KatRegFields::eK2Degrees,                    "Preoperativa K2 grader"                   },
    { KatRegFields::ePlannedRefraction,            "Planerad refraktion"                      },
    { KatRegFields::eAxisLength,                   "Axellängd i mm."                          },
    { KatRegFields::ePreopRefractionRightSphere,   "Refraktion höger preoperativt Sfäriskt"   },
    { KatRegFields::ePreopRefractionRightCylinder, "Refraktion höger preoperativt Cylinder"   },
    { KatRegFields::ePreopRefractionRightDegrees,  "Refraktion höger preoperativt Grader"     },
    { KatRegFields::ePreopRefractionLeftSphere,    "Refraktion vänster preoperativt Sfäriskt" },
    { KatRegFields::ePreopRefractionLeftCylinder,  "Refraktion vänster preoperativt Cylinder" },
    { KatRegFields::ePreopRefractionLeftDegrees,   "Refraktion vänster preoperativt Grader"   },
    { KatRegFields::eLensFormula,                  "Linsformel"                               },

    { KatRegFields::eDateOperation,                "Operationsdatum"                          }
};

// captions for the terms that belongs in a set
const static QMap<int,QString> mTermSetCaptions =
{
    { nnRightTermID,        "Höger"    },
    { nnLeftTermID,         "Vänster"  },

    { nnSKRTTermID,         "SKR/T"    },
    { nnHaigisTermID,       "Haigis"   },
    { nnHofferQTermID,      "Hoffer Q" },
    { nnHolladyTermID,      "Holladay" },
    { nnBarrettTermID,      "Barrett"  },
    { nnOtherFormulaTermID, "Annat"    }
};


// XML stuff ----------------------------------------------------------------
const static QString XMLDate2B = "DatumFörSlutkontroll";

// map of KatReg field enums  ---> XML element names
const static QMap<enum KatRegFields,QString> mKatRegFieldXMLNames =
{
// 2B:
    { KatRegFields::eRightOrLeftEye,               "Öga"                                                                  },
    { KatRegFields::ePostOpChecks,                 "EfterkontrollerPågår/PatientensMedverkanOtillräklig/PatientenAvliden" },
    { KatRegFields::eVisusRight,                   "VA"                                                             },
    { KatRegFields::eRefractionRightSphere,        "Sfärisk"                                                              },
    { KatRegFields::eRefractionRightCylinder,      "Cylinder"                                                             },
    { KatRegFields::eRefractionRightDegrees,       "Grader"                                                               },
    { KatRegFields::eVisusLeft,                    "VA"                                                             },
    { KatRegFields::eRefractionLeftSphere,         "Sfärisk"                                                              },
    { KatRegFields::eRefractionLeftCylinder,       "Cylinder"                                                             },
    { KatRegFields::eRefractionLeftDegrees,        "Grader"                                                               },
// 2A:
    { KatRegFields::eK1Diopters,                   "PreoperativaK1"                                                       },
    { KatRegFields::eK1Degrees,                    "PreoperativaK1Grader"                                                 },
    { KatRegFields::eK2Diopters,                   "PreoperativaK2"                                                       },
    { KatRegFields::eK2Degrees,                    "PreoperativaK2Grader"                                                 },
    { KatRegFields::ePlannedRefraction,            "PlaneradRefraktion"                                                   },
    { KatRegFields::eAxisLength,                   "Axellängd"                                                            },
    { KatRegFields::ePreopRefractionRightSphere,   "Sfärisk"                                                              },
    { KatRegFields::ePreopRefractionRightCylinder, "Cylinder"                                                             },
    { KatRegFields::ePreopRefractionRightDegrees,  "Grader"                                                               },
    { KatRegFields::ePreopRefractionLeftSphere,    "Sfärisk"                                                              },
    { KatRegFields::ePreopRefractionLeftCylinder,  "Cylinder"                                                             },
    { KatRegFields::ePreopRefractionLeftDegrees,   "Grader"                                                               },
    { KatRegFields::eLensFormula,                  "Linsformel"                                                           },
// op. date not used in the XML file
    { KatRegFields::eDateOperation,                ""                                                                     }
};

const static QMap<int,QString> mKatRegTermXMLNames
{
    { nnSKRTTermID,         "SKRT"     },
    { nnHaigisTermID,       "Haigis"   },
    { nnHofferQTermID,      "HofferQ"  },
    { nnHolladyTermID,      "Holladay" },
    { nnBarrettTermID,      "Barrett"  },
    { nnOtherFormulaTermID, "Annat"    },
};

// --- that's it for Uppf
