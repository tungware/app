/*--------------------------------------------------------------------------*/
/* showbox.cpp                                                              */
/*                                                                          */
/* 2024-08-02 First version                                             HS  */
/*--------------------------------------------------------------------------*/

#include "showbox.h"
#include "qcheckbox.h"
#include "qdebug.h"

// use a ctor where you pass along the specific KatRegEntry
ShowBox::ShowBox(QWidget *parent, KatRegEntryStruct st) : QDialog(parent), ui(new Ui::ShowBox)
{
    ui->setupUi(this);

// preamble you know the drill
    TWUtils::helloQt(this);

// require the 2B visit date to be set (personno. is optional)
    if (!st.dVisit.isValid())
        guruMeditation("expected a valid 2B visit date");

// have something for the window title?
    QString sTitle = "Uppföljning";
    if (!st.sPersonNo.isEmpty())
        sTitle += " " + st.sPersonNo;
    if (ccRightEye == st.cEye)
         sTitle += " höger";
    if (ccLeftEye  == st.cEye)
         sTitle += " vänster";

    sTitle += "  " + st.sVisitDateTime;  // assuming this is valid

// if we have a date of operation, show it as well
    if (!st.mAllCooked[KatRegFields::eDateOperation].sValue.isEmpty())
        sTitle += "  (op.dag " + st.mAllCooked[KatRegFields::eDateOperation].sValue + ")";

    setWindowTitle(sTitle);

// also set the title label
    ui->labelTitle->setText(sTitle);

// calculate the row height we'll use for creating QTextEdits from the one that exists already
    int nRowHeight = ui->textEditEye->height();

// --- prepare for the show with some handy lambdas
    auto setTextOrError = [st] (QTextEdit* pte, KatRegFields e, QString sPrefix = "")
    {
        if (st.mAllCooked[e].sError.isEmpty())
        {
        // no error text, so show the normal cooked value(s) with an optional prefix
            QString sText = sPrefix + st.mAllCooked[e].sValue;
            pte->setText(sText);
            pte->setToolTip(sText);  // why not. can't hurt
        }
        else
        {
        // show the error message in red
            auto palette = pte->palette();
            palette.setColor(QPalette::Text,"red"); // normal "red" as a nice foreground color
            pte->setPalette(palette);

        // get the error text and elide it if necessary (to fit inside a single line for the QTextEdit)
            QString sError = st.mAllCooked[e].sError;

            int nAvailableWidth = pte->geometry().width();
            int nExtraMargin = 24;   // give some headroom the the right (since we're always left aligned)
            if (nAvailableWidth > (nExtraMargin * 2)) // unless this is a very *narrow* textedit?
                nAvailableWidth -= nExtraMargin;

            pte->setText(QFontMetrics(pte->font()).elidedText(sError,Qt::ElideMiddle,nAvailableWidth));
            pte->setToolTip(sError); // set the full error text as a tooltip (can be helpful)
        }

    // for both normal texts and errors, align them to the left
        pte->setAlignment(Qt::AlignLeft);

    // currently we don't care about warnings (don't have any yet)
    };

    auto newTextEdit = [this,nRowHeight] (QLabel* l, int nRightSlider = 0)
    {
        QString ssLabelPrefix = "label";

    // wire up a new QTextEdit from thin air
        auto pte = new QTextEdit(this);

    // require the label's object name to have a #Row(s) suffix
    // currently we support "..1Row" up to "..9Rows"
        auto sObjectName = l->objectName();
        if (!sObjectName.endsWith("1Row") && !sObjectName.endsWith("Rows"))
            fubar("Bad object name for label");

        bool bGotDigit = false;
        int nRows = TWUtils::tossAllButDigits(sObjectName).toInt(&bGotDigit);
        if (!bGotDigit)
            fubar("Label object name didn't contain # of rows for the TextEdit");
        if ((nRows < 1) || (nRows > 9))
            fubar("# of rows must be a number 1 .. 9");

    // do the geometry fiddling (with an optional right slide)
        auto r = l->geometry();
        r.adjust(nRightSlider,nRowHeight,nRightSlider,nRowHeight * nRows);
        pte->setGeometry(r);

    // some font fiddling (reuse the same font as the label but set it to Bold for a nice effect)
        auto f = l->font();
        f.setBold(true);
        pte->setFont(f);

    // conclude with some custom (boilerplate) stuff
        pte->setFrameShape(QFrame::NoFrame);
        pte->setFrameShadow(QFrame::Plain);
        pte->setMidLineWidth(0);
        pte->setReadOnly(true);
        pte->setPlaceholderText(QString());

    // ok, light it up and return it
        pte->show();
        return pte;
    };

// --- show time -----------------------------------------------------------------
//  start with eye flavor (if we have one yet)
    if (ccRightEye == st.cEye)
        ui->textEditEye->setText("Höger öga");
    if (ccLeftEye  == st.cEye)
        ui->textEditEye->setText("Vänster öga");

    setTextOrError(newTextEdit(ui->labelKOneDiopters1Row           ),KatRegFields::eK1Diopters                 );
    setTextOrError(newTextEdit(ui->labelKOneDegrees1Row            ),KatRegFields::eK1Degrees                  );
    setTextOrError(newTextEdit(ui->labelKTwoDiopters1Row           ),KatRegFields::eK2Diopters                 );
    setTextOrError(newTextEdit(ui->labelKTwoDegrees1Row            ),KatRegFields::eK2Degrees                  );

    setTextOrError(newTextEdit(ui->labelPlannedRefraction1Row      ),KatRegFields::ePlannedRefraction          );
    setTextOrError(newTextEdit(ui->labelAxisLength1Row             ),KatRegFields::eAxisLength                 );
    setTextOrError(newTextEdit(ui->labelLensFormula2Rows           ),KatRegFields::eLensFormula                );

// more props needed for the refraction stuff
    int nXSlide = 160;                                               // slide this much
    QString ssNoMeasurement = "Ej mätbart";

// 2A refractions right
    int nX   = ui->labelPreopRefractionRight1Row->geometry().x();    // need a starting x for the "slider"
    auto pte = newTextEdit(ui->labelPreopRefractionRight1Row,0);     // create an empty textedit/placeholder
    nX      += nXSlide;

    if (st.mAllCooked[KatRegFields::ePreopRefractionRightSphere  ].sValue.isEmpty() ||
        st.mAllCooked[KatRegFields::ePreopRefractionRightCylinder].sValue.isEmpty() ||
        st.mAllCooked[KatRegFields::ePreopRefractionRightDegrees ].sValue.isEmpty())
    // if one or more field is empty, light up the text "Ej mätbar"
        pte->setText(ssNoMeasurement);

    setTextOrError(newTextEdit(ui->labelPreopRefractionRight1Row,nX),KatRegFields::ePreopRefractionRightSphere  ,"sph: ");
    nX += nXSlide;
    setTextOrError(newTextEdit(ui->labelPreopRefractionRight1Row,nX),KatRegFields::ePreopRefractionRightCylinder,"cyl: ");
    nX += nXSlide;
    setTextOrError(newTextEdit(ui->labelPreopRefractionRight1Row,nX),KatRegFields::ePreopRefractionRightDegrees ,"grader: ");


// 2A refractions left
    nX  = ui->labelPreopRefractionLeft1Row->geometry().x();   // need a starting x for the "slider"
    pte = newTextEdit(ui->labelPreopRefractionLeft1Row,0);    // create an empty textedit/placeholder
    nX += nXSlide;

    if (st.mAllCooked[KatRegFields::ePreopRefractionLeftSphere  ].sValue.isEmpty() ||
        st.mAllCooked[KatRegFields::ePreopRefractionLeftCylinder].sValue.isEmpty() ||
        st.mAllCooked[KatRegFields::ePreopRefractionLeftDegrees ].sValue.isEmpty())
    // if one or more field is empty, light up the text "Ej mätbar"
        pte->setText(ssNoMeasurement);

    setTextOrError(newTextEdit(ui->labelPreopRefractionLeft1Row,nX),KatRegFields::ePreopRefractionLeftSphere  ,"sph: ");
    nX += nXSlide;
    setTextOrError(newTextEdit(ui->labelPreopRefractionLeft1Row,nX),KatRegFields::ePreopRefractionLeftCylinder,"cyl: ");
    nX += nXSlide;
    setTextOrError(newTextEdit(ui->labelPreopRefractionLeft1Row,nX),KatRegFields::ePreopRefractionLeftDegrees ,"grader: ");

// 2B visit date
    auto pte2BDate = newTextEdit(ui->labelVisitDate1Row);
    pte2BDate->setText(st.sVisitDateTime);
    pte2BDate->setAlignment(Qt::AlignLeft);

// unusual but can happen, do we have any postop checks active? (can only be one)
    if (!st.mAllCooked[KatRegFields::ePostOpChecks].sValue.isEmpty())
    {
        const int nnPostOpX = 660; // maybe later: avoid hardwired values

    // show it as a label to the left of 2B visit date
        auto pPostOp = new QLabel(this);
        pPostOp->setText(st.mAllCooked[KatRegFields::ePostOpChecks].sValue);

        auto r = ui->labelVisitDate1Row->geometry();
        r.setLeft(nnPostOpX);
        pPostOp->setGeometry(r);

        auto f = ui->labelVisitDate1Row->font();
        f.setBold(true);
        pPostOp->setFont(f);

        pPostOp->setFrameShape(QFrame::NoFrame);
        pPostOp->setFrameShadow(QFrame::Plain);
        pPostOp->setMidLineWidth(0);

    // ok, light up the label
        pPostOp->show();
    }

// 2B refractions right (with visus)
    nX  = ui->labelVisusAndRefractionRight1Row->geometry().x();    // need a starting x for the "slider"
    pte = newTextEdit(ui->labelVisusAndRefractionRight1Row,0);     // always create an empty textedit/placeholder
    nX += nXSlide;

    if (st.mAllCooked[KatRegFields::eVisusRight             ].sValue.isEmpty() ||
        st.mAllCooked[KatRegFields::eRefractionRightSphere  ].sValue.isEmpty() ||
        st.mAllCooked[KatRegFields::eRefractionRightCylinder].sValue.isEmpty() ||
        st.mAllCooked[KatRegFields::eRefractionRightDegrees ].sValue.isEmpty())
    // if one or more field is empty, light up the text "Ej mätbar"
        pte->setText(ssNoMeasurement);

    setTextOrError(newTextEdit(ui->labelVisusAndRefractionRight1Row,nX),KatRegFields::eVisusRight             ,"VA: ");
    nX += nXSlide;
    setTextOrError(newTextEdit(ui->labelVisusAndRefractionRight1Row,nX),KatRegFields::eRefractionRightSphere  ,"sph: ");
    nX += nXSlide;
    setTextOrError(newTextEdit(ui->labelVisusAndRefractionRight1Row,nX),KatRegFields::eRefractionRightCylinder,"cyl: ");
    nX += nXSlide;
    setTextOrError(newTextEdit(ui->labelVisusAndRefractionRight1Row,nX),KatRegFields::eRefractionRightDegrees ,"grader: ");

// 2B refractions left (with visus)
    nX  = ui->labelVisusAndRefractionLeft1Row->geometry().x();    // need a starting x for the "slider"
    pte = newTextEdit(ui->labelVisusAndRefractionLeft1Row,0);     // always create an empty textedit/placeholder
    nX += nXSlide;

    if (st.mAllCooked[KatRegFields::eVisusLeft             ].sValue.isEmpty() ||
        st.mAllCooked[KatRegFields::eRefractionLeftSphere  ].sValue.isEmpty() ||
        st.mAllCooked[KatRegFields::eRefractionLeftCylinder].sValue.isEmpty() ||
        st.mAllCooked[KatRegFields::eRefractionLeftDegrees ].sValue.isEmpty())
    // if one or more field is empty, light up the text "Ej mätbar"
        pte->setText(ssNoMeasurement);

    setTextOrError(newTextEdit(ui->labelVisusAndRefractionLeft1Row,nX),KatRegFields::eVisusLeft             ,"VA: ");
    nX += nXSlide;
    setTextOrError(newTextEdit(ui->labelVisusAndRefractionLeft1Row,nX),KatRegFields::eRefractionLeftSphere  ,"sph: ");
    nX += nXSlide;
    setTextOrError(newTextEdit(ui->labelVisusAndRefractionLeft1Row,nX),KatRegFields::eRefractionLeftCylinder,"cyl: ");
    nX += nXSlide;
    setTextOrError(newTextEdit(ui->labelVisusAndRefractionLeft1Row,nX),KatRegFields::eRefractionLeftDegrees ,"grader: ");

// that's all of them, thank you
}

//----------------------------------------------------------------------------
// on_pushButtonOk_clicked
//
// 2024-02-04 First version
//----------------------------------------------------------------------------
void ShowBox::on_pushButtonOk_clicked()
{
// simply goodbye
    QDialog::accept();
}
