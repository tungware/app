#----------------------------------------------------------------------------------#
# TC2KatRegUppf.pro                                                                #
#                                                                                  #
# 2024-03-01 First version                                                     HS  #
#----------------------------------------------------------------------------------#

QT      += core gui widgets sql network

TARGET   = TC2KatRegUppf
TEMPLATE = app

HEADERS += katregtermsUppf.h mainwindow.h showbox.h settingsdialog.h
SOURCES += main.cpp mainwindow.cpp showbox.cpp settingsdialog.cpp
FORMS   += mainwindow.ui showbox.ui settingsdialog.ui

# version and buildno
VERSION  = 1.5.0.1

# customer name and description
include (NameAndDescription.pri)

# keep CareUnits.Name for nice retrieval in the About box (note: Windows-1252 not UTF8)
DEFINES += "BUILTFOR=\"$$QMAKE_TARGET_DESCRIPTION\""

# set ourselves as the company and copyright
QMAKE_TARGET_COMPANY=Tungware
QMAKE_TARGET_COPYRIGHT=Tungware

# resources stuff
RESOURCES = TC2KatRegUppf.qrc

# include our common QtProjects helper
include (../../include/QtProjects.pri)

# say hello to TWUtils, TWDBLegacy and TWTCSupport
LIBS     += $$TWLibLine(TWUtils)
LIBS     += $$TWLibLine(TWDB)
LIBS     += $$TWLibLine(TWTCSupport)

# and the #defines for the #include statements
DEFINES  += $$TWDefineInclude(TWUtils)
DEFINES  += $$TWDefineInclude(TWDB)
DEFINES  += $$TWDefineInclude(TWTCSupport)
