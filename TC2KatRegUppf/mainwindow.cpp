/*--------------------------------------------------------------------------*/
/* mainwindow.cpp                                                           */
/*                                                                          */
/* 2024-04-01 First version                                             HS  */
/* 2024-06-02 Toss kombika filtering in PAS lookup                      HS  */
/* 2024-07-10 Check that we have a date of operation in March same year HS  */
/*--------------------------------------------------------------------------*/

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "qcommandlineparser.h"
#include "qtimer.h"
#include "qevent.h"
#include "qpropertyanimation.h"
#include "qparallelanimationgroup.h"

// ShowBox and the Settings dialog
#include "showbox.h"
#include "settingsdialog.h"

// for the XML file saving
#include "qfile.h"
#include "qxmlstream.h"

//----------------------------------------------------------------------------
// MainWindow ctor
MainWindow::MainWindow(QWidget* parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);

// usual preamble (will change our current directory to where the .exe file is)
    TWUtils::helloQt(this);
    setWindowTitle(QString("Välkommen till TC2KatRegUppf (version %1)").arg(TWUtils::versionMajorMinor()));

// read in our RO settings, check major+minor version and customername with the one in the .pro file
    roSettings.setCurrentSection("Update");
#if defined(Q_OS_WIN)
// if we're on Windows, check major+minor version
    QString sBuildVersion   = TWUtils::versionMajorMinor();
    QString sMinimumVersion = roSettings.readString("MinimumVersion");
    if (sMinimumVersion > sBuildVersion)
        fubar(QString("Build %1 is lower than the required minimum %2.").arg(sBuildVersion,sMinimumVersion));
#endif

// read in lots of stuff from the general/first section of ROSettings
    roSettings.setCurrentSection("General");
    sCustomerName     = roSettings.readString("CustomerName");
    QString sExpected = QT_STRINGIFY(CUSTOMERNAME);
    if (sExpected != sCustomerName)
        fubar(QString("expected customer name: '%1' but got '%2' from the .ini file").arg(sExpected,sCustomerName));

    nCustomerNo      = roSettings.readInt("CustomerNo");
    bAutoStart       = roSettings.readBool("AutoStart");
    bBatchMode       = false;     // placeholder, we'll set it later in this function

    nCompanyID       = roSettings.readInt("CompanyID");
    nCompanyCode     = roSettings.readInt("CompanyCode");
    nCareUnitID      = roSettings.readInt("CareUnitID");
    sHSAID           = roSettings.readString("HSAID");

// (read in [DefaultPeriod] later)
// check if we have some r/w settings in AppData
// the default date range is stored in [General] and is shared ok (if more than one TC2KatReg user runs on the same desktop)
// the db server and table prefix is stored in [Intelligence%CustomerNo%] to keep possible multiple instances apart
    sIntelligenceSectionName = "Intelligence";                        // default: no R/W settings seen
    eIniFlavorForDB          = TWAppSettings::IniLocation::eEmbedded; //       -  "  -

// have something in the R/W ini file?
    QString sRWIntelligenceSectionName = sIntelligenceSectionName + QString::number(nCustomerNo);
    appDataSettings.setCurrentSection(sRWIntelligenceSectionName);
    if (appDataSettings.readIntWithDefault("UseServer",0) > 0)  // use this one as a canary (should not be 0)
    {
    // got something, so we will to use AppData settings for the db settings
        eIniFlavorForDB          = TWAppSettings::IniLocation::eAppData;
        sIntelligenceSectionName = sRWIntelligenceSectionName;  // and we will use the R/W section name
    }

// Samba time
    roSettings.setCurrentSection("Samba");
    sSambaShareName = roSettings.readString("ShareName");
    sLocalDirectory = "";
    if (roSettings.readBoolWithDefault("UseLocalDirectory",false))
    // used as a fallback when we cannot connect directly to the Samba server
        sLocalDirectory = roSettings.readStringOSSuffix("LocalDirectory");

// KatReg static stuff
    roSettings.setCurrentSection("KatReg");
    sClinicNo      = roSettings.readString("ClinicNo");
    sClinicName    = roSettings.readString("ClinicName");
    sTemplateFor2B = roSettings.readString("TemplateFor2B");

// almost done, parse the [Options] section (yes they are all optional, hence the clever section name)
    roSettings.setCurrentSection("Options");
    sCookingLogFileName     = roSettings.readStringWithDefault("CookingLogFileName","");
    bObfuscatePersonNo      = roSettings.readBoolWithDefault("ObfuscatePersonNo",false);
    bShowErrorSponges       = roSettings.readBoolWithDefault("ShowErrorSponges",false);
    bShowSponges            = roSettings.readBoolWithDefault("ShowSponges",false);
    sTCPathOverride         = roSettings.readStringWithDefault("TCPathOverride","");
    sKatRegFilePathOverride = roSettings.readStringWithDefault("KatRegFilePathOverride","");
    sFileNameVisitsNoGo     = roSettings.readStringWithDefault("FileNameVisitsNoGo","");

#if defined(Q_OS_WIN)
// on Windows, always disable the cooking log (and toss possible previous version)
    sCookingLogFileName     = "";
    QFile::remove("CookingLog.txt");
#endif

    if (!sKatRegFilePathOverride.isEmpty())
        fubar("The KatRegFilePathOverride option is not yet supported");

// setup the default date period? yes unless a date range has been specified in the command line (for batch mode)
    bBatchMode = false;

    QCommandLineParser lp;
    lp.process(qApp->arguments());
    auto slArgs = lp.positionalArguments();
    if (slArgs.count() == 2)
    {
    // need two valid dates
        dFrom = TWUtils::fromISODate(slArgs[0]);
        dTo   = TWUtils::fromISODate(slArgs[1]);

        bool bOk = (dFrom.isValid() && dTo.isValid());
        if (bOk)
            bOk = (dFrom <= dTo);                 // same day ok
        if (bOk)
            bOk = (dTo < QDate::currentDate());   // yesterday or earlier ok

    // we have two valid dates, enable batch mode
    // (if we didn't get a valid range, dFrom and dTo will be assigned new values below)
        bBatchMode = bOk;
    }

    if (!bBatchMode)
    {
    // ok let the user select the date range
    // calc. the default date range from the R/O settings
        roSettings.setCurrentSection("DefaultPeriod");
        int nPeriodMonths = roSettings.readInt("Months");
        int nPeriodWeeks  = roSettings.readInt("Weeks");
        int nPeriodDays   = roSettings.readInt("Days");

    // if we have some R/W settings try to use them instead
        appDataSettings.setCurrentSection("DefaultPeriod"); // use a common setting (regardless of customer no.)
        nPeriodMonths = appDataSettings.readIntWithDefault("Months",nPeriodMonths);
        nPeriodWeeks  = appDataSettings.readIntWithDefault("Weeks", nPeriodWeeks);
        nPeriodDays   = appDataSettings.readIntWithDefault("Days",  nPeriodDays);

    // some sanity checks please
        if ((nPeriodMonths < 0) || (nPeriodWeeks < 0) || (nPeriodDays < 0))
            fubar("don't like negative [DefaultPeriod] values");

        if ((nPeriodMonths == 0) && (nPeriodWeeks == 0) && (nPeriodDays == 0))
            fubar("sorry but at least one of Months,Weeks or Days in [DefaultPeriod] in the .ini file has to be > 0");

    // default to a period of some time specified in the settings (days, weeks or months)
        dFrom = QDate::currentDate().addDays(-1);   // default range: [yesterday to today[
        dTo   = QDate::currentDate();               //
        if (nPeriodDays > 0)
        {
            dFrom = QDate::currentDate().addDays(0 - nPeriodDays);
            if (dFrom.dayOfWeek() > 5)      // Saturday or Sunday?
            {
                dFrom = dFrom.addDays(-1);
                if (dFrom.dayOfWeek() > 5)  // try again
                    dFrom = dFrom.addDays(-1);
            }
        }

        if (nPeriodWeeks > 0)
        {
        // set a period of weeks, Monday to Monday (note: we're assuming Monday is the first day of the week)
            dFrom = QDate::currentDate().addDays(0 - 7 * nPeriodWeeks); // step # of weeks back
            while (dFrom.dayOfWeek() > Qt::Monday)
                dFrom = dFrom.addDays(-1);

            dTo = dFrom.addDays(7 * nPeriodWeeks);
        }

        if (nPeriodMonths > 0)
        {
        // set period 1st to 1st of months
            dFrom = QDate::currentDate().addMonths(0 - nPeriodMonths); // step # of months back
            if (QDate::currentDate().day() > 6)    // but if we're later than 6 days into a month
                dFrom = dFrom.addMonths(1);        // step one month forward
            dFrom = QDate(dFrom.year(),dFrom.month(),1);
            dTo = dFrom.addMonths(nPeriodMonths);
        }

        if (dTo > QDate::currentDate())     // trying to step into the future?
            dTo = QDate::currentDate();     // today is as far as we can go

    // now we have [first, last[, i.e. last is exclusive
    // make an inclusive range i.e. [first, last] by subtracting 1 day
        dTo = dTo.addDays(-1);
    }

    if (dTo < dFrom)  // make sure we're coding correctly here
        guruMeditation("dTo < dFrom :-( something is seriously wrong with the date calculations");

// set 'em on our form as the default date selection
    ui->calendarWidgetFrom->setSelectedDate(dFrom);
    ui->calendarWidgetTo->setSelectedDate(dTo);

// set the maximum allowed date range, i.e. first and last days that we allow to be selected
    QDate dEarliest = ddEarliestVisit;                  // (because we changed receipt no.s style then)
    QDate dLatest   = QDate::currentDate().addDays(-1); // yesterday is the most recent day we'll allow

    ui->calendarWidgetFrom->setMinimumDate(dEarliest);
    ui->calendarWidgetFrom->setMaximumDate(dLatest);

    ui->calendarWidgetTo->setMinimumDate(dEarliest);
    ui->calendarWidgetTo->setMaximumDate(dLatest);

// use our clicked() methods to initially set the Go-button caption
    on_calendarWidgetFrom_clicked(dFrom);
    on_calendarWidgetTo_clicked  (dTo  );

// load up the icons we use (and set their sizes on screen to 32x32)
    iconShowBox = QIcon(":/showbox.ico");
    iconTC      = QIcon(":/tc.ico");
    int nIconHeightAndWidth = 30;
    ui->tableWidgetAll->setIconSize(QSize(nIconHeightAndWidth,nIconHeightAndWidth));
    ui->tableWidgetWarnings->setIconSize(QSize(nIconHeightAndWidth,nIconHeightAndWidth));
    ui->tableWidgetErrors->setIconSize(QSize(nIconHeightAndWidth,nIconHeightAndWidth));


// ------------------------------------
// prepare the UI, start with the title
    QString sTitle = QString("KatReg Uppf för %1 (nr. %2)").arg(sClinicName,sClinicNo);
    ui->labelTitle->setText(sTitle);

    if (bObfuscatePersonNo)
        ui->labelTitle->setText("TC2KatRegUppf med testpatienter");

    setupUI();  // do the rest here

// almost done, final step: connect Samba and Intelligence (via a timed lambda)
    switchAppState(AppState::eConnecting);
    QTimer::singleShot(nnShortDelay,this,[this]
    {
    // now is a good time to check that we can create a mew file in the current directory
        if (!TWTextFileWriter::canCreateFile("TestfilKatReg.xml"))
            if (!TWUtils::okCancelBox(QString("Kan inte skapa en KatReg-xmlfil i denna mapp ('%1').\nOk att fortsätta ändå?").arg(TWUtils::getCurrentDirectory())))
                return qApp->quit();

    // ok carry on trying to connect to Samba and Intelligence
        QString sError = connectToSambaAndIntelligence();
        if (!sError.isEmpty())
        {
        // better luck next time
            TWUtils::errorBox(sError);
            return qApp->quit();
        }

    // wait for app state to change (and the Go button to be pressed)
    });
}

//----------------------------------------------------------------------------
// MainWindow main tosser
//
// 2023-12-01 First version
//----------------------------------------------------------------------------
MainWindow::~MainWindow()
{
// say goodby and close the log (if we're not on Windows)
#if !defined(Q_OS_WIN)
    if (tfwLog.isOpen())
    {
        tfwLog.ts << "That's all folks, closing at " << TWUtils::toISODateTimeNoSeconds(QDateTime::currentDateTime()) << ".\n";
        tfwLog.close();
    }
#endif

// save our mainwindow geometry
    appDataSettings.setCurrentSection("General");
    appDataSettings.writeByteArray("Geometry",saveGeometry());
    appDataSettings.sync(); // in case we're doing an ignominious exit

// if we're processing, chances are that some other thread is running
    if ((AppState::eConnecting == appState) || (AppState::eProcessing == appState))
    // if so exit the hard way
        TWUtils::ignominiousExit();

// else the slow way
    delete ui;
}

//----------------------------------------------------------------------------
// closeEvent (MainWindow extra tosser when typing Alt-F4 or clicking 'x')
//
// 2023-12-01 First version
//----------------------------------------------------------------------------
void MainWindow::closeEvent(QCloseEvent* event)
{
    Q_UNUSED(event);

#if defined(Q_OS_WIN)
// on Windows always exit the hard way regardless of app state
    TWUtils::ignominiousExit();
#endif
// on other machines only go south if we're connecting or processing
    if ((AppState::eConnecting == appState) || (AppState::eProcessing == appState))
        TWUtils::ignominiousExit();
}

//----------------------------------------------------------------------------
// setupUI
//
// 2023-12-01 First version
//----------------------------------------------------------------------------
void MainWindow::setupUI()
{
// show the calendars but hide the Go button until we're connected nice and dandy
    ui->pushButtonGo->hide();

// prepare the 3 table widgets, first the columns
    auto setupColumns = [] (QTableWidget* tw)
    {
        tw->setColumnCount(colCount);
        for (int i = 0; (i < colCount); ++i)
            tw->setColumnWidth(i,anColumnWidths[i]);
    };
    setupColumns(ui->tableWidgetAll);
    setupColumns(ui->tableWidgetWarnings);
    setupColumns(ui->tableWidgetErrors);

// and hide 'em for now
    ui->tableWidgetAll->hide();
    ui->tableWidgetWarnings->hide();
    ui->tableWidgetErrors->hide();

// set the default table widget view state
    tableViewState = eShowAll;

// prepare the visibility slider and hide it until we've started processing
    ui->visibilitySlider->hide();
    ui->visibilitySlider->setValue(tableViewState); // start with the "ShowAll" leftmost position
    refreshVisibilitySlider(tableViewState);        //

// create a new groove for the slider (the original groove vanishes due to the stylesheet applied above)
// wire up a new QFrame to act as a surrogate groove with the same parent and geometry as the slider
    QFrame* pFrame = new QFrame(ui->visibilitySlider->parentWidget());
    pFrame->setGeometry(ui->visibilitySlider->geometry());
    pFrame->setObjectName(ui->visibilitySlider->objectName() + "Frame");
    pFrame->setFrameStyle(QFrame::HLine | QFrame::Sunken);
    pFrame->setLineWidth(2);  // (looks nicer than width 1)

// change the z-order so that our custom groove is drawn under the handle (just like the real groove)
    pFrame->stackUnder(ui->visibilitySlider);

// in order for hide()/show() and resize() to work remember this QFrame
    pSliderGrooveFrame = static_cast<QWidget*>(pFrame);

// check that we really have a slider groove QFrame alive and well
    if (nullptr == pSliderGrooveFrame)
        fubar("Couldn't create a slider groove frame");
    pSliderGrooveFrame->hide(); // and hide it for now

// reset and hide the progressbar until we start the processing
    ui->progressBar->setValue(0);
    ui->progressBar->hide();

// resizing support: remember original mainwindow size and set the minimum size
    szOriginal = size();
    setMinimumSize(szOriginal); // designed size ---> minimum size (for now)

// save all our widgets original/designed geometries
    auto sg = [this] (QWidget* w) { mOriginalGeometries.insert(w,w->geometry()); };
    sg(ui->labelTitle);
    sg(ui->pushButtonExit);
    sg(ui->visibilitySlider);
    sg(pSliderGrooveFrame);   // don't forget this one
    sg(ui->frameCalendars);   // (includes calendars and the Go button)
    sg(ui->tableWidgetAll);
    sg(ui->tableWidgetWarnings);
    sg(ui->tableWidgetErrors);
    sg(ui->progressBar);
    sg(ui->labelProgress);

// finally, restore mainwindow geometry if we have one saved (unless a shift key is held down)
    appDataSettings.setCurrentSection("General");
    QByteArray baGeometry = appDataSettings.readByteArray("Geometry");
    if ((baGeometry.length() > 0) && (!TWUtils::isShiftKeyDown()))
        restoreGeometry(baGeometry);
}

//-----------------------------------------------------------------------------
// connectToSambaAndIntelligence
// returns "" when all is good, else returns an error message
//
// 2023-12-01 First version
//-----------------------------------------------------------------------------
QString MainWindow::connectToSambaAndIntelligence()
{
// say hello to Samba and the Intelligence server
    showProgress(QString(" Öppnar Samba och vårdgivarkontot för %1...").arg(sCustomerName));

// use a db prober to check our Intelligence connection
// need to do this *before* launching initial Samba crawler (since it switches app state automatically)
    QString sDBProbeError = TWTCSupport::probeSQLServer(eIniFlavorForDB,sIntelligenceSectionName);
    if (!sDBProbeError.isEmpty())
    {
    // got a db error (showstopper), hide the calendars for a visible effect
        ui->frameCalendars->hide();
        showProgress("Öppna Intelligence/vårdgivarkontot misslyckades.");

        TWUtils::errorBox(sDBProbeError);
        return "";  // tell a lie (no errors) to stay in the app so that user can go to Settings if needed
    }

// use our initial Samba instance for checking the connection
    if (sLocalDirectory.isEmpty())
    // no local directory set for Samba files? on Windows we support a remote connection as well
        scInitial.setSambaCredentialsFromIniFile();
    else
        scInitial.setLocalDirectory(sLocalDirectory);

// wire up the crawling done signal
    connect(&scInitial,&SambaCrawler::crawlingDone,this,[this] (int nMapCount)
    {
        Q_UNUSED(nMapCount);

    // if there's an error kiss the user goodbye
        QString sError = scInitial.sErrorMsg;
        if (!sError.isEmpty())
        {
            TWUtils::errorBox(sError);
            TWUtils::ignominiousExit(); // exit here directly avoiding getting stuck waiting for the Samba worker thread
        }

    // no error(s), what about Intelligence, is it connected ok? i.e. we're the last man out?
        if (bWaitingForSamba)
        {
            showProgress(QString(" Intelligence/vårdgivarkontot och Samba öppnade för %1.").arg(sCustomerName));

        // yes the app was waiting for the initial Samba crawler, so switch the app state now
            switchAppState(AppState::eConnectedOk);
         }
    });

// set it to crawl only one day back (we're not interested in any hits, just verifying we can have a chat with the server)
    scInitial.crawl(nCompanyCode,1);
    bWaitingForSamba = false; // guessing this initial crawl will finish before we've checked Intelligence

// time to launch the Intelligence connection ---------------------------------
    db.setDBErrorCallback([this](QString sError) { dbError(sError); });
    TWTCSupport::setBobbyTablesPrefixFromIniFile(eIniFlavorForDB,sIntelligenceSectionName);
    db.setConnectionName("TC2KatRegUppf1");

    QString sError = TWTCSupport::openSQLServerFromIniFile(&db,eIniFlavorForDB,sIntelligenceSectionName);
    if (!sError.isEmpty())
        return sError;  // not good, we're out of here

// match current info with what we have from the .ini file for the careunit and company
    QStringList slErrors;
    db.select("CompanyID,CareUnitID,HSAID",stTCCorral.Codes_CareUnits,QString("CareUnitID=%1").arg(nCareUnitID),
    [this,&slErrors](DBRowStruct rs)
    {
        if (nCompanyID  != rs.intValue("CompanyID"))
            slErrors += "CompanyID är inte korrekt";

        if (nCareUnitID != rs.intValue("CareUnitID"))
            slErrors += "CareUnitID är inte korrekt";

        if (sHSAID      != rs.stringValue("HSAID"))
            slErrors += "HSAID är inte korrekt";
    },eDBRowsExpectExactlyOne);

// got some error(s)? if so quit
    if (!slErrors.isEmpty())
        dbError("Kan inte fortsätta:\n" + TWUtils::stringList2CommaList(slErrors).replace(",","\n"));

// check some more stuff in Codes_Companies_v2 (mismatches here are not considered fatal)
    QStringList slWarnings;
    db.select("CompanyCode,EconomicalCatalogue",stTCCorral.Codes_Companies_v2,QString("CompanyID=%1").arg(nCompanyID),
    [this,&slWarnings](DBRowStruct rs)
    {
        if (nCompanyCode != rs.intValue("CompanyCode"))
            slWarnings += "Företagskoden är ändrad i Intelligence";

        auto sl = rs.stringValue("EconomicalCatalogue").split("/");
        if (sl.isEmpty())
            slWarnings += "Ekonomiska katalogen i Intelligence saknas (ej ifylld)";
        else
        {
            if (sl.last().isEmpty())
                sl.removeLast();

            if (!sl.isEmpty())
                if (sSambaShareName != sl.last())
                    slWarnings += "Ekonomiska katalogen i Intelligence är inte identisk med inställningarna för din Samba";
        }
    },eDBRowsExpectExactlyOne);

// done: show progress bar message saying we have a valid Intelligence connection
    QString sProgress = QString(" Intelligence/vårdgivarkontot öppnat för %1, väntar på Samba...").arg(sCustomerName);
    if (scInitial.bCrawlingDone)
        sProgress = QString(" Samba och Intelligence/vårdgivarkontot öppnade för %1.").arg(sCustomerName);
    showProgress(sProgress);

// got some warnings to show?
    if (!slWarnings.isEmpty())
        TWUtils::warningBox("Varning:\n" + TWUtils::stringList2CommaList(slWarnings).replace(",","\n"));

// we've opened Intelligence ok, what about Samba?
    if (scInitial.bCrawlingDone)
    // yes Samba is connected as well, switch app state and light up the Go button
        switchAppState(AppState::eConnectedOk);
    else
    // no set the boolean and go idle (the crawling done slot will switch app state for us)
        bWaitingForSamba = true;

// return with an empty string (meaning: no Intelligence or Samba errors)
    return "";
}

//----------------------------------------------------------------------------
// resizeEvent
//
// 2023-12-01 First version
//----------------------------------------------------------------------------
void MainWindow::resizeEvent(QResizeEvent* event)
{
// get the new size of our mainwindow
    QSize szNew = event->size();

// calc. the horizontal and vertical deltas
    int nDeltaX = szNew.width() - szOriginal.width();
    int nDeltaY = szNew.height() - szOriginal.height();

// move/resize our widgets (regardless if they're visible or not)
    auto adjustWidget = [this] (QWidget* w, int dx1, int dy1, int dx2, int dy2) { w->setGeometry(mOriginalGeometries[w].adjusted(dx1,dy1,dx2,dy2)); };

    adjustWidget(ui->labelTitle,      nDeltaX / 2, 0, nDeltaX / 2, 0);    // recenter in x, fixed in y
    adjustWidget(ui->pushButtonExit,  nDeltaX,     0, nDeltaX,     0);    // follow in x, fixed in y
    adjustWidget(ui->visibilitySlider,nDeltaX / 2, 0, nDeltaX / 2, 0);    // recenter in x, fixed in y
    adjustWidget(pSliderGrooveFrame,  nDeltaX / 2, 0, nDeltaX / 2 ,0);    // recenter in x, fixed in y

    adjustWidget(ui->frameCalendars,  nDeltaX / 2, nDeltaY / 2, nDeltaX / 2, nDeltaY / 2);   // recenter in both x and y

    adjustWidget(ui->tableWidgetAll,      0,       0, nDeltaX, nDeltaY);  // fixed at the top/left, follow in bottom/right
    adjustWidget(ui->tableWidgetWarnings, 0,       0, nDeltaX, nDeltaY);  // fixed at the top/left, follow in bottom/right
    adjustWidget(ui->tableWidgetErrors,   0,       0, nDeltaX, nDeltaY);  // fixed at the top/left, follow in bottom/right

    adjustWidget(ui->progressBar,   0, nDeltaY, nDeltaX, nDeltaY);        // fixed at the left, follow to the right and in y
    adjustWidget(ui->labelProgress, 0, nDeltaY, nDeltaX, nDeltaY);        // fixed at the left, follow to the right and in y
}

//----------------------------------------------------------------------------
// setGoButtonCaption
//
// 2023-12-01 First version
//----------------------------------------------------------------------------
void MainWindow::setGoButtonCaption()
{
// assume we have valid dFrom and dTo dates, select different caption if we have 2 dates range of just 1 date
    if (dFrom == dTo)
        ui->pushButtonGo->setText(QString("Hämta KatReg-data för %1en %2")
                                  .arg(TWUtils::getSwedishWeekdayName(dFrom),TWUtils::toISODate(dFrom)));
    else
        ui->pushButtonGo->setText(QString("Hämta KatReg-data fr.o.m. %1en %2 t.o.m. %3en %4")
                                  .arg(TWUtils::getSwedishWeekdayName(dFrom),TWUtils::toISODate(dFrom),
                                       TWUtils::getSwedishWeekdayName(dTo  ),TWUtils::toISODate(dTo  )));
}

//----------------------------------------------------------------------------
// on_calendarWidgetFrom_clicked
//
// 2023-12-01 First version
//----------------------------------------------------------------------------
void MainWindow::on_calendarWidgetFrom_clicked(const QDate &date)
{
// have a new "From" date
    dFrom = date;

// is it after the "To" date?
    if (dFrom > dTo)
    {
    // yes, set the "To" date to the same day as the "From" date
        dTo = dFrom;

    // refresh the other calendar (will call the other's slot but it's harmless)
        ui->calendarWidgetTo->setSelectedDate(dTo);
    }

// and refresh the Go button caption
    setGoButtonCaption();
}

//----------------------------------------------------------------------------
// on_calendarWidgetTo_clicked
//
// 2023-12-01 First version
//----------------------------------------------------------------------------
void MainWindow::on_calendarWidgetTo_clicked(const QDate &date)
{
// have a new "To" date
    dTo = date;

// is it before the "From" date?
    if (dTo < dFrom)
    {
    // yes, set the "From" date to the same day as the "To" date
        dFrom = dTo;

    // refresh the other calendar (will call the other's slot but it's harmless)
        ui->calendarWidgetFrom->setSelectedDate(dFrom);
    }

// and refresh the Go button caption
    setGoButtonCaption();
}

//----------------------------------------------------------------------------
// on_pushButtonExit_clicked
//
// 2023-12-01 First version
//----------------------------------------------------------------------------
void MainWindow::on_pushButtonExit_clicked()
{
// if we're processing (and on Windows), ask nicely first
    if ((AppState::eConnecting == appState) || (AppState::eProcessing == appState))
    {
    #if defined(Q_OS_WIN)
        if (!TWUtils::yesNoBox("Vill du avsluta?", "Avsluta TC2KatRegUppf"))
            return;
    #endif

    // since we're processing, jump ship immediately (avoid hanging threads etc.)
        TWUtils::ignominiousExit();
    }

// else do a more vanilla exit
    QApplication::quit();
}

//----------------------------------------------------------------------------
// switchAppState (change app state and probably also the UI)
//
// 2023-12-01 First version
//----------------------------------------------------------------------------
void MainWindow::switchAppState(AppState newAppState)
{
// hide/show different things depending on app state
    switch (newAppState)
    {
    case AppState::eConnecting    :
        break;

    case AppState::eConnectedOk   :
    // successfully connected, queue up for the Go button to be shown
        QTimer::singleShot(nnNormalDelay,this,[this] { ui->pushButtonGo->show(); });

    // autostart or batch mode enabled? if so, press the Go button automagically after a slightly (3 times) longer delay
        if ((bAutoStart) || (bBatchMode))
            QTimer::singleShot(nnNormalDelay * 3,this,[this] { on_pushButtonGo_clicked(); });

    // else wait for the user to do the honors (i.e. clicking the button)
        break;

    case AppState::eProcessing    :
    // processing: time to slide the calendars and the Go button up and show the table widget instead
        {
        // disable the Go pushbutton directly to avoid recursive double clicks
            ui->pushButtonGo->setEnabled(false);

        // fiddle with the geometries for these two widgets
        // (we don't need to animate the other two table widgets, since the ending geometry is the designed geometry)
            auto rCalendars   = ui->frameCalendars->geometry();
            auto rTableWidget = ui->tableWidgetAll->geometry();

            QPropertyAnimation* animFrame    = new QPropertyAnimation(ui->frameCalendars,"geometry");
            QPropertyAnimation* animTableAll = new QPropertyAnimation(ui->tableWidgetAll,"geometry");

            animFrame->setDuration(nnSlideAnimDuration);
            animTableAll->setDuration(nnSlideAnimDuration);

        // starting geometries for the calendars: use the current
        // for the table widget: current geometry slided down one full height
            animFrame->setStartValue(rCalendars);
            animTableAll->setStartValue(rTableWidget.translated(0,rTableWidget.height()));

        // ending geometries: for the calendars: squished at top (top y same but zero height)
        // for the table widget: the current/designed geometry
            rCalendars.setHeight(0);
            animFrame->setEndValue(rCalendars);
            animTableAll->setEndValue(rTableWidget);

        // prepare for the group animation
            QParallelAnimationGroup* animGroup = new QParallelAnimationGroup;
            animGroup->addAnimation(animFrame);
            animGroup->addAnimation(animTableAll);

        // say what to do at the end of the animation
            connect(animGroup,&QParallelAnimationGroup::finished,this,[this]
            {
                ui->frameCalendars->hide();
                ui->visibilitySlider->show();
                pSliderGrooveFrame->show();
                ui->progressBar->show();

            // do an initial restock to setup the "No warnings.." and "No errors.." rows
            // most likely we have some PAS entries by now since fetchKatRegData() is already running
                restockAllTableWidgets();
            });

        // make sure the calenders and showAll tablewidget are visible and launch the animation
            ui->frameCalendars->show();
            ui->tableWidgetAll->show();
            ui->tableWidgetWarnings->hide();
            ui->tableWidgetErrors->hide();
            animGroup->start(QAbstractAnimation::DeleteWhenStopped); // (when done also deletes the 2 anim props)
        }
        break;

    case AppState::eRubbernecking :
    // we don't need the progressbar any more, so hide it
        ui->progressBar->hide();

    // hand over the vertical screen real estate owned by the progress bar to the table widgets above it
        auto rTableWidgetAll = mOriginalGeometries[ui->tableWidgetAll]; // reuse this one for all 3
        auto rProgressBar    = mOriginalGeometries[ui->progressBar];

        rTableWidgetAll.setBottom(rProgressBar.bottom());
        rProgressBar.setHeight(0);   // (not really needed since it's hidden now, but cannot hurt)

        mOriginalGeometries[ui->tableWidgetAll]      = rTableWidgetAll;
        mOriginalGeometries[ui->tableWidgetWarnings] = rTableWidgetAll;
        mOriginalGeometries[ui->tableWidgetErrors]   = rTableWidgetAll;

        mOriginalGeometries[ui->progressBar] = rProgressBar;

    // do a dummy resize to update the screen now
        auto orgSize = size();
        resize(orgSize.width() + 1,orgSize.height());
        resize(orgSize);
    }

// we've switched ok, set our chap
    appState = newAppState;
}

//----------------------------------------------------------------------------
// refreshVisibilitySlider
//
// 2023-12-01 First version
//----------------------------------------------------------------------------
void MainWindow::refreshVisibilitySlider(TableViewStates e)
{
    auto r = ui->visibilitySlider->geometry();
    int nHeightInPixels = r.height();
    int nWidthInPixels  = r.width() / 2;  // assuming 3 positions (left, middle and right)

// construct and set the style sheet (2 parts, one for the groove and one for the handle)
    ui->visibilitySlider->setStyleSheet(QString(
                          ".QSlider::groove { background: transparent; height: %1px; } "
                          ".QSlider::handle { background: %2; border-radius: 11px; border: 1px solid gray; width: %3px;}")
                          .arg(nHeightInPixels).arg(acVisibilitySliderColors[e].name()).arg(nWidthInPixels));
}

//----------------------------------------------------------------------------
// on_visibilitySlider_valueChanged
//
// 2023-12-01 First version
//----------------------------------------------------------------------------
void MainWindow::on_visibilitySlider_valueChanged(int value)
{
// set the new view state
    auto newTableViewState = static_cast<TableViewStates>(value);

// same as before (i.e. no need to switch)?
    if (tableViewState == newTableViewState)
        return;

// ok update the viewstate
    tableViewState = newTableViewState;

// first change to color of the slider
    refreshVisibilitySlider(tableViewState);

// play hide and show with the table widgets
    switch (tableViewState)
    {
    case eShowAll      :
        ui->tableWidgetAll->clearSelection();   // clear possible prev. selection
        ui->tableWidgetAll->show();

        ui->tableWidgetWarnings->hide();
        ui->tableWidgetErrors->hide();
        break;

    case eShowWarnings :
        ui->tableWidgetAll->hide();

        ui->tableWidgetWarnings->clearSelection();
        ui->tableWidgetWarnings->show();

        ui->tableWidgetErrors->hide();
        break;

    case eShowErrors   :
        ui->tableWidgetAll->hide();
        ui->tableWidgetWarnings->hide();

        ui->tableWidgetErrors->clearSelection();
        ui->tableWidgetErrors->show();
        break;

    default:
        fubar("bad tableViewState");
    }
}

//----------------------------------------------------------------------------
// showProgress updates labelProgress and optionally steps the ProgressBar
//
// 2023-12-01 First version
//----------------------------------------------------------------------------
void MainWindow::showProgress(QString sText, int nBarValue /* = 0 */)
{
    ui->labelProgress->setText(sText);
    if (nBarValue > 0)
        ui->progressBar->setValue(nBarValue);

    qApp->processEvents();
}

//----------------------------------------------------------------------------
// on_pushButtonGo_clicked
//
// 2023-12-01 First version
// 2024-05-15 Add checks for same year and before March 1
//----------------------------------------------------------------------------
void MainWindow::on_pushButtonGo_clicked()
{
// sanity check the date range (cannot be backwards)
    if (dFrom > dTo)
        guruMeditation("didn't expect dFrom > dTo");

// both dates have to be in the same year
    if (dFrom.year() != dTo.year())
        return TWUtils::errorBox("Fr.o.m. och t.o.m. datumen måste vara samma år.");

// both dates have to be on March 1 or later that year
    if (dFrom.month() < 3)
        return TWUtils::errorBox("Fr.o.m. datumet kan inte vara i januari eller februari.");

    if (dTo.month() < 3)
        return TWUtils::errorBox("T.o.m. datumet kan inte vara i januari eller februari.");

// if yesterday is selected and time is before 7 AM, ask a question
    if (dTo == QDate::currentDate().addDays(-1))
        if (QTime::currentTime().hour() < 7)
            if (!TWUtils::yesNoBox("Innan kl. 07:00 på morgonen syns inte gårdagens pat. personnummer. Ok att fortsätta ändå?"))
                return;

// check that we've wired up ok to Intelligence and Samba
    if (AppState::eConnectedOk != appState)
        guruMeditation("appState not eConnectedOk");

// start processing, change title and app state
    if (dFrom == dTo) // same day adventure?
        ui->labelTitle->setText(QString("KatReg %1 %2:").arg(TWUtils::getSwedishWeekdayName(dFrom),TWUtils::toISODate(dFrom)));
    else
        ui->labelTitle->setText(QString("KatReg fr.o.m. %1 t.o.m. %2:").arg(TWUtils::toISODate(dFrom),TWUtils::toISODate(dTo)));

// here we go, hide the calendars and show the table widget
    switchAppState(AppState::eProcessing);


#if !defined(Q_OS_WIN)
// write a happy message in the cooking log? (if we're not on Windows)
    if (!sCookingLogFileName.isEmpty())
    {
        tfwLog.open(sCookingLogFileName);
        tfwLog.ts << "TC2KatRegUppf started at " << TWUtils::toISODateTimeNoSeconds(QDateTime::currentDateTime()) <<
                     " cooking journals from " << TWUtils::toISODate(dFrom) << " to " << TWUtils::toISODate(dTo) << ".\n";
    }
#endif

// and start the party via a timer
    QTimer::singleShot(nnNormalDelay,this,[this] { fetchKatRegData(); });
}

//----------------------------------------------------------------------------
// refreshPersonNo
//
// 2023-12-01 First version
//----------------------------------------------------------------------------
void MainWindow::refreshPersonNo()
{
// crawler still crawling?
    if (!scMain.bCrawlingDone)
        return;  // yes see you again

// obfuscation requested?
    if (bObfuscatePersonNo)
    {
    // replace the real personno. in the map with a bunch of test patients
        mPID2PersonNo.clear();  // do a white wedding on the real ones

        static auto slTestPatients = TWTCSupport::getAllTestPatients();
        if (slTestPatients.count() < 1)
            guruMeditation("didn't expect an empty list of test patients");

        for (auto st : mKatRegEntries)
        // make sure all entries in the easy-peasy map are filled with test patients
            mPID2PersonNo[st.nPID] = slTestPatients[abs(st.nPID) % slTestPatients.count()];
    }

// step thru them all and update the personno.s
    for (auto &st : mKatRegEntries)    // (& for r/w-access)
    {
    // try the easy map first
        if (mPID2PersonNo.contains(st.nPID))
        {
        // told you it was easy
            st.sPersonNo = mPID2PersonNo[st.nPID];
            continue;
        }

    // try the harder way (all entries have to make it through here at least once)
        if (st.sReceiptNo.isEmpty())  // no dice: need a nonempty receipt no.
            continue;

    // have a TC receiptno. look it up in the crawler's map
        QString sPersonNo = scMain.mRP.value(st.sReceiptNo);
        if (sPersonNo.isEmpty())
            continue;  // not found (probably the TC counter wasn't closed)

    // got a personno. is it any good?
        if (!TWUtils::checkPersonNo(sPersonNo).isEmpty())
            continue;  // some or other error, forget about it

    // ok gotcha
        st.sPersonNo = sPersonNo;

    // store it also in the easy-peasy map (for the next refresh)
        mPID2PersonNo[st.nPID] = sPersonNo;
    }
}

//----------------------------------------------------------------------------
// restockSingleTableWidget
// clear and refill a single table widget
//
// 2023-12-01 First version
//----------------------------------------------------------------------------
void MainWindow::restockSingleTableWidget(TableViewStates eState)
{
// use a helper map to keep the row listed in visit date/time order
    QMap<QString,QString> mVisitDateTime2KatRegEntry;

// support scroll to a "high water mark" (make the array static to persist across runs)
    static QDate adPrevHighest[viewStateCount];

// establish which table widget we're restocking
    QTableWidget* tw = nullptr;
    switch (eState)
    {
    case eShowAll      : tw = ui->tableWidgetAll;      break;
    case eShowWarnings : tw = ui->tableWidgetWarnings; break;
    case eShowErrors   : tw = ui->tableWidgetErrors;   break;
    default :
        guruMeditation("bad eState");
    }

    int   nRows       = 0;
    int   nHighestRow = 0;
    QDate dHighest;

    for (auto st : mKatRegEntries)
    {
    // check for sponges, show them or not?
        if (st.bSpongeDOA)    // sponge?
        {
        // establish visibility for this sponge
            if (st.bOk)
            // for normal sponges only check this boolean
                if (!bShowSponges)
                    continue;

            if (!st.bOk)
            // visibility for error sponges depends on two booleans
                if (!bShowErrorSponges && !bShowSponges)
                    continue;
        }

    // determine entry flavor
        bool bWarning = !st.sWarning.isEmpty();
        bool bError   = !st.bOk;
        if (bError == st.sError.isEmpty())  // both should either be true or false
            guruMeditation("bError and sError mismatch");

    // an error wins over a warning (i.e. when both are true, don't show this entry as a warning also)
        if (bError)
            bWarning = false;

    // not the ShowAll widget?
        if (eShowWarnings == eState)
            if (!bWarning)  // only warnings please
                continue;

        if (eShowErrors == eState)
            if (!bError)    // only errors please
                continue;

    // ok visible it is, create the key for the helper map
        QString sKey = st.sVisitDateTime + QString::number(nRows);  // (use nRows to make a unique key)
        if (mVisitDateTime2KatRegEntry.contains(sKey))              // so this should never happen
            guruMeditation("Found an existing visit-datetime+rowno. key in refreshTabletWidget (not good)");

    // this is a visible row, is it a row we should scroll to (i.e. make sure is visible)?
        if ((st.cEye == ccRightEye) || (st.cEye == ccLeftEye))
        {
        // yes if this row has the eye flavor set
            dHighest    = st.dVisit;
            nHighestRow = nRows;
        }

    // stuff it with the key to our main map and count the #
        mVisitDateTime2KatRegEntry.insert(sKey,st.sKey);   // (map data here is the key to the main map)
        ++nRows;
    }

// start from scratch with a new set of rows
    tw->clearContents();
    tw->setRowCount(nRows);

// any warnings or errors? if they just appeared we need to reset the column span for the first row
    if ((nRows > 0) && (eShowAll != eState))
    // we have visible rows now, make sure we cancel a previous setSpan() for the "No warnings/errors" caption
        if (tw->columnSpan(0,colDayOfTheWeek) > 1)
            tw->setSpan(0,colDayOfTheWeek,1,1);

// step thru the KatReg entries through the visitdatetime map
    int nRow = 0;
    QString sPrevVisitDate;  // helper for deciding if we're to show the day of the week for this row

    for (auto sVisitKey : mVisitDateTime2KatRegEntry.keys())
    {
    // get the KatReg entry for this row (and do some sanity checks)
        if (!mVisitDateTime2KatRegEntry.contains(sVisitKey))
            fubar(QString("Something is wrong with the visit date/time map ('%1')").arg(sVisitKey));
        QString sKey = mVisitDateTime2KatRegEntry[sVisitKey];
        if (!mKatRegEntries.contains(sKey))
            fubar(QString("Something is wrong with the KatReg entries map ('%1')").arg(sKey));

        auto st = mKatRegEntries[sKey];
        if (st.sKey != sKey)  // these should always match up
            guruMeditation("st.sKey != sKey (not good)");

    // determine error status for this entry
        bool bError = !st.bOk;
        if (bError == st.sError.isEmpty())  // both should either be true or false
            guruMeditation("bError and sError mismatch");

    // do the columns: first one is the key to this KatReg entry in the main map
    // (column has zero width, don't bother with readonly etc.)
        tw->setItem(nRow,colMapKey,new QTableWidgetItem(sKey));

    // --- use lambdas for composing the table widget items ---
        auto createAlignedTWI = [] (QString s, QString sToolTip, Qt::Alignment a = Qt::AlignHCenter)
        {
            auto wi = new QTableWidgetItem(s);
            setTableWidgetItemRO(wi); // set this item readonly
            wi->setTextAlignment(a);
            wi->setToolTip(sToolTip);
            return wi;
        };

        auto createCenterAlignedVWE = [createAlignedTWI] (CookedStruct vwe)
        {
        // have VWE chap, use the value and the tooltip (ignore warnings or errors)
            QString sValue = vwe.sValue;
            sValue.replace("\n"," + ");  // for multiple terms: avoid \n in the table view, use " + "
            return createAlignedTWI(sValue, vwe.sToolTip);
        };

    // day of the week (abbrev.): show only if this row is the first row for this day
        QString sValue = "";
        if (sPrevVisitDate != st.sVisitDate)
            sValue = TWUtils::getSwedishWeekdayNameU1(st.dVisit.dayOfWeek()).left(2);
        tw->setItem(nRow,colDayOfTheWeek,createAlignedTWI(sValue,"Besöksdag"));
        sPrevVisitDate = st.sVisitDate; // remember for the next row :=)

    // visit date and time
        sValue = st.sVisitDateTime;                  // default: show "YYYY-MM-DD HH:MM"
        if (tNotFoundInPAS == st.tVisit)             // unless the visit time is missing:
            sValue = st.sVisitDate + QString(6,' '); // then show 6 spaces = about the same width as " HH:MM"
        tw->setItem(nRow,colVisitDateTime,createAlignedTWI(sValue,"Datum för slutkontroll"));

    // personno.: if it's empty (because we're waiting for the Samba crawler) instead show "..."
        sValue = TWUtils::midLineEllipsis();         // pessimistic default (no personno. present)
        if (!st.sPersonNo.isEmpty())
        // got one, pretty print it
            sValue = TWTCSupport::personNoForTC(st.sPersonNo);
        tw->setItem(nRow,colPersonNo,createAlignedTWI(sValue,"Pat. personnummer"));

    // right or left eye
        tw->setItem(nRow,colEye,createCenterAlignedVWE(st.mAllCooked[KatRegFields::eRightOrLeftEye]));

    // date of operation
        auto wiDateOperation = createCenterAlignedVWE(st.mAllCooked[KatRegFields::eDateOperation]);
        auto f = wiDateOperation->font();
        f.setItalic(true);
        wiDateOperation->setFont(f);
        tw->setItem(nRow,colDateOperation,wiDateOperation);

    // show 2A fields before/left of 2B fields (yes, it is the other way around compared to the data retrieval)
    // K1 and K2 chaps
        tw->setItem(nRow,colK1Diopters,createCenterAlignedVWE(st.mAllCooked[KatRegFields::eK1Diopters]));
        tw->setItem(nRow,colK1Degrees ,createCenterAlignedVWE(st.mAllCooked[KatRegFields::eK1Degrees ]));
        tw->setItem(nRow,colK2Diopters,createCenterAlignedVWE(st.mAllCooked[KatRegFields::eK2Diopters]));
        tw->setItem(nRow,colK2Degrees, createCenterAlignedVWE(st.mAllCooked[KatRegFields::eK2Degrees ]));

    // planned refraction and axis length
        tw->setItem(nRow,colPlannedRefraction,createCenterAlignedVWE(st.mAllCooked[KatRegFields::ePlannedRefraction]));
        tw->setItem(nRow,colAxisLength       ,createCenterAlignedVWE(st.mAllCooked[KatRegFields::eAxisLength       ]));

    // 2B fields: show them unless we have warning(s) or error(s)
        if (!st.bOk)
        {
        // got one or more error(s) to show, set it to span all columns before showBox(and occluding them)
            tw->setSpan(nRow,colErrorsOrWarnings,1,colShowBox - colErrorsOrWarnings);

            if (st.sError.isEmpty())
                guruMeditation("didn't expect an empty sError");

        // collect all error texts into one
            QStringList slErrors(st.sError); // first the "master" one
            for (auto e : lKatRegFields)     // then all the katreg flavored ones
                if (!st.mAllCooked[e].sError.isEmpty())
                    slErrors += st.mAllCooked[e].sError;

            slErrors.removeDuplicates();     // (in case st.sError was a copy from the cooked errors)

        // set the error and tooltip strings
            QString sError = slErrors[0];    // for the error text, show only the top two error(s)
            if (slErrors.count() == 2)        // if there are just two errors. show them both
                sError = QString("%1 fel: %2 + %3").arg(slErrors.count()).arg(slErrors[0],slErrors[1]);
            if (slErrors.count() > 2)        // if we have more than 2, show the first two
                sError = QString("%1 fel: %2 + %3 + ...").arg(slErrors.count()).arg(slErrors[0],slErrors[1]);

            QString sToolTip = slErrors.join("\n"); // for the tooltip show each error on separate lines

            auto wiError = createAlignedTWI(" " + sError,sToolTip,Qt::AlignLeft);  // prefix with " " for better visibility
            wiError->setBackground(QBrush(acVisibilitySliderColors[eShowErrors]));
            tw->setItem(nRow,colErrorsOrWarnings,wiError);
        }
        else if (!st.sWarning.isEmpty())
        {
        // a warning, set the column span same as for error(s) above
            tw->setSpan(nRow,colErrorsOrWarnings,1,colJumpToTC - colErrorsOrWarnings);

        // there are currently very few types of warnings in TC2KatRegUppf so skip looping/fetching from mAllCooked
            auto wiWarning = createAlignedTWI(" " + st.sWarning,st.sWarning,Qt::AlignLeft);
            wiWarning->setBackground(QBrush(acVisibilitySliderColors[eShowWarnings]));
            tw->setItem(nRow,colErrorsOrWarnings,wiWarning);
        }
        else
        {
        // no error(s) or warning(s), proceed with the 2B fields
        // first, check if we need to reset a column span from a previous warning or error (spans persist across restocks/refreshes)
            if (tw->columnSpan(nRow,colErrorsOrWarnings) > 1)
                tw->setSpan(nRow,colErrorsOrWarnings,1,1);

        // visus right eye (any value terms are expected to be merged with the measurement value)
            tw->setItem(nRow,colVisusRight,createCenterAlignedVWE(st.mAllCooked[KatRegFields::eVisusRight]));

        // refraction right chaps
            tw->setItem(nRow,colRefractionRightSphere  ,createCenterAlignedVWE(st.mAllCooked[KatRegFields::eRefractionRightSphere  ]));
            tw->setItem(nRow,colRefractionRightCylinder,createCenterAlignedVWE(st.mAllCooked[KatRegFields::eRefractionRightCylinder]));
            tw->setItem(nRow,colRefractionRightDegrees ,createCenterAlignedVWE(st.mAllCooked[KatRegFields::eRefractionRightDegrees ]));

        // visus left eye (any value terms are expected to be merged into the measurement value)
            tw->setItem(nRow,colVisusLeft,createCenterAlignedVWE(st.mAllCooked[KatRegFields::eVisusLeft]));

        // refraction left chaps
            tw->setItem(nRow,colRefractionLeftSphere  ,createCenterAlignedVWE(st.mAllCooked[KatRegFields::eRefractionLeftSphere  ]));
            tw->setItem(nRow,colRefractionLeftCylinder,createCenterAlignedVWE(st.mAllCooked[KatRegFields::eRefractionLeftCylinder]));
            tw->setItem(nRow,colRefractionLeftDegrees ,createCenterAlignedVWE(st.mAllCooked[KatRegFields::eRefractionLeftDegrees ]));
        }

    // light up the "ShowBox" icon for the full Monty of this row? yes, if we have a 2B date
        if (st.dVisit.isValid())
        {
            auto wiShowBox = new QToolButton();
            wiShowBox->setIcon(iconShowBox);
            wiShowBox->setToolTip("Klicka här för att visa alla KatReg-data för denna uppföljning");
            wiShowBox->setEnabled(true);
            tw->setCellWidget(nRow,colShowBox,wiShowBox);

        // prepare a nice click handler lambda
            connect(wiShowBox,&QToolButton::clicked,this,[this,st]
            {
            // wire up a ShowBox dialog and modally wait for it when launched
                ShowBox sh(this,st);
                sh.setModal(true);
                sh.exec();
            });
        }

    // finally, if we're have a valid personno, show the jump to TC icon
        if ("" == TWUtils::checkPersonNo(st.sPersonNo))
        {
            auto wiJumpToTC = new QToolButton();
            wiJumpToTC->setIcon(iconTC);
            wiJumpToTC->setToolTip(QString("Klicka här för att öppna pat. %1 i TakeCare").arg(TWTCSupport::personNoForTC(st.sPersonNo)));
            wiJumpToTC->setEnabled(true);
            tw->setCellWidget(nRow,colJumpToTC,wiJumpToTC);

        // establish a click handler for this row: try to jump/open TC
            connect(wiJumpToTC,&QToolButton::clicked,this,[this,st]
            {
            // get the personno. (or reservno.) for this row, is it a good one?
                QString sPersonNo = st.sPersonNo;
                if (sPersonNo.isEmpty())  // no dice no good
                    return;

            // is a smartcard present? look for a username from the first card (if any) require it to be nonblank
                QString sTCUserName = TWTCSupport::getTCUserNameFromSmartcard(0);
                if (sTCUserName.isEmpty())
                    return TWUtils::warningBox("Hittar inget e-tjänstekort (behövs för att öppna pat. i TakeCare).");

            // try to establish the path to TC
                QString sTCPath = sTCPathOverride;      // if we have a preset path use that
                if (sTCPath.isEmpty())                  // else try to find it right now
                    sTCPath = TWTCSupport::getTCPath();
                if (sTCPath.isEmpty())
                // use this path as a last-ditch attempt
                    sTCPath = "C:\\TakeCare";

            // prepare the twinkle box time (if TC is not started, increase it)
            // check now before actually launching TC
                int nDelay = nnShortTCDelay;
                if (eTCAppNotStarted == TWTCSupport::getTCAppState())
                    nDelay = nnLongTCDelay;

            // bombs away
                QString sError = TWTCSupport::openTCPersonNo(sTCPath,sTCUserName,sPersonNo);
                if (!sError.isEmpty())
                // something didn't agree with the launching, show it as a warning
                    return TWUtils::warningBox(sError,"Öppna TakeCare");

            // ok TC should be launching now, issue the twinklebox as a comfort for the user
                TWUtils::twinkleBox(QString("Öppnar %1 i TakeCare...").arg(sPersonNo),"Start av TakeCare",nDelay);
            });
        }
        else
        {
            auto wiEmpty = new QTableWidgetItem("");
            setTableWidgetItemRO(wiEmpty);
            wiEmpty->setToolTip("Personnummer saknas eller är felaktigt");
            tw->setItem(nRow,colJumpToTC,wiEmpty);
        }

    // that's all for this row, next please
        ++nRow;
    }

// sanity check # of rows
    if (nRow != nRows)
        guruMeditation("nRow != nRows");

// no rows shown? if we're displaying the warning or error widget show a helper text on the first row
    if ((0 == nRows) && (eShowWarnings == eState))
    {
        tw->setRowCount(1);

        tw->setSpan(0,colDayOfTheWeek,1,colCount - colDayOfTheWeek);
        auto tw0 = new QTableWidgetItem("Inga varningar.");

        QFont fntItalic(tw->font());
        fntItalic.setItalic(true);
        tw0->setFont(fntItalic);

        setTableWidgetItemRO(tw0);
        tw->setItem(0,colDayOfTheWeek,tw0);
    }

    if ((0 == nRows) && (eShowErrors == eState))
    {
        tw->setRowCount(1);

        tw->setSpan(0,colDayOfTheWeek,1,colCount - colDayOfTheWeek);
        auto tw0 = new QTableWidgetItem("Inga fel.");

        QFont fntItalic(tw->font());
        fntItalic.setItalic(true);
        tw0->setFont(fntItalic);

        setTableWidgetItemRO(tw0);
        tw->setItem(0,colDayOfTheWeek,tw0);
    }

// finally, should we scroll to a new "high-water" mark i.e. row?
// yes if we have a new highest date
    if (dHighest > adPrevHighest[eState])
    {
    // specify the eye flavor column in case of horizontal scrolling frenzy
        tw->scrollToItem(tw->item(nHighestRow,colEye),QAbstractItemView::PositionAtTop);

    // and remember this new "high-water" row
        adPrevHighest[eState] = dHighest;
    }

// that's all for this restocking, thank you
}

//----------------------------------------------------------------------------
// restockAllTableWidgets
//
// 2023-12-01 First version
//----------------------------------------------------------------------------
void MainWindow::restockAllTableWidgets()
{
// calendars still visible (because we're busy scrolling them off the screen)?
    if (ui->frameCalendars->isVisible())
        return; // hold your horses until they're gone for good

// pretty simple, just call the 3 single chaps
    restockSingleTableWidget(eShowAll);
    restockSingleTableWidget(eShowWarnings);
    restockSingleTableWidget(eShowErrors);
}

//----------------------------------------------------------------------------
// dbError
//
// 2023-12-01 First version
//----------------------------------------------------------------------------
void MainWindow::dbError(QString sError)
{
// if the error is less than 8 chars (probably an error no.) try to prettyprint it
    if (sError.length() < 8)
        sError = TWTCSupport::prettyPrintErrors(sError);

// show an errorbox unless it's the "forcibly closed" error
    if ("db forcibly closed" != sError)
        TWUtils::errorBox(sError);

// say goodbye to a cruel world (all db errors are considered fatal)
// cannot use qApp->exit() because it will resume/continue our main (GUI) thread
    TWUtils::ignominiousExit();
}

//----------------------------------------------------------------------------
// fetchKatRegData
//
// 2024-05-01 First version
// 2024-07-12 Speedup by including template name in the first journal lookup
//----------------------------------------------------------------------------
void MainWindow::fetchKatRegData()
{
// user has pressed the Go button
// launch the main Samba crawler since we know now how many days to go back
    auto nDaysBack = dFrom.daysTo(QDate::currentDate()) + 3; // overshoot a couple of days (can't hurt)

    if (sLocalDirectory.isEmpty())
    // no local directory set for Samba files? on Windows we support a remote connection as well
        scMain.setSambaCredentialsFromIniFile();
    else
        scMain.setLocalDirectory(sLocalDirectory);

// wire up the main Samba crawler done signal
    connect(&scMain,&SambaCrawler::crawlingDone,this,[this] (int nMapCount)
    {
        Q_UNUSED(nMapCount);

    // if there's an error I'm afraid we have to call it a day
        auto sError = scMain.sErrorMsg;
        if (!sError.isEmpty())
        {
            TWUtils::errorBox(sError);
            TWUtils::ignominiousExit();
        }

    // no error(s), is the app waiting for us?
        if (bWaitingForSamba)
        // yes, do the final step from here (where the KatReg file also is written)
            burialAtSea();
    });

// reset our personno. map
    mPID2PersonNo.clear();

// and start the our main Samba crawler asynchronously
    scMain.crawl(nCompanyCode,nDaysBack);
    bWaitingForSamba = false;  // assume the Samba main crawler will finish before Intelligence (almost always true)


// ----------------------------------------------------------------------------
// ----------------- Intelligence time: start with PAS ------------------------
// Q: why are we looking in PAS when we're not interested in any fees or codes?
// A: to get the personnos. we still need go through the receiptnos.
    showProgress("Hämtar besöken från kassan...",5);

    mKatRegEntries.clear(); // prepare ourselves for the PAS retrieval

// cook our own bobbytable: select from both PAS and PAS_Billing simultaneously
    TCBobbyTables stPASAndPASBilling("PAS as pa,PAS_Billing as pb");

    db.select("pa.PatientID,StartDatetime,ReceiptNumber,VisitTypeID",stPASAndPASBilling,
              QString("IsRetail = 0 and IsCancelled = 0 and CompanyID = %1 and SavedAtCareUnitID = %2 and "
                      "StartDatetime >= '%3' and StartDatetime < '%4' and pa.PatientID = pb.PatientID and pa.DocumentID = pb.DocumentID").
                      arg(nCompanyCode).arg(nCareUnitID).arg(TWUtils::toISODate(dFrom),TWUtils::toISODate(dTo.addDays(1))),
    [this](DBRowStruct rs)
    {
    // don't forget this lambda is running on another thread (so for example user interaction is *verboten*)
        KatRegEntryStruct st;

    // TC2KatReg: create two entries (right eye and left eye) for a single PAS receipt
    // set them all to sponges/dormant state (PAS entries without matching journals are considered harmless, no casue for errors)
    // duplicate PAS entries (same patient + same visit day) are not marked as errors/duplicates (considered harmless as well)
        st.bOk            = true;  // optimistic default
        st.sError         = "";
        st.sWarning       = "";
        st.nPID           = rs.longlongValue("PatientID");

        st.bSpongeDOA     = true;  // will be unsponged when a matching eye journal is found
        st.dVisit         = rs.dateValue("StartDatetime");
        st.sVisitDate     = TWUtils::toISODate(st.dVisit);
        st.tVisit         = rs.timeValue("StartDatetime");
        st.sVisitDateTime = TWUtils::toISODateTimeNoSeconds(st.dVisit,st.tVisit);

        st.sReceiptNo     = rs.stringValue("ReceiptNumber");
        st.sVisitType     = rs.stringValue("VisitTypeID");

        st.sPersonNo      = "";
        st.cEye           = ccRightEye;  // no we're not forgetting the left eye (done in the clone further down)

    // is this a test (not a real) patient?
        if (TWTCSupport::isTestPatient(st.nPID))
        {
            st.bOk    = false;
            st.sError = "Testpatient (laddas ej upp till KatReg)";
        }

    // for KatReg we need a physical visit (visit type 0 or 1) so skip over distance contacts and absentees
        if (("0" != st.sVisitType) && ("1" != st.sVisitType))
            return;

    // save this entry in our map, create the key for this visit
        st.sKey = st.createKey(st.dVisit,st.nPID,st.cEye);

    // have a PAS entry for this visit date already? if so, just skip ove this one (considered harmless)
        if (mKatRegEntries.contains(st.sKey))
            return;

    // ok, create 2 new entries: first for the right eye
        mKatRegEntries.insert(st.sKey,st);

    // and then the left eye
        auto stLeft = st;           // clone everything from the right eye
        stLeft.cEye = ccLeftEye;    // except the eye flavor
        stLeft.sKey = stLeft.createKey(stLeft.dVisit,stLeft.nPID,stLeft.cEye);

    // the left eye should *not* be present in the map already
        if (mKatRegEntries.contains(stLeft.sKey))
            guruMeditation("Left eye PAS was already in the map (unexpected)");

        mKatRegEntries.insert(stLeft.sKey,stLeft);
    });

// --- done retrieving from PAS, next up is fetching the journals
// --------------------------------------------------------------
    restockAllTableWidgets();   // show what we've got so far (for this app: nothing yet)


// cannot count # of visits yet (there are all sponges for now)
// just step up the progressbar to 10%
    showProgress("Hämtar journaler för formulär 2B...",10);

// --- useful structs for temp storage of the journals until cooking time
// use one of these for every journal for a given patient
    struct SingleEyeJournalStruct
    {
        QDate dEvent;
        QTime tEvent;     // only used for display purposes (ShowBox and the cooking log)
        int   nDocumentID;
        int   nVersion;
        bool  bIs2B;      // true if this is a 2B flavored journal
        bool  bRightEye;  // one (pr both) can be true
        bool  bLeftEye;   //

    // store all the stuff found for a single journal
        QMap<QString, QSet<int>>   mTermSetsTermIDs; // key = QString("%1%2").arg(KeywordTermID).arg(ContainerTermID)
        QMap<QString, QStringList> mValuesTermIDs;   // (use nnNoContainer ("0") for keywords not having a container)
    };

// and one of these for each patient
    struct JournalsForPatientStruct
    {
    // use the PID as key for the maps (yes this means a maximum of 2 entries for a given patient: a right and/or left eye visit)
        PID_t nPID;

    // these visit dates also act as the right/left booleans (i.e. when date is nonempty = collect data for that eye)
        QDate d2BVisitRightEye;   // right eye visit day for the latest/youngest 2B journal
        QDate d2BVisitLeftEye;    // left eye visit day          - " -

    // a map of journals found this patient (valid for both eyes if both eye dates are set above) sorted on the document ids
        QMap<int,SingleEyeJournalStruct> mJournals; // (documentID is the key)
    };

// -------------------------------------------------
// collect all journals for all patients in this map
    QMap<PID_t,JournalsForPatientStruct> mJournalsAllPatients;


// ----------------------------------------------------------------------------------------
// ----------next up: get all the 2B journals for the specified date range ----------------
    QMap<QString,int> mPIDDocs;

// start by retrieving journals (patientids + documentids) within the date range and having the 2B template name
// bobbytables time: select from both CaseNotes and CaseNotes_Templates
    TCBobbyTables stNotesAndTemplate("CaseNotes as c,CaseNotes_Templates as t");

// (possibly we'll get get some extra hits because highest versions of a documentid could exist outside of the date range)
    db.select("distinct c.PatientID,c.DocumentID,t.CareUnitID",stNotesAndTemplate,
              QString("CreatedAtCareUnitID = %1 and EventDate >= '%2' and EventDate < '%3' and TemplateName like '%4' and "
              "c.PatientID = t.PatientID and c.DocumentID = t.DocumentID").
              arg(nCareUnitID).arg(TWUtils::toISODate(dFrom),TWUtils::toISODate(dTo.addDays(1)),sTemplateFor2B),
    [this,&mPIDDocs](DBRowStruct rs)
    {
    // make sure the CareUnitID from CaseNotes_Templates is the expected one, if not fubar out
        if (nCareUnitID != rs.intValue("CareUnitID"))
            guruMeditation("Got an unexpected or bad careunit id from CaseNotes_Templates :-(");

    // store the patientid, documentid pair as the key and store a 0 as the value (version no.)
        mPIDDocs.insert(TWUtils::stringList2CommaList({rs.stringValue("PatientID"),rs.stringValue("DocumentID")}), 0);
    });

// --------- step thru and retrieve the date and highest version no. for each PatientID, DocumentID pair found above ----------
    int nNo    = 0; // for showProgress()
    int nTotal = mPIDDocs.count();

// step thru and check the version nos. (if the highest version no. is within the specified date range)
    for (auto sKey : mPIDDocs.keys())
    {
    // count 'em
        ++nNo;

    // show the user we're making some progress (start at total progress = 10% and end at 30%)
        int nProgressStep = 20; // i.e up to 30%
        showProgress(QString("Hämtar journaler för formulär 2B (%1%)... ").arg((nNo * 100) / nTotal),10 + ((nNo * nProgressStep) / nTotal));   // (nTotal is always > 0)

    // decompose the mPIDDOCs key back into a PID and a documentID
        auto sl = TWUtils::commaList2StringList(sKey);
        if (sl.count() != 2)
            fubar("Something is seriously wrong with the PIDDocs map");
        PID_t nPID      = sl[0].toLongLong();
        int nDocumentID = sl[1].toInt();

        QDate dEvent;   // to be set by the lambda
        QTime tEvent;   //

    // look for the highest version no. (i.e. the current one) for this DocumentID
        db.select("top 1 Version,EventDate,EventTime",stTCCorral.CaseNotes,
                  QString("CreatedAtCareUnitID = %1 and PatientID = %2 and DocumentID = %3").
                  arg(nCareUnitID).arg(nPID).arg(nDocumentID),"Version desc",
        [this,sKey,&dEvent,&tEvent,&mPIDDocs](DBRowStruct rs)
        {
        // get the date and time for the highest version of this documentID
            dEvent = rs.dateValue("EventDate");
            tEvent = rs.timeValue("EventTime");

        // check the date, within our range?
            if ((dEvent >= dFrom) && (dEvent < dTo.addDays(1)))
            {
                int nVersion = rs.intValue("Version");
                if (nVersion < 1)
                    guruMeditation("Got a bad version number (lower than 1)");

                mPIDDocs[sKey] = nVersion;  // yes, refresh/update the version no.
            }
        },eDBRowsExpectExactlyOne);

    // so is this document no. still inside our date range now that we've looked for the correct/highest version no.?
        int nVersion = mPIDDocs[sKey];
        if (0 == nVersion)
            continue;   // no (just the placeholder 0) so skip over this journal

    // yes still within, but is it a test (not a real) patient?
        if (TWTCSupport::isTestPatient(nPID))
            continue;   // yes, skip over him/her

    // ok got one patient for you, store it in the map
        JournalsForPatientStruct st; // guessing we're the first documentID/journal found for this patient: create a new struct
        st.nPID               = nPID;
        st.d2BVisitRightEye   = QDate();  // set these to empty dates for now
        st.d2BVisitLeftEye    = QDate();  //

    // is there an existing struct in the map already? i.e. have a previous 2B journal for the same patient?
        if (mJournalsAllPatients.contains(nPID))
            st = mJournalsAllPatients[nPID];  // if we're not first: retrieve the existing struct

    // regardless, append this journal to the map of journals for this patient
        SingleEyeJournalStruct stEye;
        stEye.dEvent      = dEvent;
        stEye.tEvent      = tEvent;
        stEye.nDocumentID = nDocumentID;
        stEye.nVersion    = nVersion;
        stEye.bIs2B       = true;   // because we've only retrieved 2B flavored journals so far, right?
        stEye.bRightEye   = false;  // (these will be set later)
        stEye.bLeftEye    = false;  //
        stEye.mTermSetsTermIDs.clear();
        stEye.mValuesTermIDs.clear();

    // this document id should not exist already in the map
        if (st.mJournals.contains(nDocumentID))
            guruMeditation("the map of journals already has the document id %d for the PID %d",nDocumentID,nPID);

    // ok append this journal to the patient's map
        st.mJournals.insert(nDocumentID, stEye);

    // and save in the map for all patients
        mJournalsAllPatients[st.nPID] = st;
    }

// --------------------------------------------------------------------------------------------------------------
// got all the 2B journals we need, next up is to look for the 2A journals (starting with March 1 the same year)
// 2A journals = all journals that are not of 2B flavor, simple right?
    QDate dFrom2A = QDate(dFrom.year(),03,01);  // prepare the range of dates for 2A retrievals: beginning date: March 1
    if (dFrom2A > dFrom)
        guruMeditation("dFrom was before March 1, not good");
    QDate dTo2A   = dTo;                      // we'll use the same end date as the 2B interval (so the 2A interval usually will be bigger than the 2B interval)

    nNo    = 0;   // for showProgress() (starting at total progress = 30%)
    nTotal = mJournalsAllPatients.count();

    for (auto &st : mJournalsAllPatients)  // need R/W because we'll be appending the 2A journals
    {
    // count 'em
        ++nNo;

    // and show progress
        int nProgressStep = 10; // i.e. from 30% up to 40%
        showProgress(QString("Hämtar journaler för formulär 2A (%1%)... ").arg((nNo * 100) / nTotal),30 + ((nNo * nProgressStep) / nTotal));   // (nTotal is always > 0)

    // we're searching one patient at the time for all the non-2B journals,
        QSet<int> setDocumentIDs2A;   // temp. store the document ids we find here

        db.select("distinct c.DocumentID,t.CareUnitID",stNotesAndTemplate,    // reuse the same bobbytables as for the 2B retrievals above
                  QString("CreatedAtCareUnitID = %1 and EventDate >= '%2' and EventDate < '%3' and c.PatientID = %4 and TemplateName not like '%5' and "  // note: 'not like'
                          "c.PatientID = t.PatientID and c.DocumentID = t.DocumentID").
                  arg(nCareUnitID).arg(TWUtils::toISODate(dFrom2A),TWUtils::toISODate(dTo2A.addDays(1))).arg(st.nPID).arg(sTemplateFor2B),
        [this,&setDocumentIDs2A](DBRowStruct rs)
        {
        // make sure the CareUnitID from CaseNotes_Templates is the expected one, if not fubar out
            if (nCareUnitID != rs.intValue("CareUnitID"))
                guruMeditation("Got an unexpected or bad careunit id from CaseNotes_Templates during 2A lookup :-(");

            setDocumentIDs2A += rs.intValue("DocumentID");
        });

    // step thru the set of document ids for this patient and get the date for the highest/valid version no. (you know the drill)
        for (auto nDocumentID : setDocumentIDs2A)
        {
            QDate dEvent;      // to be set by the lambda
            QTime tEvent;      //

            int nVersion = 0;  // pessimistic default (means the highest version is outside the date range)

        // look for the highest version no. (i.e. the current one) for this DocumentID
            db.select("top 1 Version,EventDate,EventTime",stTCCorral.CaseNotes,
                QString("CreatedAtCareUnitID = %1 and PatientID = %2 and DocumentID = %3").
                arg(nCareUnitID).arg(st.nPID).arg(nDocumentID),"Version desc",
            [dFrom2A,dTo2A,&dEvent,&tEvent,&nVersion](DBRowStruct rs)
            {
            // is the highest version of this documentID within the selected date range?
                dEvent = rs.dateValue("EventDate");
                tEvent = rs.timeValue("EventTime");

                if ((dEvent >= dFrom2A) && (dEvent < dTo2A.addDays(1)))
                    nVersion = rs.intValue("Version"); // yes, within the date range to remember the version
            },eDBRowsExpectExactlyOne);

        // is this document still inside our date range now that we've found the correct/highest version no.?
            if (0 == nVersion)
                continue;   // no, so skip this journal

        // ok got one more 2A journal to add
            SingleEyeJournalStruct stEye;
            stEye.dEvent      = dEvent;
            stEye.tEvent      = tEvent;
            stEye.nDocumentID = nDocumentID;
            stEye.nVersion    = nVersion;
            stEye.bIs2B       = false;   // because this time we've retrieved the non-2B flavored journals (i.e. 2A journals)

            stEye.bRightEye = stEye.bLeftEye = false;   // default these to false for now
            stEye.mTermSetsTermIDs.clear();
            stEye.mValuesTermIDs.clear();

        // this document id shouldn't exist already
            if (st.mJournals.contains(nDocumentID))
                guruMeditation("the map of journals already has the document id %d for the PID %d",nDocumentID,st.nPID);

        // ok append this journal to this patient's map
            st.mJournals.insert(nDocumentID, stEye);
        }
    }


// ----------------------------------------------------------------------------------------
// have the 2A and 2B journals we need, start collecting the data, first up: eye flavor(s)
// use this lambda for making the termId+containerId key to the journal data maps
    auto makeMapKey = [] (int nKeywordTermID, int nContainerTermID = nnNoContainer) { return QString("%1%2").arg(nKeywordTermID).arg(nContainerTermID); };

    nNo    = 0;   // for showProgress() (starting at total progress = 40%)
    nTotal = mJournalsAllPatients.count();

    for (auto &st : mJournalsAllPatients)  // need R/W because we will be setting lots of things
    {
    // count 'em and show the progress stepping through the patients/PIDs
        ++nNo;
        int nProgressStep = 55; // from 40% up to 95%
        showProgress(QString("Hämtar KatReg data för uppföljning (%1%)...").arg((nNo * 100) / nTotal),50 + ((nNo * nProgressStep) / nTotal));  // (nTotal is always > 0)

    // first get akk the value term ids (a set of term numbers) for this patient
    // establish right/left eye flavor(s) and the most recent visit dates for the 2B journals
        for (auto &stEye : st.mJournals)     // step thru the journals (need R/W to set lots of stuff)
        {
            stEye.bRightEye = false;         // pessimistic defaults (no eye flavors found)
            stEye.bLeftEye  = false;         //

            db.select("KeywordTermID,ValueTermID",stTCCorral.CaseNotes_ValueTerms,
                      QString("PatientID = %1 and DocumentID = %2 and Version = %3").
                      arg(st.nPID).arg(stEye.nDocumentID).arg(stEye.nVersion),
            [makeMapKey,&stEye](DBRowStruct rs)
            {
            // stuff whatever we get into our term sets (some of it will be discarded later)
                int nKeywordTermID = rs.intValue("KeywordTermID");
                int nValueTermID   = rs.intValue("ValueTermID");

            // sanity check the term id
                if (nKeywordTermID < 1)
                    fubar("bad keyword termid (%d)",nKeywordTermID);

            // append this value termid to our keyword set map for this journal (no containers used here, plain vanilla)
                stEye.mTermSetsTermIDs[makeMapKey(nKeywordTermID)] += nValueTermID;
            });

        // in thw stuff we retrieved for this journal, did you see any right or left eye keyword terms?
            if (stEye.mTermSetsTermIDs.contains(makeMapKey(nnRightOrLeftEyeKeywordTermID)))
            {
            // yes, get the value(s) for it
                auto t = stEye.mTermSetsTermIDs[makeMapKey(nnRightOrLeftEyeKeywordTermID)];

            // set the eye flavor booleans for this journal (single pass, courtesy of QMap/QSet's deduplication)
                stEye.bRightEye = (t.contains(nnRightTermID));
                stEye.bLeftEye  = (t.contains(nnLeftTermID ));

            // --- perhaps also set the right/left 2B visit dates for this patient? (see more than one? use the latest dates)
            // need 2B journals to establish those dates, so skip over any 2As
                if (!stEye.bIs2B)
                    continue;   // 2A: next journal please

            // sanity check: make sure the date is within the kosher date range
                if (stEye.dEvent < dFrom)
                    guruMeditation("got a 2B visit date before dFrom (not good)");

                if (stEye.dEvent > dTo  )
                    guruMeditation("got a visit date later than dTo (not good)");

            // this journal applicable for the right eye?
                if (stEye.bRightEye)
                // use this journal's date as the 2B visit date for the right eye?
                // yes if it's later than the current one (or this is the first date found)
                    if (st.d2BVisitRightEye < stEye.dEvent)
                        st.d2BVisitRightEye = stEye.dEvent;

            // and/or the left?
                if (stEye.bLeftEye)
                // same here: set this date if it's newer than the current date (amd empty dates are always considered older)
                    if (st.d2BVisitLeftEye < stEye.dEvent)
                        st.d2BVisitLeftEye = stEye.dEvent;
            }
        }

    // ======= any right- or left eye flavor(s)/dates for this patient? =========
        if ((!st.d2BVisitRightEye.isValid()) && (!st.d2BVisitLeftEye.isValid()))
            continue;   // no, so fugget about him/her


    // --------------------------------------------------------------------------------------------------------
    // next up: get the operation dates, we need one for each patient and eye
    // the operation dates have to be in March and before the 2B date for the visit to be eligble/processed
        QDate dOperationRightEye; // dates of operation for the current patient
        QDate dOperationLeftEye;  // need 2 dates if we have entries for both right and left eyese

        for (auto &stEye : st.mJournals)     // need R/W to set the op. dates
        {
            int nKeywordTermID = nnDateOperationKeywordTermID;   // we're only interested in this keyword termid
            auto dFirst        = QDate(dFrom.year(),03,01);      // ocurring in this month
            auto dLast         = QDate(dFrom.year(),03,31);

            db.select("Date",stTCCorral.CaseNotes_DateTimes,
                      QString("PatientID = %1 and DocumentID = %2 and Version = %3 and KeywordTermID = %4").
                      arg(st.nPID).arg(stEye.nDocumentID).arg(stEye.nVersion).arg(nKeywordTermID),
            [&dOperationRightEye,&dOperationLeftEye,nKeywordTermID,dFirst,dLast,makeMapKey,&st,&stEye](DBRowStruct rs)
            {
            // get the date and check it's valid and within the month of March (in the same year as the visit date)
                auto dOperation = rs.dateValue("Date");
                if (!dOperation.isValid())
                    return;

                if (dOperation < dFirst)
                    return;

                if (dOperation > dLast )
                    return;

            // ok op. date, store it in our map (used later for diagnostic/show purposes)
                stEye.mValuesTermIDs[makeMapKey(nKeywordTermID)].append(TWUtils::toISODate(dOperation));

            // also custom process the operation dates right now (we might need to toss patients and/or journals)
            // is this a right-eye flavored journal?
                if (stEye.bRightEye)
                    if (st.d2BVisitRightEye.isValid())         // require an established 2B visit day
                        if (dOperation <= st.d2BVisitRightEye) // and that the operation takes place before or on that 2B visit date
                            if (dOperationRightEye < dOperation)
                            // update it only if this operation date is newer/younger (or it is the first one)
                                dOperationRightEye = dOperation;

            // and/or the left flavor?
                if (stEye.bLeftEye)
                    if (st.d2BVisitLeftEye.isValid())          // require an established 2B visit day
                        if (dOperation <= st.d2BVisitLeftEye)  // and that the operation takes place before or on that same date
                            if (dOperationLeftEye < dOperation)
                            // update it only if this operation date is newer/younger (or it is the first one)
                                dOperationLeftEye = dOperation;
            });
        }

    // --- so did we get any valid operation dates set for this patient?
        if (st.d2BVisitRightEye.isValid())
            if (!dOperationRightEye.isValid())
            // no valid op. date for the right eye? if so, reset the visit date (means no XML file for you)
                st.d2BVisitRightEye = QDate();

        if (st.d2BVisitLeftEye.isValid())
            if (!dOperationLeftEye.isValid())
            // no valid op. date for the left eye? if so, same drill here
                st.d2BVisitLeftEye = QDate();

    // still got at least one valid 2B visit day for this patient?
        if ((!st.d2BVisitRightEye.isValid()) && (!st.d2BVisitLeftEye.isValid()))
            continue;   // no none left, next patient please


    // --- tossing time: drop any journals that are dated before the operation date for that eye
    // (journals written before the operation date are not deemed worthy of our attention)
        QSet<int> setEvictedDocumentIDs;
        for (auto stEye : st.mJournals)     // step thru to amass the victims
        {
            if (st.d2BVisitRightEye.isValid())
            // right eye active for this journal?
                if (stEye.bRightEye)
                    if (stEye.dEvent < dOperationRightEye)
                        setEvictedDocumentIDs += stEye.nDocumentID;

            if (st.d2BVisitLeftEye.isValid())
            // left eye active for this journal?
                if (stEye.bLeftEye)
                    if (stEye.dEvent < dOperationLeftEye)
                        setEvictedDocumentIDs += stEye.nDocumentID;
        }

    // sayonara to you
        for (auto nID : setEvictedDocumentIDs)
            st.mJournals.remove(nID);


    // ---------------------------------------------------------------------------------------
    // next up: retrieve measurements and notes by stepping thru the journals for this patient
        for (auto &stEye : st.mJournals)     // need R/W to store stuff into the mValues map
        {
        // note: the convert(varchar,Value) is for circumventing https://bugreports.qt.io/browse/QTBUG-108912
            db.select("KeywordTermID,convert(varchar,Value) as Value",stTCCorral.CaseNotes_Measurements,
                      QString("PatientID = %1 and DocumentID = %2 and Version = %3").
                      arg(st.nPID).arg(stEye.nDocumentID).arg(stEye.nVersion),
            [makeMapKey,&stEye](DBRowStruct rs)
            {
                auto nKeywordTermID = rs.intValue("KeywordTermID");
                auto sValue         = rs.stringValueTrimmed("Value"); // use ...Trimmed() to toss possible white space junk

            // sanity check the term id
                if (nKeywordTermID < 1)
                    fubar("bad keyword termid (%d)",nKeywordTermID);

            // append this value to the value stringlists for this journal
                stEye.mValuesTermIDs[makeMapKey(nKeywordTermID)].append(sValue);
            });

        // for the Container tables, we don't care about Row numbers (extra row values we'll just treat as new values)
        // first get our stuff from the CaseNotes_ContainerMeasurements table
            db.select("ColumnHeadingTermID,KeywordTermID,convert(varchar,Value) as Value",stTCCorral.CaseNotes_ContainerMeasurements,
                      QString("PatientID = %1 and DocumentID = %2 and Version = %3").
                      arg(st.nPID).arg(stEye.nDocumentID).arg(stEye.nVersion),
            [makeMapKey,&stEye](DBRowStruct rs)
            {
                auto nKeywordTermID   = rs.intValue("ColumnHeadingTermID");
                auto nContainerTermID = rs.intValue("KeywordTermID");
                auto sValue           = rs.stringValueTrimmed("Value"); // use ...Trimmed() to toss possible white space junk

            // sanity check the term ids
                if (nKeywordTermID   < 1)
                    fubar("bad keyword termid (%d)",nKeywordTermID);

                if (nContainerTermID < 1)
                    fubar("bad container termid (%d)",nContainerTermID);

            // append this value to the list for this journal (note: we're dealing with container ids here as well)
                stEye.mValuesTermIDs[makeMapKey(nKeywordTermID,nContainerTermID)].append(sValue);
            });

        // finally the CaseNotes_ContainerNotes (note: this table has no float/value columns, so a convert() is not needed)
            db.select("ColumnHeadingTermID,KeywordTermID,Note",stTCCorral.CaseNotes_ContainerNotes,
                      QString("PatientID = %1 and DocumentID = %2 and Version = %3").
                      arg(st.nPID).arg(stEye.nDocumentID).arg(stEye.nVersion),
            [makeMapKey,&stEye](DBRowStruct rs)
            {
                auto nKeywordTermID   = rs.intValue("ColumnHeadingTermID");
                auto nContainerTermID = rs.intValue("KeywordTermID");
                auto sNote            = rs.stringValueTrimmed("Note"); // use ...Trimmed() to toss possible white space junk

            // sanity check the term ids
                if (nKeywordTermID   < 1)
                    fubar("bad keyword termid (%d)",nKeywordTermID);

                if (nContainerTermID < 1)
                    fubar("bad container termid (%d)",nContainerTermID);

            // append this note to the list for this journal (yes we reuse the same map as for the values above)
                stEye.mValuesTermIDs[makeMapKey(nKeywordTermID,nContainerTermID)].append(sNote);
            });
        }


    // ------------------------------------------------------------------------------------------
    // by now we should have all the stuff needed for this patient, it's cooking/processing time
    // use this lambda for cooking and saving a single eye
    // 2024-06-13: add a list of journal visit dates (for showing in the cooking log)
        auto cookAndSave = [this] (PID_t nPID, QDate d, QChar cEye, KatRegTermSetMap mTermSets, KatRegQListMap mValues, QStringList slJournalDatesAndTimes)
        {
        // create a key for a (possible) matching PAS entry
            QString sKatRegKey = KatRegEntryStruct::createKey(d,nPID,cEye);

        // have a matching PAS entry?
            if (!mKatRegEntries.contains(sKatRegKey))
            {
            // no, instead: create a new error entry
                KatRegEntryStruct st;
                st.bOk            = false;
                st.sError         = "Saknar registrering i kassan";
                st.sWarning       = "";
                st.nPID           = nPID;
                st.bSpongeDOA     = false;
                st.dVisit         = d;
                st.sVisitDate     = TWUtils::toISODate(st.dVisit);
                st.tVisit         = tNotFoundInPAS; // "magic" time (i.e. time for errors :~) 23:59
                st.sVisitDateTime = TWUtils::toISODateTimeNoSeconds(st.dVisit,st.tVisit);
                st.sReceiptNo     = "";
                st.sVisitType     = "";
                st.sPersonNo      = "";
                st.cEye           = cEye;

                st.sKey           = st.createKeyForErrorEntries(st.dVisit,st.nPID,st.cEye);

            // file it as an error entry and we're done
                mKatRegEntries.insert(st.sKey,st);
                return;
            }

        // check that the PAS entry is still a sponge (single pass so that should always be true)
            if (!mKatRegEntries[sKatRegKey].bSpongeDOA)
                fubar(QString("PAS entry '%1' is not a sponge").arg(sKatRegKey));

        // if we're not on Windows, add this entry to the cooking log?
        #if !defined(Q_OS_WIN)
            if (tfwLog.isOpen())
            {
                tfwLog.ts << "\n";
                tfwLog.ts << "### Cooking 2B journal " << mKatRegEntries[sKatRegKey].sVisitDateTime << ", " << mKatRegEntries[sKatRegKey].sPersonNo << ", eye: " << cEye;
                tfwLog.ts << ", PID: " << TWTCSupport::PID2String(mKatRegEntries[sKatRegKey].nPID) << ", receiptnr: " << mKatRegEntries[sKatRegKey].sReceiptNo << "\n";

            // show the dates/times of the journals we've collected from
                tfwLog.ts << QString("Katreg data collected from %1 journals: ").arg(slJournalDatesAndTimes.count());
                for (auto s : slJournalDatesAndTimes)
                    tfwLog.ts << " \"" << s << "\"";

                tfwLog.ts << "\n";
            }
        #endif

        // unsponge, cook and save
            mKatRegEntries[sKatRegKey].bSpongeDOA = false;

            mKatRegEntries[sKatRegKey].cEye       = cEye;
            mKatRegEntries[sKatRegKey].mTermSets  = mTermSets;
            mKatRegEntries[sKatRegKey].mValues    = mValues;

            mKatRegEntries[sKatRegKey].mAllCooked = cookEntry(mKatRegEntries[sKatRegKey]);

        // got any warnings in the cooking process? if so copy the 1st warning into "master" chap
            for (auto e : lKatRegFields)
                if (!mKatRegEntries[sKatRegKey].mAllCooked[e].sWarning.isEmpty())
                {
                // is there a warning text in the master already? (don't clobber existing)
                    if (mKatRegEntries[sKatRegKey].sWarning.isEmpty())
                        mKatRegEntries[sKatRegKey].sWarning = mKatRegEntries[sKatRegKey].mAllCooked[e].sWarning;
                }

        // and any cooking errors? if so copy the 1st error text into the "master" text (if it's currently empty)
            for (auto e : lKatRegFields)
                if (!mKatRegEntries[sKatRegKey].mAllCooked[e].sError.isEmpty())
                {
                // turn it off (this visit is not ok)
                    mKatRegEntries[sKatRegKey].bOk = false;

                // is there any error text(s) in the master/top copy? If not, migrate this error into it
                // (i.e. if there are several cooking errors, only copy the first found into the master)
                    if (mKatRegEntries[sKatRegKey].sError.isEmpty())
                        mKatRegEntries[sKatRegKey].sError = mKatRegEntries[sKatRegKey].mAllCooked[e].sError;
                }

        // that's all for one of this patient's eyes :-)
        };


    // ============================================
    // collecting data for the right eye?
        if (st.d2BVisitRightEye.isValid())
        {
        // collect the data into these cooking utensils/maps
            KatRegTermSetMap mTermSets;
            KatRegQListMap   mValues;

        // for the cooking log, collect the document ids for the journals we read from
            QSet<int> setDocumentIDs;

        // step thru the journals found for this patient, looking for right eye flavored ones
            for (auto stEye : st.mJournals)
            // only right eye flavors please
                if (stEye.bRightEye)
                {
                    QChar cEye = ccRightEye;

                // step thru the sets of value terms and collect those we're interested in
                    for (auto sTermIDs : stEye.mTermSetsTermIDs.keys())
                        if (hasKatRegField(stEye.bIs2B, cEye, sTermIDs))
                        {
                            mTermSets[getKatRegField(stEye.bIs2B, cEye, sTermIDs)] += stEye.mTermSetsTermIDs[sTermIDs];
                            setDocumentIDs += stEye.nDocumentID;  // remember the document id for this journal
                        }

                // add in the values (as strings) from the values map
                    for (auto sTermIDs : stEye.mValuesTermIDs.keys())
                        if (hasKatRegField(stEye.bIs2B, cEye, sTermIDs))
                        {
                            mValues[getKatRegField(stEye.bIs2B, cEye, sTermIDs)].append(stEye.mValuesTermIDs[sTermIDs]);
                            setDocumentIDs += stEye.nDocumentID;  // remember the document id for this journal
                        }
                }

        // do a 2nd pass looking for journals without eye flavors, they might have stuff applicable to the right eye
            for (auto stEye : st.mJournals)
            // only gender neutrals please
                if ((!stEye.bRightEye) && (!stEye.bLeftEye))
                {
                // note: collect only for those katreg fields that do not already contain a term set (i.e. no overwriting/appending)
                    for (auto sTermIDs : stEye.mTermSetsTermIDs.keys())
                        if (hasKatRegField(stEye.bIs2B, sTermIDs))
                        // ok journal has this KatReg field but can it be used only for the right eye?
                            if (isForRightEyeOnly(getKatRegField(stEye.bIs2B, sTermIDs)))
                            // and only copy it if that field is currently empty in our set
                                if (mTermSets[getKatRegField(stEye.bIs2B, sTermIDs)].isEmpty())
                                {
                                    mTermSets[getKatRegField(stEye.bIs2B, sTermIDs)] += stEye.mTermSetsTermIDs[sTermIDs];
                                    setDocumentIDs += stEye.nDocumentID;  // remember the document id for this journal
                                }

                // add in the values (as strings) from the values map, same idea here: collect only for those katreg fields currently empty
                    for (auto sTermIDs : stEye.mValuesTermIDs.keys())
                        if (hasKatRegField(stEye.bIs2B, sTermIDs))
                        // and require that those katreg fields can only be used for the right eye
                            if (isForRightEyeOnly(getKatRegField(stEye.bIs2B, sTermIDs)))
                                if (mValues[getKatRegField(stEye.bIs2B, sTermIDs)].isEmpty())
                                {
                                    mValues[getKatRegField(stEye.bIs2B, sTermIDs)].append(stEye.mValuesTermIDs[sTermIDs]);
                                    setDocumentIDs += stEye.nDocumentID;  // remember the document id for this journal
                                }
                }

        // *** deduplication time: mTermSets are good (we get automagic deduplication courtesy QSet)
        // but mValues can use some right now
            for (auto eKey : mValues.keys())
                mValues[eKey].removeDuplicates();

        // prepare a string list of the dates/times for the journals we read (only for diagnostic purposes)
            QStringList slJournalDatesAndTimes;
            for (auto stEye : st.mJournals)
                if (setDocumentIDs.contains(stEye.nDocumentID))
                    slJournalDatesAndTimes += TWUtils::toISODateTimeNoSeconds(stEye.dEvent, stEye.tEvent);

        // as an extra nicety, sort those dates/times
            slJournalDatesAndTimes.sort();

        // ok done collecing, cooking and saving time:
            cookAndSave(st.nPID ,st.d2BVisitRightEye, ccRightEye, mTermSets, mValues, slJournalDatesAndTimes);
        }

    // ============================================
    // or perhaps the left eye?
        if (st.d2BVisitLeftEye.isValid())
        {
        // collect into these cooking utensils/maps
            KatRegTermSetMap mTermSets;
            KatRegQListMap   mValues;

        // for the cooking log, collect the document ids for the journals we read from
            QSet<int> setDocumentIDs;

        // step thru the journals found for this patient, looking for left eye flavored ones
            for (auto stEye : st.mJournals)
            // left eyes please
                if (stEye.bLeftEye)
                {
                    QChar cEye = ccLeftEye;

                // step thru the sets of value terms and collect those we're interested in
                    for (auto sTermIDs : stEye.mTermSetsTermIDs.keys())
                        if (hasKatRegField(stEye.bIs2B, cEye, sTermIDs))
                        {
                            mTermSets[getKatRegField(stEye.bIs2B, cEye, sTermIDs)] += stEye.mTermSetsTermIDs[sTermIDs];
                            setDocumentIDs += stEye.nDocumentID;  // remember the document id for this journal
                        }

                // add in the values (as strings) from the values map
                    for (auto sTermIDs : stEye.mValuesTermIDs.keys())
                        if (hasKatRegField(stEye.bIs2B, cEye, sTermIDs))
                        {
                            mValues[getKatRegField(stEye.bIs2B, cEye, sTermIDs)].append(stEye.mValuesTermIDs[sTermIDs]);
                            setDocumentIDs += stEye.nDocumentID;  // remember the document id for this journal
                        }
                }

        // do a 2nd pass looking for journals without eye flavors, they might have stuff applicable to the left eye
            for (auto stEye : st.mJournals)
            // no flavors please
                if ((!stEye.bRightEye) && (!stEye.bLeftEye))
                {
                // note: collect only to those katreg fields that do not already contain a term set (i.e. no overwriting/appending)
                    for (auto sTermIDs : stEye.mTermSetsTermIDs.keys())
                        if (hasKatRegField(stEye.bIs2B, sTermIDs))
                        // ok journal has this KatReg field but can it be used only for the left eye?
                            if (isForLeftEyeOnly(getKatRegField(stEye.bIs2B, sTermIDs)))
                            // and only copy it if that field is currently empty in our set
                                if (mTermSets[getKatRegField(stEye.bIs2B, sTermIDs)].isEmpty())
                                {
                                    mTermSets[getKatRegField(stEye.bIs2B, sTermIDs)] += stEye.mTermSetsTermIDs[sTermIDs];
                                    setDocumentIDs += stEye.nDocumentID;  // remember the document id for this journal
                                }

                // add in the values (as strings) from the values map, same idea here: collect only for those katreg fields currently empty
                    for (auto sTermIDs : stEye.mValuesTermIDs.keys())
                        if (hasKatRegField(stEye.bIs2B, sTermIDs))
                        // and require that those katreg fields can only be used for the left eye
                            if (isForLeftEyeOnly(getKatRegField(stEye.bIs2B, sTermIDs)))
                                if (mValues[getKatRegField(stEye.bIs2B, sTermIDs)].isEmpty())
                                {
                                    mValues[getKatRegField(stEye.bIs2B, sTermIDs)].append(stEye.mValuesTermIDs[sTermIDs]);
                                    setDocumentIDs += stEye.nDocumentID;  // remember the document id for this journal
                                }
                }

        // *** deduplication time: mTermSets are good (we get automagic deduplication courtesy QSet)
        // but mValues can use some right now
            for (auto eKey : mValues.keys())
                mValues[eKey].removeDuplicates();

        // prepare a string list of the dates/times for the journals we read
            QStringList slJournalDatesAndTimes;
            for (auto stEye : st.mJournals)
                if (setDocumentIDs.contains(stEye.nDocumentID))
                    slJournalDatesAndTimes += TWUtils::toISODateTimeNoSeconds(stEye.dEvent, stEye.tEvent);

        // as an extra nicety, sort the dates/times
            slJournalDatesAndTimes.sort();

        // almost done: just the cooking left
            cookAndSave(st.nPID, st.d2BVisitLeftEye, ccLeftEye, mTermSets, mValues, slJournalDatesAndTimes);
        }

    // have at least one new entry (or two), so now is a good time to refresh the personno.
        refreshPersonNo();

    // have more stuff to show so update the table widgets
        restockAllTableWidgets();

    // next patient please
    }

// ------------------------------------------------------------------------------------
// -- getting here: all journals done, time to write the XML file and call it a day ---
    if (scMain.bCrawlingDone)
    {
    // Samba is done, proceed to the burial
        QTimer::singleShot(nnShortTCDelay,this,[this] { burialAtSea(); } );
    }
    else
    {
    // Samba is slow today (when it's finished it will call burialAtSea(), trust me)
        bWaitingForSamba = true;
        showProgress("Väntar på Samba...");
    }
}

//----------------------------------------------------------------------------
// cookEntry
// step thru and parse/cook/check the KatReg terms and values for a visit
//
// 2024-05-30 First version
// 2024-06-03 Introduce decimal number conversion helper lambdas
// 2024-07-29 Also cook the operation dates
// 2024-08-12 Add a zerofill option to reformat()
//----------------------------------------------------------------------------
KatRegCookedMap MainWindow::cookEntry(KatRegEntryStruct st)
{
// helper lambdas for the decimal numbers tweaking
    auto checkNumber = [] (QString s, double lowerLimit, double upperLimit)
    {
    // return "" if all is good and well, else return a Swedish error message
        s.replace(",","."); // make sure we have US decimal points while checking

        bool bOk = false;   // pessimistic default
        auto d   = s.toDouble(&bOk);
        if (!bOk)
            return QString("felaktigt värde: '%1'").arg(s);

        if (lowerLimit > d)
            return QString("otillåtet värde (för lågt): '%1'").arg(s);

        if (upperLimit < d)
            return QString("otillåtet värde (för högt): '%1'").arg(s);

        return QString("");
    };

    auto reformatNumber = [] (QString s, int nNoOfDecimals, bool bZeroFill, bool bAddPlusSign = false)
    {
        s.replace(",","."); // make sure we have US decimal points while reformatting

        bool bOk = false;
        auto d   = s.toDouble(&bOk);
        if (!bOk)
            fubar(QString("toDouble() failed for '%1'").arg(s));

        QString r;

    // zerofilling today?
        if (bZeroFill)
        {
        // if so, reformat to 2 digits before the decimal point
            if (abs(d) > 99.99) // make sure we're within ]-100,100[
                fubar("reformat with zerofill only supports numbers < 100");
            r = QString("%1").arg(d,((d >= 0.0) ? 3 : 4) + nNoOfDecimals,'f',nNoOfDecimals,'0');
        }
        else
        // else do a simple reformat (no zerofilling fiddling)
            r = QString::number(d,'f',nNoOfDecimals);

    // if this is a positive number, should we prefix with a plus sign?
        if ((d >= 0.0) && (bAddPlusSign))
            r.prepend("+");

    // that's all but don't forget to replace the US decimal point (if any) to a Swedish comma
        return r.replace(".",",");
    };


// return this vwe when're done (hopefully with values, no warnings or errors)
    KatRegCookedMap vweCooked;

// step thru all the katreg fields
    for (auto e : lKatRegFields)
    {
        QString sFieldCaption = mKatRegFieldCaptions[e]; // this may serve you well (for errors and tooltips)

    // get # of terms and values specified for this field/enum
        int nNoOfTerms  = 0;
        int nNoOfValues = 0;

        if (st.mTermSets.contains(e))
            nNoOfTerms = st.mTermSets[e].count();

        if (st.mValues.contains(e))
            nNoOfValues = st.mValues[e].count();

    // and get the specs. for this katreg field (+ sanity check them)
        auto pTermSetSpec = mKatRegFieldParamSpecs[e].pTermSetSpec;
        auto pValueSpec   = mKatRegFieldParamSpecs[e].pValueSpec;

        int nNoOfTermsMin  = pTermSetSpec.first;
        int nNoOfTermsMax  = pTermSetSpec.second;
        if (nNoOfTermsMin > nNoOfTermsMax)
            guruMeditation("nNoOfTermsMin > nNofOfTermsMax, not good");
        int nNoOfValuesMin = pValueSpec.first;
        int nNoOfValuesMax = pValueSpec.second;
        if (nNoOfValuesMin > nNoOfValuesMax)
            guruMeditation("nNoOfValuesMin > nNofOfValuesMax, not good");

    // also check that max # value params are 0 or 1 (we don't support multiple values)
        if (nNoOfValuesMax > 1)
            guruMeditation("nNoOfValuesMax > 1 sorry not supported");

    // checking this field matches the spec. (note: deduplication has been done)
        bool bMissing       = ((nNoOfTermsMin > nNoOfTerms) || (nNoOfValuesMin > nNoOfValues));
        bool bTooManyTerms  = (nNoOfTermsMax  < nNoOfTerms);
        bool bTooManyValues = (nNoOfValuesMax < nNoOfValues);

    // is this field spec:ed to have both a term set and values? if so check missing param(s) again
        if ((pNone != pTermSetSpec) && (pNone != pValueSpec))
        // fpr this field, require both to be missing to issue an error
            bMissing = ((nNoOfTermsMin > nNoOfTerms) && (nNoOfValuesMin > nNoOfValues));
        // note: for the "too many" error, no need to recheck

    // is there a value or a termset for this katreg field?
        if (bMissing)
        {
        // no, set a missing error and skip to next field (no term or value to display anyway)
            vweCooked[e].sError = QString("%1 saknas").arg(sFieldCaption);
            continue;
        }

    // parse the termset(s)/value in a generic way into numeric and string values
        QString sValue = "";
        int     nValue = 0;

    // if we have terms, add 'em together on separate lines as the generic value
        if (nNoOfTerms > 0)
        {
            for (auto nTermID : st.mTermSets[e])
            {
            // get the caption for this term id
                QString sCaption = QString("Okänd TC Term (ID = %1)").arg(nTermID); // pessimistic default
                if (mTermSetCaptions.contains(nTermID))
                    sCaption = mTermSetCaptions[nTermID];

            // is this the first term?
                if (!sValue.isEmpty())
                // no, prefix with a "\n" (line separator)
                    sValue += "\n";

            // add 'em
                sValue += sCaption;
            }

        // but too many of them?
            if (bTooManyTerms)
            {
                sValue.replace("\n"," och ");  // replace those line separators to fit into a single error line

                vweCooked[e].sError = QString("%1 har för många olika värden: '%2'").arg(sFieldCaption,sValue);
                continue;
            }
        }

    // if we have value(s): bake them into a single string (note: more than one value ---> "too many" error)
        if (nNoOfValues > 0)
        {
        // the first one should always be there
            if (st.mValues[e].isEmpty())
                guruMeditation("Did not expect the mValues[e] to be empty");

            sValue = st.mValues[e].first(); // note: this will erase any term parsing above (for fields that have both)
            nValue = sValue.toInt();        // try to convert to an integer value (immaterial if it fails)

        // any additional values are error chaps, stuff them anyway into the string (for a nice error display)
            for (int i = 1; (i < st.mValues[e].count()); ++i)  // yes skip over the first one
                sValue += " " + st.mValues[e][i];

        // too many values?
            if (bTooManyValues)
            {
                vweCooked[e].sError = QString("%1 har för många olika värden: '%2'").arg(sFieldCaption,sValue);
                continue;
            }
        }


    // --------------------------------------------------------------------
    // --- generic cooking done, time for KatReg field specific cooking ---
    // start with eye flavor in 2B
        if (KatRegFields::eRightOrLeftEye == e)
        {
        // check the eye terms match (they should)
            if (ccRightEye == st.cEye)
                if (!st.mTermSets[e].contains(nnRightTermID))
                    guruMeditation("right eye but no right eye term seen");

            if (ccLeftEye == st.cEye)
                if (!st.mTermSets[e].contains(nnLeftTermID))
                    guruMeditation("left eye but no left eye term seen");

            vweCooked[e].sValue   = (ccRightEye == st.cEye) ? ccRightEye : ccLeftEye;
            vweCooked[e].sToolTip = (ccRightEye == st.cEye) ? "Höger öga" : "Vänster öga";
        }

    // postop checks in 2B
        if (KatRegFields::ePostOpChecks == e)
        {
        // set an empty default (this is an optional field after all)
            QString sText = "";

        // if we have a value: cook the value from a number to a text (can only be one value)
            if (!sValue.isEmpty())
            {
                if (nnEfterkontrollPagarValue           == nValue)
                    sText = "Efterkontroller pågår";
                if (nnPatientmedverkanOtillrackligValue == nValue)
                    sText = "Patientens medverkan otillräcklig";

            // note: no support for "Patient avliden" (always set to false in the XML file)
            }

        // save what we got (probably not so much)
            vweCooked[e].sValue = sText;
        }

    // visus and refract xxx are optional fields (i.e. empty ones or those with "harmless" errors are allowed and ignored)
    // visus in 2B (right and left) is a bit tricky (can have both terms and a value)
        if ((KatRegFields::eVisusRight == e) || (KatRegFields::eVisusLeft == e))
        {
        // an optional value, default to empty in the cooked map
            vweCooked[e].sValue = "";

        // if we have a value, make sure it uses Swedish style decimal point (",")
            sValue.replace(".",",");

        // check for a term #
            QString sValueFromTerm = "";
            if (!st.mTermSets[e].isEmpty())
            {
            // use Swedish decimal point for cooking, for the XML later we'll change to US style points
            // (should only be one term in the set so just do simple if statements)
                auto s = st.mTermSets[e];
                if (s.contains(nnFR4mTermID))      sValueFromTerm = "0,08";
                if (s.contains(nnFR3mTermID))      sValueFromTerm = "0,06";
                if (s.contains(nnFR2mTermID))      sValueFromTerm = "0,04";
                if (s.contains(nnFR1mTermID))      sValueFromTerm = "0,02";
                if (s.contains(nnHRPLPTermID))     sValueFromTerm = "0,01";
                if (s.contains(nnAmaurosisTermID)) sValueFromTerm = "0,00";
            }

        // have both? then they better be the same number
            if ((!sValueFromTerm.isEmpty()) && (!sValue.isEmpty()))
                if (sValueFromTerm != sValue)
                {
                // sorry they're different, error out and skip to next field
                    vweCooked[e].sError = QString("%1 har för många olika värden: '%2 och %3'").arg(sFieldCaption,sValueFromTerm,sValue);
                    continue;
                }

        // have a term? use it by copying it into the value (which should be empty or contain the same number)
            if (!sValueFromTerm.isEmpty())
                sValue = sValueFromTerm;

        // check and reformat/pretty print
            auto sError = checkNumber(sValue,0.00,2.00);  // note: we'll check for 0.00 (special value) when writing the XML
            if (sError.isEmpty())
            // ok looks good, reformat/pretty print with 2 decimals (no zerofilling)
                vweCooked[e].sValue = reformatNumber(sValue,2,false);
        }

    // refraction in 2B and 2A, right or left spherical (allowed range [-20.00, +15.00] and prefix with "+" for positive values)
        if ((KatRegFields::eRefractionRightSphere == e)      || (KatRegFields::eRefractionLeftSphere == e) ||
            (KatRegFields::ePreopRefractionRightSphere == e) || (KatRegFields::ePreopRefractionLeftSphere == e))
        {
        // an optional value, default to empty in the cooked map
            vweCooked[e].sValue = "";

        // check, reformat and pretty print
            auto sError = checkNumber(sValue,-20.00,15.00);
            if (sError.isEmpty())
            // ok looks good, reformat/pretty print with 2 decimals and zerofilling + plus sign
                vweCooked[e].sValue = reformatNumber(sValue,2,true,true);    // true: prefix with a plus for > 0
        }

    // refraction in 2B and 2A, right or left cylinder (allowed range [-10.00, 00.00])
        if ((KatRegFields::eRefractionRightCylinder == e)      || (KatRegFields::eRefractionLeftCylinder == e) ||
            (KatRegFields::ePreopRefractionRightCylinder == e) || (KatRegFields::ePreopRefractionLeftCylinder == e))
        {
        // an optional value, default to empty in the cooked map
            vweCooked[e].sValue = "";

        // check and reformat
            auto sError = checkNumber(sValue,-10.00,00.00);
            if (sError.isEmpty())
            // ok looks good, reformat/pretty print with 2 decimals and zerofilling + plus sign
                vweCooked[e].sValue = reformatNumber(sValue,2,true,true);
        }

    // refraction in 2B and 2A, right or left degrees (allowed range [0, 180])
        if ((KatRegFields::eRefractionRightDegrees == e)      || (KatRegFields::eRefractionLeftDegrees == e) ||
            (KatRegFields::ePreopRefractionRightDegrees == e) || (KatRegFields::ePreopRefractionLeftDegrees == e))
        {
        // an optional value, default to empty in the cooked map
            vweCooked[e].sValue = "";

        // check and reformat
            auto sError = checkNumber(sValue,0,180);
            if (sError.isEmpty())
            // ok looks good, reformat/pretty print and save (no zerofilling and no decimals for the degrees)
                vweCooked[e].sValue = reformatNumber(sValue,0,false);
        }

    // the following ones are mandatory (no empty ones allowed)
    // K1/K2 diopters in 2A (allowed range [30.00, 60.00])
        if ((KatRegFields::eK1Diopters == e) || (KatRegFields::eK2Diopters == e))
        {
        // start the cooking, save what we have so far
            vweCooked[e].sValue = sValue;

            auto sError = checkNumber(sValue,30.00,60.00);
            if (!sError.isEmpty())
            {
            // failed lo/hi test or a bad number
                vweCooked[e].sError = QString("%1: %2").arg(sFieldCaption,sError);
                continue;
            }

        // ok looks good, reformat/pretty print and save (zerofill no need and 2 decimals please)
            vweCooked[e].sValue = reformatNumber(sValue,2,false);
        }

   // K1/K2 degrees in 2A (allowed range [0, 180])
        if ((KatRegFields::eK1Degrees == e) || (KatRegFields::eK2Degrees == e))
        {
        // start the cooking, save what we have so far
            vweCooked[e].sValue = sValue;

            auto sError = checkNumber(sValue,0,180);
            if (!sError.isEmpty())
            {
            // failed lo/hi test or a bad number
                vweCooked[e].sError = QString("%1: %2").arg(sFieldCaption,sError);
                continue;
            }

        // ok looks good, reformat/pretty print and save (with no zerofill and no decimals)
            vweCooked[e].sValue = reformatNumber(sValue,0,false);
        }

    // planned refraction in 2A (allowed range [-15.00, +15.00])
        if (KatRegFields::ePlannedRefraction == e)
        {
        // start the cooking, save what we have so far
            vweCooked[e].sValue = sValue;

            auto sError = checkNumber(sValue,-15.00,15.00);
            if (!sError.isEmpty())
            {
            // failed lo/hi test or a bad number
                vweCooked[e].sError = QString("%1: %2").arg(sFieldCaption,sError);
                continue;
            }

        // ok looks good, reformat/pretty print with 2 decimals and zerofill
            vweCooked[e].sValue = reformatNumber(sValue,2,true,true);    // true: prefix with a plus for > 0
        }

    // axis length in 2A (allowed range [15.00, 35.00])
        if (KatRegFields::eAxisLength == e)
        {
        // start the cooking, save what we have so far
            vweCooked[e].sValue = sValue;

            auto sError = checkNumber(sValue,15.00,35.00);
            if (!sError.isEmpty())
            {
            // failed lo/hi test or a bad number
                vweCooked[e].sError = QString("%1: %2").arg(sFieldCaption,sError);
                continue;
            }

        // ok looks good, reformat/pretty print with 2 decimals and no zerofill (not needed)
            vweCooked[e].sValue = reformatNumber(sValue,2,false);
        }

    // lens formula in 2A (value terms)
        if (KatRegFields::eLensFormula == e)
        {
        // just save the generic sValue we have from above (the term's captions concatenated together)
            vweCooked[e].sValue = sValue;

        // note: a check for at least one term being present is done above via the mKatRegFieldParamSpecs min/max map
        }

    // operation date (for diagnostic/show)
        if (KatRegFields::eDateOperation == e)
        {
        // just save the generic sValue we have from above (the term's captions concatenated together)
            vweCooked[e].sValue = sValue;

        // note: a check for at least one term being present is done above via the mKatRegFieldParamSpecs min/max map
        }


    // --- finally let's cook some tooltips for all (if it's currently empty)
        if (vweCooked[e].sToolTip.isEmpty())
        {
        // construct a nice default tooltip
            QString sToolTip = sFieldCaption + ": " + vweCooked[e].sValue;
            sToolTip.replace("\n"," + ");   // don't want those line feeds mess up the tooltip

        // however if we have an error text, prepend with that (and keep the value(s))
            if (!vweCooked[e].sError.isEmpty())
                sToolTip.prepend(vweCooked[e].sError + ": ");   // add ": " for a nice cosmetic touch

        // store it
            vweCooked[e].sToolTip = sToolTip;
        }

    // that's enough cooking for this katreg field, next please
    }

// not on Windows and dump to a cooking log?
#if !defined(Q_OS_WIN)
    if (tfwLog.isOpen())
        for (auto e : lKatRegFields)
        {
            QString sLine = mKatRegFieldCaptions[e] + ": ";

        // this field got any term ID:s?
            QSet<int> s;
            if (st.mTermSets.contains(e))
                s = st.mTermSets[e];

            for (auto t : s)
            // add 'em together
                sLine += mTermSetCaptions[t] + " ";

        // then values
            QStringList sl;
            if (st.mValues.contains(e))
                sl = st.mValues[e];

            for (auto s : sl)
                sLine += s + " ";

        // then the cooked ones
            CookedStruct stVWE;
            if (vweCooked.contains(e))
                stVWE = vweCooked[e];

            sLine += "--> " + stVWE.sValue + " " + stVWE.sError;

            tfwLog.ts << sLine << "\n";
        }
#endif

// ok we're done
    return vweCooked;
}

//----------------------------------------------------------------------------
// burialAtSea (the final steps are taken here)
//
// 2024-07-30 First version
//----------------------------------------------------------------------------
void MainWindow::burialAtSea()
{
// when we arrive here the main Samba crawler should be done
    if (!scMain.bCrawlingDone)
        guruMeditation("the main Samba crawler was not done");

// be sure to update the KatReg entries with all we got from Samba
    refreshPersonNo();

// --- step thru and check all the personno. ---
    int nNo    = 0;    // for showProgress()
    int nTotal = mKatRegEntries.count();

    for (auto &st : mKatRegEntries)   // & for r/w access
    {
    // show some progress
        ++nNo;
        showProgress(QString("Strax klar, kontrollerar pat. personnummer (%1%)...").arg((nNo * 100) / nTotal),95 + ((nNo * 5) / nTotal)); // (nTotal > 0)

    // bypass sponges (not written to the XML file, so no need for checking them)
        if (st.bSpongeDOA)
            continue;

    // is this a bad personno. or a missing/empty one?
        QString sError;
        if (!st.sPersonNo.isEmpty())
            sError = TWUtils::checkPersonNo(st.sPersonNo);

        bool bBadApple = (!sError.isEmpty() || (st.sPersonNo.isEmpty()));
        if (bBadApple)
            goto checkDOB;

    // this personno. is ok but are we to anyway check all of them?
        if (bCheckAllDOBs)
            goto checkDOB;  // yes please check this one

    // or perform a random inspection? i.e. check a selected few?
        if (bCheckRandomDOBs)
            if (1 == (rand() % 11))
            // let's check on average one of every 11 chaps
                goto checkDOB;

    // for TC2KatReg: if this pat. has a reservno. we need his/her DOB for the XML file
        if (TWUtils::isReservNo(st.sPersonNo))
            goto checkDOB;

    // we're not checking this one, assume a valid no. (not empty for sure)
        continue;   // next please

checkDOB:
    // checking time: try to get the DOB for this PID
        QDate dDOB;
        QDate dCFUDOB;

        db.select("DateOfBirth,CFUDateOfBirth",stTCCorral.PatInfo,QString("PatientID = %1").arg(st.nPID),
        [&dDOB,&dCFUDOB](DBRowStruct rs)
        {
            dDOB    = rs.dateValue("DateOfBirth");
            dCFUDOB = TWUtils::fromYMD(rs.stringValueTrimmed("CFUDateOfBirth"));  // expect YYYYMMDD from the db
        },eDBRowsExpectZeroOrOne);

    // have a bad or (most likely missing) personno? replace it
        if (bBadApple)
        {
        // arriving here: either sError is already set or we have an empty personno
        // missing from the Samba crawler's table? i.e. an empty personno?
            if (st.sPersonNo.isEmpty())
            {
                sError = "Kassa " + st.sReceiptNo.left(6) + " i TakeCare är inte avslutad";
                if (st.bOk) // if there is no prior error, set this error
                {
                    st.bOk    = false;
                    st.sError = sError;
                }
            }

        // create a new, default (empty) personno.
            st.sPersonNo = "???????? ????";

        // if we found a valid DOB, use it to construct a partial personno.
            if (dDOB.isValid())
                st.sPersonNo = TWUtils::toYMD(dDOB) + " ????";   // use "????" to show we're missing the last 4 digits

        // if DOB was no good, maybe this is a reservnr
            if (!dDOB.isValid() && dCFUDOB.isValid())
            // no birthday but have an ok CFU DOB? assume then it's a reservnummer (Region Stockholm flavored)
                st.sPersonNo = "99?????? ????";     // use even more "????" to show we're missing even more digits
        }
        else
        {
        // this pat. has no error but check the DOB anyway
            if (!dDOB.isValid())
            // maybe this a reservnr chap. try the CFUDOB
                dDOB = dCFUDOB;

        // and try again
            if (!dDOB.isValid())
            {
            // didn't get a valid date for the DOB, for KatReg this will be an error
                st.bOk    = false;
                st.sError = "Saknar giltigt födelsedatum";

                continue;   // next chap
            }

        // as an optional extra: match first 8 digits with DOB (unless this is a pat. with reservno or a testpatient)
            if (!TWUtils::isReservNo(st.sPersonNo))
                if (st.sPersonNo.left(8) != TWUtils::toYMD(dDOB))    // 8 chars = YYYYMMDD
                    if ((st.sWarning.isEmpty()) && (!TWTCSupport::isTestPatient(st.sPersonNo)))
                    // not a testpatient: issue a warning (unless this entry has got a previous warning already)
                        if (!bObfuscatePersonNo)    // don't bother if we're in an obfuscation run
                            st.sWarning = "Födelsedatum och personnr. stämmer inte överens";
        }

    // save the DOB, we need it for the XML for patients with reservno.
        st.dDOB = dDOB;  // we know DOB is valid (checked above)

    // next patient please
    }

// close the db and update the tablewidgets
    db.close();
    restockAllTableWidgets();

// we're done, switch app state to rubbernecking and write the KatReg XML file
    showProgress("Skriver KatReg-filen...",99);
    switchAppState(AppState::eRubbernecking);

    writeKatRegFile();

// do a final update (cannot hurt)
    restockAllTableWidgets();
}

//----------------------------------------------------------------------------
// writeKatRegFile
//
// 2024-07-30 First version for TC2KatRegUppf
//----------------------------------------------------------------------------
void MainWindow::writeKatRegFile()
{
// prepare the counting
    int nVisits   = 0;
    int nErrors   = 0;
    int nWarnings = 0;

// use a helper map for stepping through the entries in date + TC receiptno order (and stepping over error entries)
    QMap<QString,QString> mDateReceiptNo2KatRegEntry;
    for (auto st : mKatRegEntries)
    {
    // is this a bad one?
        if (!st.bOk)
        {
        // yes, count this as an error unless this is a hidden sponge (with an error)
            if (!st.bSpongeDOA || bShowSponges || bShowErrorSponges)
                ++nErrors;

            continue;   // next entry please
        }

    // and skip over sponges (skipping is the only reason of being a sponge in this context)
        if (st.bSpongeDOA)
            continue;

    // if we arrive here, this entry should be error free
        if (!st.sError.isEmpty())
            guruMeditation("got a nonempty error message when expecting an error-free entry");

    // also make sure we have a nonempty and valid personno
        if (st.sPersonNo.isEmpty()) // empty personno? (shouldn't happen if bOk is true)
            guruMeditation("got an empty personno when writing the KatReg file");

        if ("" != TWUtils::checkPersonNo(st.sPersonNo))
            guruMeditation("got a bad personno when writing the KatReg file");

    // count # of warnings (this is an ok entry but with a warning)
        if (!st.sWarning.isEmpty())
            ++nWarnings;

    // have a valid candidate for the XML file, save it in the map
    // append eye flavor to the key (disambiguation for bilaterals (when both right and left eye ops occur on the same day))
        ++nVisits;
        QString sKey = st.sVisitDate + st.sReceiptNo + st.cEye;
        mDateReceiptNo2KatRegEntry.insert(sKey,st.sKey);
    }

// check map size (if there's a mismatch, it could be an entry was overwritten due to a duplicate KatReg receipt no.)
// (we're supporting nnn-0n-nnnn-n and nnn-9n-nnnn-n TC receipt nos. but not nnn-1n-nnnn-n, nnn-2n-nnnn-n etc.)
    if (mDateReceiptNo2KatRegEntry.count() != nVisits)
        fubar("mismatch helper map count */vs. nVisits (%d/%d)",mDateReceiptNo2KatRegEntry.count(),nVisits);

// XML time ---------------------------------------------------
// compose a nice filename and start with some XML boilerplate
    QString sFileName = QString("UPPF%1_%2_%3_%4.xml").arg(sClinicNo,TWUtils::toYMD(dFrom).right(6),TWUtils::toYMD(dTo).right(4)).arg(nVisits);

    auto f = new QFile(sFileName);
    if (!f->open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Truncate))
        return TWUtils::errorBox(QString("Sorry det gick inte att spara KatReg-filen i:\n'%1'").arg(TWUtils::getCurrentDirectory()));

    QXmlStreamWriter wx(f);
    wx.setAutoFormatting(true); // start off with some boring boilerplates
    wx.writeStartDocument();
    wx.writeStartElement("KataraktUppföljningar");
    wx.writeAttribute("xmlns:xsi","http://www.w3.org/2001/XMLSchema-instance");
    wx.writeAttribute("xmlns:xsd","http://www.w3.org/2001/XMLSchema");

    // note: don't forget to check for Visus 0.00 (not allowed in XML file): instead change Visus to 0.01 and set "Blind" to true/1

// step thru our KatReg entries in ascending date and TC receiptno. order
    for (auto sKey : mDateReceiptNo2KatRegEntry)
    {
    // get our chap for this run
        auto st = mKatRegEntries[sKey];

    // --- helper lambdas for XML writing of booleans (as 0 or 1), dates or strings
        auto writeBoolElement   = [&wx] (QString sName, bool    b) { wx.writeTextElement(sName,(b) ? "1" : "0");       };
        auto writeDateElement   = [&wx] (QString sName, QDate   d) { wx.writeTextElement(sName,TWUtils::toISODate(d)); };
        auto writeStringElement = [&wx] (QString sName, QString s) { wx.writeTextElement(sName,s);                     };

    // --- and a fancy one
        auto writeCookedValueElement = [&wx,st] (KatRegFields f)
        {
        // get the cooked value for this katreg field
            auto sValue = st.mAllCooked[f].sValue;
            sValue.replace(",",".");  // for numbers: replace back to US decimal point for the XML

        // writes the sValue for a given katreg field
            wx.writeTextElement(mKatRegFieldXMLNames[f],sValue);
        };


    // ========= begin a new XML entry with <Uppföljning> =========
        wx.writeStartElement("Uppföljning");

    //  #1: this one is good for XML beginners
        writeStringElement("Enhet",sClinicNo);

    // --- hello <Person>
        wx.writeStartElement("Person");

    // personno. and/or reservnummer?
        QString sPersonIdentity = st.sPersonNo;
        bool bTemporary         = TWUtils::isReservNo(sPersonIdentity);
        if (bTemporary)
        {
        // make sure we have a valid DOB
            if (!st.dDOB.isValid())
                fubar(QString("Found a bad DOB for %1").arg(st.sPersonNo));

        // and use that as a temp. id
            sPersonIdentity = TWUtils::toYMD(st.dDOB);  // toss the reservno, store DOB as YYYYMMDD
        }

        writeStringElement("PersonIdentity",sPersonIdentity);
        writeBoolElement("Temporary",bTemporary);
        wx.writeEndElement();
    // --- end of <Person>

    // --- <Öga> can be right or left, but first check that we in fact have an "H" or "V" stored
        if ((ccRightEye != st.cEye) && (ccLeftEye != st.cEye))
            fubar("\"Öga\" är inte H eller V");
        writeStringElement("Öga",st.cEye);

    // --- begin <Form2A>
        wx.writeStartElement("Form2A");
        writeCookedValueElement(KatRegFields::eK1Diopters);
        writeCookedValueElement(KatRegFields::eK1Degrees);
        writeCookedValueElement(KatRegFields::eK2Diopters);
        writeCookedValueElement(KatRegFields::eK2Degrees);

        writeCookedValueElement(KatRegFields::ePlannedRefraction);
        writeCookedValueElement(KatRegFields::eAxisLength);

    // <RefraktionHöger> is optional, check if all 3 values are nonempty
        if (!st.mAllCooked[KatRegFields::ePreopRefractionRightSphere  ].sValue.isEmpty() &&
            !st.mAllCooked[KatRegFields::ePreopRefractionRightCylinder].sValue.isEmpty() &&
            !st.mAllCooked[KatRegFields::ePreopRefractionRightDegrees ].sValue.isEmpty())
        {
            wx.writeStartElement("RefraktionHöger");
            writeCookedValueElement(KatRegFields::ePreopRefractionRightSphere  );
            writeCookedValueElement(KatRegFields::ePreopRefractionRightCylinder);
            writeCookedValueElement(KatRegFields::ePreopRefractionRightDegrees );
            wx.writeEndElement();
        }

    // <RefraktionVänster> is optional, check if all 3 values are nonempty
        if (!st.mAllCooked[KatRegFields::ePreopRefractionLeftSphere  ].sValue.isEmpty() &&
            !st.mAllCooked[KatRegFields::ePreopRefractionLeftCylinder].sValue.isEmpty() &&
            !st.mAllCooked[KatRegFields::ePreopRefractionLeftDegrees ].sValue.isEmpty())
        {
            wx.writeStartElement("RefraktionVänster");
            writeCookedValueElement(KatRegFields::ePreopRefractionLeftSphere  );
            writeCookedValueElement(KatRegFields::ePreopRefractionLeftCylinder);
            writeCookedValueElement(KatRegFields::ePreopRefractionLeftDegrees );
            wx.writeEndElement();
        }

    // lens formula (a set of termids)
        wx.writeStartElement(mKatRegFieldXMLNames[KatRegFields::eLensFormula]);
        auto setFormula = st.mTermSets[KatRegFields::eLensFormula];
        writeBoolElement(mKatRegTermXMLNames[nnSKRTTermID        ],setFormula.contains(nnSKRTTermID        ));
        writeBoolElement(mKatRegTermXMLNames[nnHaigisTermID      ],setFormula.contains(nnHaigisTermID      ));
        writeBoolElement(mKatRegTermXMLNames[nnHofferQTermID     ],setFormula.contains(nnHofferQTermID     ));
        writeBoolElement(mKatRegTermXMLNames[nnHolladyTermID     ],setFormula.contains(nnHolladyTermID     ));
        writeBoolElement(mKatRegTermXMLNames[nnBarrettTermID     ],setFormula.contains(nnBarrettTermID     ));
        writeBoolElement(mKatRegTermXMLNames[nnOtherFormulaTermID],setFormula.contains(nnOtherFormulaTermID));
        wx.writeEndElement();

    // that's it for 2A
        wx.writeEndElement();

    // 2B time
        wx.writeStartElement("Form2B");

    // post op checkboxes/booleans
        auto slCaptions = mKatRegFieldXMLNames[KatRegFields::ePostOpChecks].split("/");
        if (3 != slCaptions.count())
            guruMeditation("expected 3 strings in postOp XML table");

        if (st.mValues[KatRegFields::ePostOpChecks].isEmpty())
        {
        // nothing stored? just show 0 for all 3
            writeBoolElement(slCaptions[0],false);
            writeBoolElement(slCaptions[1],false);
            writeBoolElement(slCaptions[2],false);
        }
        else
        {
        // got something, can be either one but only one
            auto nValue = st.mValues[KatRegFields::ePostOpChecks].first().toInt();
            writeBoolElement(slCaptions[0],(nnEfterkontrollPagarValue           == nValue));
            writeBoolElement(slCaptions[1],(nnPatientmedverkanOtillrackligValue == nValue));
            writeBoolElement(slCaptions[2],false /* patient's deceased not yet supported */);
        }

    // visit date (final checkup day)
        writeDateElement("DatumFörSlutkontroll",st.dVisit);

    // <HögerÖga> is optional, check if all 4 values are nonempty
        if (!st.mAllCooked[KatRegFields::eVisusRight             ].sValue.isEmpty() &&
            !st.mAllCooked[KatRegFields::eRefractionRightSphere  ].sValue.isEmpty() &&
            !st.mAllCooked[KatRegFields::eRefractionRightCylinder].sValue.isEmpty() &&
            !st.mAllCooked[KatRegFields::eRefractionRightDegrees ].sValue.isEmpty())
        {
            wx.writeStartElement("HögerÖga");

        // check for Visus 0,00 (yes the cooked value is stored with a Swedish decimal comma)
            if ("0,00" == st.mAllCooked[KatRegFields::eVisusRight].sValue)
            {
            // patient's supposedly blind on the right eye, skip over the VA value
                writeBoolElement("Blind",true);
            }
            else
            {
                writeCookedValueElement(KatRegFields::eVisusRight);
                writeBoolElement("Blind",false);
            }

            writeCookedValueElement(KatRegFields::eRefractionRightSphere  );
            writeCookedValueElement(KatRegFields::eRefractionRightCylinder);
            writeCookedValueElement(KatRegFields::eRefractionRightDegrees );

            wx.writeEndElement();
        }

    // <VänsterÖga> is optional, check if all 4 values are nonempty
        if (!st.mAllCooked[KatRegFields::eVisusLeft             ].sValue.isEmpty() &&
            !st.mAllCooked[KatRegFields::eRefractionLeftSphere  ].sValue.isEmpty() &&
            !st.mAllCooked[KatRegFields::eRefractionLeftCylinder].sValue.isEmpty() &&
            !st.mAllCooked[KatRegFields::eRefractionLeftDegrees ].sValue.isEmpty())
        {
            wx.writeStartElement("VänsterÖga");

        // check for Visus 0,00 (yes the cooked value is stored with a Swedish decimal comma)
            if ("0,00" == st.mAllCooked[KatRegFields::eVisusLeft].sValue)
            {
            // patient's supposedly blind on the left eye, skip over the VA value
                writeBoolElement("Blind",true);
            }
            else
            {
                writeCookedValueElement(KatRegFields::eVisusLeft);
                writeBoolElement("Blind",false);
            }

            writeCookedValueElement(KatRegFields::eRefractionLeftSphere  );
            writeCookedValueElement(KatRegFields::eRefractionLeftCylinder);
            writeCookedValueElement(KatRegFields::eRefractionLeftDegrees );

            wx.writeEndElement();
        }

    // that's it for 2B
        wx.writeEndElement();

    // end of <Uppföljning>
        wx.writeEndElement();
    }

    wx.writeEndElement();   // end of <KataraktUppföljningar>
    wx.writeEndDocument();  // end of document
    f->close();

// show a final progress message and a message box ---------------------------------------
    QString sMsg = QString("Uppföljningsfil %1 skapad med %2 besök.").arg(sFileName).arg(nVisits);

    if ((0 == nErrors) && (0 == nWarnings))
        sMsg += " Inga fel och inga varningar.";

    if ((0 == nErrors) && (1 == nWarnings))
        sMsg += QString(" Inga fel. 1 besök fick en varning.");

    if ((0 == nErrors) && (nWarnings > 1))
        sMsg += QString(" Inga fel. %1 besök fick varningar.").arg(nWarnings);

    if ((nErrors > 0) && (0 == nWarnings))
        sMsg += QString(" %1 besök hade fel och är inte med i filen. Inga varningar.").arg(nErrors);

    if ((nErrors > 0) && (1 == nWarnings))
        sMsg += QString(" %1 besök hade fel och är inte med i filen. 1 besök fick en varning.").arg(nErrors);

    if ((nErrors > 0) && (nWarnings > 1))
        sMsg += QString(" %1 besök hade fel och är inte med i filen. %2 besök fick varningar.").arg(nErrors).arg(nWarnings);

    showProgress(sMsg);

#if defined(Q_OS_WIN)
    sMsg.replace(". ",".\n");  // on Windows, show same message in an info box (split into multiple lines)
    TWUtils::infoBox(sMsg);
#endif
}

//----------------------------------------------------------------------------
// on_settingsButton_clicked
//
// 2023-10-16 First version
//----------------------------------------------------------------------------
void MainWindow::on_settingsButton_clicked()
{
// wire up a Settings dialog
    SettingsDialog sd(this);
    sd.setModal(true);
    sd.show();

// before launching the dialog, get our current settings
    roSettings.setCurrentSection("DefaultPeriod");
    int nPeriodMonths = roSettings.readInt("Months");
    int nPeriodWeeks  = roSettings.readInt("Weeks");
    int nPeriodDays   = roSettings.readInt("Days");

// if we have some R/W settings, try to use them instead
    appDataSettings.setCurrentSection("DefaultPeriod");
    nPeriodMonths = appDataSettings.readIntWithDefault("Months",nPeriodMonths);
    nPeriodWeeks  = appDataSettings.readIntWithDefault("Weeks", nPeriodWeeks);
    nPeriodDays   = appDataSettings.readIntWithDefault("Days",  nPeriodDays);

// got the settings, calc. the index we'll use in the combo box
    int nDefaultDateRangeIndex = 0; // default: one day range
    if (1 == nPeriodWeeks)
        nDefaultDateRangeIndex = 1;
    if (2 == nPeriodWeeks)
        nDefaultDateRangeIndex = 2;
    if (1 == nPeriodMonths)
        nDefaultDateRangeIndex = 3;
    if (2 == nPeriodMonths)
        nDefaultDateRangeIndex = 4;

// get the db and table prefix settings
    roSettings.setCurrentSection("Intelligence");
    int nUseServer      = roSettings.readInt("UseServer");
    int nUseTablePrefix = roSettings.readInt("UseTablePrefix");

// if we already have some R/W settings for those, use them instead
    if (TWAppSettings::IniLocation::eAppData == eIniFlavorForDB)
    {
        appDataSettings.setCurrentSection(sIntelligenceSectionName);
        nUseServer      = appDataSettings.readInt("UseServer");
        nUseTablePrefix = appDataSettings.readInt("UseTablePrefix");
    }

// calc. the combox index
    int nDBServerAndPrefixIndex = 0; // default: PRSINTDB01
    if ((1 == nUseServer) && (1 == nUseTablePrefix))
        nDBServerAndPrefixIndex = 0;
    if ((2 == nUseServer) && (1 == nUseTablePrefix))
        nDBServerAndPrefixIndex = 1;
    if ((1 == nUseServer) && (2 == nUseTablePrefix))
        nDBServerAndPrefixIndex = 2;
    if ((2 == nUseServer) && (2 == nUseTablePrefix))
        nDBServerAndPrefixIndex = 3;

// set it them now in the dialog class and launch the modal dialog
    sd.setDefaultDateRangeIndex(nDefaultDateRangeIndex);
    sd.setDBServerAndPrefixIndex(nDBServerAndPrefixIndex);

    if (QDialog::Accepted == sd.exec())  // wait for user to click Ok or Cancel
    {
    // user pressed Ok, any changes to the settings?
        if (nDefaultDateRangeIndex != sd.nDefaultDateRangeIndex)
        {
        // got a new date range, write it to the R/W ini file
            nDefaultDateRangeIndex = sd.nDefaultDateRangeIndex;

            int nPeriodMonths = 0;
            int nPeriodWeeks  = 0;
            int nPeriodDays   = 0;
            if (0 == nDefaultDateRangeIndex)
                nPeriodDays   = 1;
            if (1 == nDefaultDateRangeIndex)
                nPeriodWeeks  = 1;
            if (2 == nDefaultDateRangeIndex)
                nPeriodWeeks  = 2;
            if (3 == nDefaultDateRangeIndex)
                nPeriodMonths = 1;
            if (4 == nDefaultDateRangeIndex)
                nPeriodMonths = 2;

            appDataSettings.setCurrentSection("DefaultPeriod");
            appDataSettings.writeInt("Months",nPeriodMonths);
            appDataSettings.writeInt("Weeks" ,nPeriodWeeks );
            appDataSettings.writeInt("Days"  ,nPeriodDays  );
        }

        if (nDBServerAndPrefixIndex != sd.nDBServerAndPrefixIndex)
        {
        // got a new DB setting, write it to the R/W ini file
            nDBServerAndPrefixIndex = sd.nDBServerAndPrefixIndex;

        // default is 1,1
            int nUseServer      = 1;
            int nUseTablePrefix = 1;

            if (0 == nDBServerAndPrefixIndex)
            {
                nUseServer      = 1;
                nUseTablePrefix = 1;
            }
            if (1 == nDBServerAndPrefixIndex)
            {
                nUseServer      = 2;
                nUseTablePrefix = 1;
            }
            if (2 == nDBServerAndPrefixIndex)
            {
                nUseServer      = 1;
                nUseTablePrefix = 2;
            }
            if (3 == nDBServerAndPrefixIndex)
            {
                nUseServer      = 2;
                nUseTablePrefix = 2;
            }

        // we need to copy the whole R/O shebang/section (for the benefit of TWTCSupport)
            auto copySetting = [this] (QString sKeyName) { appDataSettings.writeString(sKeyName,roSettings.readString(sKeyName)); };

            roSettings.setCurrentSection("Intelligence");
            appDataSettings.setCurrentSection("Intelligence" + QString::number(nCustomerNo)); // don't use sIntelligenceSectionName

            copySetting("UserName");
            copySetting("EncryptedPassword");
            copySetting("Database");
            copySetting("PortNo");
            appDataSettings.writeInt("UseServer"     ,nUseServer     );
            appDataSettings.writeInt("UseTablePrefix",nUseTablePrefix);
            copySetting("Server1");
            copySetting("Server2");
            copySetting("Server1TablePrefix1");
            copySetting("Server1TablePrefix2");
            copySetting("Server2TablePrefix1");
            copySetting("Server2TablePrefix2");
            
        // written lots of stuff, so make a flush here
            appDataSettings.sync();
        }
    }

// that's for the Setting dialog
}
