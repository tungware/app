/*--------------------------------------------------------------------------*/
/* main.cpp                                                                 */
/*                                                                          */
/* 2023-03-23 First version                                             HS  */
/*--------------------------------------------------------------------------*/

#include "mainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
// do the things we need to do before QApplication starts
    TWUtils::beforeQApplication();

    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    return a.exec();
}
