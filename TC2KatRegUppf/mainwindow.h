/*--------------------------------------------------------------------------*/
/* mainwindow.h                                                             */
/*                                                                          */
/* 2024-04-01 First version                                             HS  */
/*--------------------------------------------------------------------------*/

#pragma once
#include <QMainWindow>

// pull in the Tungware libraries
#include INCLUDETWUTILS
#include INCLUDETWDB
#include INCLUDETWTCSUPPORT

// need this for the readonly() lambda
#include "qtablewidget.h"

// pull in the katreg Uppf terms
#include "katregtermsUppf.h"

// where we are in the app
enum class AppState { eConnecting, eConnectedOk, eProcessing, eRubbernecking };

// animation time when sliding up the calendars
const int nnSlideAnimDuration = 444;

// table widget stuff -------------------------------
enum  TableViewStates { eShowAll, eShowWarnings, eShowErrors, viewStateCount };
const QColor acVisibilitySliderColors[viewStateCount] = { "white", "yellow", "#ff9070"};

enum  TableColumns    { colMapKey /* width 0 for this column (i.e. hidden) */,
                        colDayOfTheWeek, colVisitDateTime, colPersonNo, colEye, colDateOperation,
                        colK1Diopters, colK1Degrees, colK2Diopters, colK2Degrees,
                        colPlannedRefraction, colAxisLength,
                        // colPreopRefractionRightSphere, colPreopRefractionRightCylinder, colPreopRefractionRightDegrees,
                        // colPreopRefractionLeftSphere, colPreopRefractionLeftCylinder, colPreopRefractionLeftDegrees,
                        // colLensFormula,
                        colErrorsOrWarnings,    // visible only if we have warning(s) or error(s), using setSpan()
                        colVisusRight, colRefractionRightSphere, colRefractionRightCylinder, colRefractionRightDegrees,
                        colVisusLeft, colRefractionLeftSphere, colRefractionLeftCylinder, colRefractionLeftDegrees,
                        colShowBox, colJumpToTC, colCount };

// column widths                      MKey  DW  DVT  Pno Eye DtOp K1 K1D  K2 K2D  PR  AL ErrWarn, VisusR RRS RRC RRD VisusL RLS RLC RLD Showbox JumpTC
// *** when ShowBox works, restore the width from 0 to 40
const int anColumnWidths[colCount] = { 00, 50, 210, 190, 40, 140, 80, 60, 80, 60, 80, 80,     00,     60, 72, 72, 72,    60, 72, 72, 72,     40,    40 };

// more table widget stuff
static auto setTableWidgetItemRO = [] (QTableWidgetItem* tw) { tw->setFlags(tw->flags() ^ Qt::ItemIsEditable); };
const QTime tNotFoundInPAS(23,59,00);  // sort the rows so entries missing in PAS end up as the last entries for each day

// TC open patient click twinklebox delays (in milliseconds)
const int   nnShortTCDelay  = 222;
const int   nnLongTCDelay   = 555;

// we started this adventure for real in 2024 (i.e. first allowed date)
const QDate ddEarliestVisit(2024,03,01);

// --------------------------------------------------------
// KatReg stuff
using KatRegTermSetMap = QMap<enum KatRegFields, QSet<int>>;   // hash/set of all value terms for a KatReg field
using KatRegQListMap   = QMap<enum KatRegFields, QStringList>; // a list for raw (db) values

// KatReg cooked/human readable values/warnings/errors/tooltips
struct CookedStruct
{
    QString sValue;
    QString sWarning;
    QString sError;
    QString sToolTip;
};
using KatRegCookedMap = QMap<enum KatRegFields,CookedStruct>;

// entry struct (note: one for every patient and eye flavor)
struct KatRegEntryStruct
{
    bool    bOk;            // if bOk is false then expect some Swedish/localized text in sError
    QString sError;         // if nonempty: this vist has an error and it will not be saved/written to file
    QString sWarning;       // if nonempty: warning (this visit will be written anyway, i.e. bOk is true)
    PID_t   nPID;           // XTEA encrypted personno (used in Intelligence instead of the real thing)

    bool    bSpongeDOA;     // visits not to be uploaded/written (can be ok or have an error)
    QDate   dVisit;         // visit date: either PAS date or journal date (the same for ok entries)
    QString sVisitDate;     // same as above but as an ISO printable version (YYYY-MM-DD) for convenience
    QTime   tVisit;         // time from PAS or 23:59:00 if that's missing (so they're sorted *after* normal ones)
    QString sVisitDateTime; // printable version e.g. (YYYY-MM-DD HH:MM) of the above dVisit + tVisit

// PAS stuff
    QString sReceiptNo;     // receiptno from TC: billing counter (nnn-nn) + 4 digits (1000...9999) + check digit)
    QString sVisitType;     // one digit '0' -- '9' (TC has letters also but they're not used for KatReg)

// patient stuff
    QString sPersonNo;      // no hyphens or spaces but can have ???? question marks for an incomplete no.
    QDate   dDOB;           // needed for pat. with reservnr (TC2KatRegUppf's output (XML) file)

// KatReg stuff
    QChar            cEye;       // "H" or "V" (shortform for right or left in Swedish)
    KatRegTermSetMap mTermSets;  // hash/set of the value terms found for the field
    KatRegQListMap   mValues;    // map of raw db values (for the non-value terms)

    KatRegCookedMap  mAllCooked; // map of cooked/human readable katreg values/warnings/errors

// the key for sorting/finding this entry in our KatReg map
    QString sKey; // visitday as YYYYMMDD concatenated with the PID and a letter "H" or "V" (right or left eye)
                  // (duplicate/error entries have timeofday and a random number added as a suffix)

// key creations: visit date into 8 digits (YYYYMMDD) and concatenate with the PID and either an "H" or an "V"
    static QString createKey(QDate d, PID_t nPID, QChar cEye)
    {
        return TWUtils::toYMD(d) + TWTCSupport::PID2String(nPID) + cEye;
    }

// for an error entry
    static QString createKeyForErrorEntries(QDate d, PID_t nPID, QChar cEye)
    {
    // append # of milliseconds elapsed since midnight and a random number to the normal key
    // (this is used to disambiguate duplicate error entries occurring for the same PAS and/or CaseNotes)
        QString sDisambiguationTail = QString::number(QTime::currentTime().msecsSinceStartOfDay()) + QString::number(rand());
        return createKey(d,nPID,cEye) + sDisambiguationTail;
    }
};

// ----------------------------------------------------
// say hello to MainWindow
namespace Ui { class MainWindow; }

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget* parent = nullptr);
    ~MainWindow();
    void closeEvent(QCloseEvent* event);

// db and the Samba crawlers
    TWAsyncDB    db;
    SambaCrawler scInitial;
    SambaCrawler scMain;

// settings double flavors (one r/o and one r/w)
    TWAppSettings roSettings     {TWAppSettings::eEmbedded};
    TWAppSettings appDataSettings{TWAppSettings::eAppData};

// use either R/O or R/W flavor for the db settings
    TWAppSettings::IniLocation eIniFlavorForDB;
    QString sIntelligenceSectionName; // for finding our R/W section

// customer stuff -------------------------------------
    QString sCustomerName;
    int     nCustomerNo;             // number starting with 1
    bool    bAutoStart;              // if true: don't wait for the Go button to be clicked
    bool    bBatchMode;              // true if two valid dates are on the command line
    bool    bCheckRandomDOBs;        // if true: check DOBS for some but not all of them (random selection)
    bool    bCheckAllDOBs;           // if true: check DOBs for all (PersonNo vs Y,M,D in Intelligence's vPatInfo view)
    int     nCompanyID;
    int     nCompanyCode;
    int     nCareUnitID;
    QString sHSAID;                  // (only verified with the current value in Codes_CareUnits at start)

    QString sSambaShareName;         // for error checking in Codes_Companies_v2
    QString sLocalDirectory;         // if nonempty: use this instead of connecting remotely to the Samba server

// KatReg static stuff from ROSettings.ini ------------
    QString sClinicNo;
    QString sClinicName;
    QString sTemplateFor2B;          // name of the journal template where we'll find the 2B terms

// options galore -------------------------------------
    QString sCookingLogFileName;     // if nonempty: save debug/cooking information to the file
    bool    bObfuscatePersonNo;      // if true: use personno. from testpatients only and don't create an KatReg file
    bool    bShowErrorSponges;       // if true show sponges with error(s) but hide the other sponges
    bool    bShowSponges;            // if true show *all* sponges regardless of error/warnings
    QString sTCPathOverride;         // if nonempty: use this path as first choice for launching TC
    QString sKatRegFilePathOverride; // if nonempty: use this path as the directory for the KatReg file
    QString sFileNameVisitsNoGo;     // if nonempty: a file with a list of dates, personnos. + eye flavors to skip

// resize/refresh stuff -------------------------------
    QSize                szOriginal;           // initial/original mainwindow size
    QMap<QWidget*,QRect> mOriginalGeometries;  // map of the original/designed widget geometries (used when resizing)

#if !defined(Q_OS_WIN)
// if we're not on Windows, support an optonal cooking log
    TWTextFileWriter tfwLog;
#endif

// icons we use
    QIcon iconShowBox;
    QIcon iconTC;

// -- presenting our big wheels ------------------------
    AppState                        appState;
    TableViewStates                 tableViewState;
    QDate                           dFrom,dTo;
    QMap<QString,KatRegEntryStruct> mKatRegEntries;
    bool                            bWaitingForSamba;
    MapPID2PersonNo_t               mPID2PersonNo;

// startup: connect to Samba and Intelligence
    QString connectToSambaAndIntelligence();

// our own override of the virtual resizeEvent()
    void virtual resizeEvent(QResizeEvent* event);

// more UI functions
    void setupUI();
    void setGoButtonCaption();
    void switchAppState(AppState newAppState);
    void refreshVisibilitySlider(TableViewStates e);
    void refreshTableWidget();
    void showProgress(QString sText,int nBarValue = 0);
    void restockSingleTableWidget(TableViewStates eState);   // 3 flavors
    void restockAllTableWidgets();

// fetching
    void refreshPersonNo();
    void dbError(QString sError);
    void fetchKatRegData();

// cooking
    KatRegCookedMap cookEntry(KatRegEntryStruct st);

// wrapping up
    void burialAtSea();

// writing the XML
    void writeKatRegFile();

// Qt slot chaps
private slots:
    void on_calendarWidgetFrom_clicked(const QDate &date);
    void on_calendarWidgetTo_clicked(const QDate &date);
    void on_pushButtonExit_clicked();
    void on_visibilitySlider_valueChanged(int value);
    void on_pushButtonGo_clicked();
    void on_settingsButton_clicked();

private:
    Ui::MainWindow* ui;
    QWidget* pSliderGrooveFrame;    // for the visibility slider's groove
};
