/*--------------------------------------------------------------------------*/
/* main.cpp                                                                 */
/*                                                                          */
/* 2018-08-13 First version                                             HS  */
/*--------------------------------------------------------------------------*/

#include "mainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    TWUtils::beforeQApplication();
    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    return a.exec();
}
