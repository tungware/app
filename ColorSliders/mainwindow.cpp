/*--------------------------------------------------------------------------*/
/* mainwindow.cpp                                                           */
/*                                                                          */
/* 2018-08-13 First version                                             HS  */
/* 2019-12-06 Introduce custom colored sliders (using stylesheets)      HS  */
/* 2021-09-28 Update to use TWUtil's default font                       HS  */
/*--------------------------------------------------------------------------*/

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "qclipboard.h"

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setWindowTitle("Welcome to ColorSliders");

// fix fusion and Qt quirks and say our window is fixedsize
    setFont(QFont(TWUtils::getDefaultFontName(),48));
    TWUtils::helloQt(this);
    TWUtils::fixedSizeWindow(this);

// make sure the tab order is good
    setTabOrder(ui->sliderR,ui->sliderG);
    setTabOrder(ui->sliderG,ui->sliderB);
    setTabOrder(ui->sliderB,ui->lineEdit);
    setTabOrder(ui->lineEdit,ui->pushButtonCopy);
    setTabOrder(ui->pushButtonCopy,ui->pushButtonExit);

// set initial lineedit to #FACEBA (will trigger textChanged event that sets our c and the sliders)
    ui->lineEdit->setText("#FACEBA");
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_lineEdit_textChanged(const QString &arg1)
{
// user typed something in the edit box (or the text was set by us)
    if (7 != arg1.length())
    // if it's not 7 chars (e.g. #AABBCC), skip this one
        return;

// try to parse it into a valid color
    if (!QColor(arg1).isValid())
    // no dice
        return;

// ok accept it
    c = QColor(arg1);

// set the sliders for this color
    ui->sliderR->setValue(c.red());
    ui->sliderG->setValue(c.green());
    ui->sliderB->setValue(c.blue());

// and refresh the edit and button
    updateForColor();
}

void MainWindow::updateForColor()
{
    ui->lineEdit->setStyleSheet(TWUtils::backgroundStyle(c));
    ui->lineEdit->setText(c.name().toUpper());
    ui->lineEdit->setFont(QFont(TWUtils::getDefaultFontName(),120));

    ui->pushButtonCopy->setText(QString("Copy \"%1\"").arg(c.name().toUpper()));
}

void MainWindow::on_sliderR_valueChanged(int value)
{
// change red in c
    c = QColor(value,c.green(),c.blue());

    updateForColor();

// for a nice effect also change ther read slider's color
    customizeSlider(ui->sliderR,QColor(value,0,0));
}

void MainWindow::on_sliderG_valueChanged(int value)
{
// change green in c
    c = QColor(c.red(),value,c.blue());

    updateForColor();

// for a nice effect also change the green slider's color
    customizeSlider(ui->sliderG,QColor(0,value,0));
}

void MainWindow::on_sliderB_valueChanged(int value)
{
// change blue in c
    c = QColor(c.red(),c.green(),value);

    updateForColor();

// for a nice effect also  change the blue slider's color
    customizeSlider(ui->sliderB,QColor(0,0,value));
}

void MainWindow::on_pushButtonCopy_clicked()
{
// use the app clipboard
    qApp->clipboard()->setText(c.name().toUpper());
}

void MainWindow::on_pushButtonExit_clicked()
{
    qApp->exit();
}

void MainWindow::customizeSlider(QSlider* pSlider, QColor c)
{
    if (nullptr == pSlider)
        guruMeditation("bad slider");

// let the customizing commence
    static int nnBorderWidth  = 1;
    static int nnBorderRadius = 7;  // for nice rounded corners
    static QColor ccBorder("grey");
    static int nnLineWidth    = 2;
    static int nnMidLineWidth = 0;

// construct and set the style sheet (two parts, one for the groove and one for the handle)
    pSlider->setStyleSheet(QString(
         ".QSlider::groove { background: transparent; height: %1px; } "
         ".QSlider::handle { background: %2; border-radius: %3px; border: %4px solid %5; width: %6px;}")
        .arg(pSlider->geometry().height()).arg(c.name()).arg(nnBorderRadius)
        .arg(nnBorderWidth).arg(ccBorder.name()).arg(pSlider->geometry().width() / 6));

// need to recreate the QSlider's groove? (the original is now invisible due to the stylesheet applied above)
    QString sFrameObjectName = pSlider->objectName() + "Frame";  // we'll be using a QFrame
    if (nullptr == pSlider->parentWidget()->findChild<QFrame*>(sFrameObjectName))
    {
    // yes, so wire up a new QFrame to act as a surrogate groove, use the same parent and geometry as the slider
        QFrame* pFrame = new QFrame(pSlider->parentWidget());
        pFrame->setGeometry(pSlider->geometry());
        pFrame->setObjectName(sFrameObjectName);
        pFrame->setFrameStyle(((Qt::Horizontal == pSlider->orientation()) ? QFrame::HLine : QFrame::VLine) | QFrame::Sunken);
        pFrame->setLineWidth(nnLineWidth);
        pFrame->setMidLineWidth(nnMidLineWidth);

    // finally, change the z-order so that our custom groove is drawn under the handle (just like the real groove)
        pFrame->stackUnder(pSlider);
    }
}
