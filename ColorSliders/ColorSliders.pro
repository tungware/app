#----------------------------------------------------------------------------#
# ColorSliders.pro                                                           #
#                                                                            #
# 2018-08-13 First version                                               HS  #
#----------------------------------------------------------------------------#

QT      += core gui widgets
TARGET   = ColorSliders
TEMPLATE = app

SOURCES += main.cpp mainwindow.cpp
HEADERS += mainwindow.h
FORMS   += mainwindow.ui

# include our common QtProjects helper
include(../../include/QtProjects.pri)

# link in TWUtils
LIBS    += $$TWLibLine(TWUtils)

# and set the #define for the .h file #include statement
DEFINES += $$TWStaticSupport()
DEFINES += $$TWDefineInclude(TWUtils)
