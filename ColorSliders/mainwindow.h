/*--------------------------------------------------------------------------*/
/* mainwindow.h                                                             */
/*                                                                          */
/* 2018-08-13 First version                                             HS  */
/*--------------------------------------------------------------------------*/

#include <QMainWindow>
#include INCLUDETWUTILS
#include "qslider.h"

namespace Ui { class MainWindow; }

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

// couple of big wheels
    QColor c;

// updater
    void updateForColor();

private slots:
    void on_lineEdit_textChanged(const QString &arg1);

    void on_sliderR_valueChanged(int value);
    void on_sliderG_valueChanged(int value);
    void on_sliderB_valueChanged(int value);

    void on_pushButtonCopy_clicked();
    void on_pushButtonExit_clicked();

    void customizeSlider(QSlider* sl, QColor c);

private:
    Ui::MainWindow *ui;
};
